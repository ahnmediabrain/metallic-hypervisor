
#pragma once

void InitKernelAreaAllocator();

// Allocates 4K-aligned memory for kernel use.
// (this memory will be retained after kernel entry)
EFI_STATUS
AllocateKernelArea(
    IN UINTN Bytes,
    IN OUT void **Memory
);

void kPrintMemoryMap();
void kAllocateAndBuildMemoryMap(UINTN *CountPtr, K_MEMORY_MAP **MapPtr);
