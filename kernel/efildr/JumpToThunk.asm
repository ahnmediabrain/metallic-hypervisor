; JumpToThunk.asm : 커널로 점프하는 Thunk로 점프

_DATA SEGMENT
_DATA ENDS

_TEXT SEGMENT
; note that we should preserve rcx, rdx, r8 and r9
kJumpToThunk PROC
	mov rax, 7C00h
	jmp rax
kJumpToThunk ENDP
_TEXT ENDS

END
