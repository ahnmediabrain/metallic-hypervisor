﻿
#include "stdafx.h"
#include "UefiApiWrapper.h"
#include "GlobalVariable.h"
#include "StdLib.h"
#include "Utility.h"
#include "Graphics.h"


/*
kGetGOP: UEFI 프레임 버퍼 얻기

@param    (없음)
@retval (EFI 오류 코드)

*/

EFI_STATUS kGetGOP(EFI_GRAPHICS_OUTPUT_PROTOCOL **ppGOP)
{
    EFI_STATUS efi_err;
    UINTN uiNumHandles;
    EFI_HANDLE hGOPHandle;
    EFI_HANDLE *phGOPHandleBuf;
    EFI_GRAPHICS_OUTPUT_PROTOCOL *pGOP;

    *ppGOP = NULL; // NULL indicates failure
    efi_err = LocateHandleBuffer(ByProtocol, &GraphicsOutputProtocolGuid, 0, &uiNumHandles, &phGOPHandleBuf);

    if (EFI_ERROR(efi_err))
        return efi_err;

    // GOP 핸들 얻기
    if (uiNumHandles == 1)
    {
        hGOPHandle = *phGOPHandleBuf;
    }
    else
    {
        /*
        - 물리적인 장치에 연결된 핸들을 리턴
        - GOP 핸들이 2개 이상인 경우, PCI I/O 프로토콜이 attach된 핸들을 찾고,
        이러한 핸들이 없으면 EDID 프로토콜이 attach된 핸들을 찾음
        - GOP 핸들이 2개 이상이나 위의 방법으로 되지 않으면 첫 번째 GOP 핸들을 리턴
        */

        hGOPHandle = *phGOPHandleBuf;
    }

    FreePool(phGOPHandleBuf);

    // GOP 열기
    efi_err = OpenProtocol(hGOPHandle, &GraphicsOutputProtocolGuid, (void **)&pGOP, g_ImageHandle, NULL, EFI_OPEN_PROTOCOL_EXCLUSIVE);
    if (EFI_ERROR(efi_err))
        return efi_err;

    *ppGOP = pGOP;
    return EFI_SUCCESS;
}

/*
kSetupUEFIFramebuffer: UEFI 프레임버퍼 설정

@param    pGMDesc        프레임버퍼 정보가 담길 구조체의 포인터
pGOP        EFI GOP 핸들

@retval    (EFI 오류 코드)

*/

EFI_STATUS kSetupEFIFramebuffer(K_GRAPHICS_MODE_DESC *pGMDesc, EFI_GRAPHICS_OUTPUT_PROTOCOL *pGOP)
{
    EFI_STATUS efi_err;
    UINTN uiSize;
    UINT32 uiModeNum, uiVertRes, uiHorzRes;
    BOOL bFound;
    EFI_GRAPHICS_OUTPUT_PROTOCOL_MODE *pGraphicsMode;
    EFI_GRAPHICS_OUTPUT_MODE_INFORMATION *pGraphicsInfo;

    /* 그래픽 모드 찾기 (K_DEFAULT_HORZRES * K_DEFAULT_VERTRES에 가장 가까운 모드) */

    bFound = FALSE;
    uiVertRes = 0;
    uiHorzRes = 0;
    for (UINT32 i = 0; i < pGOP->Mode->MaxMode; ++i)
    {
        pGOP->QueryMode(pGOP, i, &uiSize, &pGraphicsInfo);

        /* 프레임버퍼가 지원되지 않으면 건너뜀 */
        if (pGraphicsInfo->PixelFormat == PixelBltOnly)
            continue;

        bFound = TRUE;
        if (diff(pGraphicsInfo->HorizontalResolution, K_DEFAULT_HORZRES) + diff(pGraphicsInfo->VerticalResolution, K_DEFAULT_VERTRES)
            < diff(uiHorzRes, K_DEFAULT_HORZRES) + diff(uiVertRes, K_DEFAULT_VERTRES)
            || (uiHorzRes == 0 && uiVertRes == 0))
        {
            uiModeNum = i;
            uiHorzRes = pGraphicsInfo->HorizontalResolution;
            uiVertRes = pGraphicsInfo->VerticalResolution;
        }
    }

    if (bFound == FALSE)
        return EFI_NOT_FOUND;

    /* 그래픽 모드 설정 */
    if (EFI_ERROR((efi_err = pGOP->SetMode(pGOP, uiModeNum))))
        return efi_err;

    /* 현재 설정된 그래픽 모드의 정보 */

    pGraphicsMode = pGOP->Mode;
    pGraphicsInfo = pGraphicsMode->Info;

    // 그래픽 모드에 대한 기본적인 정보 설정
    pGMDesc->framebuffer_base_phys = (K_ADDRESS)pGraphicsMode->FrameBufferBase;
    pGMDesc->framebuffer_size = (K_SIZE)pGraphicsMode->FrameBufferSize;
    pGMDesc->resHorz = pGraphicsInfo->HorizontalResolution;
    pGMDesc->resVert = pGraphicsInfo->VerticalResolution;
    pGMDesc->ccPixelPerScanLine = pGraphicsInfo->PixelsPerScanLine;
    pGMDesc->videoAttr = 0;
    pGMDesc->videoType = K_VIDEO_TYPE_EFI;

    // 그래픽 모드의 각 색 채널의 비트 위치와 수 설정
    switch (pGraphicsInfo->PixelFormat)
    {
    case PixelRedGreenBlueReserved8BitPerColor:
        pGMDesc->videoDepth = 32;
        pGMDesc->pixelMode = K_VIDEO_PIXEL_RGBX;
        break;
    case PixelBlueGreenRedReserved8BitPerColor:
        pGMDesc->videoDepth = 32;
        pGMDesc->pixelMode = K_VIDEO_PIXEL_BGRX;
        break;
    case PixelBitMask:
        kPrintf(TEXT("Bitmask unsupported\r\n"));
        return EFI_UNSUPPORTED;
    }

    return EFI_SUCCESS;
}


K_GRAPHICS_MODE_DESC kSetupGraphics() {
    EFI_GRAPHICS_OUTPUT_PROTOCOL *pGOP;
    K_GRAPHICS_MODE_DESC GMDesc;
    kGetGOP(&pGOP);
    EFI_STATUS status = kSetupEFIFramebuffer(&GMDesc, pGOP);
    kEfiPrintErrorFunc(TEXT("EFI Framebuffer setup: "), status);
    return GMDesc;
}
