
#include "stdafx.h"
#include "GlobalVariable.h"
#include "StdLib.h"
#include "UefiApiWrapper.h"
#include "Utility.h"

#include "MemoryMap.h"

// The type of memory to allocate. Normal allocations (that is,
//   allocations by any UEFI application) are of type EfiLoaderData.
//   MemoryType values in the range 0x80000000..0xFFFFFFFF are
//   reserved for use by UEFI OS loaders that are provided by
//   operating system vendors. The only illegal memory type values
//   are those in the range EfiMaxMemoryType..0x7FFFFFFF.
// Important Note: since some firmwares have buggy routine
//   and hence cannot handle user-defined memory types,
//   we just use EfiLoaderData type (which equals 2) and
//   record the allocation information for the use of memory map
//   building routine.
#define EFI_MEMORY_TYPE_KERNEL_AREA     ((EFI_MEMORY_TYPE)2)

static struct KernelMemoryDescriptor {
    UINT64          PhysicalStart;
    UINT64          Size;
} KMD[512];
static UINT64 KMDCount;

void InitKernelAreaAllocator() {
    KMDCount = 0;
}

EFI_STATUS
AllocateKernelArea(
    IN UINTN Bytes,
    IN OUT void **Memory
)
{
    if (Bytes == 0) {
        return EFI_SUCCESS;
    }
    if (KMDCount == sizeof(KMD) / sizeof(KMD[0])) {
        // Cannot allocate more than we can record
        return EFI_OUT_OF_RESOURCES;
    }

    UINTN Pages = (Bytes + EFI_PAGE_SIZE - 1) / EFI_PAGE_SIZE;
    EFI_PHYSICAL_ADDRESS Addr = 1ULL << 36; // max. 64G
    EFI_STATUS ret = AllocatePages(AllocateMaxAddress, EFI_MEMORY_TYPE_KERNEL_AREA, Pages, &Addr);
    *Memory = (void *)Addr;
    if (!EFI_ERROR(ret)) {
        auto &kmd = KMD[KMDCount++];
        kmd.PhysicalStart = (UINT64)Addr;
        kmd.Size = Pages * EFI_PAGE_SIZE;
    }
    else {
        kPrintf(TEXT("AllocateKernelArea: Failed to allocate %llu pages\r\n"), Pages);
    }
    return ret;
}

void kPrintMemoryMap() {
    UINTN MemoryMapSize;
    EFI_MEMORY_DESCRIPTOR *MemoryMap;
    UINTN MapKey;
    UINTN DescriptorSize;
    UINT32 DescriptorVersion;
    UINTN DescriptorCount;

    // 메모리 맵 저장에 필요한 버퍼 크기 얻기
    // Note: The actual size of the buffer allocated for the consequent call
    // to GetMemoryMap() should be bigger then the value returned in MemoryMapSize,
    // since allocation of the new buffer may potentially increase memory map size.
    MemoryMapSize = 0;
    GetMemoryMap(&MemoryMapSize, NULL, &MapKey, &DescriptorSize, &DescriptorVersion);
    MemoryMapSize *= 2; // 그냥 넉넉하게...

    // 메모리 맵 받아오기
    kEfiPrintError(kMalloc(MemoryMapSize, (void **)&MemoryMap));
    kMemSet(MemoryMap, 0, MemoryMapSize);
    kEfiPrintError(GetMemoryMap(&MemoryMapSize, MemoryMap, &MapKey, &DescriptorSize, &DescriptorVersion));
    DescriptorCount = MemoryMapSize / DescriptorSize;

    // 메모리 맵 열거
    EFI_MEMORY_DESCRIPTOR *MemoryMapEntry;
    for (UINTN i = 0; i < DescriptorCount; i++) {
        MemoryMapEntry = ((EFI_MEMORY_DESCRIPTOR *)(((BYTE *)MemoryMap) + (DescriptorSize * i)));

        CHAR16 memtype[9];
        const CHAR16 *p = kEfiMemoryTypeDesc((EFI_MEMORY_TYPE)MemoryMapEntry->Type);
        if (p) {
            kStrCpy(memtype, p);
        }
        else {
            UINTN bufsz = sizeof(memtype);
            kSPrintf(memtype, &bufsz, TEXT("%08llX"), MemoryMapEntry->Type);
        }

        UINT64 start = (UINT64)MemoryMapEntry->PhysicalStart;
        UINT64 size = MemoryMapEntry->NumberOfPages * (UINT64)EFI_PAGE_SIZE;
        UINT64 end = start + size;
        kPrintf(TEXT("TYPE=%s START=%16llX END=%16llX SIZE=%lld\r\n"),
            memtype, start, end, size);

        if (i % 10 == 9 && i < DescriptorCount - 1) {
            kPrintf(TEXT("-- press any key for remaining output --\r\n"));
            kGetCh();
        }
    }

    kFree(MemoryMap);
}

struct MMAP_EVENT {
    UINTN           Address;
    K_MEMORY_TYPE   Type;
    bool            Starting;       // true = starting, false = ending
    MMAP_EVENT() {}
    MMAP_EVENT(UINTN Address_val, K_MEMORY_TYPE Type_val, bool Starting_val)
        : Address(Address_val), Type(Type_val), Starting(Starting_val) {}
};

void kAllocateAndBuildMemoryMap(UINTN *CountPtr, K_MEMORY_MAP **MapPtr) {
    UINTN EfiMapSize;
    EFI_MEMORY_DESCRIPTOR *EfiMap;
    UINTN MapKey;
    UINTN DescriptorSize;
    UINT32 DescriptorVersion;
    UINTN DescriptorCount;

    // 메모리 맵 저장에 필요한 버퍼 크기 얻기
    // Note: The actual size of the buffer allocated for the consequent call
    // to GetMemoryMap() should be bigger then the value returned in MemoryMapSize,
    // since allocation of the new buffer may potentially increase memory map size.
    EfiMapSize = 0;
    GetMemoryMap(&EfiMapSize, NULL, &MapKey, &DescriptorSize, &DescriptorVersion);
    kPrintf(TEXT("EFI map size: %llu bytes\r\n"), EfiMapSize);
    EfiMapSize = EfiMapSize * 2 + sizeof(EFI_MEMORY_DESCRIPTOR) * 10; // 그냥 넉넉하게...

    // 메모리 맵 받아오기
    EFI_STATUS e;
    if (EFI_ERROR(e = kMalloc(EfiMapSize, (void **)&EfiMap))) {
        kEfiPrintErrorFunc(TEXT("kAllocateAndBuildMemoryMap: Failed to allocate memory. Reason: "), e);
        return;
    }
    kMemSet(EfiMap, 0, EfiMapSize);
    kPrintf(TEXT("GetMemoryMap service address: 0x%llX\r\n"), g_SystemTable->BootServices->GetMemoryMap);
    e = GetMemoryMap(&EfiMapSize, EfiMap, &MapKey, &DescriptorSize, &DescriptorVersion);
    kEfiPrintErrorFunc(TEXT("GetMemoryMap: "), e);
    DescriptorCount = EfiMapSize / DescriptorSize;
    kPrintf(TEXT("EFI memory descriptor count = %llu\r\n"), DescriptorCount);

    // EFI 메모리 맵을 자체 포맷으로 변환
    K_MEMORY_MAP *Maps;
    AllocateKernelArea(sizeof(K_MEMORY_MAP) * ((DescriptorCount + KMDCount) * 3 + 1), (void **)&Maps);
    UINTN ActualDescriptorCount = 0;
    for (UINTN i = 0; i < DescriptorCount; i++) {
        EFI_MEMORY_DESCRIPTOR *EfiDesc = (EFI_MEMORY_DESCRIPTOR *)((UINTN)EfiMap + i * DescriptorSize);
        K_MEMORY_MAP &map = Maps[ActualDescriptorCount];
        map.StartingAddr = EfiDesc->PhysicalStart;
        map.Size = EfiDesc->NumberOfPages * EFI_PAGE_SIZE;
        if (((map.StartingAddr % EFI_PAGE_SIZE) != 0) ||
            (map.Size == 0) ||
            (map.StartingAddr + map.Size) < map.StartingAddr) {
            // Invalid entry
            continue;
        }
        switch (EfiDesc->Type) {
        case EfiLoaderCode:
        case EfiLoaderData:
        case EfiBootServicesCode:
        case EfiBootServicesData:
        case EfiConventionalMemory:
            map.Type = K_MEMORY_AVAILABLE;
            break;
        case EfiACPIReclaimMemory:
            map.Type = K_MEMORY_ACPI_REC;
            break;
        case EfiACPIMemoryNVS:
            map.Type = K_MEMORY_ACPI_NVS;
            break;
        case EfiMemoryMappedIO:
        case EfiMemoryMappedIOPortSpace:
            map.Type = K_MEMORY_MMIO;
            break;
        case EfiRuntimeServicesCode:
        case EfiRuntimeServicesData:
            map.Type = K_MEMORY_FW_RUNTIME;
            break;
        case EfiReservedMemoryType:
        case EfiPalCode:
            map.Type = K_MEMORY_RESERVED;
            break;
        case EfiUnusableMemory:
        default:
            map.Type = K_MEMORY_UNUSABLE;
            break;
        }
        ActualDescriptorCount++;
    }
    for (UINTN i = 0; i < KMDCount; i++) {
        KernelMemoryDescriptor &kmd = KMD[i];
        K_MEMORY_MAP &map = Maps[ActualDescriptorCount++];
        map.StartingAddr = kmd.PhysicalStart;
        map.Size = kmd.Size;
        map.Type = K_MEMORY_KERNEL_AREA;
        kPrintf(TEXT("Kernel area. start=%16llX, size=%16llX\r\n"), map.StartingAddr, map.Size);
    }
    kFree(EfiMap);

    // 메모리 맵을 오름차순으로 정렬하고, 겹치는 것이 있으면 수정함.
    // 이때 Type 숫자가 작은 것을 우선순위가 높은 것으로 취급하여 우선함.
    // 처리는 1-D Plane Sweeping 알고리즘을 사용함.
    MMAP_EVENT *pEvents;
    kMalloc(sizeof(MMAP_EVENT) * 2 * ActualDescriptorCount, (void **)&pEvents);
    for (UINTN i = 0; i < ActualDescriptorCount; i++) {
        K_MEMORY_MAP &map = Maps[i];
        pEvents[2 * i] = MMAP_EVENT(map.StartingAddr, map.Type, true);
        pEvents[2 * i + 1] = MMAP_EVENT(map.StartingAddr + map.Size, map.Type, false);
    }
    kSort(pEvents, 2 * ActualDescriptorCount, sizeof(MMAP_EVENT),
        [](const void *left, const void *right) -> bool {
        MMAP_EVENT *L = (MMAP_EVENT *)left;
        MMAP_EVENT *R = (MMAP_EVENT *)right;
        return (L->Address < R->Address) ||
            (L->Address == R->Address && L->Starting == false && R->Starting == true);
    });

    UINTN FinalDescriptorCount = 0;
    UINTN PAddress = 0;
    K_MEMORY_TYPE PTypesCount[K_MEMORY_TYPE_COUNT];
    kMemSet(PTypesCount, 0, sizeof(PTypesCount));
    for (UINTN i = 0; i < 2 * ActualDescriptorCount; i++) {
        MMAP_EVENT &evt = pEvents[i];
        K_MEMORY_TYPE PMinType = K_MEMORY_TYPE_COUNT;
        for (K_MEMORY_TYPE j = 0; j < K_MEMORY_TYPE_COUNT; j++) {
            if (PTypesCount[j] > 0) {
                PMinType = j;
                break;
            }
        }
        if (PMinType < K_MEMORY_TYPE_COUNT && PAddress < evt.Address) {
            if (FinalDescriptorCount > 0 &&
                Maps[FinalDescriptorCount - 1].Type == PMinType &&
                Maps[FinalDescriptorCount - 1].StartingAddr + Maps[FinalDescriptorCount - 1].Size == PAddress) {
                // Merge with previous memory region
                Maps[FinalDescriptorCount - 1].Size += evt.Address - PAddress;
            }
            else {
                K_MEMORY_MAP &map = Maps[FinalDescriptorCount++];
                map.StartingAddr = PAddress;
                map.Size = evt.Address - PAddress;
                map.Type = PMinType;
            }
        }

        if (evt.Starting) {
            PTypesCount[evt.Type]++;
        }
        else { // Ending
            PTypesCount[evt.Type]--;
        }
        PAddress = evt.Address;
    }
    kFree(pEvents);

    *CountPtr = FinalDescriptorCount;
    *MapPtr = Maps;

    for (UINTN i = 0; i < FinalDescriptorCount; i++) {
        K_MEMORY_MAP &map = Maps[i];
        kPrintf(TEXT("Type=%llu Start=%16llX End=%16llX Size=%llu\r\n"),
            map.Type, map.StartingAddr, map.StartingAddr + map.Size, map.Size);
        if (i % 10 == 9 && (i < (FinalDescriptorCount - 1))) kGetCh();
    }

    kGetCh();
}
