﻿
#include "StdAfx.h"
#include "GlobalVariable.h"
#include "UefiAPIWrapper.h"
#include "Utility.h"
#include "StdLib.h"

/*
kPrintf: UEFI 서비스를 이용한 문자열 출력

@param    FormatString        포맷 스트링
@retval    (없음)

*/

void __cdecl kPrintf (const CHAR16 *FormatString, ...)
{
    UINTN uiRequiredBufferSize = 0;

    // 필요한 버퍼 크기 얻기
    VA_LIST ap;
    VA_START(ap, FormatString);
    kVSPrintf(NULL, &uiRequiredBufferSize, FormatString, ap);
    VA_END(ap);

    // 필요한 버퍼 크기만큼 메모리를 할당하여 메모리로 출력
    CHAR16 *OutputBuffer;
    VA_START(ap, FormatString);
    kMalloc(uiRequiredBufferSize, (void **) &OutputBuffer);
    kMemSet(OutputBuffer, 0, uiRequiredBufferSize);
    kVSPrintf(OutputBuffer, &uiRequiredBufferSize, FormatString, ap);
    VA_END(ap);
    
    // 메모리의 내용을 UEFI 서비스를 통해 출력
    OutputString(OutputBuffer);
    kFree(OutputBuffer);
}

/*
kSPrintf: 주어진 버퍼로 문자열을 출력

@param    OutputBuffer        출력 버퍼
        OutputBufferSize    OutputBuffer의 크기. OutputBuffer의 크기가 충분하지 않으면 필요한 크기가 리턴됨
        FormatString        포맷 스트링

@retval    (없음)

*/

void __cdecl kSPrintf (CHAR16 *OutputBuffer, UINTN *OutputBufferSize, const CHAR16 *FormatString, ...)
{
    VA_LIST ap;
    VA_START(ap, FormatString);
    kVSPrintf(OutputBuffer, OutputBufferSize, FormatString, ap);
    VA_END(ap);
}

/*
kVSPrintf: Printf 계열 함수의 내부 루틴

@param    OutputBuffer        출력 버퍼
        OutputBufferSize    OutputBuffer의 크기(바이트). OutputBuffer의 크기가 충분하지 않으면 필요한 크기가 리턴됨
                            * 크기는 길이와 다름 (크기 = 길이 * sizeof(문자 1개))
        FormatString        포맷 스트링
        ap                    가변 인자 리스트

@retval    (없음)

*/

#define kVSPrintf_Num(_type, _scale, _upper, _signed) \
    bNumber = TRUE; iValue = VA_ARG(ap, _type); Scale = _scale; bUpper = _upper; bSigned = _signed;

void __cdecl kVSPrintf (CHAR16 *OutputBuffer, UINTN *OutputBufferSize, const CHAR16 *FormatString, VA_LIST ap)
{
    VA_LIST ap_backup = ap;

    CONST CHAR16 *FormatString_1;
    CHAR16 *OutputBuffer_1;
    CONST CHAR16 *String_Ptr;
    CHAR16 CurChar;
    
    BOOL bOut;
    BOOL bNumberGoto;
    BOOL bNumber;
    UINT64 iSignLen;
    INT64 iValue;
    UINT64 Scale;
    BOOL bUpper;
    UINT64 iWidth;
    BOOL bSigned;

    CHAR16 Buffer[128];

    /* 필요한 버퍼 크기를 계산 */
    /* 버퍼 크기가 충분하지 않으면 OutputBufferSize에 필요한 크기를 리턴 */

    UINTN OutputBufferSize_Required = 0;
    FormatString_1 = FormatString;
    bNumberGoto = FALSE;
    bNumber = FALSE;
    iWidth = 0;

    while((CurChar = *FormatString_1++) != CHAR('\0'))
    {
        bNumberGoto = FALSE;
        bOut = FALSE;
        switch(CurChar)
        {
        case CHAR('%'):
            CurChar = *FormatString_1;
            iSignLen = 0;

            // %__d, %__u, %__x, %__X (__는 2자리 10진 정수 (00-99))

            // %_
            if(ISNUMBER(CurChar))
            {
                // %__
                if(ISNUMBER(*(FormatString_1 + 1)))
                {
                    // %___ : 올바르지 않음. 건너뜀
                    if(ISNUMBER(*(FormatString_1 + 2)))
                    {
                        break;
                    }
                    else // %__(c) : 올바를 가능성이 있음. 아래 루틴으로 넘김
                    {
                        // __를 정수형으로 변환
                        bNumberGoto = TRUE;
                        iWidth = kAToU64(FormatString_1, 10);
                        FormatString_1 += 2;
                        CurChar = *FormatString_1;
                    }
                }
                else // %_(c) : 올바르지 않음. 건너뜀
                {
                    break;
                }
            }

            // %lld
            if(!kMemCmp(FormatString_1, TEXT("lld"), 3*sizeof(CHAR16)))
            {
                kVSPrintf_Num(INT64, 10, FALSE, TRUE);
                iSignLen += 3;
                goto check1;
            }

            // %llu
            if(!kMemCmp(FormatString_1, TEXT("llu"), 3*sizeof(CHAR16)))
            {
                kVSPrintf_Num(UINT64, 10, FALSE, FALSE);
                iSignLen += 3;
                goto check1;
            }

            // %llx
            if(!kMemCmp(FormatString_1, TEXT("llx"), 3*sizeof(CHAR16)))
            {
                kVSPrintf_Num(UINT64, 16, FALSE, FALSE);
                iSignLen += 3;
                goto check1;
            }
            
            // %llX
            if(!kMemCmp(FormatString_1, TEXT("llX"), 3*sizeof(CHAR16)))
            {
                kVSPrintf_Num(UINT64, 16, TRUE, FALSE);
                iSignLen += 3;
                goto check1;
            }

            // %c, %s, %%, %d, %u, %U, %x, %X
            switch(CurChar)
            {
            case CHAR('c'):
                OutputBufferSize_Required += 1;
                FormatString_1 += 1;
                bOut = TRUE;
                break;
            case CHAR('s'):
                OutputBufferSize_Required += kStrLen(VA_ARG(ap, CONST CHAR16 *));
                FormatString_1 += 1;
                bOut = TRUE;
                break;
            case CHAR('%'):
                OutputBufferSize_Required += 1;
                FormatString_1 += 1;
                bOut = TRUE;
                break;
            case CHAR('d'):
                kVSPrintf_Num(INT32, 10, FALSE, TRUE);
                iSignLen += 1;
                break;
            case CHAR('u'):
                kVSPrintf_Num(UINT32, 10, FALSE, FALSE);
                iSignLen += 1;
                break;
            case CHAR('x'):
                kVSPrintf_Num(UINT32, 16, FALSE, FALSE);
                iSignLen += 1;
                break;
            case CHAR('X'):
                kVSPrintf_Num(UINT32, 16, TRUE, FALSE);
                iSignLen += 1;
                break;
            }
check1:
            if(bNumber == TRUE)
            {
                if(bSigned)
                    kIToA64(Buffer, iValue, Scale, bUpper, iWidth);
                else
                    kUToA64(Buffer, iValue, Scale, bUpper, iWidth);

                OutputBufferSize_Required += kStrLen(Buffer);
                FormatString_1 += iSignLen;
                bOut = TRUE;
                bNumber = FALSE;

                iWidth = 0;
            }
            else
            {
                if(bNumberGoto)
                {
                    bNumberGoto = FALSE;
                    FormatString_1 -= 2;
                }
            }
            break;
        }
        
        if(bOut == FALSE)
            ++OutputBufferSize_Required;
    }

    OutputBufferSize_Required += 1; // CHAR('\0') 용 공간
    OutputBufferSize_Required *= sizeof(CHAR16);

    if(*OutputBufferSize < OutputBufferSize_Required)
    {
        *OutputBufferSize = OutputBufferSize_Required;
        return;
    }

    /* 실제 출력 루틴 */

    ap = ap_backup;

    OutputBuffer_1 = OutputBuffer;
    FormatString_1 = FormatString;
    bNumberGoto = FALSE;
    bNumber = FALSE;
    iWidth = 0;
        
    while((CurChar = *FormatString_1++) != CHAR('\0'))
    {
        bNumberGoto = FALSE;
        bOut = FALSE;
        switch(CurChar)
        {
        case CHAR('%'):
            CurChar = *FormatString_1;
            iSignLen = 0;

            // %__d, %__u, %__x, %__X (__는 2자리 10진 정수 (00-99))

            // %_
            if(ISNUMBER(CurChar))
            {
                // %__
                if(ISNUMBER(*(FormatString_1 + 1)))
                {
                    // %___ : 올바르지 않음. 건너뜀
                    if(ISNUMBER(*(FormatString_1 + 2)))
                    {
                        break;
                    }
                    else // %__(c) : 올바를 가능성이 있음. 아래 루틴으로 넘김
                    {
                        // __를 정수형으로 변환
                        bNumberGoto = TRUE;
                        iWidth = kAToU64(FormatString_1, 10);
                        FormatString_1 += 2;
                        CurChar = *FormatString_1;
                    }
                }
                else // %_(c) : 올바르지 않음. 건너뜀
                {
                    break;
                }
            }

            // %lld
            if(!kMemCmp(FormatString_1, TEXT("lld"), 3*sizeof(CHAR16)))
            {
                kVSPrintf_Num(INT64, 10, FALSE, TRUE);
                iSignLen += 3;
                goto check2;
            }

            // %llu
            if(!kMemCmp(FormatString_1, TEXT("llu"), 3*sizeof(CHAR16)))
            {
                kVSPrintf_Num(UINT64, 10, FALSE, FALSE);
                iSignLen += 3;
                goto check2;
            }

            // %llx
            if(!kMemCmp(FormatString_1, TEXT("llx"), 3*sizeof(CHAR16)))
            {
                kVSPrintf_Num(UINT64, 16, FALSE, FALSE);
                iSignLen += 3;
                goto check2;
            }
            
            // %llX
            if(!kMemCmp(FormatString_1, TEXT("llX"), 3*sizeof(CHAR16)))
            {
                kVSPrintf_Num(UINT64, 16, TRUE, FALSE);
                iSignLen += 3;
                goto check2;
            }

            // %c, %s, %%, %d, %u, %U, %x, %X
            switch(CurChar)
            {
            case CHAR('c'):
                *OutputBuffer_1++ = VA_ARG(ap, CHAR16);
                FormatString_1 += 1;
                bOut = TRUE;
                break;
            case CHAR('s'):
                String_Ptr = VA_ARG(ap, CONST CHAR16 *);
                kStrCpy(OutputBuffer_1, String_Ptr);
                OutputBuffer_1 += kStrLen(String_Ptr);
                FormatString_1 += 1;
                bOut = TRUE;
                break;
            case CHAR('%'):
                *OutputBuffer_1++ = CHAR('%');
                FormatString_1 += 1;
                bOut = TRUE;
                break;
            case CHAR('d'):
                kVSPrintf_Num(INT32, 10, FALSE, TRUE);
                iSignLen += 1;
                break;
            case CHAR('u'):
                kVSPrintf_Num(UINT32, 10, FALSE, FALSE);
                iSignLen += 1;
                break;
            case CHAR('x'):
                kVSPrintf_Num(UINT32, 16, FALSE, FALSE);
                iSignLen += 1;
                break;
            case CHAR('X'):
                kVSPrintf_Num(UINT32, 16, TRUE, FALSE);
                iSignLen += 1;
                break;
            }
check2:
            if(bNumber == TRUE)
            {
                if(bSigned)
                    kIToA64(Buffer, iValue, Scale, bUpper, iWidth);
                else
                    kUToA64(Buffer, iValue, Scale, bUpper, iWidth);
                
                kStrCpy(OutputBuffer_1, Buffer);
                OutputBuffer_1 += kStrLen(Buffer);
                FormatString_1 += iSignLen;
                bOut = TRUE;
                bNumber = FALSE;

                iWidth = 0;
            }
            else
            {
                if(bNumberGoto)
                {
                    bNumberGoto = FALSE;
                    FormatString_1 -= 2;
                }
            }
            break;
        }

        if(bOut == FALSE)
        {
            *OutputBuffer_1++ = CurChar;
        }
    }

    *OutputBuffer_1++ = CHAR('\0');
}

/*
kStrCpy: 문자열 복사

@param    Destination        대상
        Source            원본

@retval    (없음)

*/

void kStrCpy (CHAR16 *Destination, const CHAR16 *Source)
{
    do
    {
        *Destination++ = *Source++;
    } while (*Source != CHAR('\0'));

    *Destination = CHAR('\0');
}

/*
kStrLen: 문자열의 길이 계산

@param    String        입력 문자열

@retval    입력 문자열의 길이

*/

UINTN kStrLen (const CHAR16 *String)
{
    const CHAR16 *String_1 = String;
    while(*String_1 != CHAR('\0'))
        ++String_1;

    return (String_1 - String);
}

/*
kStrCmp: 문자열 비교

@param    String1        입력 문자열 1
        String2        입력 문자열 2

@retval    String1 == String2 : 0 리턴
        String1 > String2 : 양수 리턴
        String1 < String2 : 음수 리턴
        * 비교는 NULL character을 만날 때까지 진행되며,
        String1에서 c1, String2에서 c2의 서로 다른 문자가 발견되면
        c1 - c2 의 부호에 따라 양수 또는 음수를 리턴
*/

INTN kStrCmp (const CHAR16 *String1, const CHAR16 *String2)
{
    while(*String1 == *String2 && *String1 && *String2)
        ++String1, ++String2;

    return (INTN) SIGN(*String1 - *String2);
}

/*
kStrInv: 문자열 뒤집기
        kStrLen(String)에 의해 얻어지는 문자열의 길이를 바탕으로 뒤집기 연산 수행

@param    String        입력 문자열

@retval    (없음)

*/

void kStrInv (CHAR16 *String)
{
    UINTN Len = kStrLen(String);
    if(Len <= 1)
        return;

    UINTN i;
    UINTN j = Len / 2;

    CHAR16 temp;
    for(i=0; i<j; i++)
    {
        // String의 i번째 항목과 Len-i-1번째 항목을 바꿈
        temp = String[i];
        String[i] = String[Len-i-1];
        String[Len-i-1] = temp;
    }
}

/*
kMemSet: 메모리를 초기화함

@param    Mem            초기화할 메모리
        Value        초기화할 값
        Size        초기화할 크기

@retval    (없음)

*/

void kMemSet (void *Mem, BYTE Value, K_SIZE Size)
{
    BYTE *pMem = (BYTE *) Mem;
    for(K_SIZE i=0; i<Size; i++)
        *pMem++ = Value;
}

/*
kMemCpy: 메모리 복사

@param    Destination        대상
        Source            원본

@retval    (없음)

*/

void kMemCpy (void *Destination, const void *Source, K_SIZE Num)
{
    BYTE *Destination2 = (BYTE *) Destination;
    const BYTE *Source2 = (const BYTE *) Source;

    for(UINTN i=0; i<Num; ++i)
        *Destination2++ = *Source2++;
}

/*
kMemCmp: 문자열 비교

@param    Mem1        입력 메모리 1
        Mem2        입력 메모리 2
        Size        비교할 바이트 수

@retval    Mem1 == Mem2 : 0 리턴
        Mem1 > Mem2 : 양수 리턴
        Mem1 < Mem2 : 음수 리턴
        * 비교는 Size 바이트만큼 진행되며,
        Mem1에서 c1, Mem2에서 c2의 서로 다른 바이트가 발견되면
        c1 - c2 의 부호에 따라 양수 또는 음수를 리턴
*/

INTN kMemCmp (const void *Mem1, const void *Mem2, K_SIZE Size)
{
    BYTE *pMem1 = (BYTE *) Mem1;
    BYTE *pMem2 = (BYTE *) Mem2;

    INTN ret = 0;

    UINTN i;
    for(i=0; i<Size; i++)
    {
        if(*pMem1 != *pMem2)
        {
            ret = ((*pMem1 > *pMem2) ? 1 : (-1));
            break;
        }

        ++pMem1;
        ++pMem2;
    }

    return ret;
}

/*
kAToI64: 문자열을 부호 있는 64비트 정수형으로 변환

@param    String        변환할 문자열
        Scale        진법

@retval    변환된 정수

*/

INT64 kAToI64 (const CHAR16 *String, UINT64 Scale)
{
    if(!String)
        return -1;

    if(Scale <= 1 || Scale > 36)
        Scale = 10;

    UINT64 iNumber = 0;
    UINT64 iTemp;
    BOOL bNegative;
    CHAR16 chTemp;

    if(*String == CHAR('-'))
    {
        bNegative = TRUE;
        ++String;
    }
    else
        bNegative = FALSE;

    if(Scale <= 10)
    {
        CHAR16 chRangeMax = (CHAR16)(CHAR('0') + Scale - 1);

        while((chTemp = *String) != CHAR('\0'))
        {
            if(chTemp >= CHAR('0') && chTemp <= chRangeMax)
            {
                iTemp = chTemp - CHAR('0');
                iNumber *= Scale;
                iNumber += iTemp;
                ++String;
            }
            else
                 break;
        }
    }
    else
    {
        CHAR16 chRangeMaxUpper = (CHAR16)(CHAR('A') + Scale - 11);
        CHAR16 chRangeMaxLower = (CHAR16)(CHAR('a') + Scale - 11);
        BOOL bNumber;

        while((chTemp = *String) != CHAR('\0'))
        {
            bNumber = FALSE;
            if(chTemp >= CHAR('0') && chTemp <= CHAR('9'))
            {
                iTemp = chTemp - CHAR('0');
                bNumber = TRUE;
            }
            else if(chTemp >= CHAR('A') && chTemp <= chRangeMaxUpper)
            {
                iTemp = 10 + chTemp - CHAR('A');
                bNumber = TRUE;
            }
            else if(chTemp >= CHAR('a') && chTemp <= chRangeMaxLower)
            {
                iTemp = 10 + chTemp - CHAR('a');
                bNumber = TRUE;
            }

            if(bNumber)
            {
                iNumber *= Scale;
                iNumber += iTemp;
                ++String;
            }
            else
                break;
        }
    }

    INT64 iRet = (INT64) iNumber;
    if(bNegative)
        iRet = -iRet;

    return iRet;
}

/*
kAToU64: 문자열을 부호 없는 64비트 정수형으로 변환

@param    String        변환할 문자열
        Scale        진법

@retval    변환된 정수

*/

UINT64 kAToU64 (const CHAR16 *String, UINT64 Scale)
{
    if(!String)
        return -1;

    if(Scale <= 1 || Scale > 36)
        Scale = 10;

    UINT64 iNumber = 0;
    UINT64 iTemp;
    CHAR16 chTemp;

    if(Scale <= 10)
    {
        CHAR16 chRangeMax = (CHAR16)(CHAR('0') + Scale - 1);

        while((chTemp = *String) != CHAR('\0'))
        {
            if(chTemp >= CHAR('0') && chTemp <= chRangeMax)
            {
                iTemp = chTemp - CHAR('0');
                iNumber *= Scale;
                iNumber += iTemp;
                ++String;
            }
            else
                break;
        }
    }
    else
    {
        CHAR16 chRangeMaxUpper = (CHAR16)(CHAR('A') + Scale - 11);
        CHAR16 chRangeMaxLower = (CHAR16)(CHAR('a') + Scale - 11);
        BOOL bNumber;

        while((chTemp = *String) != CHAR('\0'))
        {
            bNumber = FALSE;
            if(chTemp >= CHAR('0') && chTemp <= CHAR('9'))
            {
                iTemp = chTemp - CHAR('0');
                bNumber = TRUE;
            }
            else if(chTemp >= CHAR('A') && chTemp <= chRangeMaxUpper)
            {
                iTemp = 10 + chTemp - CHAR('A');
                bNumber = TRUE;
            }
            else if(chTemp >= CHAR('a') && chTemp <= chRangeMaxLower)
            {
                iTemp = 10 + chTemp - CHAR('a');
                bNumber = TRUE;
            }

            if(bNumber == TRUE)
            {
                iNumber *= Scale;
                iNumber += iTemp;
                ++String;
            }
            else
                break;
        }
    }

    return iNumber;
}

static const CHAR16 g_ConvTableUpper[36] = 
{
CHAR('0'), CHAR('1'), CHAR('2'), CHAR('3'), CHAR('4'), CHAR('5'), CHAR('6'), CHAR('7'),
CHAR('8'), CHAR('9'), CHAR('A'), CHAR('B'), CHAR('C'), CHAR('D'), CHAR('E'), CHAR('F'), 
CHAR('G'), CHAR('H'), CHAR('I'), CHAR('J'), CHAR('K'), CHAR('L'), CHAR('M'), CHAR('N'), 
CHAR('O'), CHAR('P'), CHAR('Q'), CHAR('R'), CHAR('S'), CHAR('T'), CHAR('U'), CHAR('V'), 
CHAR('W'), CHAR('X'), CHAR('Y'), CHAR('Z')
};

static const CHAR16 g_ConvTableLower[36] = 
{
CHAR('0'), CHAR('1'), CHAR('2'), CHAR('3'), CHAR('4'), CHAR('5'), CHAR('6'), CHAR('7'),
CHAR('8'), CHAR('9'), CHAR('a'), CHAR('b'), CHAR('c'), CHAR('d'), CHAR('e'), CHAR('f'), 
CHAR('g'), CHAR('h'), CHAR('i'), CHAR('j'), CHAR('k'), CHAR('l'), CHAR('m'), CHAR('n'), 
CHAR('o'), CHAR('p'), CHAR('q'), CHAR('r'), CHAR('s'), CHAR('t'), CHAR('u'), CHAR('v'), 
CHAR('w'), CHAR('x'), CHAR('y'), CHAR('z')
};

/*
kIToA64: 부호 있는 64비트 정수형을 문자열로 변환

@param    Buffer        출력 버퍼
        iValue        변환할 값
        Scale        진법
        bUpper        대문자/소문자 (0 이외의 값: 대문자, 0(FALSE): 소문자)
        iWidth        부호를 제외한 자릿수 부분의 수. 부족하면 0으로 채워지고 넘치면 잘림
                    0으로 지정하면 무시됨
*/

void kIToA64 (CHAR16 *Buffer, INT64 iValue, UINT64 Scale, BOOL bUpper, UINT64 iWidth)
{
    if(Scale <= 1 || Scale > 36)
        Scale = 10;

    CHAR16 *Buffer_1 = Buffer;

    if(iValue < 0)
        *Buffer_1++ = CHAR('-');

    UINT64 AbsValue = ABS(iValue);

    if(AbsValue == 0)
    {
        iWidth = MAX(iWidth, 1);
        for(UINTN i=0; i<iWidth; i++)
            *Buffer_1++ = CHAR('0');
    }
    else
    {
        const CHAR16 *ConvTable = bUpper ? g_ConvTableUpper : g_ConvTableLower;
        if(iWidth == 0)
        {
            while(AbsValue != 0)
            {
                *Buffer_1++ = ConvTable[AbsValue % Scale];
                AbsValue /= Scale;
            }
        }
        else
        {
            UINTN i;
            for(i=0; i<iWidth; i++)
            {
                *Buffer_1++ = ConvTable[AbsValue % Scale];
                AbsValue /= Scale;
            }
        }
    }

    *Buffer_1 = CHAR('\0');

    if(iValue < 0)
        kStrInv(Buffer + 1);
    else
        kStrInv(Buffer);
}

/*
kUToA64: 부호 없는 64비트 정수형을 문자열로 변환

@param    Buffer        출력 버퍼
        iValue        변환할 값
        Scale        진법
        bUpper        대문자/소문자 (0 이외의 값: 대문자, 0(FALSE): 소문자)
        iWidth        부호를 제외한 자릿수 부분의 수. 부족하면 0으로 채워지고 넘치면 잘림
                    0으로 지정하면 무시됨
*/

void kUToA64 (CHAR16 *Buffer, UINT64 iValue, UINT64 Scale, BOOL bUpper, UINT64 iWidth)
{
    if(Scale <= 1 || Scale > 36)
        Scale = 10;

    CHAR16 *Buffer_1 = Buffer;

    if(iValue == 0)
    {
        iWidth = MAX(iWidth, 1);
        for(UINTN i=0; i<iWidth; i++)
            *Buffer_1++ = CHAR('0');
    }
    else
    {
        const CHAR16 *ConvTable = bUpper ? g_ConvTableUpper : g_ConvTableLower;
        if(iWidth == 0)
        {
            while(iValue != 0)
            {
                *Buffer_1++ = ConvTable[iValue % Scale];
                iValue /= Scale;
            }
        }
        else
        {
            UINTN i;
            for(i=0; i<iWidth; i++)
            {
                *Buffer_1++ = ConvTable[iValue % Scale];
                iValue /= Scale;
            }
        }
    }

    *Buffer_1 = CHAR('\0');
    kStrInv(Buffer);
}

/*
kSort: 정렬 수행

@param    Base        정렬할 대상의 메모리 상의 주소
        Num            정렬할 대상의 수
        Size        정렬할 대상 1개의 크기
        CompFunc    대상을 비교하는 함수
            -> 원형: bool CompFunc (const void* p1, const void* p2)
            -> Return Value   true  : P1이 반드시 P2 앞에 와야 함.
                              false : P1이 P2와 동등하거나 P2 뒤에 와야 함.

@retval    (없음)

*/

void kSortInternal (void *Base, K_SIZE Size, bool (*CompFunc)(const void *, const void *), UINTN iStart, UINTN iEnd)
{
    if(iEnd < iStart)
        return;

    BYTE *t;
    kMalloc(Size, (void **) &t);

    UINTN count = iEnd - iStart + 1;

    BYTE *start = ((BYTE *) Base) + (Size * iStart);
    BYTE *p, *q;

    /* 삽입정렬 */
    for(UINTN i=0; i<count; ++i)
    {
        /* 인덱스 n이 i <= n <= count - 1 인 항목 중에서 
           가장 앞쪽에 오는 항목을 찾아서
           i번째 항목과 해당 항목을 바꿈 */

        q = start + (Size * i);
        for(UINTN j = i + 1; j<count; j++)
        {
            p = start + (Size * j);
            if(CompFunc(p, q) == true)
                q = p;
        }

        p = start + (Size * i);
        if(p != q)
        {
            /* i번째 항목(p)과 항목 q를 바꿈*/

            kMemCpy(t, p, Size);
            kMemCpy(p, q, Size);
            kMemCpy(q, t, Size);
        }
    }

    kFree(t);
}

void kSort (void *Base, K_SIZE Num, K_SIZE Size, bool (*CompFunc)(const void *, const void *))
{
    kSortInternal(Base, Size, CompFunc, 0, Num - 1);
}

/*
kReadFileIntoMemory: 메모리를 할당하여 주어진 파일 핸들이 가리키는 파일의 전체를 읽고
                     그 포인터를 반환

@param    File        읽을 파일의 핸들
          ppBuffer    파일이 읽힌 메모리 버퍼의 주소 (kMalloc로 할당->kFree로 해제되어야 함)
          pszFile     파일 크기가 저장될 메모리 주소

*/

EFI_STATUS kReadFileIntoMemory(EFI_FILE_PROTOCOL * File, void ** ppBuffer, UINTN * pszFile)
{
    /* 파일 크기 얻기 */

    UINTN szBuffer = 0;
    EFI_STATUS EfiStatus = File->GetInfo(File, &FileInfoGuid, &szBuffer, NULL);
    kPrintf(TEXT("1"));
    if (EfiStatus != EFI_BUFFER_TOO_SMALL && EFI_ERROR(EfiStatus)) return EfiStatus;

    BYTE *pBuffer;
    EfiStatus = kMalloc(szBuffer, (void **)&pBuffer);
    kEfiPrintErrorFunc(TEXT("2 "), EfiStatus);
    if (EFI_ERROR(EfiStatus)) return EfiStatus;
    EfiStatus = File->GetInfo(File, &FileInfoGuid, &szBuffer, pBuffer);
    kEfiPrintErrorFunc(TEXT("3 "), EfiStatus);
    if (EFI_ERROR(EfiStatus)) return EfiStatus;

    UINT64 szFile = ((EFI_FILE_INFO *)pBuffer)->FileSize;
    kFree(pBuffer);
    kPrintf(TEXT("File Size: %lld\r\n"), szFile);

    if (!szFile) {
        *ppBuffer = NULL;
        if (pszFile) *pszFile = 0;
        return EFI_END_OF_FILE;
    }

    /* 파일 읽기 */

    UINT64 uiOldPos;
    File->GetPosition(File, &uiOldPos);

    EfiStatus = kMalloc(szFile, (void **)&pBuffer);
    kEfiPrintErrorFunc(TEXT("4 "), EfiStatus);
    if (EFI_ERROR(EfiStatus)) return EfiStatus;
    EfiStatus = File->SetPosition(File, 0);
    kEfiPrintErrorFunc(TEXT("5 "), EfiStatus);
    EfiStatus = File->Read(File, &szFile, pBuffer);
    kEfiPrintErrorFunc(TEXT("6 "), EfiStatus);

    EfiStatus = File->SetPosition(File, uiOldPos);
    kEfiPrintErrorFunc(TEXT("7 "), EfiStatus);

    *ppBuffer = pBuffer;
    if (pszFile) *pszFile = szFile;

    return EFI_SUCCESS;
}

/*
kIsPowerOfTwo64: 정수가 2의 거듭제곱인지 판별

@param    Num        입력 정수
@retval    2의 거듭제곱인지 여부

*/

BOOL kIsPowerOfTwo64(UINT64 Num)
{
    return ((Num) && (!((Num-1)&Num)));
}

/*
kGetMaxBit: 정수의 최상위 비트 번호를 반환

@param    Num                    정수
@retval    최상위 비트 번호    찾을 경우 0-63 범위의 수를 리턴, Num이 0이면 (UINT64)-1 을 리턴

*/

UINT64 kGetMaxBit(UINT64 Num)
{
    UINT64 retval = (UINT64)-1;
    for (UINTN i = 63; i != 0; --i)
    {
        if ((((UINT64)1) << i) & Num)
        {
            retval = i;
            break;
        }
    }

    return retval;
}

/*
kGetMinBit: 정수의 최하위 비트 번호를 반환

@param    Num                    정수
@retval    최하위 비트 번호    찾을 경우 0-63 범위의 수를 리턴, Num이 0이면 (UINT64)-1 을 리턴

*/

UINT64 kGetMinBit(UINT64 Num)
{
    UINT64 retval = (UINT64)-1;
    for (UINTN i = 0; i < 64; ++i)
    {
        if ((((UINT64)1) << i) & Num)
        {
            retval = i;
            break;
        }
    }

    return retval;
}
