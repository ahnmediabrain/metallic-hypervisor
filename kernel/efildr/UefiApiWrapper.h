﻿
#pragma once

// Console I/O Services
EFI_STATUS
EFIAPI
ReadKeyStroke(
    OUT EFI_INPUT_KEY                  *Key
    );

// Event, Timer, and Task Priority Services

// Memory Allocation Services

EFI_STATUS
EFIAPI
AllocatePages (
  IN EFI_ALLOCATE_TYPE Type,
  IN EFI_MEMORY_TYPE MemoryType,
  IN UINTN Pages,
  IN OUT EFI_PHYSICAL_ADDRESS *Memory
  );

EFI_STATUS
FreePages (
  IN EFI_PHYSICAL_ADDRESS Memory,
  IN UINTN Pages
  );

EFI_STATUS
EFIAPI
GetMemoryMap (
  IN OUT UINTN *MemoryMapSize,
  IN OUT EFI_MEMORY_DESCRIPTOR *MemoryMap,
  OUT UINTN *MapKey,
  OUT UINTN *DescriptorSize,
  OUT UINT32 *DescriptorVersion
  );

EFI_STATUS
EFIAPI
AllocatePool (
  IN EFI_MEMORY_TYPE PoolType,
  IN UINTN Size,
  OUT VOID **Buffer
  );

EFI_STATUS
EFIAPI
FreePool (
  IN VOID *Buffer
  );

// Allocates memory from memory pool.
// (this memory will be considered as free after kernel entry)
EFI_STATUS
EFIAPI
kMalloc (
  IN UINTN Size,
  OUT VOID **Buffer
  );

EFI_STATUS
EFIAPI
kFree (
  IN VOID *Buffer
  );

// Protocol Handler Services
EFI_STATUS
EFIAPI
OpenProtocol (
  IN EFI_HANDLE Handle,
  IN EFI_GUID *Protocol,
  OUT VOID **Interface, OPTIONAL
  IN EFI_HANDLE AgentHandle,
  IN EFI_HANDLE ControllerHandle,
  IN UINT32 Attributes
  );

EFI_STATUS
EFIAPI
CloseProtocol (
  IN EFI_HANDLE Handle,
  IN EFI_GUID *Protocol,
  IN EFI_HANDLE AgentHandle,
  IN EFI_HANDLE ControllerHandle
  );

EFI_STATUS
EFIAPI
LocateHandleBuffer (
  IN EFI_LOCATE_SEARCH_TYPE SearchType,
  IN EFI_GUID *Protocol OPTIONAL,
  IN VOID *SearchKey OPTIONAL,
  IN OUT UINTN *NoHandles,
  OUT EFI_HANDLE **Buffer
  );


// Image Services
EFI_STATUS
EFIAPI
Exit (
  IN EFI_HANDLE ImageHandle,
  IN EFI_STATUS ExitStatus,
  IN UINTN ExitDataSize,
  IN CHAR16 *ExitData OPTIONAL
  );

EFI_STATUS
EFIAPI
ExitBootServices (
  IN UINTN MapKey
  );

// Video API
EFI_STATUS
EFIAPI
OutputString (
  IN CHAR16 *String
  );

// Protocols - Media Access

// Simple File System Protocol
EFI_STATUS
EFIAPI
OpenVolume (
  IN EFI_SIMPLE_FILE_SYSTEM_PROTOCOL *This,
  OUT EFI_FILE_PROTOCOL **Root
  );

// EFI File Protocol
EFI_STATUS
EFIAPI
OpenSystemVolume (
  OUT EFI_SIMPLE_FILE_SYSTEM_PROTOCOL **pSFSP,
  OUT EFI_FILE_PROTOCOL **ppSystemVolume
  );

// Boot services
void kExitBootServices();
