﻿
#include "StdAfx.h"
#include "UefiAPIWrapper.h"
#include "StdLib.h"
#include "Utility.h"

typedef struct INTERNAL_EFI_STATUS_TABLE
{
    EFI_STATUS    Code;
    CONST CHAR16*    String;

} INTERNAL_EFI_STATUS_TABLE;

#define TABLE_COUNT_ERROR        39
#define TABLE_COUNT_WARNING        5

const INTERNAL_EFI_STATUS_TABLE g_TableError[TABLE_COUNT_ERROR] = 
{
    {EFI_SUCCESS, TEXT("Success")},
    {EFI_LOAD_ERROR, TEXT("Load error")},
    {EFI_INVALID_PARAMETER, TEXT("Invalid paramater")},
    {EFI_UNSUPPORTED, TEXT("Unsupported")},
    {EFI_BAD_BUFFER_SIZE, TEXT("Bad buffer size")},
    {EFI_BUFFER_TOO_SMALL, TEXT("Buffer too small")},
    {EFI_NOT_READY, TEXT("Not ready")},
    {EFI_DEVICE_ERROR, TEXT("Device error")},
    {EFI_WRITE_PROTECTED, TEXT("Write protected")},
    {EFI_OUT_OF_RESOURCES, TEXT("Out of resources")},
    {EFI_VOLUME_CORRUPTED, TEXT("Volume corrupted")},
    {EFI_VOLUME_FULL, TEXT("Volume full")},
    {EFI_NO_MEDIA, TEXT("No media")},
    {EFI_MEDIA_CHANGED, TEXT("Media changed")},
    {EFI_NOT_FOUND, TEXT("Not found")},
    {EFI_ACCESS_DENIED, TEXT("Access denied")},
    {EFI_NO_RESPONSE, TEXT("No response")},
    {EFI_NO_MAPPING, TEXT("No mapping")},
    {EFI_TIMEOUT, TEXT("Timeout")},
    {EFI_NOT_STARTED, TEXT("Not started")},
    {EFI_ALREADY_STARTED, TEXT("Already started")},
    {EFI_ABORTED, TEXT("Aborted")},
    {EFI_ICMP_ERROR, TEXT("ICMP error")},
    {EFI_TFTP_ERROR, TEXT("TFTP error")},
    {EFI_PROTOCOL_ERROR, TEXT("Protocol error")},
    {EFI_INCOMPATIBLE_VERSION, TEXT("Incompatible version")},
    {EFI_SECURITY_VIOLATION, TEXT("Security violation")},
    {EFI_CRC_ERROR, TEXT("CRC error")},
    {EFI_END_OF_MEDIA, TEXT("End of media")},
    {EFI_END_OF_FILE, TEXT("End of file")},
    {EFI_INVALID_LANGUAGE, TEXT("Invalid language")},
    {EFI_COMPROMISED_DATA, TEXT("Compromised data")},
    {EFI_NETWORK_UNREACHABLE, TEXT("Network unreachable")},
    {EFI_HOST_UNREACHABLE, TEXT("Host unreachable")},
    {EFI_PROTOCOL_UNREACHABLE, TEXT("Protocol unreachable")},
    {EFI_PORT_UNREACHABLE, TEXT("Port unreachable")},
    {EFI_CONNECTION_FIN, TEXT("Connection fin")},
    {EFI_CONNECTION_RESET, TEXT("Connection reset")},
    {EFI_CONNECTION_REFUSED, TEXT("Connection refused")},
};

const INTERNAL_EFI_STATUS_TABLE g_TableWarning[TABLE_COUNT_WARNING] = 
{
    {EFI_WARN_UNKNOWN_GLYPH, TEXT("Unknown glyph")},
    {EFI_WARN_DELETE_FAILURE, TEXT("Delete failure")},
    {EFI_WARN_WRITE_FAILURE, TEXT("Write failure")},
    {EFI_WARN_BUFFER_TOO_SMALL, TEXT("Buffer too small")},
    {EFI_WARN_STALE_DATA, TEXT("Stale data")}
};

CHAR16 kGetCh()
{
    EFI_INPUT_KEY key;
    while (ReadKeyStroke(&key) == EFI_NOT_READY);
    return key.UnicodeChar;
}

const CHAR16 *kEfiMemoryTypeDesc(EFI_MEMORY_TYPE Type)
{
    switch (Type) {
    case EfiReservedMemoryType:
        return TEXT("RESERVED");
    case EfiLoaderCode:
        return TEXT("LDRCODE ");
    case EfiLoaderData:
        return TEXT("LDRDATA ");
    case EfiBootServicesCode:
        return TEXT("BTSVCODE");
    case EfiBootServicesData:
        return TEXT("BTSVDATA");
    case EfiRuntimeServicesCode:
        return TEXT("RTSVCODE");
    case EfiRuntimeServicesData:
        return TEXT("RTSVDATA");
    case EfiConventionalMemory:
        return TEXT("CONVNMEM");
    case EfiUnusableMemory:
        return TEXT("UNUSBMEM");
    case EfiACPIReclaimMemory:
        return TEXT("ACPIRECL");
    case EfiACPIMemoryNVS:
        return TEXT("ACPINVS ");
    case EfiMemoryMappedIO:
        return TEXT("MMIO    ");
    case EfiMemoryMappedIOPortSpace:
        return TEXT("MMIOPSPC");
    case EfiPalCode:
        return TEXT("PALCODE ");
    }
    return nullptr;
}

void kEfiPrintError(EFI_STATUS Code)
{
    if(Code == EFI_SUCCESS)
    {
        OutputString(TEXT("EFI: SUCCESS\r\n"));
        return;
    }

    const INTERNAL_EFI_STATUS_TABLE *Table;
    UINTN Count;
    if(EFI_ERROR(Code))
    {
        Table = g_TableError;
        Count = TABLE_COUNT_ERROR;
        OutputString(TEXT("EFI: ERROR: "));
    }
    else
    {
        Table = g_TableWarning;
        Count = TABLE_COUNT_WARNING;
        OutputString(TEXT("EFI: WARNING: "));
    }

    UINTN i;
    UINTN Index;
    BOOL bFound = FALSE;

    for(i=0; i<Count; i++)
    {
        if(Table[i].Code == Code)
        {
            Index = i;
            bFound = TRUE;
            break;
        }
    }

    if(bFound == FALSE)
    {
    //    OutputString(TEXT("Unknown\r\n"));
        kPrintf(TEXT("Unknown Code: %llX\r\n"), Code);
        return;
    }

    OutputString((CHAR16 *)Table[Index].String);
    OutputString((CHAR16 *)TEXT("\r\n"));
}

void kEfiPrintErrorFunc(const CHAR16 *String, EFI_STATUS Code)
{
    kPrintf(TEXT("%s"), String);
    kEfiPrintError(Code);
}

EFI_STATUS kEfiWriteErrorFile(EFI_FILE_PROTOCOL *pVolume, const CHAR16 *pszString, const CHAR16 *pszFileName)
{
    EFI_FILE_PROTOCOL *pLogsDir;
    EFI_FILE_PROTOCOL *pLogFile;

    if (!pVolume || !pszString || !pszFileName)
        return EFI_INVALID_PARAMETER;

    pVolume->Open(pVolume, &pLogsDir, TEXT("\\AhnOS\\Config\\Logs"), EFI_FILE_MODE_READ | EFI_FILE_MODE_WRITE, 0);

    pLogsDir->Close(pLogsDir);
    
    return EFI_SUCCESS;
}