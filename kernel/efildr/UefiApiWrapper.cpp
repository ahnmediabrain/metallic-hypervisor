﻿
#include "StdAfx.h"
#include "GlobalVariable.h"
#include "StdLib.h"
#include "Utility.h"
#include "UefiAPIWrapper.h"

// Console I/O Services

EFI_STATUS
EFIAPI
ReadKeyStroke(
    OUT EFI_INPUT_KEY *Key)
{
    return g_SystemTable->ConIn->ReadKeyStroke(
        g_SystemTable->ConIn,
        Key
    );
}

// Event, Timer, and Task Priority Services

// Memory Allocation Services

EFI_STATUS
EFIAPI
AllocatePages(
    IN EFI_ALLOCATE_TYPE Type,
    IN EFI_MEMORY_TYPE MemoryType,
    IN UINTN Pages,
    IN OUT EFI_PHYSICAL_ADDRESS *Memory
)
{
    return g_SystemTable->BootServices->AllocatePages(
        Type,
        MemoryType,
        Pages,
        Memory
    );
}

EFI_STATUS
FreePages(
    IN EFI_PHYSICAL_ADDRESS Memory,
    IN UINTN Pages
)
{
    return g_SystemTable->BootServices->FreePages(
        Memory,
        Pages
    );
}

EFI_STATUS
GetMemoryMap(
    IN OUT UINTN *MemoryMapSize,
    IN OUT EFI_MEMORY_DESCRIPTOR *MemoryMap,
    OUT UINTN *MapKey,
    OUT UINTN *DescriptorSize,
    OUT UINT32 *DescriptorVersion
)
{
    return g_SystemTable->BootServices->GetMemoryMap(
        MemoryMapSize,
        MemoryMap,
        MapKey,
        DescriptorSize,
        DescriptorVersion
    );
}

EFI_STATUS
AllocatePool(
    IN EFI_MEMORY_TYPE PoolType,
    IN UINTN Size,
    OUT VOID **Buffer
)
{
    return g_SystemTable->BootServices->AllocatePool(
        PoolType,
        Size,
        Buffer
    );
}

EFI_STATUS
FreePool(
    IN VOID *Buffer
)
{
    return g_SystemTable->BootServices->FreePool(Buffer);
}

EFI_STATUS
EFIAPI
kMalloc(
    IN UINTN Size,
    OUT VOID **Buffer
)
{
    return AllocatePool(EfiLoaderData, Size, Buffer);
}

EFI_STATUS
EFIAPI
kFree(
    IN VOID *Buffer
)
{
    return FreePool(Buffer);
}

// Protocol Handler Services
EFI_STATUS
EFIAPI
OpenProtocol(
    IN EFI_HANDLE Handle,
    IN EFI_GUID *Protocol,
    OUT VOID **Interface, OPTIONAL
    IN EFI_HANDLE AgentHandle,
    IN EFI_HANDLE ControllerHandle,
    IN UINT32 Attributes
)
{
    return g_SystemTable->BootServices->OpenProtocol(
        Handle,
        Protocol,
        Interface,
        AgentHandle,
        ControllerHandle,
        Attributes
    );
}

EFI_STATUS
EFIAPI
CloseProtocol(
    IN EFI_HANDLE Handle,
    IN EFI_GUID *Protocol,
    IN EFI_HANDLE AgentHandle,
    IN EFI_HANDLE ControllerHandle
)
{
    return g_SystemTable->BootServices->CloseProtocol(
        Handle,
        Protocol,
        AgentHandle,
        ControllerHandle
    );
}

EFI_STATUS
EFIAPI
LocateHandleBuffer(
    IN EFI_LOCATE_SEARCH_TYPE SearchType,
    IN EFI_GUID *Protocol OPTIONAL,
    IN VOID *SearchKey OPTIONAL,
    IN OUT UINTN *NoHandles,
    OUT EFI_HANDLE **Buffer
)
{
    return g_SystemTable->BootServices->LocateHandleBuffer(
        SearchType,
        Protocol,
        SearchKey,
        NoHandles,
        Buffer
    );
}

// Image Services
EFI_STATUS
EFIAPI
Exit(
    IN EFI_HANDLE ImageHandle,
    IN EFI_STATUS ExitStatus,
    IN UINTN ExitDataSize,
    IN CHAR16 *ExitData OPTIONAL
)
{
    return g_SystemTable->BootServices->Exit(
        ImageHandle,
        ExitStatus,
        ExitDataSize,
        ExitData
    );
}

EFI_STATUS
EFIAPI
ExitBootServices(
    IN UINTN MapKey
)
{
    return g_SystemTable->BootServices->ExitBootServices(
        g_ImageHandle,
        MapKey
    );
}

// Video API

EFI_STATUS
OutputString(
    IN CHAR16 *String
)
{
    if (!g_SystemTable->ConOut || !g_SystemTable->ConOut->OutputString)
        return EFI_NOT_FOUND;

    EFI_STATUS ret = g_SystemTable->ConOut->OutputString(g_SystemTable->ConOut, String);

    if (ret != EFI_SUCCESS)
        g_SystemTable->ConOut->OutputString(g_SystemTable->ConOut, TEXT("\r\nAn error occurred.\r\n\r\n"));

    return ret;
}

// Protocols - Media Access

// Simple File System Protocol
EFI_STATUS
EFIAPI
OpenVolume(
    IN EFI_SIMPLE_FILE_SYSTEM_PROTOCOL *This,
    OUT EFI_FILE_PROTOCOL **Root
)
{
    return EFI_SUCCESS;
}

EFI_STATUS
EFIAPI
OpenSystemVolume (
    OUT EFI_SIMPLE_FILE_SYSTEM_PROTOCOL **pSFSP,
    OUT EFI_FILE_PROTOCOL **ppSystemVolume
)
{
    EFI_STATUS e;
    EFI_SIMPLE_FILE_SYSTEM_PROTOCOL *SimpleFileSystemProtocol;

    *pSFSP = nullptr;
    *ppSystemVolume = nullptr;
    e = OpenProtocol(g_LoadedImage->DeviceHandle, &SimpleFileSystemProtocolGuid,
        (void **)&SimpleFileSystemProtocol, g_ImageHandle, NULL, EFI_OPEN_PROTOCOL_EXCLUSIVE);
    if (EFI_ERROR(e)) return e;
    e = SimpleFileSystemProtocol->OpenVolume(SimpleFileSystemProtocol, ppSystemVolume);
    *pSFSP = SimpleFileSystemProtocol;
    return e;
}

void kExitBootServices() {
    UINTN EfiMapSize;
    EFI_MEMORY_DESCRIPTOR *EfiMap;
    UINTN DescriptorSize;
    UINT32 DescriptorVersion;
    UINTN MapKey;

    EfiMapSize = 0;
    GetMemoryMap(&EfiMapSize, NULL, &MapKey, &DescriptorSize, &DescriptorVersion);
    EfiMapSize = EfiMapSize * 2 + sizeof(EFI_MEMORY_DESCRIPTOR) * 10; // 그냥 넉넉하게...

    kMalloc(EfiMapSize, (void **)&EfiMap);
    kMemSet(EfiMap, 0, EfiMapSize);
    GetMemoryMap(&EfiMapSize, EfiMap, &MapKey, &DescriptorSize, &DescriptorVersion);

    ExitBootServices(MapKey);
}

