﻿
#include "stdafx.h"
#include "UefiApiWrapper.h"
#include "GlobalVariable.h"
#include "StdLib.h"
#include "Utility.h"
#include "PeLoader.h"
#include "MemoryMap.h"
#include "Graphics.h"
#include "JumpToThunk.h"
#include "main.h"


static void *pLoadedKernel;
static void *KernelStack;
static UINTN KernelEntryDataPtr;
static UINTN KernelVirtualBase;
static UINTN StackVirtualBase;
static UINTN KernelEntryDataVirtualBase;
static UINTN *PageTableBase;

EFI_STATUS kReadFileFromSystemVolume(CHAR16 *path, void **pAllocatedBuf, UINTN *szRead) {
    EFI_STATUS e;
    EFI_SIMPLE_FILE_SYSTEM_PROTOCOL *pSFSP;
    EFI_FILE_PROTOCOL *pSystemVolume, *pFile;
    e = OpenSystemVolume(&pSFSP, &pSystemVolume);
    kEfiPrintErrorFunc(TEXT("OpenSystemVolume: "), e);
    if (EFI_ERROR(e)) return e;
    e = pSystemVolume->Open(pSystemVolume, &pFile, path, EFI_FILE_MODE_READ, 0);
    kEfiPrintErrorFunc(TEXT("File open: "), e);
    if (EFI_ERROR(e)) return e;
    e = kReadFileIntoMemory(pFile, pAllocatedBuf, szRead);
    kEfiPrintErrorFunc(TEXT("kReadFileIntoMemory: "), e);
    if (EFI_ERROR(e)) return e;
    pFile->Close(pFile);
    pSystemVolume->Close(pSystemVolume);
    CloseProtocol(g_LoadedImage->DeviceHandle, &SimpleFileSystemProtocolGuid, g_ImageHandle, NULL);
    return EFI_SUCCESS;
}

void kLocateACPITable(K_KERNEL_ENTRY_DATA *Data) {
    Data->AcpiDesc.Available = 0;
    const EFI_GUID AcpiTableGuid = { 0x8868e871,0xe4f1,0x11d3,0xbc,0x22,0x0,0x80,0xc7,0x3c,0x88,0x81 };
    for (UINTN i = 0; i < g_SystemTable->NumberOfTableEntries; i++) {
        EFI_GUID &GuidValue = g_SystemTable->ConfigurationTable[i].VendorGuid;
        if (!kMemCmp(&AcpiTableGuid, &GuidValue, sizeof(EFI_GUID))) {
            Data->AcpiDesc.Available = 1;
            Data->AcpiDesc.RsdpAddr = (UINTN)g_SystemTable->ConfigurationTable[i].VendorTable;
            kPrintf(TEXT("Got ACPI RSDP address at %llX\r\n"), Data->AcpiDesc.RsdpAddr);
            break;
        }
    }
    if (!Data->AcpiDesc.Available) {
        kPrintf(TEXT("Couldn't find ACPI table\r\n"));
    }
}
EFI_STATUS kBuildPageTable() {
    UINTN Pre_MemoryMapCount;
    K_MEMORY_MAP *Pre_MemoryMap;
    kPrintf(TEXT("Building physical memory pre-map...\r\n"));
    kAllocateAndBuildMemoryMap(&Pre_MemoryMapCount, &Pre_MemoryMap);
    kPrintf(TEXT("Built physical memory pre-map, count = %llu\r\n"), Pre_MemoryMapCount);

    // Estimate size of memory needed for temporary page table
    UINTN NumberOf4KPages = 0;
    UINTN NumberOfRuntimePages = 0;
    for (UINTN i = 0; i < Pre_MemoryMapCount; i++) {
        K_MEMORY_MAP &map = Pre_MemoryMap[i];
        switch (map.Type) {
        case K_MEMORY_FW_RUNTIME:
            NumberOfRuntimePages += map.Size / EFI_PAGE_SIZE;
        case K_MEMORY_KERNEL_AREA:
            NumberOf4KPages += map.Size / EFI_PAGE_SIZE;
            break;
        }
    }

    NumberOf4KPages += Pre_MemoryMapCount * sizeof(K_MEMORY_MAP) / 4096 * 2 + 10;
    if (NumberOf4KPages > 16777216) {
        kPrintf(TEXT("Error: %llu 4K pages (kernel area + firmware runtime area) can't be fit within 64GB\r\n"), NumberOf4KPages);
        return EFI_LOAD_ERROR;
    }
    else {
        kPrintf(TEXT("Total page count: %llu, runtime area: %llu\r\n"),
            NumberOf4KPages, NumberOfRuntimePages);
    }

    // Build temporary page table
    UINTN NumberOf2MPtrs = (NumberOf4KPages + 511) / 512;
    UINTN NumberOfEntryPages = 2 + 1 + 1 + 64 + 64 + NumberOf2MPtrs + NumberOf4KPages;
    UINTN *PageTable;
    EFI_STATUS e = AllocateKernelArea(NumberOfEntryPages * 4096, (void **)&PageTable);
    if (EFI_ERROR(e)) {
        kPrintf(TEXT("Failed to allocate memory for kernel page table\r\n"));
        return e;
    }
    // Step 0: Lower 2MB
    // Step 0a: PDP table
    kMemSet(PageTable, 0, 4096);
    PageTable[0] = (UINTN)(&PageTable[512]) | 0x13; // PCD=1, RW=1, P=1
    PageTable += 512;
    // Step 0b: PD table (2MB identity mapping)
    kMemSet(PageTable, 0, 4096);
    PageTable[0] = 0x0 | 0x93; // PS=1, PCD=1, RW=1, P=1
    PageTable += 512;
    // Step 1: PML4 table
    PageTableBase = PageTable;
    kMemSet(PageTable, 0, 4096);
    PageTable[0] = (UINTN)(&PageTable[-1024]) | 0x13; // PCD=1, RW=1, P=1
    PageTable[511] = (UINTN)(&PageTable[512]) | 0x13; // PCD=1, RW=1, P=1
    PageTable += 512;
    // Step 2: PDP table
    kMemSet(PageTable, 0, 4096);
    for (UINTN i = 384; i < 512; i++) {
        PageTable[i] = (UINTN)(&PageTable[512 + (i - 384) * 512]) | 0x13; // PCD=1, RW=1, P=1
    }
    PageTable += 512;
    // Step 3: PD table
    // Step 3a: Identity-mapped 64G region
    for (UINTN i = 0; i < 64; i++) {
        for (UINTN j = 0; j < 512; j++) {
            static const UINTN OneGiga = 1 << 30;
            static const UINTN TwoMega = 1 << 21;
            PageTable[j] = (i * OneGiga + j * TwoMega) | 0x93; // PS=1, PCD=1, RW=1, P=1
        }
        PageTable += 512;
    }
    // Step 3b: kernel & runtime region
    kMemSet(PageTable, 0, 4096 * 64);
    for (UINTN i = 0; i < NumberOf2MPtrs; i++) {
        UINTN NextBase = (UINTN)(&PageTable[512 * 64 + i]);
        PageTable[i] = NextBase | 0x13; // PCD=1, RW=1, P=1
    }
    PageTable += 512 * 64;
    // Step 4: PT table for kernel & runtime region
    KernelVirtualBase = 0;
    StackVirtualBase = 0;
    KernelEntryDataVirtualBase = 0;
    for (UINTN i = 0, j = 0; j < Pre_MemoryMapCount; j++) {
        K_MEMORY_MAP &map = Pre_MemoryMap[j];
        if (map.Type == K_MEMORY_FW_RUNTIME || map.Type == K_MEMORY_KERNEL_AREA) {
            for (UINTN p = 0; p < map.Size; p += 4096) {
                UINTN PhysicalAddress = map.StartingAddr + p;
                UINTN VirtualAddress = K_KERNEL_UPPER_64G + (i * 4096);
                if (PhysicalAddress == (UINTN)pLoadedKernel) {
                    KernelVirtualBase = VirtualAddress;
                }
                else if (PhysicalAddress == (UINTN)KernelStack) {
                    StackVirtualBase = VirtualAddress;
                }
                else if (PhysicalAddress == KernelEntryDataPtr) {
                    KernelEntryDataVirtualBase = VirtualAddress;
                }
                PageTable[i++] = PhysicalAddress | 0x13; // PCD=1, RW=1, P=1
            }
        }
    }
    for (UINTN i = NumberOf4KPages; (i & 511) != 0; i++) {
        PageTable[i] = 0;
    }

    if (KernelVirtualBase == 0 || StackVirtualBase == 0 || KernelEntryDataVirtualBase == 0) {
        kPrintf(TEXT("Error: couldn't map kernel and/or associated data onto upper 64G\r\n"));
        return EFI_LOAD_ERROR;
    }
    kPrintf(TEXT("Built kernel page table\r\n"));
    kFree(Pre_MemoryMap);

    return EFI_SUCCESS;
}

void kJumpToKernel(K_KERNEL_ENTRY_DATA *Data) {
    // Jump to thunk
    // rcx : Kernel entry data (virtual)
    // rdx : Kernel entrypoint base address (virtual)
    // r8  : Kernel stack top (virtual)
    // r9  : PML4 base address (physical)
    // 0:  41 0f 22 d9             mov    cr3, r9
    // 4:  49 83 e8 08             sub    r8,  0x8
    // 8:  49 83 e0 f8             and    r8,  0xfffffffffffffff8
    // c : 4c 89 c4                mov    rsp, r8
    // f : ff e2                   jmp    rdx
    BYTE thunk_code[] = { 0x41,0x0f,0x22,0xd9,0x49,0x83,0xe8,0x08,0x49,0x83,0xe0,0xf8,0x4c,0x89,0xc4,0xff,0xe2 };
    UINTN thunk_addr = 0x7C00;
    kMemCpy((void *)thunk_addr, thunk_code, sizeof(thunk_code));

    kJumpToThunk(KernelEntryDataVirtualBase, Data->Kernel_EntryPoint,
        Data->Kernel_Stack_Bottom + Data->Kernel_Stack_Size - 32, (UINTN)PageTableBase); // never returns
}

void kLaunchKernel() {
    EFI_STATUS e;

    void *pKernel;
    UINTN szKernel;
    e = kReadFileFromSystemVolume(TEXT("\\EFI\\BOOT\\mkernel"), &pKernel, &szKernel);
    kEfiPrintErrorFunc(TEXT("kReadFileFromSystemVolume: "), e);

    if (EFI_ERROR(e)) {
        kPrintf(TEXT("Error: Failed to read mkernel\r\n"));
        return;
    }
    UINTN szLoadedKernel;
    e = kPeCalcLoadedSize(pKernel, szKernel, &szLoadedKernel);
    kEfiPrintErrorFunc(TEXT("kPeCalcLoadedSize: "), e);
    kPrintf(TEXT("szLoadedKernel = %lld\r\n"), szLoadedKernel);
    kGetCh();

    AllocateKernelArea(szLoadedKernel, &pLoadedKernel);

    void *pFont;
    UINTN szFont;
    e = kReadFileFromSystemVolume(TEXT("\\EFI\\BOOT\\FONTDATA.PSF"), &pFont, &szFont);
    if (EFI_ERROR(e)) {
        kPrintf(TEXT("Error: Failed to read FONTDATA.PSF\r\n"));
        return;
    }

    UINTN RamdiskSize = K_RAMDISK_HEADER_SIZE + sizeof(K_RAMDISK_FILE) + szFont;
    UINTN FwSpecificSize = sizeof(K_FWSPECIFIC_EFI);
    UINTN KernelEntryDataSize = sizeof(K_KERNEL_ENTRY_DATA) + RamdiskSize + FwSpecificSize;
    AllocateKernelArea(KernelEntryDataSize, (void **)&KernelEntryDataPtr);

    K_RAMDISK *Ramdisk = (K_RAMDISK *)(KernelEntryDataPtr + sizeof(K_KERNEL_ENTRY_DATA));
    Ramdisk->FileCount = 1;
    K_RAMDISK_FILE *pFontFile = (K_RAMDISK_FILE *)((UINTN)Ramdisk + K_RAMDISK_HEADER_SIZE);
    pFontFile->Size = szFont;
    kMemCpy(pFontFile->FileName, TEXT("FONTFILE"), 16);
    pFontFile->DataOffset = K_RAMDISK_HEADER_SIZE + sizeof(K_RAMDISK_FILE);
    kMemCpy((void *)(((UINTN)Ramdisk) + pFontFile->DataOffset), pFont, szFont);
    kPrintf(TEXT("Allocated ramdisk for boot font, size = %llu\r\n"), szFont);

    UINTN KernelStackSize = 6 * EFI_PAGE_SIZE;
    AllocateKernelArea(KernelStackSize, (void **)&KernelStack);
    kPrintf(TEXT("Allocated kernel stack, size = %llu\r\n"), KernelStackSize);

    K_KERNEL_ENTRY_DATA *Data = (K_KERNEL_ENTRY_DATA *)KernelEntryDataPtr;
    Data->Size = sizeof(K_KERNEL_ENTRY_DATA);
    Data->Kernel_Stack_Size = KernelStackSize;
    Data->KernelImageSize = szLoadedKernel;
    Data->RamdiskOffset = sizeof(K_KERNEL_ENTRY_DATA);

    kLocateACPITable(Data);

    Data->FwType = FW_TYPE_EFI;
    UINTN FwSpecificOffset = sizeof(K_KERNEL_ENTRY_DATA) + RamdiskSize;
    K_FWSPECIFIC_EFI *FwEfi = (K_FWSPECIFIC_EFI *)(KernelEntryDataPtr + FwSpecificOffset);
    FwEfi->Size = sizeof(K_FWSPECIFIC_EFI);
    FwEfi->pEfiSystemTable = g_SystemTable;
    Data->FwSpecificOffset = FwSpecificOffset;

    if (EFI_ERROR(e = kBuildPageTable())) {
        kPrintf(TEXT("Failed to build page table\r\n"));
        return;
    }

    UINTN EntryPointOffset;
    e = kLoadPe(pKernel, szKernel, pLoadedKernel, szLoadedKernel, KernelVirtualBase, &EntryPointOffset);
    kEfiPrintErrorFunc(TEXT("kLoadPe: "), e);

    Data->Kernel_Addr = KernelVirtualBase;
    Data->Kernel_EntryPoint = KernelVirtualBase + EntryPointOffset;
    Data->Kernel_Stack_Bottom = StackVirtualBase;

    kPrintf(TEXT("Kernel loaded at: phys 0x%llX, virt 0x%llX\r\n"), pLoadedKernel, KernelVirtualBase);
    kPrintf(TEXT("Entrypoint address: virt 0x%llX\r\n"), Data->Kernel_EntryPoint);

    // MemoryMapPtr is guaranteed to be below 64G
    UINTN MemoryMapPtr;
    kAllocateAndBuildMemoryMap(&Data->MemoryMapCount, (K_MEMORY_MAP **)&MemoryMapPtr);
    Data->MemoryMap = (K_MEMORY_MAP *)(0xFFFFFFE000000000ULL + MemoryMapPtr);
    Data->GraphicsModeDesc = kSetupGraphics();
    kExitBootServices();

    kJumpToKernel(Data);
}

EFI_STATUS kMain() {
    OutputString(TEXT("AhnOSLdr v1.0\r\nWritten by B.K.Ahn\r\nCopyright (C) 2013-PRESENT B.K.Ahn\r\n\r\n"));
    kPrintMemoryMap();
    InitKernelAreaAllocator();
    kLaunchKernel();
    return EFI_LOAD_ERROR;     // will not reached in normal cases
}
