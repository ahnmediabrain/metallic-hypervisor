﻿
#pragma once

extern EFI_HANDLE g_ImageHandle;
extern EFI_SYSTEM_TABLE *g_SystemTable;
extern EFI_LOADED_IMAGE_PROTOCOL *g_LoadedImage;

extern EFI_GUID LoadedImageProtocolGuid;
extern EFI_GUID DevicePathProtocolGuid;
extern EFI_GUID SimpleFileSystemProtocolGuid;
extern EFI_GUID FileInfoGuid;
extern EFI_GUID GraphicsOutputProtocolGuid;