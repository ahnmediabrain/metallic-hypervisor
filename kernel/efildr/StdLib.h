﻿
#pragma once

/* Print string */

void __cdecl kPrintf (const CHAR16 *FormatString, ...);
void __cdecl kSPrintf (CHAR16 *OutputBuffer, UINTN *OutputBufferSize, const CHAR16 *FormatString, ...);
void __cdecl kVSPrintf (CHAR16 *OutputBuffer, UINTN *OutputBufferSize, const CHAR16 *FormatString, VA_LIST ap);

/* String/Memory copy/set/move/len/compare */

void kStrCpy (CHAR16 *Destination, const CHAR16 *Source);
UINTN kStrLen (const CHAR16 *String);
INTN kStrCmp (const CHAR16 *String1, const CHAR16 *String2);
void kStrInv (CHAR16 *String);

void kMemSet (void *Mem, BYTE Value, K_SIZE Size);
void kMemCpy (void *Destination, const void *Source, K_SIZE Num);
INTN kMemCmp (const void *Mem1, const void *Mem2, K_SIZE Size);

/* Integer <--> String conversion */

INT64 kAToI64 (const CHAR16 *String, UINT64 Scale);
UINT64 kAToU64 (const CHAR16 *String, UINT64 Scale);
void kIToA64 (CHAR16 *Buffer, INT64 iValue, UINT64 Scale, BOOL bUpper, UINT64 iWidth = 0);
void kUToA64 (CHAR16 *Buffer, UINT64 iValue, UINT64 Scale, BOOL bUpper, UINT64 iWidth = 0);

/* Sorting */

void kSort (void *Base, K_SIZE Num, K_SIZE Size, bool (*CompFunc)(const void *, const void *));

/* File I/O */
EFI_STATUS kReadFileIntoMemory(EFI_FILE_PROTOCOL *File, void **ppBuffer, UINTN *pszFile);

/* Miscellaneous */

BOOL kIsPowerOfTwo64(UINT64 Num);
UINT64 kGetMaxBit(UINT64 Num);
UINT64 kGetMinBit(UINT64 Num);
