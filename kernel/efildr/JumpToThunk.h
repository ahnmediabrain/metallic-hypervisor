﻿
#pragma once

extern "C" {
    void kJumpToThunk(UINTN EntryDataPtr, UINTN EntryPoint, UINTN StackTop, UINTN PML4Base);
}
