﻿
#include "stdafx.h"
#include "GlobalVariable.h"
#include "UefiApiWrapper.h"
#include "StdLib.h"
#include "Utility.h"
#include "main.h"

EFI_STATUS UefiEntryPoint(EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable)
{
    EFI_STATUS EfiStatus;

    /* 전역 변수 초기화 */

    // ImageHandle, SystemTable
    g_ImageHandle = ImageHandle;
    g_SystemTable = SystemTable;

    /* 디버깅 용도 */

    EfiStatus = OpenProtocol(g_ImageHandle, &LoadedImageProtocolGuid, (void **)&g_LoadedImage,
        g_ImageHandle, NULL, EFI_OPEN_PROTOCOL_EXCLUSIVE);

    kEfiPrintErrorFunc(TEXT("1: "), EfiStatus);

    kPrintf(TEXT("Image base: 0x%llX\r\n"), g_LoadedImage->ImageBase);

    /* should not return back if initialization succeedes */
    return kMain();
}
