﻿
#pragma once

CHAR16 kGetCh();

// 8-character desc string if known type; otherwise nullptr
const CHAR16 *kEfiMemoryTypeDesc(EFI_MEMORY_TYPE Type);

void kEfiPrintError(EFI_STATUS Code);
void kEfiPrintErrorFunc(const CHAR16 *String, EFI_STATUS Code);
EFI_STATUS kEfiWriteErrorFile(EFI_FILE_PROTOCOL *pVolume, const CHAR16 *pszString, const CHAR16 *pszFileName);