﻿
#pragma once


/* Flags for IRQ handler entry */

#define IRQ_FLAG_SHAREABLE          0x0
#define IRQ_FLAG_EXCLUSIVE          0x1
#define IRQ_FLAG_PARAM_NONE         (0b00 << 1)     // Handler gets nothing (nullptr)
#define IRQ_FLAG_PARAM_USERDATA     (0b01 << 1)     // Handler gets user param
#define IRQ_FLAG_PARAM_CONTEXT      (0b10 << 1)     // Handler gets Context*
#define IRQ_FLAG_PARAM_EXTENDED     (0b11 << 1)     // Handler gets IrqContext*
#define IRQ_FLAG_PARAM_MASK         (0b11 << 1)
#define IRQ_FLAG_USE_VECTOR         0x08            // Try to acquire specific vector
#define IRQ_FLAG_KERNEL             0x10
#define IRQ_FLAG_VECTOR_HEAD        0x20            // First vector of block allocation (ignored; only for internal use)

struct Context;

/* IrqContext:
 *   The IrqContext data structure for retrieving
 *   both the Context and the user data. Note that
 *   in order to retrieve the error code and/or the
 *   vector number, a handler must be registered
 *   with IRQ_FLAG_PARAM_EXTENDED.
 */
struct IrqContext {
    Context     *pContext;
    void        *UserData;
    UINT64      ErrorCode;
    UINT32      IntVec;
};

/* User IRQ handler prototype */
typedef void(*IrqCallback)(void *Param);

/* Interrupt re-routing callback.
 * The value supplied is guaranteed to be between
 * zero and kApicGetMaxIntRoutableApicID(), inclusive. */
typedef void(*IrqRerouter)(UINT32 TargetApicID);

/* Initializes IRQ handling subsystem's internal data structure */
void kInitializeIrqSubsystem();

/* kRegisterIrqCallback:
 *   블록 개수(IDTBlockCount)만큼 정렬해 할당하고, 핸들러(Handler)를 등록하고, IRQ 벡터 값을 반환한다(pIntVec).
 *   플래그(Flags)에는 해당 인터럽트 벡터가 공유되는 것이 납득할 만한지에 대한 정보가 저장된다.
 *   (EXCLUSIVE로 설정하면 블록 전체가 비공유로 할당되며, SHARED로 설정하면 하나 이상이 공유될 수 있음)
 *   커널 모드 할당이 아닌 경우, pIntVec에 nullptr을 전달할 수 있다. 그렇게 되면 인터럽트 벡터가 반환되지 않는다.
 *   인터럽트 리라우팅을 지원하지 않는 경우 Rerouter를 nullptr로 설정한다.
 *   커널 모드 할당의 경우 Rerouter 및 RerouterContext는 무시된다.
 *   만약 IDTBlockCount가 2의 거듭제곱 꼴이 아니거나 16보다 큰 경우, E_INVALID_PARAMETER가 반환된다.
 *   IDTBlockCount가 2보다 큰 경우, IRQ_FLAG_EXCLUSIVE 또는 IRQ_FLAG_SHAREABLE 플래그는 무시되며
 *   무조건 IRQ_FLAG_EXCLUSIVE로 처리된다.
 *   IDT 할당에 실패한 경우 E_OUTOFRESOURCE가 반환된다.
 *   할당에 성공한 경우 E_SUCCESS가 반환된다.이 경우 *pIntVec에 IRQ 벡터의 시작 번호가 저장된다.
 *   Flags에 IRQ_FLAG_USE_VECTOR를 설정한 경우, Head Vector로 *pIntVec을 시도하고 불가능한 경우
 *   E_OUTOFRESOURCE를 반환한다.
 * Note:
 *   커널 내부 함수는 인터럽트 번호 0-63 영역을 할당받을 수 있다. 외부 드라이버는 64-251만 가능하다.
 *   즉 kRegisterIrqCallback은 커널 내부에서만 활용되며 외부에 노출될 때는 별도의 인터페이스 함수를 통한다.
 *   커널 내부 함수는 pIntVec을 통해 할당받을 인터럽트 벡터를 명시하여야 한다.
 *   이 경우 IRQ_FLAG_EXCLUSIVE 또는 IRQ_FLAG_SHAREABLE 플래그는 무시되며 무조건 IRQ_FLAG_SHAREABLE로 처리된다.
 *   (cf. 커널 외부 호출은 pIntVec을 통해 Out만 수행)
 *   커널 내부 호출인 경우 이를 알리기 위해 Flags에 IRQ_FLAG_KERNEL을 명시하여야 한다.
 *   또한, 만약 [*pIntVec, *pIntVec + IDTBlockCount - 1]이 [0, 63]에 포함되지 않으면 E_INVALID_PARAMETER가 반환된다.
 *   같은 핸들러를 여러 번 등록하는 경우 서로 다른 엔트리로 취급되므로, 서로 다른 설정으로 등록할 수 있다.
 */
OS_STATUS kRegisterIrqCallback(IrqCallback Handler,
    UINT32 IDTBlockCount, UINT32 Flags, void *UserData, UINT32 *pIntVec,
    IrqRerouter Rerouter, void *RerouterContext);

/* kUnregisterIrqCallback:
:*   kRegisterIrqCallback으로 등록한 핸들러(Handler)를 해제한다.
 *   kRegisterIrqCallback 등록 시 반환되었던 인터럽트 번호(IntVec)를 알려주면
 *   빠르게 해제가 가능하다.
 *   만약 찾지 못했거나, 찾았지만 IntVec이 Head가 아니라면 E_NOTFOUND가 반환된다.
 *   만약 동일한 핸들러로 둘 이상의 인터럽트에 대해 Head로 등록되어 있으면
 *   둘 중 어느 것을 해제할 지가 모호하므로 E_UNSUPPORTED가 반환된다.
 *   해제에 성공하면 E_SUCCESS가 반환된다. 별도로 되돌려주는 값은 없다.
 *   UserData를 돌려받고 싶으면 돌려받을 공간(포인터)을 제공해야 한다.
 */
OS_STATUS kUnregisterIrqCallback(IrqCallback Handler, UINT32 IntVec = 0xFFFFFFFF, void **pUserData = nullptr);


/* Fixed interrupt vector allocations */

#define IRQ_LEGACY_BASE         32      // 32-47
#define IRQ_TIMER               48      // 48
#define IRQ_SCHED_SYNC          49      // 49
#define IRQ_VM_SYNC             50      // 50
