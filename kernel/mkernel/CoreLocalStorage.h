
#pragma once

struct alignas(4096) CoreLocalStorage {
    UINT64              StackSize;
    BYTE                Stack[24568];           // Stack must reside at the front of CLS
    alignas(4096) BYTE  VMXONRegion[4096];
    alignas(16) BYTE    GDT[64];
    alignas(16) BYTE    IDT[4112];              // IDT Entries (4,096 bytes) + IDTR (10 bytes) + Pad (6 bytes)
    alignas(16) BYTE    TSS[112];
    BYTE                IDTStack[8000];         // 8,000 bytes for alignment to 4K boundary
    BYTE                NMIStack[8192];
    BYTE                SchedulerData[4096];
};

extern "C" {
    // Sets the base address of core local storage.
    // Note that the supplied address must be 4KB-aligned.
    void kSetCoreLocalStorageBase(UINT64 Address, UINT64 Size);
    // Gets the address of core local storage of the current core.
    // Note that this must be extern "C" in order to allow it
    //   to be called from AP entrypoint thunk. (stack address)
    CoreLocalStorage *kGetCoreLocalStorage();
    UINTN kGetCoreLocalStorageBase();
    UINTN kGetCoreLocalStorageSize();
}
