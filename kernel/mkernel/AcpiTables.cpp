﻿
#include "stdafx.h"
#include "AcpiTables.h"

bool kAcpiCheckSignature(void *Target, const void *Source)
{
    BYTE *TargetPtr = (BYTE *)Target;
    const BYTE *SourcePtr = (BYTE *)Source;
    while (*SourcePtr) {
        if (*TargetPtr++ != *SourcePtr++)
            return false;
    }
    return true;
}

bool kAcpiValidateChecksum(void *Target, K_SIZE bytes)
{
    BYTE *TargetPtr = (BYTE *)Target;
    BYTE Checksum = 0;
    while (bytes-- > 0) Checksum += *TargetPtr++;
    return (Checksum == 0);
}

