
#pragma once

#include "SyncObjs.h"

// Buddy block allocator that works in O(lg n),
// where n is the number of pages managed.
// Note that you need to map the virtual memory
// in advance for BuddyAllocator::Bitmap and
// BuddyAllocator::FreeList storage.
// This allocator assumes page size of 4K.
struct BuddyAllocator {
public:
    static UINT64 const INVALID;            // Placeholder for invalid pointer.

public:
    K_SPINLOCK      Lock;                   // The synchronization object.
    UINT64          PhysicalStart;          // Physical address of managed memory.
    UINT64          NumberOfPagesLogCeil;   // ceil(lg(# of pages managed)).
    UINT64          TotalPages;             // # of pages managed.
    BuddyAllocator *Prev;                   // Virtual address of previous allocator. nullptr if none.
    BuddyAllocator *Next;                   // Virtual address of next allocator. nullptr if none.
    UINT64          FreeListHead[53];       // Level 0 corresponds to single page here. INVALID if not available.
    BYTE           *Bitmap;                 // Virtual address of the bitmap storage.
    UINT64         *FreeList;               // Virtual address of the free list storage (doubly linked).

public:
    // Initializes the buddy block allocation data structure.
    // Note: (1) the number of pages doesn't need to be a power of two.
    //       (2) current object base address must have at least the space reported by GetStorageRequired.
    void Install(UINT64 PhysicalStart, UINT64 Pages);
    // Allocates smallest block greater than or equal to specified size.
    // Returns physical address allocated.
    // Return value of BuddyAllocator::INVALID indicates failure.
    UINT64 TryAlloc(UINT64 Bytes, UINT64 *PagesAllocated);
    // Frees the memory allocated from this pool.
    // Returns the size of that memory block in pages.
    UINT64 Free(UINT64 PhysicalAddress);
    // Checks if a physical address is within the memory region
    // managed by current object.
    bool Owns(UINT64 PhysicalAddress);

public:
    // Returns required storage size in bytes.
    // Note that the storage size is guaranteed to be
    // at most max(4095, (1 << NumberOfPagesLogCeli) * 33)
    // when NumberOfPagesLogCeli >= 2.
    static UINT64 GetStorageRequired(UINT64 NumberOfPagesLogCeli);

private:
    // Allocates specified block of specified level,
    // assuming the block is currently free.
    UINT64 AllocInternal(UINT64 lv, UINT64 PageNum);
    // Frees specified block of specified level,
    // assuming the block is currently in allocated state.
    void FreeInternal(UINT64 lv, UINT64 PageNum);
    // Splits specified block into specified level,
    // assuming the block is currently free.
    void Split(UINT64 lv, UINT64 PageNum, UINT64 lv_target);
    // Merges specified block with adjacent blocks as much as
    // possible, assuming the block is currently free.
    void Merge(UINT64 lv, UINT64 PageNum);

private:
    void AddToFreeList(UINT64 lv, UINT64 PageNum);
    void RemoveFromFreeList(UINT64 lv, UINT64 PageNum);
    bool IsInUse(UINT64 lv, UINT64 PageNum);
    void SetUse(UINT64 lv, UINT64 PageNum, bool value);
    UINT64 &ListNext(UINT64 lv, UINT64 PageNum);
    UINT64 &ListPrev(UINT64 lv, UINT64 PageNum);
    UINT64 ItemIndex(UINT64 lv, UINT64 PageNum);
};
