_DATA SEGMENT
_DATA ENDS

_TEXT SEGMENT

PUBLIC kInterruptEntryPointThunkStart
PUBLIC kInterruptEntryPointThunkEnd

; See Interrupt.cpp for implementation
kInterruptHandler PROTO C

; Saves current context onto the stack.
; Note that this needs to be in accordance with the Context structure
;   defined in Context.h.
SAVECONTEXT MACRO
	sub rsp, 8		; pushfq
	sub rsp, 8		; push rip
	push r15
	push r14
	push r13
	push r12
	push r11
	push r10
	push r9
	push r8
	push rdi
	push rsi
	push rbp
	sub rsp, 8		; push rsp
	push rdx
	push rcx
	push rbx
	push rax
	mov rax, QWORD PTR [rsp+144+24]	; rflags
	mov QWORD PTR [rsp+136], rax
	mov rax, QWORD PTR [rsp+144+8]	; rip
	mov QWORD PTR [rsp+128], rax
	mov rax, QWORD PTR [rsp+144+32]	; rsp
	mov QWORD PTR [rsp+32], rax
ENDM

; Loads (restores) current context from the stack.
; Note that this needs to be in accordance with the Context structure
;   defined in Context.h.
LOADCONTEXT MACRO
	mov rax, QWORD PTR [rsp+32]		; rsp
	mov QWORD PTR [rsp+144+32], rax
	mov rax, QWORD PTR [rsp+128]	; rip
	mov QWORD PTR [rsp+144+8], rax
	mov rax, QWORD PTR [rsp+136]	; rflags
	mov QWORD PTR [rsp+144+24], rax
	pop rax
	pop rbx
	pop rcx
	pop rdx
	add rsp, 8		; pop rsp
	pop rbp
	pop rsi
	pop rdi
	pop r8
	pop r9
	pop r10
	pop r11
	pop r12
	pop r13
	pop r14
	pop r15
	add rsp, 8		; pop rip
	add rsp, 8		; popfq
ENDM

kInterruptEntryPoint PROC
	mov rdx, rsp				; The pointer to the saved context. Any modifications to
								; this context will result in (sort of) context switch.
	mov r8, QWORD PTR [rsp+144]	; The error code.
	sub rsp, 32					; Parameter homing space for Microsoft x64 calling convention.
	call kInterruptHandler		; The interrupt entrypoint thunk has already stored
								; the IRQ number to rcx.
	add rsp, 32
	LOADCONTEXT
	add rsp, 8					; Skip the error code.
	iretq
kInterruptEntryPoint ENDP


; Be careful to keep this piece of code
; emit constant-sized instruction.
; (e.g. using 'jmp label' is prone to variable-length encoding)
EmitInterruptEntryPointThunk MACRO IRQNumber
	LOCAL L1
	LOCAL L2
	LOCAL L3
	push rax
	mov rax, IRQNumber
	cmp rax, 7
	jle L2
	cmp rax, 9
	je L2
	cmp rax, 14
	jle L1
	cmp rax, 17
	je L1
	cmp rax, 18
	jne L2
L1:
	pop rax
	jmp L3
L2:
	mov rax, QWORD PTR [rsp]
	mov QWORD PTR [rsp], 0		; Put dummy error code.
L3:
	SAVECONTEXT
	mov rcx, IRQNumber
	mov rax, kInterruptEntryPoint
	jmp rax
ENDM

kInterruptEntryPointThunkStart:
Counter = 0
REPEAT 256
	EmitInterruptEntryPointThunk Counter
	Counter = Counter + 1
ENDM
kInterruptEntryPointThunkEnd:

kTestInterrupt PROC
	int 80h
	ret
kTestInterrupt ENDP

_TEXT ENDS

END
