
#pragma once

// calculates the size needed to host kernel
OS_STATUS kPeCalcLoadedSize(void *pFileInMemory, UINTN szFile, UINTN *pszLoadedImage);
// loads the kernel into the specified location
// note that this function assumes identity paging
OS_STATUS kLoadPe(void *pFileInMemory, UINTN szFile, void *pOutputBuffer, UINTN szOutputBuffer,
    UINTN TrueImageBase, UINTN *pEntryPointOffset);
