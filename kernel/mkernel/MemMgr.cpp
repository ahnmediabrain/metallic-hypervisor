
#include "stdafx.h"
#include "AsmUtils.h"
#include "Console.h"
#include "Debug.h"
#include "GlobalVariable.h"
#include "MemMgr.h"
#include "MemMgrBuddy.h"
#include "StdLib.h"
#include "SyncObjs.h"


static BuddyAllocator *s_AllocatorHead;
static MemoryStatus s_MemoryStatus;

void kMemMgrInit() {
    UINTN TotalPages = 0;
    UINTN Buddies = 0;

    UINTN MapCount = g_KernelEntryData->MemoryMapCount;
    kConsolePrintf(TEXT("Memory Map Count = %llu\r\n"), MapCount);
    BuddyAllocator *Prev = nullptr;
    for (UINTN i = 0; i < MapCount; i++) {
        K_MEMORY_MAP map = g_KernelEntryData->MemoryMap[i];
        if (map.Type == K_MEMORY_AVAILABLE && map.Size > 0) {
            /* Skip Low 1MB */
            if (map.StartingAddr < 0x100000) {
                if (map.StartingAddr + map.Size <= 0x100000) {
                    kConsolePrintf(TEXT("%llu(NOALLOC) "), i);
                    continue;
                }
                map.Size = map.StartingAddr + map.Size - 0x100000;
                map.StartingAddr = 0x100000;
            }

            /* Actual allocation */
            UINTN PhysicalPages = map.Size / 4096;
            UINTN LogCeli = kHighestBitSet(PhysicalPages - 1) + 1; // celing function
            UINTN RequiredBytes = BuddyAllocator::GetStorageRequired(LogCeli);
            UINTN RequiredPages = (RequiredBytes + 4095) / 4096;
            UINTN PagesAllocated = PhysicalPages - RequiredPages;
            if (PagesAllocated < 1) {
                kConsolePrintf(TEXT("%llu(NOALLOC) "), i);
                continue;
            }
            BuddyAllocator *Allocator = (BuddyAllocator *)
                kMapPhysicalMemory((void *)(map.StartingAddr + map.Size - RequiredBytes), RequiredBytes);
            Allocator->Install(map.StartingAddr, PagesAllocated);
            Allocator->Prev = Prev;
            if (Prev != nullptr) Prev->Next = Allocator;
            if (Prev == nullptr) s_AllocatorHead = Allocator;
            Prev = Allocator;
            TotalPages += PagesAllocated;
            Buddies += 1;
            kConsolePrintf(TEXT("%llu(ALLOC) "), i);
        }
        else {
            kConsolePrintf(TEXT("%llu(NOALLOC) "), i);
        }
    }
    kConsolePrintf(TEXT("\r\n"));
    if (TotalPages < 2048) {
        kConsolePrintf(TEXT("Error: insufficient memory pool (%llu bytes). Minimum 8 MiB required.\r\n"), TotalPages * 4096);
        kHang();
    }
    else {
        kConsolePrintf(TEXT("Memory pool has total of %llu pages\r\n"), TotalPages);
    }
    Prev->Next = nullptr;

    s_MemoryStatus.TotalPages = TotalPages;
    s_MemoryStatus.AllocatedPages = 0;
    s_MemoryStatus.AvailablePages = TotalPages;
    s_MemoryStatus.Buddies = Buddies;
}

void *kAllocPhysicalPages(UINTN Pages) {
    UINTN Bytes = Pages * 4096;
    BuddyAllocator *Allocator = s_AllocatorHead;
    while (Allocator != nullptr) {
        UINTN PagesAllocated;
        UINTN ptr = Allocator->TryAlloc(Bytes, &PagesAllocated);
        if (ptr != BuddyAllocator::INVALID) {
            s_MemoryStatus.AllocatedPages += PagesAllocated;
            s_MemoryStatus.AvailablePages -= PagesAllocated;
            return (void *)ptr;
        }
        Allocator = Allocator->Next;
    }
    kConsolePrintf(TEXT("Bug: Failed to allocate %llu pages\r\n"), Pages);
    //kHang();
    return nullptr;
}
void kFreePhysicalPages(UINTN PhysicalAddress) {
    BuddyAllocator *Allocator = s_AllocatorHead;
    while (Allocator != nullptr) {
        if (Allocator->Owns(PhysicalAddress)) {
            UINT64 Pages = Allocator->Free(PhysicalAddress);
            s_MemoryStatus.AllocatedPages -= Pages;
            s_MemoryStatus.AvailablePages += Pages;
            break;
        }
        Allocator = Allocator->Next;
    }
    if (Allocator == nullptr) {
        kConsolePrintf(TEXT("Bug: tried to free unallocated or invalid physical memory at %p\r\n"), PhysicalAddress);
        kHang();
    }
}

void *kAllocPages(UINTN Pages) {
    return kAllocContiguousPages(Pages);
}

void *kAllocContiguousPages(UINTN Pages) {
    void *PhysicalAddress = kAllocPhysicalPages(Pages);
    return kMapPhysicalMemory(PhysicalAddress, Pages * 4096);
}

void kFreePages(void *Address) {
    UINTN PhysicalAddress = kUnmapPhysicalMemory(Address);
    kFreePhysicalPages(PhysicalAddress);
}

void *kMalloc(UINTN Bytes) {
    kAssert(Bytes > 0);
    // FIXME: fix this to use some kind of specialized storage
    //        for handling small-sized allocation requests
    UINTN Pages = (Bytes + 4095) / 4096;
    return kAllocPages(Pages);
}

void kFree(void *Address) {
    kFreePages(Address);
}

void *kMapPhysicalMemory(void *PhysicalAddress, UINTN Bytes) {
    UINTN AddressValue = (UINTN)PhysicalAddress;
    if ((AddressValue + Bytes) < AddressValue) {
        return nullptr;
    }
    if (AddressValue < (64ULL << 30) &&
        (AddressValue + Bytes) <= (64ULL << 30)) {
        return (void *)((-128ULL << 30) + AddressValue);
    }
    else {
        // not implemented error
        kConsolePrintf(TEXT("Bug: mapping of physical memory above 64G is not implemented\r\n"));
        kHang();
    }
    return nullptr;
}

UINTN kUnmapPhysicalMemory(void *VirtualAddress) {
    UINTN AddressValue = (UINTN)VirtualAddress;
    if (AddressValue >= (-128ULL << 30) && AddressValue < (-64ULL << 30)) {
        // nothing to do
        return (AddressValue - (-128ULL << 30));
    }
    else {
        // not implemented error
        kConsolePrintf(TEXT("Bug: (un)mapping of %p (physical memory above 64G) is not implemented\r\n"), VirtualAddress);
        kHang();
        return 0;
    }
}

UINTN kVirt2Phys(UINTN VirtualAddress)
{
    // FIXME: do a look-up on page table
    if (VirtualAddress >= (-128ULL << 30) && VirtualAddress < (-64ULL << 30)) {
        // nothing to do
        return (VirtualAddress - (-128ULL << 30));
    }
    else {
        // not implemented error
        kConsolePrintf(TEXT("Bug: (un)mapping of %p (physical memory above 64G) is not implemented\r\n"), VirtualAddress);
        kHang();
        return INVALID_MEMORY_ADDRESS;
    }

}

void kGetMemoryStatus(MemoryStatus *pMemoryStatus) {
    kMemCpy(pMemoryStatus, &s_MemoryStatus, sizeof(MemoryStatus));
}

void kRunMemoryAllocationTest() {
    void *D = kAllocPages(35);
    kFreePages(D);

    BYTE **A = (BYTE **)kAllocPages(2);
    BYTE **B = A;
    BYTE *C;

    kConsolePrintf(TEXT("%p "), A);
    for (UINTN i = 0; i < 256; i++) {
        C = (BYTE *)kAllocPages(1);
        kMemSet(C, 8, 4096);
        kConsolePrintf(TEXT("%p "), C);
        *B++ = C;
        C = (BYTE *)kAllocPages(4);
        kMemSet(C, 2, 16384);
        *B++ = C;
        kConsolePrintf(TEXT("%p "), C);
        C = (BYTE *)kAllocPages(2);
        kMemSet(C, 4, 8192);
        *B++ = C;
        kConsolePrintf(TEXT("%p "), C);
        C = (BYTE *)kAllocPages(8);
        kMemSet(C, 1, 32768);
        *B++ = C;
        kConsolePrintf(TEXT("%p "), C);
    }

    for (UINTN i = 0; i < 1024; i++) {
        BYTE *p = A[i];
        kConsolePrintf(TEXT("[%llu, %p] "), i, p);
        UINTN sum = 0;
        UINTN u = 0;
        switch (i % 4) {
        case 0:u = 4096; break;
        case 1:u = 16384; break;
        case 2:u = 8192; break;
        case 3:u = 32768; break;
        }
        for (UINTN j = 0; j < u; j++) {
            sum += p[j];
        }
        kAssert(sum == 32768);
    }
    kConsolePrintf(TEXT("\r\nVerified summation\r\n"));
    for (UINTN i = 0; i < 1024; i++) {
        kFreePages((void *)A[i]);
        kConsolePrintf(TEXT("%llu "), i);
    }

    kFreePages(A);
    kConsolePrintf(TEXT("\r\n"));
}
