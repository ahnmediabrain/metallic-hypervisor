
#include "stdafx.h"
#include "hw/apic/Apic.h"
#include "AsmUtils.h"
#include "Console.h"
#include "CoreLocalStorage.h"
#include "Debug.h"
#include "StdLib.h"

#include "Descriptor.h"


//******** GDT ********//

#pragma pack(push, 1)
struct GDTEntry8 {
    UINT16          SegLimit1;      // Unused in 64bit
    UINT16          Base1;          // Unused in 64bit
    UINT8           Base2;          // Unused in 64bit
    UINT8           Flags1;         // Set P=1, DPL=0
    UINT8           Flags2;         // Set G=1
    UINT8           Base3;          // Unused in 64bit
};
struct GDTEntry16 {
    UINT16          SegLimit1;      // TSS size in bytes
    UINT16          Base1;          // TSS base 15:00
    UINT8           Base2;          // TSS base 23:16
    UINT8           Flags1;         // Set P=1, DPL=0
    UINT8           Flags2;         // Set G=0
    UINT8           Base3;          // TSS base 31:24
    UINT32          Base4;          // TSS base 63:32
    UINT32          Reserved;       // Must be zero
};
struct GDTR {
    WORD            GDTSizeMinusOne;
    UINT64          GDTAddress;
};
#pragma pack(pop)

/*

.NULLDESC:
dq 0x00

.GDT_CODEDESC64:      ; 0x08
dw 0xFFFF         ; SegLimit 15:00
dw 0x0000         ; Base     15:00
db 0x00           ; Base     23:16
db 0x9A           ; P, DPL, S, Type=0xA
db 0xAF           ; G=1, D/B=0, L=1, AVL=0, SegLimit 19:16
db 0x00           ; Base     31:24

.GDT_DATADESC64:      ; 0x10
dw 0xFFFF         ; SegLimit 15:00
dw 0x0000         ; Base     15:00
db 0x00           ; Base     23:16
db 0x92           ; P=1, DPL=0, S=0, Type=0x2
db 0xAF           ; G=1, D/B=0, L=1, AVL=0, SegLimit 19:16
db 0x00           ; Base     31:24

.GDT_TSSDESC64:       ; 0x18      
dw 0x0067         ; SegLimit 15:00
dw 0x0000         ; Base     15:00
db 0x00           ; Base     23:16
db 0x89           ; P=1, DPL=0, S=0, Type=0x9
db 0x00           ; G=0, D/B=0, AVL=0, SegLimit 19:16
db 0x00           ; Base     31:24
dd 0x00000000     ; Base     63:32
dd 0x00000000     ; Reserved

*/

static const GDTEntry8 GDTTemplate[] = {
    { 0x0000, 0x0000, 0x00, 0x00, 0x00, 0x00 }, // Null descriptor
    { 0xFFFF, 0x0000, 0x00, 0x9A, 0xAF, 0x00 }, // Code descriptor
    { 0xFFFF, 0x0000, 0x00, 0x92, 0xAF, 0x00 }, // Data descriptor
    { 0x0067, 0x0000, 0x00, 0x89, 0x00, 0x00 }, // TSS  descriptor 1
    { 0x0000, 0x0000, 0x00, 0x00, 0x00, 0x00 }, // TSS  descriptor 2
};


void kSetupGDT()
{
    kConsolePrintf(TEXT("Replacing GDT on Core %u\r\n"), kApicGetApicID());

    BYTE *LocalGDT = kGetCoreLocalStorage()->GDT;
    static_assert(sizeof(CoreLocalStorage::GDT) >= (sizeof(GDTTemplate) + sizeof(GDTR)), "Insufficient GDT storage");
    kAssert(((UINTN)LocalGDT) % 16 == 0);
    kMemCpy(LocalGDT, GDTTemplate, sizeof(GDTTemplate));

    GDTEntry16 *TSSEntry = (GDTEntry16 *)(LocalGDT + 0x18);
    UINT64 TSS = (UINT64)kGetCoreLocalStorage()->TSS;
    TSSEntry->Base1 = (UINT16)TSS;
    TSSEntry->Base2 = (UINT8)(TSS >> 16);
    TSSEntry->Base3 = (UINT8)(TSS >> 24);
    TSSEntry->Base4 = (UINT32)(TSS >> 32);

    GDTR *LocalGDTR = (GDTR *)(LocalGDT + sizeof(GDTTemplate));
    LocalGDTR->GDTSizeMinusOne = sizeof(GDTTemplate) - 1;
    LocalGDTR->GDTAddress = (UINT64)LocalGDT;

    kLoadGDTR(LocalGDTR, GDT_CS, GDT_DS);
}

//******** IDT & TSS ********//

#pragma pack (push, 1)

struct alignas(16) IDTEntry {
    UINT16      CodeOffset1;            // Offset (15:0)
    UINT16      CodeSegmentSelector;    // CS register
    BYTE        IST;                    // bit 2:0 = IST offset 
    BYTE        Flags;
    UINT16      CodeOffset2;            // Offset (31:16)
    UINT32      CodeOffset3;            // Offset (63:32)
    UINT32      Reserved;
};
struct IDTR {
    WORD        IDTSize;
    UINT64      IDTAddress;
};
struct IDTData {
    IDTEntry    Vector[256];
    IDTR        Base;
};

#pragma pack (pop)

extern "C" unsigned char kInterruptEntryPointThunkStart[];
extern "C" unsigned char kInterruptEntryPointThunkEnd[];

static void kSetupTSS() {
    auto pCLS = kGetCoreLocalStorage();
    BYTE *TSS = pCLS->TSS;

    kMemSet(TSS, 0, 104);
    *(QWORD *)(TSS + 36) = (QWORD)pCLS->IDTStack + sizeof(pCLS->IDTStack);  // IST1
    *(QWORD *)(TSS + 44) = (QWORD)pCLS->NMIStack + sizeof(pCLS->NMIStack);  // IST2

    // From the Intel manual (SDM Vol.1 Section 18.5.2):
    //  "If the I/O bit map base address is greater than or
    //   equal to the TSS segment limit, there is no I/O
    //   permission map, and all I/O instructions generate
    //   exceptions when the CPL is greater than the current IOPL."
    // Hence we need not provide the I/O bitmap at all.
    *(DWORD *)(TSS + 100) = 105;                    // Beyond TSS limit

    kLoadTR(GDT_TSS); // the entry at offset 0x18 in GDT
}
void kInstallIDT() {
    kSetupTSS();

    IDTData *pData = (IDTData *)kGetCoreLocalStorage()->IDT;
    UINT64 HandlerAddress = (UINT64)kInterruptEntryPointThunkStart;
    UINT64 HandlerThunkSize = (kInterruptEntryPointThunkEnd - kInterruptEntryPointThunkStart) / 256;
    kAssert((kInterruptEntryPointThunkEnd - kInterruptEntryPointThunkStart) % 256 == 0);
    kConsolePrintf(TEXT("IDT Handler Base Address = %p, Size = %llu\r\n"), HandlerAddress, HandlerThunkSize);
    for (UINTN i = 0; i < 256; i++) {
        IDTEntry *pEntry = &pData->Vector[i];
        pEntry->CodeSegmentSelector = GDT_CS;
        pEntry->CodeOffset1 = (UINT16)HandlerAddress;
        pEntry->CodeOffset2 = (UINT16)(HandlerAddress >> 16);
        pEntry->CodeOffset3 = (UINT32)(HandlerAddress >> 32);
        pEntry->IST = 1; // Switch stack
        pEntry->Flags = 0x8E; // P=0x1, DPL=0, S=0, Type=0xE (64-bit Interrupt Gate)
        pEntry->Reserved = 0;
        HandlerAddress += HandlerThunkSize;
    }
    IDTR *pIDTR = &pData->Base;
    pIDTR->IDTSize = sizeof(pData->Vector) - 1;
    pIDTR->IDTAddress = (UINT64)&pData->Vector;
    kLoadIDTR(pIDTR);
}
