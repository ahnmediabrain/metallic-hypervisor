﻿
#pragma once

#include "hw/apic/Apic.h"
#include "AcpiTables.h"
#include "Interrupt.h"


#define INTIN_POL_DEFAULT           (0b00 << 0)
#define INTIN_POL_ACTIVEHIGH        (0b01 << 0)
#define INTIN_POL_ACTIVELOW         (0b11 << 0)
#define INTIN_POL_MASK              (0b11 << 0)
#define INTIN_TRIGGER_DEFAULT       (0b00 << 2)
#define INTIN_TRIGGER_EDGE          (0b01 << 2)
#define INTIN_TRIGGER_LEVEL         (0b11 << 2)
#define INTIN_TRIGGER_MASK          (0b11 << 2)


void kInitializeAcpiSubsystem();

void kAcpiParse(UINTN RsdpAddr);
void kStartApplicationProcessors();

struct IOAPIC_INPUT_DESC;
OS_STATUS kAcpiIsaToIoApicInputDesc(UINT32 IsaPinNum, IOAPIC_INPUT_DESC *Desc);
OS_STATUS kAcpiGsiToIoApicInputDesc(UINT32 GsiNum, IOAPIC_INPUT_DESC *Desc);

/*
 * @param AllowShare : 
 */
OS_STATUS kAcpiRegisterGsiHandler(UINT32 GsiNum, IrqCallback Handler, bool AllowShare,
    bool IsLevelTrigger, bool IsActiveLow, void *UserData);
OS_STATUS kAcpiUnregisterGsiHandler(UINT32 GsiNum, IrqCallback Handler, void **pUserData = nullptr);

/* Only intended for use in PCI initialization code.
 * Other components must access through the PCI subsystem. */
AcpiMcfgEntry *kAcpiFindMcfgEntry(UINT16 PciSegGroupNum, UINT8 BusNum);

OS_STATUS kAcpiInitializeAcpica();
void kAcpiPowerOff();
