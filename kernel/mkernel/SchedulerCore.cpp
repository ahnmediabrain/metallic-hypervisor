
#include "stdafx.h"
#include "hw/apic/Apic.h"
#include "AsmUtils.h"
#include "Console.h"
#include "CoreLocalStorage.h"
#include "GlobalVariable.h"
#include "Debug.h"
#include "Interrupt.h"
#include "MemMgr.h"
#include "Time.h"
#include "Scheduler.h"
#include "SchedulerCore.h"
#include "StdLib.h"
#include "SyncObjs.h"
#include "ds/Queue.h"


static struct SchedGlobalData {
    K_SPINLOCK          Lock;
    Queue               RunQueue;
} s_SchedGlobalData;

static void IdleThread();
static Thread *ExtractFromRunQueue(); // Returns nullptr if no threads are available.
static void DestroyThreadImpl(Thread *pThread);

// The context switch callback for scheduler.
// The scheduler must save the context provided
// and load a new one for execution.
// After the callback returns, the context will be
// switched to the new one provided by the callback
// through the ISR context restoration routine.
static void SchedCallback(void *Data);

extern volatile UINTN g_ThreadIDCounter; /* defined on Scheduler.cpp */
void kInitializeScheduler() {
    UINT32 IntVec;

    kSpinlockInit(&s_SchedGlobalData.Lock);
    kInitQueue(&s_SchedGlobalData.RunQueue);

    IntVec = IRQ_TIMER;
    kRegisterIrqCallback(SchedCallback, 1, 
        IRQ_FLAG_KERNEL | IRQ_FLAG_PARAM_CONTEXT, nullptr, &IntVec, nullptr, nullptr);
    IntVec = IRQ_SCHED_SYNC;
    kRegisterIrqCallback(SchedCallback, 1,
        IRQ_FLAG_KERNEL | IRQ_FLAG_PARAM_CONTEXT, nullptr, &IntVec, nullptr, nullptr);

    g_ThreadIDCounter = 0;
}

void kStartScheduler() {
    static_assert(sizeof(CoreLocalStorage::SchedulerData) >= sizeof(SchedLocalData), "Insufficient storage for SchedLocalData");

    SchedLocalData *pLocalData = (SchedLocalData *)kGetCoreLocalStorage()->SchedulerData;
    pLocalData->CurrentThread = nullptr;
    kMemSet(&pLocalData->IdleThreadContext, 0xCD, sizeof(Context));
    pLocalData->IdleThreadContext.RFLAGS = 0x200; // Enable Interrupts
    pLocalData->IdleThreadContext.RIP = (QWORD)IdleThread;
    pLocalData->IdleThreadContext.RSP = (QWORD)(pLocalData->IdleThreadStack) + sizeof(pLocalData->IdleThreadStack) - 32;

    kApicTimerStart(1 + g_ApicTimerFreq * 30 / 1000, true, IRQ_TIMER);
    IdleThread();
}

void kAddToRunQueue(Thread *pThread) {
    kSpinlockAcquire(&s_SchedGlobalData.Lock);
    kEnqueue(&s_SchedGlobalData.RunQueue, (UINTN)pThread);
    //kConsolePrintf(TEXT("%lld threads waiting\r\n"), s_SchedGlobalData.RunQueue.ItemCount);
    kSpinlockRelease(&s_SchedGlobalData.Lock);
}
static Thread *ExtractFromRunQueue() {
    kSpinlockAcquire(&s_SchedGlobalData.Lock);
    Thread *pThread;
    if (!kDequeue(&s_SchedGlobalData.RunQueue, (UINTN *)&pThread)) {
        pThread = nullptr;
    }
    kSpinlockRelease(&s_SchedGlobalData.Lock);
    return pThread;
}

void kYield() {
    kGenerateInterrupt(IRQ_SCHED_SYNC);
}

void kSleep(UINT64 Milliseconds) {
    UINT64 StartTime = kGetTimeCounterValue();
    UINT64 Frequency = kGetTimeCounterFrequency();
    UINT64 WaitTicks = (Milliseconds * Frequency + 1000 - 1) / 1000;

    UINT64 ElapsedTicks;
    do {
        kYield();
        UINT64 CurrentTime = kGetTimeCounterValue();
        ElapsedTicks = (UINT64)(CurrentTime - StartTime);
    } while (ElapsedTicks < WaitTicks);
}

UINTN kIncrementThreadRefCount(Thread *pThread) {
    // No lock here since this call will never make the 
    // reference count reach zero.
    // (* if it reaches zero, the data structures needs to be freed)
    return kInterlockedIncrement64(&pThread->RefCount);
}

UINTN kDecrementThreadRefCount(Thread *pThread) {
    kSpinlockAcquire(&pThread->Lock);
    UINTN RefValue = kInterlockedDecrement64(&pThread->RefCount);
    if (RefValue == 0 && (pThread->Flags & THREAD_WAITING_OBJ_FREE)) {
        kSpinlockRelease(&pThread->Lock);
        kFree(pThread);
    }
    else {
        kSpinlockRelease(&pThread->Lock);
    }
    return RefValue;
}

static void IdleThread() {
    kConsolePrintf(TEXT("Idle thread on core %u\r\n"), kApicGetApicID());
    kEnableInterrupt(TRUE);
    while (1) {
        kHalt();
        //kConsolePrintf(TEXT("%u-%X-%llX "), kLocalApicReadReg32(LOCAL_APIC_TIMER_CURCOUNT), kLocalApicReadReg32(LOCAL_APIC_TIMER_LVT), kReadRFlags());
    }
}

static void DestroyThreadImpl(Thread *pThread) {
    // Free all memory except the structure itself
    // and set the free-pending flag.
    kSpinlockAcquire(&pThread->Lock);
    kFreePages(pThread->Stack);
    pThread->Flags |= THREAD_WAITING_OBJ_FREE;
    kSpinlockRelease(&pThread->Lock);

    // Decrement the reference count of the scheduler.
    kDecrementThreadRefCount(pThread);
}

static void SchedCallback(void *Data) {
    Context *pContext = (Context *)Data;
    SchedLocalData *pLocalData = (SchedLocalData *)kGetCoreLocalStorage()->SchedulerData;

    Thread *CurrentThread = pLocalData->CurrentThread;
    if (CurrentThread != nullptr) {
        kSpinlockAcquire(&CurrentThread->Lock);
        if (CurrentThread->Flags & THREAD_KILL_PENDING) {
            kAssert(!(CurrentThread->Flags & THREAD_WAITING_OBJ_FREE));
            kSpinlockRelease(&CurrentThread->Lock);
            DestroyThreadImpl(CurrentThread);
        }
        else {
            kMemCpy(&CurrentThread->ThreadContext, pContext, sizeof(Context));
            kAddToRunQueue(CurrentThread);
            kSpinlockRelease(&CurrentThread->Lock);
        }
    }

    // We put the old thread into the run queue first
    //   because there might be no thread to switch to
    //   from the current thread.
    while (1) {
        Thread *NextThread = ExtractFromRunQueue();
        if (NextThread != nullptr) {
            kSpinlockAcquire(&NextThread->Lock);
            if (NextThread->Flags & THREAD_KILL_PENDING) {
                kAssert(!(NextThread->Flags & THREAD_WAITING_OBJ_FREE));
                kSpinlockRelease(&NextThread->Lock);
                DestroyThreadImpl(NextThread);
                continue;
            }
            else {
                //kConsolePrintf(TEXT("Switching to thread (ptr=%p), core=%d\r\n"), NextThread, kApicGetApicID());
                kMemCpy(pContext, &NextThread->ThreadContext, sizeof(Context));
                kSpinlockRelease(&NextThread->Lock);
            }
        }
        else {
            //kConsolePrintf(TEXT("No threads available, core=%d\r\n"), kApicGetApicID());
            kMemCpy(pContext, &pLocalData->IdleThreadContext, sizeof(Context));
        }
        pLocalData->CurrentThread = NextThread;
        break;
    }
}
