; file		APStartup.asm
; org.date	2014/02/19
; author	Byeongkeun Ahn
; brief		Application Processor의 16비트 엔트리 포인트

; ********  Memory map  ********
; 0x04000-0x05000 : stack for loader code
; 0x05000-0x06000 : data from BSP
; 0x06000-0x20000 : code & data
; 0x20000-0x80000 : temporary stack per APIC ID (each having 1K)

[ORG 0x06000]
[BITS 16]

START:
	jmp 0x0000:TRUE_START

TRUE_START:
	; Disable interrupts
	cli

	; Setup data segment register
	mov ax, 0x0
	mov ds, ax

	; The A20 gate is already enabled by BSP
	; So, switch directly to 32bit protected mode

	lgdt [GDTR]
	mov eax, 0x4000003B			; Paging (bit 31) disabled
	mov cr0, eax				; Switch!
	jmp dword 0x08:START_32		; Far jump to load cs register


; GDT structure has to be aligned
align 8, db 0x00

; Note that we have to prepare GDT to include
; both 32-bit and 64-bit descriptors
; because in 64-bit we cannot do a far-jump
; (need to do it before switching to 64-bit mode)

; For reference, please see:
;   AMD64 System Programmer's Manual, Volume 2 (System Programming),
;   Section 4.8 'Long-Mode Segment Descriptors'.

GDT:

.GDT_NULLDESC:
	dq 0

.GDT_CODEDESC:        ; 0x08
	dw 0xFFFF         ; SegLimit 15:00
	dw 0x0000         ; Base     15:00
	db 0x00           ; Base     23:16
	db 0x9A           ; P, DPL, S, Type
	db 0xCF           ; G, D/B, L, AVL, SegLimit 19:16
	db 0x00           ; Base     31:24

.GDT_DATADESC:        ; 0x10
	dw 0xFFFF         ; SegLimit 15:00
	dw 0x0000         ; Base     15:00
	db 0x00           ; Base     23:16
	db 0x92           ; P=1, DPL=0, S=0, Type
	db 0xCF           ; G=1, D/B=1, L=0, AVL=0, SegLimit 19:16
	db 0x00           ; Base     31:24
	
.GDT_CODEDESC64:      ; 0x18
	dw 0xFFFF         ; SegLimit 15:00
	dw 0x0000         ; Base     15:00
	db 0x00           ; Base     23:16
	db 0x9A           ; P, DPL, S, Type
	db 0xAF           ; G=1, D/B=0, L=1, AVL=0, SegLimit 19:16
	db 0x00           ; Base     31:24

.GDT_DATADESC64:      ; 0x20
	dw 0xFFFF         ; SegLimit 15:00
	dw 0x0000         ; Base     15:00
	db 0x00           ; Base     23:16
	db 0x92           ; P=1, DPL=0, S=0, Type
	db 0xAF           ; G=1, D/B=0, L=1, AVL=0, SegLimit 19:16
	db 0x00           ; Base     31:24

GDTR:

	dw GDTR - GDT - 1	; size of GDT
	dd GDT				; address of GDT


[BITS 32]

START_32:
	; Reload segment registers
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	; Setup stack
	mov esp, 0x06000	; x86 stack is full-descending

	; For more information on activating Long-Mode,
	;   please refer to AMD64 Architecture Programmer's Manual, 
	;   Volume 2 (System Programming), Section 14.6 'Enabling
	;   and Activating Long Mode'.

	; Enable PAE (physical address extension)
	mov eax, cr4
	bts eax, 5
	mov cr4, eax

	; Setup paging
	; (Identity-mapping for the first 4GB of the address space
	;  using the page size of 2MB)
	; Note that paging goes through PML4E->PDPE->PDE.
	; (Actually, there is one more level called PTE, but it is
	;  not used here due to the choice of 2MB page size)

	mov eax, PDPE_64
	or eax, 0x3
	mov [PML4E_64], eax

	mov eax, PDE_64
	or eax, 0x13			; PCD=1, RW=1, P=1
	mov [PDPE_64], eax
	add eax, 4096
	mov [PDPE_64+8], eax
	add eax, 4096
	mov [PDPE_64+16], eax
	add eax, 4096
	mov [PDPE_64+24], eax

	xor ecx, ecx
	mov edx, PDE_64
repeat:
	mov eax, ecx
	shl eax, 21
	or eax, 0x93			; PS=1, PCD=1, RW=1, P=1
	mov [edx+0], eax
	mov eax, ecx
	shr eax, 11
	mov [edx+4], eax
	inc ecx
	add edx, 8
	cmp ecx, 512*4
	jl repeat

	mov eax, PML4E_64
	mov cr3, eax

	; Enable Long Mode
	mov ecx, 0xC0000080
	rdmsr
	bts eax, 8
	wrmsr

	; Enable paging
	; (This will set EFER.LMA (Long Mode Enable) bit to 1,
	;  indicating that the Long Mode has been enabled)
	mov eax, cr0
	bts eax, 31
	mov cr0, eax

	jmp 0x18:START_64


; Page table structure has to be aligned
align 4096, db 0x00

PML4E_64:
	times 4096 db 0
PDPE_64:
	times 4096 db 0
PDE_64:
	times 4096*4 db 0


[BITS 64]

START_64:
	mov ax, 0x20
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax

	; Update page table base register to use kernel's virtual memory space
	mov rax, QWORD [0x5018]
	mov cr3, rax
	
	; Note that we don't setup stack here
	; since that will be done by the 64bit kernel entrypoint thunk
	; (So be careful not to use it)

	mov rax, QWORD [0x5000]
	jmp rax
