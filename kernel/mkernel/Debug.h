﻿
#pragma once

// When something goes wrong, we call this function
// and put computer into an infinite loop
void kHang(const char *file = nullptr, int line = -1);
// When something that should hold doesn't, hang up
#if defined(_DEBUG) || defined(DEBUG)
#define kAssert(expr) \
    if (!(expr)) \
        kHang(__FILE__, __LINE__)
#else
#define kAssert(expr) (expr)
#endif

void kPrintStackTrace(QWORD Unused = 0);
