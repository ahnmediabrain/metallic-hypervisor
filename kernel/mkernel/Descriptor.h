
#pragma once


#define GDT_CS      0x08
#define GDT_DS      0x10
#define GDT_TSS     0x18

// Creates and installs GDT for current processor.
// The interrupt must be disabled prior to the call
// to this procedure.
void kSetupGDT();

// Installs IDT into IDTR. Note that this should be called from each core.
// Assumes APIC has been already enabled.
void kInstallIDT();

// Tests the interrupt handling infrastructure.
// The implementation is in the file 'InterruptAsm.asm'.
extern "C" void kTestInterrupt();
