
#pragma once

#include "hw/apic/IoApic.h"

extern K_KERNEL_ENTRY_DATA *g_KernelEntryData;
extern UINT64 g_APCount;
extern UINT64 g_ApicTimerFreq;
extern ApicConfig g_ApicConfig;
