
#include "stdafx.h"
#include "StdLib.h"
#include "Console.h"
#include "PeLoader.h"

OS_STATUS kPeCalcLoadedSize(void *pFileInMemory, UINTN szBuffer, UINTN *OS_STATUS) {
    // is it PE?
    IMAGE_DOS_HEADER *pDosHdr = (IMAGE_DOS_HEADER *)pFileInMemory;
    if (pDosHdr->e_magic != IMAGE_DOS_SIGNATURE)
        return E_INVALID_INPUT;
    IMAGE_NT_HEADERS64 *pNtHdr = (IMAGE_NT_HEADERS64 *)((BYTE *)pFileInMemory + pDosHdr->e_lfanew);
    if (pNtHdr->Signature != IMAGE_NT_SIGNATURE)
        return E_INVALID_INPUT;
    IMAGE_FILE_HEADER *pFileHdr = &pNtHdr->FileHeader;
    if (pFileHdr->Machine != IMAGE_FILE_MACHINE_AMD64)
        return E_INVALID_INPUT;
    if (pFileHdr->SizeOfOptionalHeader == 0)
        return E_INVALID_INPUT;
    IMAGE_OPTIONAL_HEADER64 *pOptHdr = &pNtHdr->OptionalHeader;
    IMAGE_SECTION_HEADER *pSecHdrs = (IMAGE_SECTION_HEADER *)((BYTE *)pOptHdr + pFileHdr->SizeOfOptionalHeader);
    UINTN NumberOfSections = pFileHdr->NumberOfSections;
    // copy sections from file to memory
    DWORD szMax = 0;
    for (UINTN i = 0; i < NumberOfSections; i++) {
        DWORD VirtualAddress = pSecHdrs[i].VirtualAddress;
        DWORD SizeOfRawData = pSecHdrs[i].SizeOfRawData;
        szMax = max(szMax, VirtualAddress + SizeOfRawData);
    }
    *OS_STATUS = szMax;
    return E_SUCCESS;
}

OS_STATUS kLoadPe(void *pFileInMemory, UINTN szBuffer, void *pOutputBuffer, UINTN szOutputBuffer, UINTN TrueImageBase, UINTN *pEntryPointOffset) {
    // is it PE?
    IMAGE_DOS_HEADER *pDosHdr = (IMAGE_DOS_HEADER *)pFileInMemory;
    if (pDosHdr->e_magic != IMAGE_DOS_SIGNATURE)
        return E_INVALID_INPUT;
    IMAGE_NT_HEADERS64 *pNtHdr = (IMAGE_NT_HEADERS64 *)((BYTE *)pFileInMemory + pDosHdr->e_lfanew);
    if (pNtHdr->Signature != IMAGE_NT_SIGNATURE)
        return E_INVALID_INPUT;
    IMAGE_FILE_HEADER *pFileHdr = &pNtHdr->FileHeader;
    if (pFileHdr->Machine != IMAGE_FILE_MACHINE_AMD64)
        return E_INVALID_INPUT;
    if (pFileHdr->SizeOfOptionalHeader == 0)
        return E_INVALID_INPUT;
    kConsolePrintf(TEXT("kLoadPe 1\r\n"));
    IMAGE_OPTIONAL_HEADER64 *pOptHdr = &pNtHdr->OptionalHeader;
    IMAGE_SECTION_HEADER *pSecHdrs = (IMAGE_SECTION_HEADER *)((BYTE *)pOptHdr + pFileHdr->SizeOfOptionalHeader);
    UINTN NumberOfSections = pFileHdr->NumberOfSections;
    // load and relocate in memory
    for (UINTN i = 0; i < NumberOfSections; i++) {
        // copy sections from file to memory
        DWORD VirtualAddress = pSecHdrs[i].VirtualAddress;
        DWORD PointerToRawData = pSecHdrs[i].PointerToRawData;
        DWORD SizeOfRawData = pSecHdrs[i].SizeOfRawData;
        BYTE *pSrc = (BYTE *)pFileInMemory + PointerToRawData;
        BYTE *pDst = (BYTE *)pOutputBuffer + VirtualAddress;
        if (VirtualAddress + SizeOfRawData > szOutputBuffer)
            return E_BUFFER_TOO_SMALL;
        kMemCpy(pDst, pSrc, SizeOfRawData);
    }
    kConsolePrintf(TEXT("kLoadPe 2\r\n"));
    // perform relocation
    if (pOptHdr->NumberOfRvaAndSizes > IMAGE_DIRECTORY_ENTRY_BASERELOC) {
        IMAGE_DATA_DIRECTORY *pRelocDir = &pOptHdr->DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC];
        IMAGE_BASE_RELOCATION *pReloc = (IMAGE_BASE_RELOCATION *)((BYTE *)pOutputBuffer + pRelocDir->VirtualAddress);
        IMAGE_BASE_RELOCATION *pRelocEnd = (IMAGE_BASE_RELOCATION *)((BYTE *)pReloc + pRelocDir->Size);
        kConsolePrintf(TEXT("Reloc VA=0x%llX, Size=%d\r\n"), pRelocDir->VirtualAddress, pRelocDir->Size);
        UINTN RelocDelta = TrueImageBase - pOptHdr->ImageBase;
        while (pReloc < pRelocEnd) { // when there's no relocation table, the loop will not run
                                     // patch bytes
            UINTN NumberOfEntries = (pReloc->SizeOfBlock - 8) / 2;
            for (UINTN i = 0; i < NumberOfEntries; i++) {
                WORD wVal = pReloc->TypeOffset[i];
                WORD wType = (wVal & 0xF000) >> 12;
                WORD wOffset = wVal & 0x0FFF;
                UINTN PatchAddress = (UINTN)pOutputBuffer + pReloc->VirtualAddress + wOffset;
                LONG Temp;
                switch (wType) {
                case IMAGE_REL_BASED_HIGH:
                    *((WORD *)PatchAddress) += (WORD)(RelocDelta >> 16);
                    break;
                case IMAGE_REL_BASED_LOW:
                    *((WORD *)PatchAddress) += (WORD)RelocDelta;
                case IMAGE_REL_BASED_HIGHLOW:
                    *((DWORD *)PatchAddress) += (DWORD)RelocDelta;
                    break;
                case IMAGE_REL_BASED_HIGHADJ:
                    if (i == NumberOfEntries - 1) {
                        kConsolePrintf(TEXT("HIGHADJ\r\n"));
                        return E_INVALID_INPUT;
                    }
                    Temp = *((WORD *)PatchAddress) << 16;
                    Temp += pReloc->TypeOffset[i + 1];
                    Temp += (LONG)RelocDelta;
                    Temp += 0x8000;
                    *((WORD *)PatchAddress) = Temp >> 16;
                    i += 1;
                    break;
                case IMAGE_REL_BASED_DIR64:
                    *((UINT64 *)PatchAddress) += RelocDelta;
                    break;
                case IMAGE_REL_BASED_ABSOLUTE:
                    break;
                default:
                    kConsolePrintf(TEXT("UNKNOWN RELOCATION 0x%X\r\n"), wVal);
                    return E_UNSUPPORTED;
                }
            }
            // step forward
            pReloc = (IMAGE_BASE_RELOCATION *)((BYTE *)pReloc + pReloc->SizeOfBlock);
        }
    }
    // final wrap up
    kConsolePrintf(TEXT("OutputBuffer addr: 0x%llX\r\n"), pOutputBuffer);
    *pEntryPointOffset = pOptHdr->AddressOfEntryPoint;
    return E_SUCCESS;
}
