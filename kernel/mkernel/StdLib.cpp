﻿
#include "stdafx.h"
#include "StdLib.h"

/*
kStrCpy: 문자열 복사

@param    Destination        대상
Source            원본

@retval    (없음)

*/

void kStrCpy(CHAR16 *Destination, const CHAR16 *Source)
{
    do
    {
        *Destination++ = *Source++;
    } while (*Source != CHAR('\0'));

    *Destination = CHAR('\0');
}

/*
kStrLen: 문자열의 길이 계산

@param    String        입력 문자열

@retval    입력 문자열의 길이

*/

UINTN kStrLen(const CHAR16 *String)
{
    const CHAR16 *String_1 = String;
    while (*String_1 != CHAR('\0'))
        ++String_1;

    return (String_1 - String);
}

/*
kStrCmp: 문자열 비교

@param    String1        입력 문자열 1
String2        입력 문자열 2

@retval    String1 == String2 : 0 리턴
String1 > String2 : 양수 리턴
String1 < String2 : 음수 리턴
* 비교는 NULL character을 만날 때까지 진행되며,
String1에서 c1, String2에서 c2의 서로 다른 문자가 발견되면
c1 - c2 의 부호에 따라 양수 또는 음수를 리턴
*/

INTN kStrCmp(const CHAR16 *String1, const CHAR16 *String2)
{
    while (*String1 == *String2 && *String1 && *String2)
        ++String1, ++String2;

    return (INTN)SIGN(*String1 - *String2);
}

/*
kStrNCmp: 최대 개수가 있는 문자열 비교

@param    String1        입력 문자열 1
String2        입력 문자열 2

@retval    String1 == String2 : 0 리턴
String1 > String2 : 양수 리턴
String1 < String2 : 음수 리턴
* 비교는 최대 개수를 채우거나 NULL character을 만날 때까지 진행되며,
String1에서 c1, String2에서 c2의 서로 다른 문자가 발견되면
c1 - c2 의 부호에 따라 양수 또는 음수를 리턴
*/

INTN kStrNCmp(const CHAR16 *String1, const CHAR16 *String2, INTN Count)
{
    while (*String1 == *String2 && *String1 && *String2 && Count)
        ++String1, ++String2, --Count;

    if (Count == 0) return 0;
    return (INTN)SIGN(*String1 - *String2);
}

/*
kStrNCmp8: 최대 개수가 있는 ANSI 문자열 비교

@param    String1        입력 문자열 1
String2        입력 문자열 2

@retval    String1 == String2 : 0 리턴
String1 > String2 : 양수 리턴
String1 < String2 : 음수 리턴
* 비교는 최대 개수를 채우거나 NULL character을 만날 때까지 진행되며,
String1에서 c1, String2에서 c2의 서로 다른 문자가 발견되면
c1 - c2 의 부호에 따라 양수 또는 음수를 리턴
*/

INTN kStrNCmp8(const char *String1, const char *String2, INTN Count)
{
    while (*String1 == *String2 && *String1 && *String2 && Count)
        ++String1, ++String2, --Count;

    if (Count == 0) return 0;
    return (INTN)SIGN(*String1 - *String2);
}

/*
kStrInv: 문자열 뒤집기
kStrLen(String)에 의해 얻어지는 문자열의 길이를 바탕으로 뒤집기 연산 수행

@param    String        입력 문자열

@retval    (없음)

*/

void kStrInv(CHAR16 *String)
{
    UINTN Len = kStrLen(String);
    if (Len <= 1)
        return;

    UINTN i;
    UINTN j = Len / 2;

    CHAR16 temp;
    for (i = 0; i < j; i++)
    {
        // String의 i번째 항목과 Len-i-1번째 항목을 바꿈
        temp = String[i];
        String[i] = String[Len - i - 1];
        String[Len - i - 1] = temp;
    }
}

/*
kMemSet: 메모리를 초기화함

@param    Mem            초기화할 메모리
Value        초기화할 값
Size        초기화할 크기

@retval    (없음)

*/

void kMemSet(void *Mem, BYTE Value, K_SIZE Size)
{
    BYTE *pMem = (BYTE *)Mem;
    for (K_SIZE i = 0; i < Size; i++)
        *pMem++ = Value;
}

/*
kMemCpy: 메모리 복사

@param    Destination        대상
Source            원본

@retval    (없음)

*/

void kMemCpy(void *Destination, const void *Source, K_SIZE Num)
{
    BYTE *Destination2 = (BYTE *)Destination;
    const BYTE *Source2 = (const BYTE *)Source;

    for (UINTN i = 0; i < Num; ++i)
        *Destination2++ = *Source2++;
}

/*
kMemCmp: 문자열 비교

@param    Mem1        입력 메모리 1
Mem2        입력 메모리 2
Size        비교할 바이트 수

@retval    Mem1 == Mem2 : 0 리턴
Mem1 > Mem2 : 양수 리턴
Mem1 < Mem2 : 음수 리턴
* 비교는 Size 바이트만큼 진행되며,
Mem1에서 c1, Mem2에서 c2의 서로 다른 바이트가 발견되면
c1 - c2 의 부호에 따라 양수 또는 음수를 리턴
*/

INTN kMemCmp(const void *Mem1, const void *Mem2, K_SIZE Size)
{
    BYTE *pMem1 = (BYTE *)Mem1;
    BYTE *pMem2 = (BYTE *)Mem2;

    INTN ret = 0;

    UINTN i;
    for (i = 0; i < Size; i++)
    {
        if (*pMem1 != *pMem2)
        {
            ret = ((*pMem1 > *pMem2) ? 1 : (-1));
            break;
        }

        ++pMem1;
        ++pMem2;
    }

    return ret;
}

void *kMemMove(void *Destination, const void *Source, K_SIZE Num)
{
    BYTE *DestPtr = (BYTE *)Destination;
    BYTE *SrcPtr = (BYTE *)Source;
    if (DestPtr < SrcPtr) {
        while (Num-- > 0) *DestPtr++ = *SrcPtr++;
    }
    else if (DestPtr > SrcPtr) {
        DestPtr += Num;
        SrcPtr += Num;
        while (Num-- > 0) *--DestPtr = *--SrcPtr;
    }
    else { // DestPtr == SrcPtr : No need to copy
    }
    return Destination; // Return value per standard
}

/*
kAToI64: 문자열을 부호 있는 64비트 정수형으로 변환

@param    String        변환할 문자열
Scale        진법

@retval    변환된 정수

*/

INT64 kAToI64(const CHAR16 *String, UINT64 Scale)
{
    if (!String)
        return -1;

    if (Scale <= 1 || Scale > 36)
        Scale = 10;

    UINT64 iNumber = 0;
    UINT64 iTemp;
    BOOL bNegative;
    CHAR16 chTemp;

    if (*String == CHAR('-'))
    {
        bNegative = TRUE;
        ++String;
    }
    else
        bNegative = FALSE;

    if (Scale <= 10)
    {
        CHAR16 chRangeMax = (CHAR16)(CHAR('0') + Scale - 1);

        while ((chTemp = *String) != CHAR('\0'))
        {
            if (chTemp >= CHAR('0') && chTemp <= chRangeMax)
            {
                iTemp = chTemp - CHAR('0');
                iNumber *= Scale;
                iNumber += iTemp;
                ++String;
            }
            else
                break;
        }
    }
    else
    {
        CHAR16 chRangeMaxUpper = (CHAR16)(CHAR('A') + Scale - 11);
        CHAR16 chRangeMaxLower = (CHAR16)(CHAR('a') + Scale - 11);
        BOOL bNumber;

        while ((chTemp = *String) != CHAR('\0'))
        {
            bNumber = FALSE;
            if (chTemp >= CHAR('0') && chTemp <= CHAR('9'))
            {
                iTemp = chTemp - CHAR('0');
                bNumber = TRUE;
            }
            else if (chTemp >= CHAR('A') && chTemp <= chRangeMaxUpper)
            {
                iTemp = 10 + chTemp - CHAR('A');
                bNumber = TRUE;
            }
            else if (chTemp >= CHAR('a') && chTemp <= chRangeMaxLower)
            {
                iTemp = 10 + chTemp - CHAR('a');
                bNumber = TRUE;
            }

            if (bNumber)
            {
                iNumber *= Scale;
                iNumber += iTemp;
                ++String;
            }
            else
                break;
        }
    }

    INT64 iRet = (INT64)iNumber;
    if (bNegative)
        iRet = -iRet;

    return iRet;
}

/*
kAToU64: 문자열을 부호 없는 64비트 정수형으로 변환

@param    String        변환할 문자열
Scale        진법

@retval    변환된 정수

*/

UINT64 kAToU64(const CHAR16 *String, UINT64 Scale)
{
    if (!String)
        return -1;

    if (Scale <= 1 || Scale > 36)
        Scale = 10;

    UINT64 iNumber = 0;
    UINT64 iTemp;
    CHAR16 chTemp;

    if (Scale <= 10)
    {
        CHAR16 chRangeMax = (CHAR16)(CHAR('0') + Scale - 1);

        while ((chTemp = *String) != CHAR('\0'))
        {
            if (chTemp >= CHAR('0') && chTemp <= chRangeMax)
            {
                iTemp = chTemp - CHAR('0');
                iNumber *= Scale;
                iNumber += iTemp;
                ++String;
            }
            else
                break;
        }
    }
    else
    {
        CHAR16 chRangeMaxUpper = (CHAR16)(CHAR('A') + Scale - 11);
        CHAR16 chRangeMaxLower = (CHAR16)(CHAR('a') + Scale - 11);
        BOOL bNumber;

        while ((chTemp = *String) != CHAR('\0'))
        {
            bNumber = FALSE;
            if (chTemp >= CHAR('0') && chTemp <= CHAR('9'))
            {
                iTemp = chTemp - CHAR('0');
                bNumber = TRUE;
            }
            else if (chTemp >= CHAR('A') && chTemp <= chRangeMaxUpper)
            {
                iTemp = 10 + chTemp - CHAR('A');
                bNumber = TRUE;
            }
            else if (chTemp >= CHAR('a') && chTemp <= chRangeMaxLower)
            {
                iTemp = 10 + chTemp - CHAR('a');
                bNumber = TRUE;
            }

            if (bNumber == TRUE)
            {
                iNumber *= Scale;
                iNumber += iTemp;
                ++String;
            }
            else
                break;
        }
    }

    return iNumber;
}

static const CHAR16 g_ConvTableUpper[36] =
{
    CHAR('0'), CHAR('1'), CHAR('2'), CHAR('3'), CHAR('4'), CHAR('5'), CHAR('6'), CHAR('7'),
    CHAR('8'), CHAR('9'), CHAR('A'), CHAR('B'), CHAR('C'), CHAR('D'), CHAR('E'), CHAR('F'),
    CHAR('G'), CHAR('H'), CHAR('I'), CHAR('J'), CHAR('K'), CHAR('L'), CHAR('M'), CHAR('N'),
    CHAR('O'), CHAR('P'), CHAR('Q'), CHAR('R'), CHAR('S'), CHAR('T'), CHAR('U'), CHAR('V'),
    CHAR('W'), CHAR('X'), CHAR('Y'), CHAR('Z')
};

static const CHAR16 g_ConvTableLower[36] =
{
    CHAR('0'), CHAR('1'), CHAR('2'), CHAR('3'), CHAR('4'), CHAR('5'), CHAR('6'), CHAR('7'),
    CHAR('8'), CHAR('9'), CHAR('a'), CHAR('b'), CHAR('c'), CHAR('d'), CHAR('e'), CHAR('f'),
    CHAR('g'), CHAR('h'), CHAR('i'), CHAR('j'), CHAR('k'), CHAR('l'), CHAR('m'), CHAR('n'),
    CHAR('o'), CHAR('p'), CHAR('q'), CHAR('r'), CHAR('s'), CHAR('t'), CHAR('u'), CHAR('v'),
    CHAR('w'), CHAR('x'), CHAR('y'), CHAR('z')
};

/*
kIToA64: 부호 있는 64비트 정수형을 문자열로 변환

@param    Buffer        출력 버퍼
iValue        변환할 값
Scale        진법
bUpper        대문자/소문자 (0 이외의 값: 대문자, 0(FALSE): 소문자)
iWidth        부호를 제외한 자릿수 부분의 수. 부족하면 0으로 채워지고 넘치면 잘림
0으로 지정하면 무시됨
*/

void kIToA64(CHAR16 *Buffer, INT64 iValue, UINT64 Scale, BOOL bUpper, UINT64 iWidth)
{
    if (Scale <= 1 || Scale > 36)
        Scale = 10;

    CHAR16 *Buffer_1 = Buffer;

    if (iValue < 0)
        *Buffer_1++ = CHAR('-');

    UINT64 AbsValue = ABS(iValue);

    if (AbsValue == 0)
    {
        iWidth = MAX(iWidth, 1);
        for (UINTN i = 0; i < iWidth; i++)
            *Buffer_1++ = CHAR('0');
    }
    else
    {
        const CHAR16 *ConvTable = bUpper ? g_ConvTableUpper : g_ConvTableLower;
        if (iWidth == 0)
        {
            while (AbsValue != 0)
            {
                *Buffer_1++ = ConvTable[AbsValue % Scale];
                AbsValue /= Scale;
            }
        }
        else
        {
            UINTN i;
            for (i = 0; i < iWidth; i++)
            {
                *Buffer_1++ = ConvTable[AbsValue % Scale];
                AbsValue /= Scale;
            }
        }
    }

    *Buffer_1 = CHAR('\0');

    if (iValue < 0)
        kStrInv(Buffer + 1);
    else
        kStrInv(Buffer);
}

/*
kUToA64: 부호 없는 64비트 정수형을 문자열로 변환

@param    Buffer        출력 버퍼
iValue        변환할 값
Scale        진법
bUpper        대문자/소문자 (0 이외의 값: 대문자, 0(FALSE): 소문자)
iWidth        부호를 제외한 자릿수 부분의 수. 부족하면 0으로 채워지고 넘치면 잘림
0으로 지정하면 무시됨
*/

void kUToA64(CHAR16 *Buffer, UINT64 iValue, UINT64 Scale, BOOL bUpper, UINT64 iWidth)
{
    if (Scale <= 1 || Scale > 36)
        Scale = 10;

    CHAR16 *Buffer_1 = Buffer;

    if (iValue == 0)
    {
        iWidth = MAX(iWidth, 1);
        for (UINTN i = 0; i < iWidth; i++)
            *Buffer_1++ = CHAR('0');
    }
    else
    {
        const CHAR16 *ConvTable = bUpper ? g_ConvTableUpper : g_ConvTableLower;
        if (iWidth == 0)
        {
            while (iValue != 0)
            {
                *Buffer_1++ = ConvTable[iValue % Scale];
                iValue /= Scale;
            }
        }
        else
        {
            UINTN i;
            for (i = 0; i < iWidth; i++)
            {
                *Buffer_1++ = ConvTable[iValue % Scale];
                iValue /= Scale;
            }
        }
    }

    *Buffer_1 = CHAR('\0');
    kStrInv(Buffer);
}


/*
kSort: 정렬 수행

@param    Base        정렬할 대상의 메모리 상의 주소
Num            정렬할 대상의 수
Size        정렬할 대상 1개의 크기
CompFunc    대상을 비교하는 함수
-> 원형: bool CompFunc (const void* p1, const void* p2)
-> Return Value   true  : P1이 반드시 P2 앞에 와야 함.
false : P1이 P2와 동등하거나 P2 뒤에 와야 함.

@retval    (없음)

*/

void kSort(void *Base, K_SIZE Num, K_SIZE Size, bool(*CompFunc)(const void *, const void *))
{
    if (Num == 0)
        return;

    BYTE *start = ((BYTE *)Base);
    BYTE *p, *q;

    /* 삽입정렬 */
    for (UINTN i = 0; i < Num; ++i)
    {
        /* 인덱스 n이 i <= n <= count - 1 인 항목 중에서
        가장 앞쪽에 오는 항목을 찾아서
        i번째 항목과 해당 항목을 바꿈 */

        q = start + (Size * i);
        for (UINTN j = i + 1; j < Num; j++)
        {
            p = start + (Size * j);
            if (CompFunc(p, q) == true)
                q = p;
        }

        p = start + (Size * i);
        if (p != q)
        {
            /* i번째 항목(p)과 항목 q를 바꿈*/
            for (UINTN j = 0; j < Size; j++) {
                BYTE tmp = p[j];
                p[j] = q[j];
                q[j] = tmp;
            }
        }
    }
}

/*
kIsPowerOfTwo64: 정수가 2의 거듭제곱인지 판별

@param    Num        입력 정수
@retval    2의 거듭제곱인지 여부

*/

BOOL kIsPowerOfTwo64(UINT64 Num)
{
    return ((Num) && (!((Num - 1)&Num)));
}

/*
kGetMaxBit: 정수의 최상위 비트 번호를 반환

@param    Num                    정수
@retval    최상위 비트 번호    찾을 경우 0-63 범위의 수를 리턴, Num이 0이면 (UINT64)-1 을 리턴

*/

UINT64 kGetMaxBit(UINT64 Num)
{
    UINT64 retval = (UINT64)-1;
    for (UINTN i = 63; i != 0; --i)
    {
        if ((((UINT64)1) << i) & Num)
        {
            retval = i;
            break;
        }
    }

    return retval;
}

/*
kGetMinBit: 정수의 최하위 비트 번호를 반환

@param    Num                    정수
@retval    최하위 비트 번호    찾을 경우 0-63 범위의 수를 리턴, Num이 0이면 (UINT64)-1 을 리턴

*/

UINT64 kGetMinBit(UINT64 Num)
{
    UINT64 retval = (UINT64)-1;
    for (UINTN i = 0; i < 64; ++i)
    {
        if ((((UINT64)1) << i) & Num)
        {
            retval = i;
            break;
        }
    }

    return retval;
}
