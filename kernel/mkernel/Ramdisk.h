
#pragma once

struct RamdiskFileDesc {
    // File size in bytes.
    UINTN           Size;
    // Pointer to the data.
    void           *Data;
};

// Finds specified file in the boot ramdisk.
// Note that the contents in pFileDesc is only valid when this function succeeds.
OS_STATUS kRamdiskFindFile(const CHAR16 *Path, RamdiskFileDesc *pFileDesc);
