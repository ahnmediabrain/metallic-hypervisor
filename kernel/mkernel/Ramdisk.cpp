
#include "stdafx.h"
#include "GlobalVariable.h"
#include "StdLib.h"
#include "Ramdisk.h"


OS_STATUS kRamdiskFindFile(const CHAR16 *Path, RamdiskFileDesc *pFileDesc) {
    CHAR16 NormalizedName[8];
    UINTN i;
    for (i = 0; i < 8; i++) {
        if (Path[i] == L'\0') break;
        NormalizedName[i] = Path[i];
    }
    while (i < 8) {
        NormalizedName[i] = ' ';
        i++;
    }
    K_RAMDISK *Ramdisk = (K_RAMDISK *)(((UINTN)g_KernelEntryData) + g_KernelEntryData->RamdiskOffset);
    for (i = 0; i < Ramdisk->FileCount; i++) {
        K_RAMDISK_FILE *pFile = &Ramdisk->Files[i];
        if (!kMemCmp(pFile->FileName, NormalizedName, 16)) {
            pFileDesc->Size = pFile->Size;
            pFileDesc->Data = (void *)(((UINTN)Ramdisk) + pFile->DataOffset);
            return E_SUCCESS;
        }
    }
    return E_NOTFOUND;
}
