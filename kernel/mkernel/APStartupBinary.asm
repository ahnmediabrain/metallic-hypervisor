; file		APStartupBinary.asm
; org.date	2018/06/29
; author	Byeongkeun Ahn
; brief		Application Processor의 16비트 엔트리 포인트 코드를
;			인클루드하여 C언어 커널에서 사용할 수 있게 하는 브릿지


section .data
global APStartupBinaryCodeSize
global APStartupBinaryCode

APStartupBinaryCodeSize:
	dd APStartupBinaryCodeEnd - $ - 4
APStartupBinaryCode:
	incbin "APStartup.bin"
APStartupBinaryCodeEnd:
