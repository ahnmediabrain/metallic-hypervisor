﻿
#include "stdafx.h"
#include "Console.h"
#include "Debug.h"
#include "GlobalVariable.h"

void kHang(const char *file, int line) {
    if (file != nullptr) {
        if (line != -1) {
            kConsolePrintf(TEXT("Assert failed on file %S, line %d\r\n"), file, line);
        }
        else {
            kConsolePrintf(TEXT("Assert failed on file %S (line unknown)\r\n"), file);
        }
    }
    //kPrintStackTrace();
    kConsolePrintf(TEXT("Hanging\r\n"));
    while (true);
}

void kPrintStackTrace(QWORD Unused) {
#if defined(DEBUG) || defined (_DEBUG)
    QWORD *pReturnAddress = ((QWORD *)&Unused);
    for (int i = 0; i < 200; i++) {
        QWORD Value = pReturnAddress[i];
        if (Value >= g_KernelEntryData->Kernel_Addr &&
            Value < (g_KernelEntryData->Kernel_Addr + g_KernelEntryData->KernelImageSize)) {
            kConsolePrintf(TEXT("%16.16llX (mkernel+%16.16llX)\r\n"),
                Value, Value - g_KernelEntryData->Kernel_Addr);
        }
        else {
            //kConsolePrintf(TEXT("%16.16llX\r\n"), Value);
        }
    }
#endif
}
