
#pragma once

// Note: Pointle stands for 'opaque pointer'
typedef UINTN ProcessPointle;
typedef UINTN ThreadPointle;


struct CreateProcessConfig {

};

// Creates a process.
OS_STATUS kCreateProcess(CreateProcessConfig *pConfig, ProcessPointle *pProcessPointle = nullptr);


#define USER_THREAD_STACKSZ_DEFAULT         1048576
#define KERNEL_THREAD_STACKSZ_DEFAULT       65536
typedef UINTN(*THREADENTRYPOINT)(void *UserData);

struct CreateThreadConfig {
    ProcessPointle          Parent;         // Parent process; supplying EMPTY_HANDLE will create a kernel thread.
    THREADENTRYPOINT        EntryPoint;
    UINTN                   StackSize;      // Stack size. (will be rounded up to 4K unit / must be >0 and <=16M)
    void                   *UserData;
};

// Creates a thread.
OS_STATUS kCreateThread(CreateThreadConfig *pConfig, ThreadPointle *pThreadPointle = nullptr);

// Exits current thread. This can only be called from the thread itself.
// Kernel codes (e.g. interrupt handlers) should not call this routine
// since it may quit some wrong thread.
void kExitThread(QWORD ExitCode);
// Kills the specified thread. Note that the caller
// must ensure that the supplied pointle is valid.
OS_STATUS kKillThread(ThreadPointle Pointle);
// Kills the specified process.
OS_STATUS kKillProcess(ProcessPointle Pointle);
// Increments the reference count and returns new reference count.
UINTN kIncrementThreadRefCount(ThreadPointle Pointle);
// Decrements the reference count and returns new reference count.
// If the thread is kill-pending and has been completely dereferenced by this call,
// the accompanied data structure is freed.
UINTN kDecrementThreadRefCount(ThreadPointle Pointle);

// The valid thread ID ranges are from 0 to 0x7FFF_FFFF_FFFF_FFFFULL.
#define IDLE_THREAD_ID_BASE     (0x7FFFFFFFFFFFFFFFULL)     // (Actual ID) = IDLE_THREAD_ID_BASE - (UINTN)LocalApicID

ThreadPointle kGetCurrentThread();
UINTN kGetCurrentThreadID();
UINTN kGetThreadID(ThreadPointle Pointle);
//ThreadPointle kOpenThread(UINTN ThreadID);

// Updates the current thread's user data and returns the old value.
void *kSetThreadUserData(void *NewValue);
// Gets the current thread's user data.
void *kGetThreadUserData();

// Tests the threading implementation.
void kTestThreading();

