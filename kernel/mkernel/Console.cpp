﻿
#include "stdafx.h"
#include "StdLib.h"
#include "Psf.h"
#include <stdarg.h>
#include "SyncObjs.h"
#include "Console.h"

#define CONSOLE_CHAR_BUF_SIZE   16384

struct K_CONSOLE_DATA {
    K_SPINLOCK              Spinlock;
    bool                    Initialized;
    K_GRAPHICS_MODE_DESC   *pGMDesc;
    PSF1_HANDLE             Psf1Handle;
    UINT32                  CharActualWidth;    // Width not including horizontal margin
    UINT32                  CharActualHeight;   // Height not including vertical margin
    UINT32                  HorizontalMargin;
    UINT32                  VerticalMargin;
    UINT32                  CharWidth;      // Width including horizontal margin
    UINT32                  CharHeight;     // Height including vertical margin
    UINT32                  Width;          // Horizontal char count
    UINT32                  Height;         // Vertical char count
    UINT32                  PosX;           // Horizontal caret position
    UINT32                  PosY;           // Vertical caret position
    COLOR                   Foreground;
    COLOR                   Background;
    CHAR16                  Buffer[CONSOLE_CHAR_BUF_SIZE];
};
static K_CONSOLE_DATA ConData;

/* Declaration of Internal Utility Functions */

struct FormatDesc {
    bool        LeftJustify : 1;
    bool        ForceIncludeSign : 1;
    bool        ReserveSpaceForSign : 1;
    bool        IncludeSharp : 1;
    bool        ZeroPad : 1;
    bool        MinCharNumToPrintSet : 1;
    bool        PrecisionSet : 1;
    UINT32      MinCharNumToPrint;
    UINT32      Precision;
    enum {
        None = 0,
        HH, H, L, LL, J, Z, T, UpperL
    } Subspecifier;
};

static void ConsoleInit(K_GRAPHICS_MODE_DESC *pGMDesc, void *pFontFileOnMemory);
static UINT32 ConsoleWidth();
static UINT32 ConsoleHeight();
static void ConsoleGotoXY(UINT32 X, UINT32 Y);
static UINT32 ConsolePosX();
static UINT32 ConsolePosY();
static void ConsolePutCharAtCurrentPos(CHAR16 c);
static void ConsolePutChar(CHAR16 c); // put char and advance caret by one
static void ConsolePuts(const CHAR16 *str);
template <typename T>
static void ConsolePrintf(const T *FormatString, va_list vl);
static void ConsoleAdvanceCaret();
static void ConsoleClearCurrentPos();
static void ConsoleClearScreen();

template <typename T>
static void PrintDecimal(T number, FormatDesc &fd);
static void PrintHex(UINT64 number, bool uppercase, FormatDesc &fd);
template <typename T>
static void PrintString(const T *str, FormatDesc &fd);

static UINT32 Pos2BufPos(UINT32 X, UINT32 Y);
static void DrawCharAt(UINT32 X, UINT32 Y, CHAR16 c);
static void DrawUnderlineAt(UINT32 X, UINT32 Y, COLOR c);
static void DrawUnderline(COLOR c);     // Put underline of color c at current position
static void PutUnderline();             // Put underline of foreground color at current position
static void ClearUnderline();           // Put underline of background color at current position

static void PutPixelAt(UINT32 X, UINT32 Y, COLOR c);    // Print a pixel of color c at (X, Y) on the screen

/* Exposed APIs */

static void ConsoleInit(K_GRAPHICS_MODE_DESC *pGMDesc, void *pFontFileOnMemory)
{
    kSpinlockInit(&ConData.Spinlock);
    ConData.pGMDesc = pGMDesc;
    if (E_FAILED(Psf1Init(pFontFileOnMemory, &ConData.Psf1Handle))) {
        for (;;);
    }

    ConData.CharActualWidth = Psf1GetCharWidth(&ConData.Psf1Handle);
    ConData.CharActualHeight = Psf1GetCharHeight(&ConData.Psf1Handle);
    ConData.HorizontalMargin = 0;
    ConData.VerticalMargin = 3;
    ConData.CharWidth = ConData.CharActualWidth + ConData.HorizontalMargin;
    ConData.CharHeight = ConData.CharActualHeight + ConData.VerticalMargin;
    ConData.Width = pGMDesc->resHorz / ConData.CharWidth;
    ConData.Height = min(pGMDesc->resVert / ConData.CharHeight, CONSOLE_CHAR_BUF_SIZE / ConData.Width);
    ConData.PosX = 0;
    ConData.PosY = 0;
    ConData.Foreground = RGB(190, 190, 190);
    ConData.Background = RGB(0, 0, 0);

    for (UINT32 i = 0; i < ConData.Width * ConData.Height; i++) {
        ConData.Buffer[i] = L' ';
    }

    //ConsoleClearScreen();
    PutUnderline();
}

static UINT32 ConsoleWidth()
{
    return ConData.Width;
}
static UINT32 ConsoleHeight()
{
    return ConData.Height;
}

static void ConsoleGotoXY(UINT32 X, UINT32 Y)
{
    if (X < ConData.Width && Y < ConData.Height)
    {
        ClearUnderline();
        ConData.PosX = X;
        ConData.PosY = Y;
        PutUnderline();
    }
}

static UINT32 ConsolePosX()
{
    return ConData.PosX;
}
static UINT32 ConsolePosY()
{
    return ConData.PosY;
}

static void ConsolePutCharAtCurrentPos(CHAR16 c)
{
    ConData.Buffer[Pos2BufPos(ConData.PosX, ConData.PosY)] = c;
    DrawCharAt(ConData.PosX, ConData.PosY, c);
}

static void ConsolePutChar(CHAR16 c)
{
    switch (c) {
    case L'\r':
        ConsoleGotoXY(0, ConsolePosY());
        break;
    case L'\n':
        if (ConsolePosY() == ConData.Height - 1) {
            UINT32 W = ConData.Width;
            UINT32 H = ConData.Height;
            // Scroll character buffer
            kMemMove(ConData.Buffer, ConData.Buffer + W, sizeof(CHAR16) * W * (H - 1));
            for (UINT32 i = W * (H - 1); i < W * H; i++) {
                ConData.Buffer[i] = L' ';
            }
            // Scroll screen
            UINTN bytePerPixel = ConData.pGMDesc->videoDepth / 8;
            UINTN pixelPerLine = ConData.CharHeight;
            UINTN line_bytes = ConData.pGMDesc->ccPixelPerScanLine * bytePerPixel * pixelPerLine;
            UINTN address = ConData.pGMDesc->framebuffer_base_virt;
            DrawUnderlineAt(ConsolePosX(), H - 1, ConData.Background);
            kMemMove((void *)address, (void *)(address + line_bytes), line_bytes * (H - 1));
            kMemSet((void *)(address + line_bytes * (H - 1)), 0, line_bytes);
            DrawUnderlineAt(ConsolePosX(), H - 1, ConData.Foreground);
            // Note that we don't need to move the caret in this case
        }
        else {
            ConsoleGotoXY(ConsolePosX(), ConsolePosY() + 1);
        }
        break;
    default:
        ConsolePutCharAtCurrentPos(c);
        ConsoleAdvanceCaret();
    }
}

static void ConsolePuts(const CHAR16 *str)
{
    while (*str) {
        ConsolePutChar(*str++);
    }
}

template <typename T>
static void ConsolePrintf(const T *FormatString, va_list vl)
{
    static_assert(sizeof(T) <= 2, "ConsolePrintf needs CHAR8 or CHAR16");

    while (*FormatString) {
        CHAR16 c = *FormatString++;
        if (c == L'%') {
            FormatDesc fd;
            kMemSet(&fd, 0, sizeof(fd));

            /* Flags */
            while (1) {
                CHAR16 a = *FormatString;
                bool Handled = true;
                switch (a) {
                case '-':
                    fd.LeftJustify = true;
                    break;
                case '+':
                    fd.ForceIncludeSign = true;
                    break;
                case ' ':
                    fd.ReserveSpaceForSign = true;
                    break;
                case '#':
                    fd.IncludeSharp = true;
                    break;
                case '0':
                    fd.ZeroPad = true;
                    break;
                default:
                    Handled = false;
                    break;
                }
                if (!Handled) break;
                FormatString++;
            }

            /* Width */
            fd.MinCharNumToPrintSet = false;
            if (*FormatString == '*') {
                fd.MinCharNumToPrint = va_arg(vl, UINT32);
                fd.MinCharNumToPrintSet = true;
                FormatString++;
            }
            else {
                while (1) {
                    CHAR16 a = *FormatString;
                    if (a >= '0' && a <= '9') {
                        fd.MinCharNumToPrintSet = true;
                        UINT64 NewVal = fd.MinCharNumToPrint;
                        NewVal = NewVal * 10 + (a - '0');
                        if (NewVal >= 0x10000)
                            NewVal = 0x10000;
                        fd.MinCharNumToPrint = (UINT32)NewVal;
                        FormatString++;
                    }
                    else {
                        break;
                    }
                }
            }

            /* Precision */
            fd.PrecisionSet = false;
            if (*FormatString == '.') {
                fd.PrecisionSet = true;
                FormatString++;
                if (*FormatString == '*') {
                    fd.Precision = va_arg(vl, UINT32);
                    FormatString++;
                }
                else {
                    while (1) {
                        CHAR16 a = *FormatString;
                        if (a >= '0' && a <= '9') {
                            UINT64 NewVal = fd.Precision;
                            NewVal = NewVal * 10 + (a - '0');
                            if (NewVal >= 0x10000)
                                NewVal = 0x10000;
                            fd.Precision = (UINT32)NewVal;
                            FormatString++;
                        }
                        else {
                            break;
                        }
                    }
                }
            }

            /* Length sub-specifier */
            switch (*FormatString) {
            case 'h':
                FormatString++;
                if (*FormatString == 'h') {
                    fd.Subspecifier = FormatDesc::HH;
                    FormatString++;
                }
                else
                    fd.Subspecifier = FormatDesc::H;
                break;
            case 'l': // lowercase L
                FormatString++;
                if (*FormatString == 'l') {
                    fd.Subspecifier = FormatDesc::LL;
                    FormatString++;
                }
                else
                    fd.Subspecifier = FormatDesc::LL;
                break;
            case 'j':
                FormatString++;
                fd.Subspecifier = FormatDesc::J;
                break;
            case 'z':
                FormatString++;
                fd.Subspecifier = FormatDesc::Z;
                break;
            case 't':
                FormatString++;
                fd.Subspecifier = FormatDesc::T;
                break;
            case 'L':
                FormatString++;
                fd.Subspecifier = FormatDesc::UpperL;
                break;
            }

            /* Specifier */
            if (*FormatString == 'd') {
                INT64 number;
                if (fd.Subspecifier == FormatDesc::LL)
                    number = va_arg(vl, INT64);
                else
                    number = va_arg(vl, INT32);
                PrintDecimal(number, fd);
                FormatString += 1;
            }
            else if (*FormatString == 'u') {
                UINT64 number;
                if (fd.Subspecifier == FormatDesc::LL)
                    number = va_arg(vl, UINT64);
                else
                    number = va_arg(vl, UINT32);
                PrintDecimal(number, fd);
                FormatString += 1;
            }
            else if (*FormatString == 'x') {
                UINT64 number;
                if (fd.Subspecifier == FormatDesc::LL)
                    number = va_arg(vl, UINT64);
                else
                    number = va_arg(vl, UINT32);
                PrintHex(number, false, fd);
                FormatString += 1;
            }
            else if (*FormatString == 'X') {
                UINT64 number;
                if (fd.Subspecifier == FormatDesc::LL)
                    number = va_arg(vl, UINT64);
                else
                    number = va_arg(vl, UINT32);
                PrintHex(number, true, fd);
                FormatString += 1;
            }
            else if (*FormatString == 'p') {
                UINTN number = va_arg(vl, UINTN);
                ConsolePuts(TEXT("0x"));
                fd.IncludeSharp = false;
                PrintHex(number, true, fd);
                FormatString += 1;
            }
            else if (*FormatString == 'c') {
                CHAR16 ch = va_arg(vl, CHAR16);
                ConsolePutChar(ch);
                FormatString += 1;
            }
            else if ((sizeof(T) == 2 && *FormatString == 's') ||
                (sizeof(T) == 1 && *FormatString == 'S')) {
                // Unicode string
                const CHAR16 *str = va_arg(vl, const CHAR16 *);
                PrintString(str, fd);
                FormatString += 1;
            }
            else if ((sizeof(T) == 2 && *FormatString == 'S') ||
                (sizeof(T) == 1 && *FormatString == 's')) {
                // ANSI string
                const char *str = va_arg(vl, const char *);
                PrintString(str, fd);
                FormatString += 1;
            }
            else if (*FormatString == '%') {
                ConsolePutChar(L'%');
                FormatString += 1;
            }
            else {
                /* Unknown or unsupported format specifier */
                ConsolePutChar(c);
            }
        }
        else {
            ConsolePutChar(c);
        }
    }
}
static void ConsoleAdvanceCaret()
{
    if (ConsolePosX() == kConsoleWidth() - 1) {
        ConsolePuts(TEXT("\r\n"));
    }
    else {
        ConsoleGotoXY(ConsolePosX() + 1, ConsolePosY());
    }
}

static void ConsoleClearCurrentPos()
{
    ConsolePutCharAtCurrentPos(L' ');
}

static void ConsoleClearScreen()
{
    for (UINT32 y = 0; y < ConData.pGMDesc->resVert; y++) {
        for (UINT32 x = 0; x < ConData.pGMDesc->resHorz; x++) {
            PutPixelAt(x, y, ConData.Background);
        }
    }
}

/* Internal Utility Functions */

template <typename T>
static void PrintDecimal(T number, FormatDesc &fd)
{
    static_assert(sizeof(T) == 8, "PrintDecimal needs either INT64 or UINT64");

    if (fd.IncludeSharp && number != 0) {
        ConsolePuts(TEXT("0"));
    }
    if (fd.ForceIncludeSign && number > 0) {
        ConsolePutChar(L'+');
    }
    else if (number < 0) {
        ConsolePutChar(L'-');
        number = -number;
    }
    else if (fd.ReserveSpaceForSign) {
        ConsolePutChar(L' ');
    }

    UINT64 UnsignedNumber = (UINT64)number;

    UINT64 HighestPowerOfTen = 10000000000000000000ULL; // 10^19
    UINT32 Digits = 20;
    while (UnsignedNumber < HighestPowerOfTen) {
        HighestPowerOfTen /= 10;
        Digits--;
    }
    if (UnsignedNumber == 0) {
        Digits = 1;
    }
    if (Digits < fd.Precision) {
        for (UINT32 i = Digits; i < fd.Precision; i++) {
            Digits++;
        }
    }
    /* left padding */
    if (Digits < fd.MinCharNumToPrint && !fd.LeftJustify) {
        for (UINT32 i = Digits; i < fd.MinCharNumToPrint; i++)
            ConsolePutChar(L' ');
    }

    /* print */
    while (Digits > 20) {
        ConsolePutChar(L'0');
        Digits--;
    }
    HighestPowerOfTen = 1;
    for (UINT32 i = 1; i < Digits; i++) HighestPowerOfTen *= 10;
    while (HighestPowerOfTen) {
        ConsolePutChar((CHAR16)(L'0' + UnsignedNumber / HighestPowerOfTen));
        UnsignedNumber %= HighestPowerOfTen;
        HighestPowerOfTen /= 10;
    }

    /* right padding */
    if (Digits < fd.MinCharNumToPrint && fd.LeftJustify) {
        for (UINT32 i = Digits; i < fd.MinCharNumToPrint; i++)
            ConsolePutChar(L' ');
    }
}

static void PrintHex(UINT64 number, bool uppercase, FormatDesc &fd)
{
    if (fd.IncludeSharp && number != 0) {
        if (uppercase)
            ConsolePuts(TEXT("0X"));
        else
            ConsolePuts(TEXT("0x"));
    }

    UINT64 HighestPowerOfSixteen = 1ULL << 60; // 2^60
    UINT32 Digits = 16;
    while (number < HighestPowerOfSixteen) {
        HighestPowerOfSixteen /= 16;
        Digits--;
    }
    if (number == 0) {
        Digits = 1;
    }
    if (Digits < fd.Precision) {
        for (UINT32 i = Digits; i < fd.Precision; i++) {
            Digits++;
        }
    }
    /* left padding */
    if (Digits < fd.MinCharNumToPrint && !fd.LeftJustify) {
        for (UINT32 i = Digits; i < fd.MinCharNumToPrint; i++)
            ConsolePutChar(L' ');
    }

    /* print */
    while (Digits > 16) {
        ConsolePutChar(L'0');
        Digits--;
    }
    HighestPowerOfSixteen = 1;
    for (UINT32 i = 1; i < Digits; i++) HighestPowerOfSixteen *= 16;

    static const CHAR16 *Uppercases = TEXT("0123456789ABCDEF");
    static const CHAR16 *Lowercases = TEXT("0123456789abcdef");
    const CHAR16 *Chars = uppercase ? Uppercases : Lowercases;
    while (HighestPowerOfSixteen) {
        CHAR16 c = Chars[number / HighestPowerOfSixteen];
        ConsolePutChar(c);
        number %= HighestPowerOfSixteen;
        HighestPowerOfSixteen /= 16;
    }

    /* right padding */
    if (Digits < fd.MinCharNumToPrint && fd.LeftJustify) {
        for (UINT32 i = Digits; i < fd.MinCharNumToPrint; i++)
            ConsolePutChar(L' ');
    }
}

template <typename T>
static void PrintString(const T *str, FormatDesc &fd) {
    static_assert(sizeof(T) <= 2, "PrintString requires CHAR8 or CHAR16");

    UINT32 Len = 0;
    for (const T *p = str; *p && Len < 0x10000 /* artificial string length limit */; p++)
        Len++;
    if (fd.PrecisionSet)
        Len = min(Len, fd.Precision);

    UINT32 LeftPad, RightPad;
    if (fd.MinCharNumToPrint > Len) {
        if (fd.LeftJustify)
            LeftPad = 0, RightPad = fd.MinCharNumToPrint - Len;
        else
            LeftPad = fd.MinCharNumToPrint - Len, RightPad = 0;
    }
    else {
        LeftPad = RightPad = 0;
    }

    for (UINT32 i = 0; i < LeftPad; i++)
        ConsolePutChar(' ');
    for (UINT32 i = 0; i < Len; i++)
        ConsolePutChar(str[i]);
    for (UINT32 i = 0; i < RightPad; i++)
        ConsolePutChar(' ');
}

UINT32 Pos2BufPos(UINT32 X, UINT32 Y)
{
    return X + Y * ConData.Width;
}

static void DrawCharAt(UINT32 X, UINT32 Y, CHAR16 c)
{
    unsigned short key = ConData.Psf1Handle.CharMap[c];
    if (key == 0xFFFF) {
        key = ConData.Psf1Handle.CharMap[L'?'];
    }
    BYTE *pixels = ConData.Psf1Handle.Data[key].Pixels;

    UINT32 xBase = ConData.CharWidth * X;
    UINT32 yBase = ConData.CharHeight * Y;
    for (UINT32 y = 0; y < ConData.CharActualHeight; y++) {
        for (UINT32 x = 0; x < ConData.CharActualWidth; x++) {
            COLOR cr;
            if (pixels[y] & (1 << (7 - x))) {
                cr = ConData.Foreground;
            }
            else {
                cr = ConData.Background;
            }
            PutPixelAt(xBase + x, yBase + y, cr);
        }
    }
}

static void DrawUnderlineAt(UINT32 X, UINT32 Y, COLOR c)
{
    UINT32 xBase = ConData.CharWidth * X;
    UINT32 yBase = ConData.CharHeight * Y + ConData.CharActualHeight;
    for (UINT32 y = 0; y < ConData.VerticalMargin; y++) {
        for (UINT32 x = 0; x < ConData.CharActualWidth; x++) {
            PutPixelAt(xBase + x, yBase + y, c);
        }
    }
}
static void DrawUnderline(COLOR c)
{
    DrawUnderlineAt(ConData.PosX, ConData.PosY, c);
}
static void PutUnderline()
{
    DrawUnderline(ConData.Foreground);
}
static void ClearUnderline()
{
    DrawUnderline(ConData.Background);
}

static void PutPixelAt(UINT32 X, UINT32 Y, COLOR c)
{
    DWORD WriteValue = 0;
    switch (ConData.pGMDesc->pixelMode) {
    case K_VIDEO_PIXEL_BGRX:
        WriteValue = c;
        break;
    case K_VIDEO_PIXEL_RGBX:
        WriteValue = (DWORD)COLOR_R(c);
        WriteValue |= ((DWORD)COLOR_G(c)) << 8;
        WriteValue |= ((DWORD)COLOR_B(c)) << 16;
        break;
    }
    UINTN bytePerPixel = ConData.pGMDesc->videoDepth / 8;
    UINTN address = ConData.pGMDesc->framebuffer_base_virt;
    address += bytePerPixel * (X + Y * ConData.pGMDesc->ccPixelPerScanLine);
    *(DWORD *)address = WriteValue;
}

/* External Interface Wrappers for Synchronization */

void kConsoleInitAsDevNull() {
    ConData.Initialized = false;
}

void kConsoleInit(K_GRAPHICS_MODE_DESC *pGMDesc, void *pFontFileOnMemory) {
    ConsoleInit(pGMDesc, pFontFileOnMemory);
    ConData.Initialized = true;
}
UINT32 kConsoleWidth() {
    if (!ConData.Initialized) return 0;
    return ConsoleWidth();
}
UINT32 kConsoleHeight() {
    if (!ConData.Initialized) return 0;
    return ConsoleHeight();
}
void kConsoleGotoXY(UINT32 X, UINT32 Y) {
    if (!ConData.Initialized) return;
    kSpinlockAcquire(&ConData.Spinlock);
    ConsoleGotoXY(X, Y);
    kSpinlockRelease(&ConData.Spinlock);
}
UINT32 kConsolePosX() {
    if (!ConData.Initialized) return 0;
    kSpinlockAcquire(&ConData.Spinlock);
    UINT32 Result = ConsolePosX();
    kSpinlockRelease(&ConData.Spinlock);
    return Result;
}
UINT32 kConsolePosY() {
    if (!ConData.Initialized) return 0;
    kSpinlockAcquire(&ConData.Spinlock);
    UINT32 Result = ConsolePosY();
    kSpinlockRelease(&ConData.Spinlock);
    return Result;
}
void kConsolePutCharAtCurrentPos(CHAR16 c) {
    if (!ConData.Initialized) return;
    kSpinlockAcquire(&ConData.Spinlock);
    ConsolePutCharAtCurrentPos(c);
    kSpinlockRelease(&ConData.Spinlock);
}
void kConsolePutChar(CHAR16 c) {
    if (!ConData.Initialized) return;
    kSpinlockAcquire(&ConData.Spinlock);
    ConsolePutChar(c);
    kSpinlockRelease(&ConData.Spinlock);
}
void kConsolePuts(const CHAR16 *str) {
    if (!ConData.Initialized) return;
    kSpinlockAcquire(&ConData.Spinlock);
    ConsolePuts(str);
    kSpinlockRelease(&ConData.Spinlock);
}
void kConsolePrintf(const CHAR16 *FormatString, ...) {
    if (!ConData.Initialized) return;
    kSpinlockAcquire(&ConData.Spinlock);
    va_list vl;
    va_start(vl, FormatString);
    ConsolePrintf(FormatString, vl);
    va_end(vl);
    kSpinlockRelease(&ConData.Spinlock);
}
void kConsoleVPrintf(const CHAR16 *FormatString, va_list vl) {
    if (!ConData.Initialized) return;
    kSpinlockAcquire(&ConData.Spinlock);
    ConsolePrintf(FormatString, vl);
    kSpinlockRelease(&ConData.Spinlock);
}
void kConsolePrintf8(const char *FormatString, ...) {
    if (!ConData.Initialized) return;
    kSpinlockAcquire(&ConData.Spinlock);
    va_list vl;
    va_start(vl, FormatString);
    ConsolePrintf(FormatString, vl);
    va_end(vl);
    kSpinlockRelease(&ConData.Spinlock);
}
void kConsoleVPrintf8(const char *FormatString, va_list vl) {
    if (!ConData.Initialized) return;
    kSpinlockAcquire(&ConData.Spinlock);
    ConsolePrintf(FormatString, vl);
    kSpinlockRelease(&ConData.Spinlock);
}
void kConsoleAdvanceCaret() {
    if (!ConData.Initialized) return;
    kSpinlockAcquire(&ConData.Spinlock);
    ConsoleAdvanceCaret();
    kSpinlockRelease(&ConData.Spinlock);
}
void kConsoleClearCurrentPos() {
    if (!ConData.Initialized) return;
    kSpinlockAcquire(&ConData.Spinlock);
    ConsoleClearCurrentPos();
    kSpinlockRelease(&ConData.Spinlock);
}
void kConsoleClearScreen() {
    if (!ConData.Initialized) return;
    kSpinlockAcquire(&ConData.Spinlock);
    ConsoleClearScreen();
    kSpinlockRelease(&ConData.Spinlock);
}

void kConsoleRunTest()
{
    for (int i = 0; i < 10000; i++) {
        ConsolePuts(TEXT("NEW!"));
        CHAR16 p[6];
        int val = i;
        for (int j = 0; j < 5; j++) {
            p[4 - j] = L'0' + (val % 10);
            val /= 10;
        }
        p[5] = L'\0';
        ConsolePuts(p);
        ConsolePuts(TEXT("\r\n"));
        for (int a = 0; a < 10000000; a++);
    }
}
