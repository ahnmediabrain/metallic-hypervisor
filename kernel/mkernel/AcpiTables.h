﻿
#pragma once

#pragma pack(push, 1)

/* RSDP (Root System Description Pointer) */

#define ACPI_SIG_RSDP               "RSD PTR "

struct AcpiRsdp {
    BYTE            Signature[8];
    BYTE            Checksum;
    BYTE            OEMID[6];
    BYTE            Revision;
    UINT32          RsdtAddress;
    UINT32          Length;
    UINT64          XsdtAddress;
    BYTE            ExtendedChecksum;
    BYTE            Reserved[3];
};

struct AcpiHeader {
    BYTE            Signature[4];
    UINT32          Length;
    BYTE            Revision;
    BYTE            Checksum;
    BYTE            OEMID[6];
    BYTE            OEMTableID[8];
    UINT32          OEMRevision;
    BYTE            CreatorID[4];
    UINT32          CreatorRevision;
};

/* RSDT (Root System Description Table) */

#define ACPI_SIG_RSDT               "RSDT"
#define ACPI_SIG_XSDT               "XSDT"

struct AcpiRsdt {
    AcpiHeader      Header;
    UINT32          PointerToOtherSDTs[1];
};
struct AcpiXsdt {
    AcpiHeader      Header;
    UINT64          PointerToOtherSDTs[1];
};

/* MADT (Multiple APIC Description Table) */

#define ACPI_SIG_MADT               "APIC"
#define ACPI_MADT_LEGACY_PIC        (1UL << 0)      // Is 8259 PIC present? (It should be disabled if so)

struct AcpiMadt {
    AcpiHeader      Header;
    UINT32          LocalAPICAddress;
    UINT32          Flags;
};

#define MADT_TYPE_LOCAL_APIC            0
#define MADT_TYPE_IO_APIC               1
#define MADT_TYPE_INT_SRC_OVRR          2
#define MADT_TYPE_NMI_SRC               3
#define MADT_TYPE_LOCAL_APIC_NMI        4
#define MADT_TYPE_LOCAL_APIC_ADDR_OVRR  5
#define MADT_TYPE_IO_SAPIC              6
#define MADT_TYPE_LOCAL_SAPIC           7
#define MADT_TYPE_PLATFORM_INT_SRC      8
#define MADT_TYPE_LOCAL_X2APIC          9
#define MADT_TYPE_LOCAL_X2APIC_NMI      10

#define ACPI_MADT_LOCAL_APIC_ENABLED    (1UL << 0)

struct AcpiMadtLocalApic {
    BYTE            Type;
    BYTE            Length;
    BYTE            ACPIProcessorID;
    BYTE            APICID;
    UINT32          Flags;
};

struct AcpiMadtIoApic {
    BYTE            Type;
    BYTE            Length;
    BYTE            IOAPICID;
    BYTE            Reserved;
    UINT32          IOAPICAddress;
    UINT32          GlobalSysIntBase;   // Global System Interrupt Base
};

struct AcpiMadtIntSrcOvrr {
    BYTE            Type;
    BYTE            Length;
    BYTE            Bus;                // Constant (=0), meaning ISA bus
    BYTE            Source;             // Bus-relative interrupt source (IRQ)
    UINT32          GlobalSysInt;       // The Global System Interrupt that this
                                        // bus-relative interrupt source wil signal
    UINT16          Flags;
};

struct AcpiMadtNmiSrc {
    BYTE            Type;
    BYTE            Length;
    UINT16          Flags;
    UINT32          GlobalSysInt;       // The Global System Interrupt that this
                                        // NMI will signal
};

struct AcpiMadtLocalApicNmi {
    BYTE            Type;
    BYTE            Length;
    BYTE            ACPIProcessorID;
    UINT16          Flags;
    BYTE            LocalAPICLINTNum;   // Local APIC interrupt input LINTn to which
                                        // NMI is connected
};

struct AcpiMadtLocalApicAddrOvrr {
    BYTE            Type;
    BYTE            Length;
    BYTE            Reserved[2];
    UINT64          LocalAPICAddress;
};

// Note that SAPIC is only used on IA64 systems
// (hence we need not care about the following three)

struct AcpiMadtIoSApic {
    BYTE            Type;
    BYTE            Length;
    BYTE            IOAPICID;
    BYTE            Reserved;
    UINT32          GlobalSysIntBase;
    UINT64          IOSAPICAddress;
};

struct AcpiMadtLocalSApic {
    BYTE            Type;
    BYTE            Length;
    BYTE            ACPIProcessorID;
    BYTE            LocalSAPICID;
    BYTE            LocalSAPICEID;
    BYTE            Reserved[3];
    UINT32          Flags;
    UINT32          ACPIProcessorUIDVal;
    BYTE            ACPIProcessorUIDStr[1];
};

struct AcpiMadtPlatformIntSrcOvrr {
    BYTE            Type;
    BYTE            Length;
    UINT16          Flags;
    BYTE            InterruptType;
    BYTE            ProcessorID;
    BYTE            ProcessorEID;
    BYTE            IOSAPICVector;
    UINT32          GlobalSysInt;
    UINT32          PlatformIntSrcFlags;
};

struct AcpiMadtLocalX2Apic {
    BYTE            Type;
    BYTE            Length;
    UINT16          Reserved;
    UINT32          X2ApicID;
    UINT32          Flags;
    UINT32          AcpiProcessorUid;
};

struct AcpiMadtLocalX2ApicNmi {
    BYTE            Type;
    BYTE            Length;
    UINT16          Flags;
    UINT32          AcpiProcessorUid;
    BYTE            LocalX2ApicLintNum;
    BYTE            Reserved[3];
};

/* MCFG (Memory-mapped ConFiGuration description table) */

#define ACPI_SIG_MCFG               "MCFG"

struct AcpiMcfg {
    AcpiHeader      Header;
    UINT8           Reserved[8];
};
struct AcpiMcfgEntry {
    UINT64          BaseAddress;
    UINT16          PciSegGroupNum;
    UINT8           StartBusNum;
    UINT8           EndBusNum;
    UINT8           Reserved[4];
};

/* ACPI Table APIs */

// Compares whether Target has the signature specified in Source,
// assuming Source is a NULL-terminated string
bool kAcpiCheckSignature(void *Target, const void *Source);

// Checks whether the entire structure sums to zero
bool kAcpiValidateChecksum(void *Target, K_SIZE bytes);

#pragma pack(pop)
