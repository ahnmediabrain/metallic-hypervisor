﻿
// Psf.h : PC Screen Font support

#pragma once

#pragma pack(push, 1)

#define PSF1_MAGIC          0x0436  // magic in little endian (0x36, 0x04)
#define PSF1_MODE_256_NU    0       // 256 chars in palette
#define PSF1_MODE_512_NU    1       // 512 chars in palette
#define PSF1_MODE_256_U     2       // 256 chars in palette with many-to-one mapping table
#define PSF1_MODE_512_U     3       // 512 chars in palette with many-to-one mapping table

#define PSF1_FONT_WIDTH     8       // fixed for PSF1 format

typedef struct {
    WORD            Magic;
    BYTE            Mode;
    BYTE            LineHeight;
} PSF1_HEADER;

typedef struct {
    struct {
        // Each byte storing 8-bit pixel data for each line
        // Valid: Pixels[0 .. LineHeight-1]
        BYTE        Pixels[16];
    } Data[512];
    WORD            CharMap[65536]; // 0xFFFF if not mapped
    WORD            LineHeight;
    WORD            PaletteCount;
} PSF1_HANDLE;

OS_STATUS Psf1Init(void *InputStream, PSF1_HANDLE *pHdl);
UINT32 Psf1GetCharWidth(PSF1_HANDLE *pHdl);
UINT32 Psf1GetCharHeight(PSF1_HANDLE *pHdl);

#pragma pack(pop)
