
#include "stdafx.h"
#include "Debug.h"
#include "CoreLocalStorage.h"
#include "hw/apic/Apic.h"

static CoreLocalStorage *CLSPtr;


void kSetCoreLocalStorageBase(UINT64 Address, UINT64 Size)
{
    kAssert(Address % 4096 == 0);
    CLSPtr = (CoreLocalStorage *)Address;
}

CoreLocalStorage* kGetCoreLocalStorage()
{
    UINT32 ApicID = kApicGetApicID();
    return CLSPtr + ApicID;
}

UINTN kGetCoreLocalStorageBase()
{
    return (UINTN)CLSPtr;
}

UINTN kGetCoreLocalStorageSize()
{
    return sizeof(CoreLocalStorage);
}
