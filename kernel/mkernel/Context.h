
#pragma once

#pragma pack (push, 1)
struct Context {
    QWORD           RAX;
    QWORD           RBX;
    QWORD           RCX;
    QWORD           RDX;
    QWORD           RSP;
    QWORD           RBP;
    QWORD           RSI;
    QWORD           RDI;
    QWORD           R8;
    QWORD           R9;
    QWORD           R10;
    QWORD           R11;
    QWORD           R12;
    QWORD           R13;
    QWORD           R14;
    QWORD           R15;
    QWORD           RIP;
    QWORD           RFLAGS;
};

struct SIMDContext {
#define DECLARE_FPREG(x) \
    union { BYTE XMM##x[16]; BYTE YMM##x[32]; BYTE ZMM##x[64]; };
    DECLARE_FPREG(0);
    DECLARE_FPREG(1);
    DECLARE_FPREG(2);
    DECLARE_FPREG(3);
    DECLARE_FPREG(4);
    DECLARE_FPREG(5);
    DECLARE_FPREG(6);
    DECLARE_FPREG(7);
    DECLARE_FPREG(8);
    DECLARE_FPREG(9);
    DECLARE_FPREG(10);
    DECLARE_FPREG(11);
    DECLARE_FPREG(12);
    DECLARE_FPREG(13);
    DECLARE_FPREG(14);
    DECLARE_FPREG(15);
    DECLARE_FPREG(16);
    DECLARE_FPREG(17);
    DECLARE_FPREG(18);
    DECLARE_FPREG(19);
    DECLARE_FPREG(20);
    DECLARE_FPREG(21);
    DECLARE_FPREG(22);
    DECLARE_FPREG(23);
    DECLARE_FPREG(24);
    DECLARE_FPREG(25);
    DECLARE_FPREG(26);
    DECLARE_FPREG(27);
    DECLARE_FPREG(28);
    DECLARE_FPREG(29);
    DECLARE_FPREG(30);
    DECLARE_FPREG(31);
#undef DECLARE_FPREG
};

#pragma pack(pop)
