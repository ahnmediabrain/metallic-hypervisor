
#pragma once

struct MemoryStatus {
    UINTN       Buddies;
    UINTN       TotalPages;
    UINTN       AllocatedPages;
    UINTN       AvailablePages;
};

#define INVALID_MEMORY_ADDRESS  ((UINTN)-1)

// Initializes the memory manager.
void kMemMgrInit();
// Allocates contiguous virtual memory in units of 4KB pages (aligned).
// Note that the allocated memory may not be contiguous in physical memory.
// Use kAllocContiguousPages to allocate contiguous physical memory.
// Returns the mapped virtual address, but nullptr if allocation fails.
void *kAllocPages(UINTN Pages);
// Allocates contiguous physical memory and maps it onto contiguous virtual memory.
// Returns the mapped virtual address, but nullptr if allocation fails.
void *kAllocContiguousPages(UINTN Pages);
// Frees memory allocated by kAllocPages or kAllocContiguousPages.
// Params: Address (virtual address that was allocated)
void kFreePages(void *Address);
// Allocates contiguous virtual memory using the slab allocator.
// Returns the mapped virtual address, but nullptr if allocation fails.
void *kMalloc(UINTN Bytes);
// Frees memory allocated by kFree.
void kFree(void *Address);
// Maps contiguous physical memory onto virtual address space
void *kMapPhysicalMemory(void *PhysicalAddress, UINTN Bytes);
// Unmaps physical memory mapped by kMapPhysicalMemory.
// Returns the physical address corresponding to the virtual address.
UINTN kUnmapPhysicalMemory(void *VirtualAddress);
// Gets the physical address of currently mapped virtual address.
// Returns INVALID_MEMORY_ADDRESS if the address is not mapped.
UINTN kVirt2Phys(UINTN VirtualAddress);
// Querys current memory status.
void kGetMemoryStatus(MemoryStatus *pMemoryStatus);
// Tests the memory allocation system.
void kRunMemoryAllocationTest();
