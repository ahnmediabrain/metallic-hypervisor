
#pragma once


#include "Scheduler.h"

#define K_WAIT_FOREVER      ((UINT64)-1)

// Reentrant, ref-counting spinlock.
// A spinlock is owned by a core that has acquired
// the lock. Note that a spinlock always disables
// interrupt upon acquisition, contrary to mutex.
struct alignas(8) K_SPINLOCK {
    volatile UINTN      OwnerApicID;
    volatile UINT32     RefCount;
    volatile UINT32     InterruptEnable;
};

// Initializes the spinlock data structure.
void kSpinlockInit(K_SPINLOCK *pSpinlock);

// Acquires the spinlock and disables interrupt.
// If the calling core owns the lock, the reference counter is incremented.
void kSpinlockAcquire(K_SPINLOCK *pSpinlock);

// Releases the spinlock by decrementing the reference counter.
// The interruptibility is also restored to the previous state.
void kSpinlockRelease(K_SPINLOCK *pSpinlock);

// The readers-writer spinlock for multiple
// concurrent readers.
// Note that this lock doesn't enable or disable
// interrupts upon acquisition or release,
// contrary to K_SPINLOCK. Hence it should be
// done manually if such measure is needed.
struct alignas(8) K_RWSPINLOCK {
    volatile UINT64     ReaderLock;
    volatile UINT64     WriterLock;
    volatile UINT64     ReaderCount;
};

void kRwSpinlockInit(K_RWSPINLOCK *pLock);
void kRwSpinlockAcquireReader(K_RWSPINLOCK *pLock);
void kRwSpinlockReleaseReader(K_RWSPINLOCK *pLock);
void kRwSpinlockAcquireWriter(K_RWSPINLOCK *pLock);
OS_STATUS kRwSpinlockReleaseWriter(K_RWSPINLOCK *pLock);


// Simple re-entrant mutex.
// Note that acquiring mutex doesn't disable interrupts.
// Also note that a thread can acquire the same mutex
// multiple times.
// Finally, note that a mutex may not be unlocked by another thread.
// Hence, for user-level APIs, the thread ownership check must be
// implemented separately.
struct alignas(8) K_MUTEX {
    volatile UINTN          RefCount;
    volatile ThreadPointle  OwnerThread;
};

// Initializes the mutex data structure.
void kMutexInit(K_MUTEX *pMutex);

// Acquires the mutex.
// Timeout is specified in microseconds (us).
// Here, timeout of zero means immediate return
// and timeout of -1 means infinite waiting.
// Returns E_SUCCESS if acquisition was successful;
// if it timeouts then E_TIMEOUT is returned.
OS_STATUS kMutexAcquire(K_MUTEX *pMutex, UINT64 Timeout = K_WAIT_FOREVER);

// Releases the mutex.
// If the mutex was not in owned state, the procedure fails
// with the error E_SYNCOBJ_NOTOWNED. In this case, the reference
// count is not modified.
OS_STATUS kMutexRelease(K_MUTEX *pMutex);


// Simple semaphore.
struct alignas(8) K_SEMAPHORE {
    volatile UINTN      MaxClients;
    volatile UINTN      RefCount;
};

// Initializes the mutex data structure.
// The MaxClients parameter must be greater than zero
// and less than the maximum value permissible for the integer type.
// (For user-mode calls, the bridge code must perform parameter validation)
void kSemaphoreInit(K_SEMAPHORE *pSemaphore, UINTN MaxClients, UINTN InitialClients);

// Acquires the semaphore.
// Timeout is specified in microseconds (us).
// Here, timeout of zero means immediate return
// and timeout of -1 means infinite waiting.
// Returns E_SUCCESS if acquisition was successful;
// if it timeouts then E_TIMEOUT is returned.
// The number of clients directly after acquisition is returned
// in the pNumClients pointer, if it is not nullptr.
OS_STATUS kSemaphoreAcquire(K_SEMAPHORE *pSemaphore, UINT64 Timeout = K_WAIT_FOREVER, UINTN *pNumClients = nullptr);

// Releases the semaphore.
// The number of clients directly after release is returned
// in the pNumClients pointer, if it is not nullptr.
// If the semaphore was not in owned state, the procedure fails
// with the error E_SYNCOBJ_NOTOWNED. In this case, the reference
// count is not modified.
OS_STATUS kSemaphoreRelease(K_SEMAPHORE *pSemaphore, UINTN *pNumClients);
