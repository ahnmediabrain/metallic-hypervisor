
#include "stdafx.h"
#include "Acpi.h"
#include "AsmUtils.h"
#include "Console.h"
#include "Interrupt.h"
#include "hw/apic/IoApic.h"
#include "Pit.h"
#include "SyncObjs.h"
#include "Time.h"


static volatile UINT64 s_PITCounter = 0;
static volatile UINT64 s_TscFrequency = 0;

typedef UINT64(*GETTIMECOUNTERVALUE)();
typedef UINT64(*GETTIMECOUNTERFREQUENCY)();

static GETTIMECOUNTERVALUE s_GetValue;
static GETTIMECOUNTERFREQUENCY s_GetFrequency;

static bool TscIsAvailable();
static UINT64 TscGetValue();
static UINT64 TscGetFrequency();
static void PitInterruptHandler(void *Unused);
static UINT64 PitGetValue();
static UINT64 PitGetFrequency();


void kInitializeTimeCounterSubsystem() {
    UINT32 IntVec;

    // PIT remains turned on throughout system runtime.
    // Note that, however, its interrupt will remain masked
    // if another interrupt source is available.
    kInitializePITForPeriodicMode();

    // Check if constant-frequency rdtsc is available
    // If available, then use it
    if (TscIsAvailable()) {
        s_GetValue = TscGetValue;
        s_GetFrequency = TscGetFrequency;

        UINT64 Start = kReadTSC();
        // wait for 250 ms
        kPITWaitMicroseconds(250000);
        UINT64 End = kReadTSC();
        // multiply by 4 to get 1000 ms
        s_TscFrequency = (UINT64)(End - Start) * 4;

        kConsolePrintf(TEXT("Time: Using invariant TSC, frequency = %llu\r\n"), s_TscFrequency);
        return;
    }

    // If not, use the PIT roll-over counter
    s_GetValue = PitGetValue;
    s_GetFrequency = PitGetFrequency;
    s_PITCounter = 0;

    IntVec = IRQ_LEGACY_BASE + 0 /* PIT IRQ Number */;
    kRegisterIrqCallback(PitInterruptHandler, 1, IRQ_FLAG_KERNEL,
        nullptr, &IntVec, nullptr, nullptr);

    IOAPIC_INPUT_DESC ioapic_desc;
    kAcpiIsaToIoApicInputDesc(0 /* PIT IRQ Number */, &ioapic_desc);
    
    bool IsLevelTrigger;
    switch (ioapic_desc.Flags & INTIN_TRIGGER_MASK) {
    case INTIN_TRIGGER_LEVEL:
        IsLevelTrigger = true;
        break;
    case INTIN_TRIGGER_EDGE:
    case INTIN_TRIGGER_DEFAULT:
    default:
        IsLevelTrigger = false;
        break;
    }

    bool IsActiveLow;
    switch (ioapic_desc.Flags & INTIN_POL_MASK) {
    case INTIN_POL_ACTIVEHIGH:
        IsActiveLow = false;
        break;
    case INTIN_POL_ACTIVELOW:
    case INTIN_POL_DEFAULT:
    default:
        IsActiveLow = true;
        break;
    }

    kIoApicSetupRedirection(ioapic_desc.VirtualAddress, ioapic_desc.PinNum,
        kApicGetApicID(), IntVec, IsLevelTrigger, IsActiveLow);

    kConsolePrintf(TEXT("Time: Using 8254 PIT\r\n"));
}

UINT64 kGetTimeCounterValue() {
    return s_GetValue();
}

UINT64 kGetTimeCounterFrequency() {
    return s_GetFrequency();
}

void kSpinWait(UINT64 Microseconds) {
    UINT64 StartTime = kGetTimeCounterValue();
    UINT64 Frequency = kGetTimeCounterFrequency();
    UINT64 WaitTicks = (Microseconds * Frequency + 1000000 - 1) / 1000000;

    BOOL bInterruptEnable = kEnableInterrupt(FALSE);
    UINT64 ElapsedTicks;
    do {
        UINT64 CurrentTime = kGetTimeCounterValue();
        ElapsedTicks = (UINT64)(CurrentTime - StartTime);
    } while (ElapsedTicks < WaitTicks);
    kEnableInterrupt(bInterruptEnable);
}

//******** Internal support functions ********//

static bool TscIsAvailable() {
    CPUID_RESULT cpuid;
    kCPUID(0x80000000, 0, &cpuid);
    if (cpuid.EAX >= 0x80000007) {
        // CPUID EAX=0x80000007 with EDX bit 8 set
        // indicates invariant TSC is available.
        // It is not available otherwise.
        kCPUID(0x80000007, 0, &cpuid);
        return !!(cpuid.EDX & 0x100);
    }
    return false;
}
static UINT64 TscGetValue() {
    return kReadTSC();
}
static UINT64 TscGetFrequency() {
    return s_TscFrequency;
}

static void PitInterruptHandler(void *Unused) {
    kInterlockedIncrement64(&s_PITCounter);
    kInitializePITForOneShotMode();
    kInterlockedIncrement64(&s_PITCounter);
}
static UINT64 PitGetValue() {
    while (1) {
        UINT64 CurrentCounter = kInterlockedRead64(&s_PITCounter);
        UINT16 NewValue = kPITReadCounter();
        UINT64 CurrentCounter2 = kInterlockedRead64(&s_PITCounter);
        if (((CurrentCounter % 2) == 0) && CurrentCounter == CurrentCounter2) {
            return (CurrentCounter * 0x10000 / 2) + (0xFFFF - NewValue);
        }
    }
}
static UINT64 PitGetFrequency() {
    return PIT_FREQUENCY;
}
