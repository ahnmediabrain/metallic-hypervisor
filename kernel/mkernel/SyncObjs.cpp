
#include "stdafx.h"
#include "AsmUtils.h"
#include "hw/apic/Apic.h"
#include "Debug.h"
#include "Time.h"
#include "SchedulerCore.h"
#include "SyncObjs.h"


#define NO_OWNER            ((UINTN)-1)
#define SEMAPHORE_LOCKED    ((UINTN)-1)


//******** Spinlock ********//

void kSpinlockInit(K_SPINLOCK *pSpinlock) {
    pSpinlock->RefCount = 0;
    pSpinlock->OwnerApicID = NO_OWNER;
}

void kSpinlockAcquire(K_SPINLOCK *pSpinlock) {
    BOOL bInterruptEnable = kEnableInterrupt(FALSE);
    UINT32 ApicID = kApicGetApicID();
    if (kInterlockedCompareAndExchange64(&pSpinlock->OwnerApicID, ApicID, ApicID) == ApicID) {
        pSpinlock->RefCount++;
    }
    else {
        while (kInterlockedCompareAndExchange64(&pSpinlock->OwnerApicID, NO_OWNER, ApicID) != ApicID);
        kAssert(pSpinlock->OwnerApicID == ApicID);
        pSpinlock->RefCount = 1;
        pSpinlock->InterruptEnable = !!bInterruptEnable;
    }
}

void kSpinlockRelease(K_SPINLOCK *pSpinlock) {
    UINT32 ApicID = kApicGetApicID();
    kAssert(pSpinlock->OwnerApicID == ApicID);
    kAssert(pSpinlock->RefCount >= 1);
    if (pSpinlock->RefCount > 1) {
        pSpinlock->RefCount--;
    }
    else {
        pSpinlock->RefCount = 0;
        BOOL bInterruptEnable = !!pSpinlock->InterruptEnable;
        kAssert(kInterlockedCompareAndExchange64(&pSpinlock->OwnerApicID, ApicID, NO_OWNER) == ApicID);
        kEnableInterrupt(bInterruptEnable);
    }
}

//******** R/W Spinlock ********//

void kRwSpinlockInit(K_RWSPINLOCK *pLock) {
    pLock->ReaderLock = 0;
    pLock->WriterLock = 0;
    pLock->ReaderCount = 0;
}
void kRwSpinlockAcquireReader(K_RWSPINLOCK *pLock) {
    while (kInterlockedCompareAndExchange64(&pLock->ReaderLock, 0, 1) != 0);
    UINT64 ReaderCount = kInterlockedIncrement64(&pLock->ReaderCount);
    if (ReaderCount == 1) {
        kRwSpinlockAcquireWriter(pLock);
    }
    kInterlockedCompareAndExchange64(&pLock->ReaderLock, 1, 0);
}
void kRwSpinlockReleaseReader(K_RWSPINLOCK *pLock) {
    while (kInterlockedCompareAndExchange64(&pLock->ReaderLock, 0, 1) != 0);
    UINT64 ReaderCount = kInterlockedDecrement64(&pLock->ReaderCount);
    if (ReaderCount == 0) {
        kRwSpinlockReleaseWriter(pLock);
    }
    kInterlockedCompareAndExchange64(&pLock->ReaderLock, 1, 0);
}
void kRwSpinlockAcquireWriter(K_RWSPINLOCK *pLock) {
    while (kInterlockedCompareAndExchange64(&pLock->WriterLock, 0, 1) != 0);
}
OS_STATUS kRwSpinlockReleaseWriter(K_RWSPINLOCK *pLock) {
    if (kInterlockedCompareAndExchange64(&pLock->WriterLock, 1, 0) != 1) {
        return E_SYNCOBJ_NOTOWNED;
    }
    return E_SUCCESS;
}

//******** Mutex ********//

void kMutexInit(K_MUTEX *pMutex) {
    pMutex->RefCount = 0;
    pMutex->OwnerThread = 0; /* a valid pointle never equals zero */
}
OS_STATUS kMutexAcquire(K_MUTEX *pMutex, UINT64 Timeout) {
    if (kInterlockedRead64(&pMutex->OwnerThread) == kGetCurrentThread()) {
        pMutex->RefCount++;
        return E_SUCCESS;
    }

    UINT64 StartTime = kGetTimeCounterValue();
    UINT64 Frequency = kGetTimeCounterFrequency();
    while (kInterlockedCompareAndExchange64(&pMutex->RefCount, 0, 1) != 0) {
        if (Timeout != K_WAIT_FOREVER) {
            UINT64 ElapsedTicks = (UINT64)(kGetTimeCounterValue() - StartTime);
            if (Timeout == 0 || ElapsedTicks * 1000000 >= Timeout * Frequency) {
                return E_TIMEOUT;
            }
        }
        kYield();
    }
    pMutex->OwnerThread = kGetCurrentThread();
    return E_SUCCESS;
}
OS_STATUS kMutexRelease(K_MUTEX *pMutex) {
    if (kInterlockedRead64((volatile UINT64 *)&pMutex->OwnerThread) != kGetCurrentThread())
        return E_SYNCOBJ_NOTOWNED;

    if (pMutex->RefCount == 1)
        pMutex->OwnerThread = 0; /* a valid pointle never equals zero */

    kInterlockedDecrement64(&pMutex->RefCount);
    return E_SUCCESS;
}

//******** Semaphore ********//

void kSemaphoreInit(K_SEMAPHORE *pSemaphore, UINTN MaxClients, UINTN InitialClients) {
    kAssert(MaxClients > 0);
    kAssert(MaxClients != SEMAPHORE_LOCKED);
    kAssert(InitialClients <= MaxClients);
    pSemaphore->MaxClients = MaxClients;
    pSemaphore->RefCount = InitialClients;
}

OS_STATUS kSemaphoreAcquire(K_SEMAPHORE *pSemaphore, UINT64 Timeout, UINTN *pNumClients) {
    UINTN MaxClients = pSemaphore->MaxClients;
    UINTN RefCount;
    UINT64 StartTime = kGetTimeCounterValue();
    UINT64 Frequency = kGetTimeCounterFrequency();

    while (1) {
        do {
            RefCount = kInterlockedExchange64(&pSemaphore->RefCount, SEMAPHORE_LOCKED);
        } while (RefCount != SEMAPHORE_LOCKED);
        if (RefCount < MaxClients) {
            kInterlockedExchange64(&pSemaphore->RefCount, RefCount + 1);
            break;
        }
        else {
            kInterlockedExchange64(&pSemaphore->RefCount, RefCount);
            if (Timeout != K_WAIT_FOREVER) {
                UINT64 ElapsedTicks = (UINT64)(kGetTimeCounterValue() - StartTime);
                if (Timeout == 0 || ElapsedTicks * 1000000 >= Timeout * Frequency) {
                    return E_TIMEOUT;
                }
            }
            kYield();
        }
    }
    if (pNumClients != nullptr)
        *pNumClients = RefCount + 1;

    return E_SUCCESS;
}

OS_STATUS kSemaphoreRelease(K_SEMAPHORE *pSemaphore, UINTN *pNumClients) {
    UINTN RefCount;
    do {
        RefCount = kInterlockedExchange64(&pSemaphore->RefCount, SEMAPHORE_LOCKED);
    } while (RefCount != SEMAPHORE_LOCKED);
    if (RefCount == 0) {
        *pNumClients = RefCount;
        kInterlockedExchange64(&pSemaphore->RefCount, RefCount);
        return E_SYNCOBJ_NOTOWNED;
    }
    kInterlockedExchange64(&pSemaphore->RefCount, RefCount - 1);
    if (pNumClients != nullptr)
        *pNumClients = RefCount - 1;
    return E_SUCCESS;
}
