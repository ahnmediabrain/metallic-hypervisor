﻿
#include "stdafx.h"
#include "AsmUtils.h"
#include "Console.h"
#include "Debug.h"
#include "Pit.h"


#define PIT_STATE_PREINIT       0
#define PIT_STATE_PERIODIC      1
#define PIT_STATE_ONESHOT       2
static UINTN PITState = PIT_STATE_PREINIT;

void kInitializePITForPeriodicMode() {
    if (PITState == PIT_STATE_PERIODIC)
        return;

    // Setup PIT as continuous decrementing mode.
    kPortOut8(PIT_PORT_CTL, PIT_CONTROL_COUNTER0 | PIT_MODE2 | PIT_RW_LSBMSB);
    kPortOut8(PIT_PORT_COUNTER0, 0xFF);
    kPortOut8(PIT_PORT_COUNTER0, 0xFF);
    PITState = PIT_STATE_PERIODIC;
}
void kInitializePITForOneShotMode() {
    // Setup PIT as one-shot decrementing mode, starting from 0xFFFF.
    kPortOut8(PIT_PORT_CTL, PIT_CONTROL_COUNTER0 | PIT_MODE0 | PIT_RW_LSBMSB);
    kPortOut8(PIT_PORT_COUNTER0, 0xFF);
    kPortOut8(PIT_PORT_COUNTER0, 0xFF);
    PITState = PIT_STATE_ONESHOT;
}

UINT16 kPITReadCounter() {
    kPortOut8(PIT_PORT_CTL, PIT_CONTROL_COUNTER0 | PIT_RW_LATCH);
    BYTE LowByte = kPortIn8(PIT_PORT_COUNTER0);
    BYTE HighByte = kPortIn8(PIT_PORT_COUNTER0);
    UINT16 CurrentTick = (LowByte) | ((UINT16)HighByte) << 8;
    return CurrentTick;
}

void kPITWaitMicroseconds(UINT64 microseconds) {
    kAssert(PITState == PIT_STATE_PERIODIC);

    // Poll-wait until the counter reaches specified value
    UINT64 TotalClocks = (microseconds * PIT_FREQUENCY + 1000000ULL - 1) / 1000000ULL;
    UINT64 Ticks = 0;
    UINT16 PreviousTick = kPITReadCounter();
    while (Ticks < TotalClocks) {
        UINT16 CurrentTick = kPITReadCounter();
        UINT16 TickElapsed = (UINT16)(PreviousTick - CurrentTick); // UINT16 casting is mandatory to make it wrap
        PreviousTick = CurrentTick;
        Ticks += TickElapsed;
    }
}

void kTestPIT() {
    for (UINT64 i = 0; ; i++) {
        kPITWaitMicroseconds(1000000);
        kConsolePrintf(TEXT("PIT Test: %lld seconds elapsed\r\n"), i);
    }
}
