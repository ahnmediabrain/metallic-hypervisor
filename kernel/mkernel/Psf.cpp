﻿
#include "stdafx.h"
#include "StdLib.h"
#include "Psf.h"

OS_STATUS Psf1Init(void *VoidInputStream, PSF1_HANDLE *pHdl)
{
    unsigned char *InputStream = (unsigned char *)VoidInputStream;
    PSF1_HEADER *pHdr = (PSF1_HEADER *)InputStream;
    if (pHdr->Magic != PSF1_MAGIC)
        return E_INVALID_PARAMETER;
    if (pHdr->Mode > PSF1_MODE_512_U)
        return E_UNSUPPORTED;
    if (pHdr->LineHeight == 0 || pHdr->LineHeight > 16)
        return E_UNSUPPORTED;

    pHdl->LineHeight = pHdr->LineHeight;

    bool MappingTablePresent = false;
    switch (pHdr->Mode) {
    case PSF1_MODE_256_U:
        MappingTablePresent = true;
    case PSF1_MODE_256_NU:
        pHdl->PaletteCount = 256;
        break;
    case PSF1_MODE_512_U:
        MappingTablePresent = true;
    case PSF1_MODE_512_NU:
        pHdl->PaletteCount = 512;
        break;
    }

    for (UINTN i = 0; i < pHdl->PaletteCount; i++) {
        unsigned char *addr = InputStream + sizeof(PSF1_HEADER);
        addr += i * pHdl->LineHeight;
        kMemCpy(pHdl->Data[i].Pixels, addr, pHdl->LineHeight);
    }

    for (unsigned short i = 0; i < pHdl->PaletteCount; i++) {
        pHdl->CharMap[i] = 0xFFFF; // Initialize as invalid
    }
    if (MappingTablePresent) {
        UINTN Offset = sizeof(PSF1_HEADER) + pHdl->LineHeight * pHdl->PaletteCount;
        unsigned short *MappingTable = (unsigned short *)(InputStream + Offset);
        for (unsigned short i = 0; i < pHdl->PaletteCount; i++) {
            unsigned short Entry;
            while ((Entry = *MappingTable++) != 0xFFFF) {
                pHdl->CharMap[Entry] = i;
            }
        }
    }
    else {
        // Identity mapping
        for (unsigned short i = 0; i < pHdl->PaletteCount; i++) {
            pHdl->CharMap[i] = i;
        }
    }

    return E_SUCCESS;
}

UINT32 Psf1GetCharWidth(PSF1_HANDLE *pHdl)
{
    return 8;
}
UINT32 Psf1GetCharHeight(PSF1_HANDLE *pHdl)
{
    return pHdl->LineHeight;
}
