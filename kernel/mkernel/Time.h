
#pragma once


// Initializes the time counter subsystem.
// This procedure must be called before any calls
// to any of the other time counter-related procedures.
// Note that after the call to this procedure,
// the kPITWaitMicroseconds() procedure can no longer
// be used. So any task that requires PIT must be
// performed prior to calling this procedure.
void kInitializeTimeCounterSubsystem();

// Queries the current time counter value.
// This time counter is guaranteed to be
// incremented at a fixed frequency that is
// reported by kGetTimerCounterFrequency.
// The counter value is the same across all
// processors on the machine.
// Note that the start value of the counter
// is not guaranteed to be any fixed value,
// so the caller must only use relative timing
// or calibrate the counter with an external
// time source.
UINT64 kGetTimeCounterValue();

// Queries the time counter frequency (per second).
// This function only needs to be called once
// since the frequency is fixed at boot and
// does not change afterwards.
UINT64 kGetTimeCounterFrequency();

// Spin-wait for the specified duration
// without context switching.
// Interrupt is disabled during waiting.
void kSpinWait(UINT64 Microseconds);
