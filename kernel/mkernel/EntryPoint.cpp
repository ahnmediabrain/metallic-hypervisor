﻿
#include "stdafx.h"
#include "Acpi.h"
#include "AsmUtils.h"
#include "APEntryPoint.h"
#include "Console.h"
#include "CoreLocalStorage.h"
#include "Descriptor.h"
#include "Interrupt.h"
#include "GlobalVariable.h"
#include "Debug.h"
#include "MemMgr.h"
#include "Shell.h"
#include "StdLib.h"
#include "Scheduler.h"
#include "SchedulerCore.h"
#include "Pci.h"
#include "Pit.h"
#include "Ramdisk.h"
#include "Time.h"
#include "VisualCppConstructors.h"

#include "hw/apic/Apic.h"
#include "vm/VM.h"


static void kCommonEntryPoint();

void kPrintTestGraphics(K_GRAPHICS_MODE_DESC *pGMDesc) {
    UINTN bytePerPixel = pGMDesc->videoDepth / 8;
    UINT32 i;
    for (i = 0; i < pGMDesc->resVert / 2; i++) {
        for (UINT32 j = 0; j < pGMDesc->resHorz; j++) {
            unsigned char *ptr = (unsigned char *)pGMDesc->framebuffer_base_virt;
            ptr += (i * pGMDesc->resHorz + j) * bytePerPixel;
            *ptr++ = 255;
            *ptr++ = 0;
            *ptr++ = 0;
        }
    }
    for (; i < pGMDesc->resVert; i++) {
        for (UINT32 j = 0; j < pGMDesc->resHorz; j++) {
            unsigned char *ptr = (unsigned char *)pGMDesc->framebuffer_base_virt;
            ptr += (i * pGMDesc->resHorz + j) * bytePerPixel;
            *ptr++ = 0;
            *ptr++ = 255;
            *ptr++ = 255;
        }
    }
}

// Entry Point for BSP
void OSAPI kEntryPoint(K_KERNEL_ENTRY_DATA *Data) {
    VisualCppCallConstructors();

    kEnableInterrupt(FALSE);
    g_KernelEntryData = Data;

    kConsoleInitAsDevNull();
    kMemMgrInit();

    /* Enable Local APIC */
    OS_STATUS e = kApicInitializeSubsystem();
    if (E_FAILED(e)) {
        kConsolePrintf(TEXT("Fatal: Failed to initialize APIC subsystem\r\n"));
        kHang();
    }
    kApicEnableApic();

    /* Map graphics framebuffer region */
    K_GRAPHICS_MODE_DESC *pGMDesc = &Data->GraphicsModeDesc;
    pGMDesc->framebuffer_base_virt = (UINTN)kMapPhysicalMemory((void *)pGMDesc->framebuffer_base_phys, pGMDesc->framebuffer_size);

    RamdiskFileDesc FontFile;
    if (E_FAILED(kRamdiskFindFile(TEXT("FONTFILE"), &FontFile))) {
        kPrintTestGraphics(pGMDesc);
        kHang(); /* As kernel cannot return to somewhere upon error, we just hang up here */
    }

    /* Print basic kernel information */
    kConsoleInit(&Data->GraphicsModeDesc, FontFile.Data);
    kConsolePuts(TEXT("Metallic Kernel\r\nCopyright (C) 2013-2018 AhnMedia\r\n\r\n"));
    kConsolePrintf(TEXT("Kernel Loaded at %p, Size = %llu\r\n"), Data->Kernel_Addr, Data->KernelImageSize);
    kConsolePrintf(TEXT("Graphics Buffer base address is %p(phys), %p(virt)\r\n"),
        pGMDesc->framebuffer_base_phys, pGMDesc->framebuffer_base_virt);

    /* Print memory information */
    MemoryStatus ms;
    kGetMemoryStatus(&ms);
    kConsolePrintf(TEXT("Total Memory: %llu bytes (%llu regions)\r\n"), ms.TotalPages * 4096, ms.Buddies);
    kConsolePrintf(TEXT("Available Memory: %llu bytes\r\n"), ms.AvailablePages * 4096);

    /* Measure Local APIC timer frequency */
    kInitializePITForPeriodicMode();
    kApicTimerStart(0xFFFFFFFF, false, 0);
    kPITWaitMicroseconds(1000000);
    g_ApicTimerFreq = 0xFFFFFFFF - kApicTimerReadCounter();
    kApicTimerStop();
    kConsolePrintf(TEXT("Local APIC Timer has frequency %llu\r\n"), g_ApicTimerFreq);

    /* Parse ACPI table */
    if (!Data->AcpiDesc.Available) {
        kConsolePuts(TEXT("Fatal: ACPI 2.0 or higher is not available\r\n"));
        kConsolePuts(TEXT("Halting; Please reset the system\r\n"));
        kHang();
    }
    kInitializeAcpiSubsystem();
    kAcpiParse(Data->AcpiDesc.RsdpAddr);

    /* Setup descriptors */
    kInitializeIrqSubsystem();
    kSetupGDT();
    kInstallIDT();
    kEnableInterrupt(TRUE);
    kTestInterrupt();
    kTestInterrupt();

    /* Initialize Timer (necessary for ACPICA) */
    kInitializeTimeCounterSubsystem();

    /* Load ACPICA */
    e = kAcpiInitializeAcpica();
    if (E_FAILED(e)) {
        kHang();
    }

    /*
        VMConfig vmc;
        vmc.VCpuCount = 2;
        vmc.MemoryInBytes = 1048576 * 10;
        VMHandle vmh;
        kVMCreate(&vmc, &vmh);
        kVMPowerOn(vmh);
    */

    kInitializePciSubsystem();
    kInitializeScheduler();

    /* Wake up other cores as we're done initializing subsystems */
    kStartApplicationProcessors();

    /* Enter loop! */
    kCommonEntryPoint();
}

// Entry Point for AP
extern "C" void kAPEntryPoint() {
    /* signal kernel for AP kernel entry */
    kInterlockedIncrement64(&g_APCount);

    /* setup interrupt-related things */
    kApicEnableApic();
    kSetupGDT();
    kInstallIDT();
    kEnableInterrupt(TRUE);
    kConsolePrintf(TEXT("AP %u is in kernel! Yahoo!\r\n"), kApicGetApicID());

    kTestInterrupt();
    kTestInterrupt();
    kConsolePrintf(TEXT("AP %u: CLS = %p, Stack top = %p, size = %llu\r\n"),
        kApicGetApicID(),
        kGetCoreLocalStorage(),
        (UINTN)kGetCoreLocalStorage() + kGetCoreLocalStorage()->StackSize, kGetCoreLocalStorage()->StackSize);

    /* Enter loop! */
    kCommonEntryPoint();
}

static void kZeroIOPL() {
    // sets IOPL to zero
    UINT64 RFlags = kReadRFlags();
    RFlags &= ~(0b11ULL << 12);
    kWriteRFlags(RFlags);
}


/* Common Entry Point
 * (interrupt is enabled upon entry) */
static void kCommonEntryPoint() {
    kZeroIOPL();
    //kEnterVMXOperation();

    if (kIsBSP()) {
        kTestThreading();

        CreateThreadConfig threadConfig;
        threadConfig.Parent = EMPTY_HANDLE;
        threadConfig.EntryPoint = kShellThread;
        threadConfig.StackSize = KERNEL_THREAD_STACKSZ_DEFAULT;
        threadConfig.UserData = nullptr; // unused
        kCreateThread(&threadConfig, nullptr);
    }

    kStartScheduler();
}
