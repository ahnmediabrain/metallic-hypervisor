
#pragma once


#include "hw/apic/ApicInternal.h"

ApicInterface *kX2ApicTryGetInterface();


/* x2APIC access MSR indices */

#define X2APIC_ID_REG                   0x802
#define X2APIC_EOI                      0x80B
#define X2APIC_TPR                      0x808
#define X2APIC_SPURIOUS_INT_VEC_REG     0x80F
#define X2APIC_CMCI_LVT                 0x82F
#define X2APIC_INT_CMD_REG              0x830
#define X2APIC_TIMER_LVT                0x832
#define X2APIC_THERMSENS_LVT            0x833
#define X2APIC_PERFCNT_LVT              0x834
#define X2APIC_LINT0_LVT                0x835
#define X2APIC_LINT1_LVT                0x836
#define X2APIC_ERROR_LVT                0x837
#define X2APIC_TIMER_INITCOUNT          0x838
#define X2APIC_TIMER_CURCOUNT           0x839
#define X2APIC_TIMER_DIVCONFIG          0x83E

