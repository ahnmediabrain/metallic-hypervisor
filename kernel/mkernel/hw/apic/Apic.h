
/* Include file for unified APIC access APIs.
 * Note: here, 'APIC' means Local APIC and
 *       'I/O APIC' means I/O APIC. ('Local' is omitted)
 */

#pragma once

/* ICR (interrupt command register) Flags */

#define LOCAL_APIC_DELIV_FIXED                  (0b000 << 8)
#define LOCAL_APIC_DELIV_LOWPR                  (0b001 << 8)
#define LOCAL_APIC_DELIV_INIT                   (0b101 << 8)
#define LOCAL_APIC_DELIV_SIPI                   (0b110 << 8)

#define LOCAL_APIC_SEND_PENDING                 (1 << 12) /* Only applicable to xAPIC (not x2APIC) */

#define LOCAL_APIC_DEASSERT                     (0 << 14)
#define LOCAL_APIC_ASSERT                       (1 << 14)

#define LOCAL_APIC_TRIGGER_EDGE                 (0 << 15)
#define LOCAL_APIC_TRIGGER_LEVEL                (1 << 15)

#define LOCAL_APIC_DST_NO_SHORTHAND             (0b00 << 18)
#define LOCAL_APIC_DST_SELF                     (0b01 << 18)
#define LOCAL_APIC_DST_ALL                      (0b10 << 18)
#define LOCAL_APIC_DST_ALL_NOSELF               (0b11 << 18)

/* LVT Flags */

#define LOCAL_APIC_LVT_MASKED                   (1 << 16)

/* Local APIC access APIs */

OS_STATUS kApicInitializeSubsystem();
void kApicEnableApic(); /* Enables and setups local APIC on current core */

UINT32 kApicGetMaxApicID(); /* Gets how many processors are addressable */
UINT32 kApicGetMaxIntRoutableApicID(); /* Gets how many processors are targeted for interrupt delivery */

UINT32 kApicGetApicID(); /* Gets the APIC ID of current core */
bool kIsBSP(); /* Checks if current core is BSP */
void kApicSendEoi(); /* Sends EOI to Local APIC */

/* Starts Local APIC timer on current core.
 * @param InitialCounter : must be greater than zero
 * @param Periodic : periodic if true, one-shot if false
 * @param IntVec   : >0 to generate interrupt, =0 to not
 */
void kApicTimerStart(UINT32 InitialCounter, bool Periodic, UINT32 IntVec);
UINT32 kApicTimerReadCounter(); /* Reads current counter value */
void kApicTimerStop(); /* Stops Local APIC timer on current core */

/* Sends IPI (inter-processor interrupt) to another core.
 * @param Flags : Lower 32-bits of the Interrupt Command Register.
 * @param TargetApicID : Target core to signal. Honored only when 
 *   LOCAL_APIC_DST_NO_SHORTHAND is specified.
 */
void kApicSendIPI(UINT32 Flags, UINT32 TargetApicID);

/* Wakes up the specified processor.
 * A value of 0xFFFF_FFFF is not supported.
 * Note that this requires the time counting subsystem for proper timing of
 * the modified version of the Intel Universal Start-up Algorithm.
 */
OS_STATUS kApicWakeUpProcessor(UINT32 TargetApicID, UINT32 StartAddress, bool(*CheckWake)());
