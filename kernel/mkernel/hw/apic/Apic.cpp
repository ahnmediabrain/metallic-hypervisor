
#include "stdafx.h"
#include "AsmUtils.h"
#include "Debug.h"
#include "Time.h"
#include "hw/apic/Apic.h"
#include "hw/apic/ApicInternal.h"
#include "hw/apic/xApic.h"
#include "hw/apic/x2Apic.h"


static ApicInterface *s_Impl = nullptr;


OS_STATUS kApicInitializeSubsystem() {
    s_Impl = kX2ApicTryGetInterface();
    if (s_Impl != nullptr)
        return s_Impl->InitializeSubsystem();

    s_Impl = kXApicTryGetInterface();
    if (s_Impl != nullptr)
        return s_Impl->InitializeSubsystem();

    return E_UNSUPPORTED;
}

/* Wrapper implementation */

void kApicEnableApic() {
    s_Impl->EnableApic();
}

UINT32 kApicGetMaxApicID() {
    return s_Impl->GetMaxApicID();
}
UINT32 kApicGetMaxIntRoutableApicID() {
    return s_Impl->GetMaxIntRoutableApicID();
}

UINT32 kApicGetApicID() {
    return s_Impl->GetApicID();
}
bool kIsBSP() {
    return s_Impl->IsBSP();
}
void kApicSendEoi() {
    return s_Impl->SendEoi();
}

void kApicTimerStart(UINT32 InitialCounter, bool Periodic, UINT32 IntVec) {
    s_Impl->TimerStart(InitialCounter, Periodic, IntVec);
}
UINT32 kApicTimerReadCounter() {
    return s_Impl->TimerReadCounter();
}
void kApicTimerStop() {
    return s_Impl->TimerStop();
}

void kApicSendIPI(UINT32 Flags, UINT32 TargetApicID) {
    s_Impl->SendIPI(Flags, TargetApicID);
}

OS_STATUS kApicWakeUpProcessor(UINT32 TargetApicID, UINT32 StartAddress, bool(*CheckWake)()) {
    /* StartAddress must be in the form of 0x000XY000 */
    if ((StartAddress & 0xFFF) != 0)
        return E_INVALID_PARAMETER;
    if (StartAddress >= 0x100000)
        return E_INVALID_PARAMETER;
    /* Broadcast IPI is not supported */
    if (TargetApicID > kApicGetMaxApicID())
        return E_INVALID_PARAMETER;

    UINT32 IcrLow_INIT = LOCAL_APIC_DELIV_INIT | LOCAL_APIC_ASSERT |
        LOCAL_APIC_TRIGGER_LEVEL | LOCAL_APIC_DST_NO_SHORTHAND;
    kApicSendIPI(IcrLow_INIT, TargetApicID);
    kSpinWait(10000); /* wait 10 ms */

    UINT32 IcrLow_SIPI = LOCAL_APIC_DELIV_SIPI | LOCAL_APIC_ASSERT |
        LOCAL_APIC_TRIGGER_EDGE | LOCAL_APIC_DST_NO_SHORTHAND |
        ((BYTE)(StartAddress >> 12));

    for (int tries = 0; tries < 4; tries++) {
        kApicSendIPI(IcrLow_SIPI, TargetApicID);
        kSpinWait(5000); /* wait 5 ms */
        if (CheckWake())
            return E_SUCCESS;
    }

    return E_HWERROR;
}
