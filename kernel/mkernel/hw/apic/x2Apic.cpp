
#include "stdafx.h"
#include <MSR.h>
#include "AsmUtils.h"
#include "hw/apic/Apic.h"
#include "hw/apic/x2Apic.h"


ApicInterface s_X2ApicImpl;

static OS_STATUS x2ApicInitializeSubsystem();
static void x2ApicEnableApic();
static UINT32 x2ApicGetMaxApicID();
static UINT32 x2ApicGetMaxIntRoutableApicID();
static UINT32 x2ApicGetApicID();
static bool x2ApicIsBSP();
static void x2ApicSendEoi();
static void x2ApicTimerStart(UINT32 InitialCounter, bool Periodic, UINT32 IntVec);
static UINT32 x2ApicTimerReadCounter();
static void x2ApicTimerStop();
static void x2ApicSendIPI(UINT32 Flags, UINT32 TargetApicID);

ApicInterface *kX2ApicTryGetInterface() {
    /* ensure that x2APIC is supported */
    CPUID_RESULT cpuid;
    kCPUID(1, 0, &cpuid);
    if (!(cpuid.ECX & (1 << 21)))
        return nullptr;

    s_X2ApicImpl.InitializeSubsystem = x2ApicInitializeSubsystem;
    s_X2ApicImpl.EnableApic = x2ApicEnableApic;
    s_X2ApicImpl.GetMaxApicID = x2ApicGetMaxApicID;
    s_X2ApicImpl.GetMaxIntRoutableApicID = x2ApicGetMaxIntRoutableApicID;
    s_X2ApicImpl.GetApicID = x2ApicGetApicID;
    s_X2ApicImpl.IsBSP = x2ApicIsBSP;
    s_X2ApicImpl.SendEoi = x2ApicSendEoi;
    s_X2ApicImpl.TimerStart = x2ApicTimerStart;
    s_X2ApicImpl.TimerReadCounter = x2ApicTimerReadCounter;
    s_X2ApicImpl.TimerStop = x2ApicTimerStop;
    s_X2ApicImpl.SendIPI = x2ApicSendIPI;

    return &s_X2ApicImpl;
}


/* x2APIC implementation */

static OS_STATUS x2ApicInitializeSubsystem() {
    /* no special work to do */
    return E_SUCCESS;
}

static void x2ApicEnableApic() {
    /* Enable x2APIC MSR access.
     * Note that conventional APIC enable bit must also be set */
    UINT64 ApicBaseReg = kReadMSR(IA32_APIC_BASE);
    ApicBaseReg |= IA32_APIC_BASE_ENABLE;
    ApicBaseReg |= IA32_APIC_BASE_ENABLE_X2APIC;
    kWriteMSR(IA32_APIC_BASE, ApicBaseReg);

    /* Set local enable bit */
    UINT64 SpuriousIntVectorReg = kReadMSR(X2APIC_SPURIOUS_INT_VEC_REG);
    SpuriousIntVectorReg |= LOCAL_APIC_DELIV_LOWPR;
    SpuriousIntVectorReg |= 0xFF; // spurious interrupt vector
    kWriteMSR(X2APIC_SPURIOUS_INT_VEC_REG, SpuriousIntVectorReg);
    kWriteMSR(X2APIC_TPR, 0); // receive all interrupts

    /* Mask LVT entries */
    kWriteMSR(X2APIC_TIMER_LVT, LOCAL_APIC_LVT_MASKED);
    kWriteMSR(X2APIC_CMCI_LVT, LOCAL_APIC_LVT_MASKED);
    kWriteMSR(X2APIC_LINT0_LVT, LOCAL_APIC_LVT_MASKED);
    kWriteMSR(X2APIC_LINT1_LVT, LOCAL_APIC_LVT_MASKED);
    kWriteMSR(X2APIC_ERROR_LVT, LOCAL_APIC_LVT_MASKED);
    kWriteMSR(X2APIC_PERFCNT_LVT, LOCAL_APIC_LVT_MASKED);
    kWriteMSR(X2APIC_THERMSENS_LVT, LOCAL_APIC_LVT_MASKED);
}
static UINT32 x2ApicGetMaxApicID() {
    return 0xFFFFFFFE;
}
static UINT32 x2ApicGetMaxIntRoutableApicID() {
    return 0xE;
}
static UINT32 x2ApicGetApicID() {
    return (UINT32)kReadMSR(X2APIC_ID_REG);
}
static bool x2ApicIsBSP() {
    return !!(kReadMSR(IA32_APIC_BASE) & IA32_APIC_BASE_BSP);
}
static void x2ApicSendEoi() {
    kWriteMSR(X2APIC_EOI, 0);
}
static void x2ApicTimerStart(UINT32 InitialCounter, bool Periodic, UINT32 IntVec) {
    UINT32 TimerLVT = 0;
    TimerLVT |= (Periodic) ? (0b01 << 17) : (0b00 /* one-shot */ << 17);
    if (IntVec == 0) TimerLVT |= LOCAL_APIC_LVT_MASKED;
    else TimerLVT |= IntVec;
    x2ApicTimerStop();

    kWriteMSR(X2APIC_TIMER_DIVCONFIG, 0b0001 /* divide by 4 */);
    kWriteMSR(X2APIC_TIMER_LVT, TimerLVT); /* bits[18:17]=01 -> periodic */
    kWriteMSR(X2APIC_TIMER_INITCOUNT, InitialCounter); /* start timer */
}
static UINT32 x2ApicTimerReadCounter() {
    /* For x2APIC, zero must be written in order to notify EOI.
     * Any non-zero value will cause #GP. */
    return (UINT32)kReadMSR(X2APIC_TIMER_CURCOUNT);
}
static void x2ApicTimerStop() {
    /* Writing zero to the timer initial count register effectively
     * disables the timer, both in one-shot and periodic mode. */
    kWriteMSR(X2APIC_TIMER_INITCOUNT, 0);
}

static void x2ApicSendIPI(UINT32 Flags, UINT32 TargetApicID) {
    /* The Pending bit is not necessary per x2APIC spec */
    kWriteMSR(X2APIC_INT_CMD_REG, Flags | (((UINT64)TargetApicID) << 32));
}
