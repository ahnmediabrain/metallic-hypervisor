
#pragma once


#include "hw/apic/ApicInternal.h"

ApicInterface *kXApicTryGetInterface();


/* Local APIC MMIO register indices */

#define LOCAL_APIC_ID_REG                       0x020
#define LOCAL_APIC_VER_REG                      0x030       // Local APIC version register
#define LOCAL_APIC_TPR                          0x080       // Task-priority register
#define LOCAL_APIC_PPR                          0x0A0       // Processor-priority register
#define LOCAL_APIC_EOI                          0x0B0       // End-of-interrupt register
#define LOCAL_APIC_LDR                          0x0D0       // Logical destination register
#define LOCAL_APIC_DFR                          0x0E0       // Destination format register
#define LOCAL_APIC_SPURIOUS_INT_VEC_REG         0x0F0
#define LOCAL_APIC_CMCI_LVT                     0x2F0
#define LOCAL_APIC_INT_CMD_REG_LOW              0x300
#define LOCAL_APIC_INT_CMD_REG_HIGH             0x310
#define LOCAL_APIC_TIMER_LVT                    0x320
#define LOCAL_APIC_THERMSENS_LVT                0x330
#define LOCAL_APIC_PERFCNT_LVT                  0x340
#define LOCAL_APIC_LINT0_LVT                    0x350
#define LOCAL_APIC_LINT1_LVT                    0x360
#define LOCAL_APIC_ERROR_LVT                    0x370
#define LOCAL_APIC_TIMER_INITCOUNT              0x380
#define LOCAL_APIC_TIMER_CURCOUNT               0x390
#define LOCAL_APIC_TIMER_DIVCONFIG              0x3E0
#define LOCAL_APIC_AMD_EXTENDED_FEATURE         0x400
#define LOCAL_APIC_AMD_EXTENDED_CONTROL         0x410

