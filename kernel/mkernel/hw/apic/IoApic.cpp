
#include "stdafx.h"
#include "hw/apic/Apic.h"
#include "hw/apic/IoApic.h"


//******** I/O APIC APIs ********//

void kIoApicMaskAll(UINT64 VirtualBase) {
    UINT32 InputCount = kIoApicGetInputCount(VirtualBase);
    for (UINT32 PinNum = 0; PinNum < InputCount; PinNum++) {
        kIoApicDisableRedirection(VirtualBase, PinNum);
    }
}

UINT32 kIoApicReadReg32(UINT64 VirtualBase, UINT32 Index) {
    volatile UINT32 *IndexPtr = (volatile UINT32 *)(VirtualBase + 0x00);
    volatile UINT32 *DataPtr = (volatile UINT32 *)(VirtualBase + 0x10);

    *IndexPtr = Index;
    UINT32 Data = *DataPtr;
    return Data;
}
void kIoApicWriteReg32(UINT64 VirtualBase, UINT32 Index, UINT32 Value) {
    volatile UINT32 *IndexPtr = (volatile UINT32 *)(VirtualBase + 0x00);
    volatile UINT32 *DataPtr = (volatile UINT32 *)(VirtualBase + 0x10);

    *IndexPtr = Index;
    *DataPtr = Value;
}

UINT32 kIoApicGetInputCount(UINT64 VirtualBase) {
    UINT32 VerReg = kIoApicReadReg32(VirtualBase, IOAPIC_REG_VER);
    return ((VerReg >> 16) & 0xFF) + 1;
}

void kIoApicSetupRedirection(UINT64 VirtualBase, UINT32 PinNum, BYTE TargetApicID, UINT32 IntVec,
    bool IsLevelTrigger, bool IsActiveLow) {
    UINT64 Value = 0;
    Value |= ((UINT64)TargetApicID) << 56;
    Value |= IOAPIC_REDTBL_DSTLOGICAL;
    Value |= (IsLevelTrigger) ? (IOAPIC_REDTBL_LVTRIGGER) : (IOAPIC_REDTBL_EGTRIGGER);
    Value |= (IsActiveLow) ? (IOAPIC_REDTBL_ACTIVELOW) : (IOAPIC_REDTBL_ACTIVEHIGH);
    Value |= LOCAL_APIC_DELIV_FIXED;
    Value |= (UINT8)IntVec;

    /* disable routing temporarily to avoid stale routing */
    kIoApicDisableRedirection(VirtualBase, PinNum);

    /* clear masked flag later (write high dword first) */
    UINT32 Index = IOAPIC_REG_REDTBL_BASE + PinNum * 2;
    kIoApicWriteReg32(VirtualBase, Index + 1, (UINT32)(Value >> 32));
    kIoApicWriteReg32(VirtualBase, Index, (UINT32)Value);
}

void kIoApicChangeRedirection(UINT64 VirtualBase, UINT32 PinNum, BYTE NewTargetApicID) {
    /* Read high part of the redirection table entry
     * and replace the target APIC ID part with the given one */
    UINT32 Index = IOAPIC_REG_REDTBL_BASE + PinNum * 2;
    UINT32 Value = kIoApicReadReg32(VirtualBase, Index + 1);
    Value &= ~0xFF000000;
    Value |= ((UINT32)NewTargetApicID) << 24;
    kIoApicWriteReg32(VirtualBase, Index, Value);
}

void kIoApicDisableRedirection(UINT64 VirtualBase, UINT32 PinNum) {
    /* only need to set the masked flag */
    UINT32 Index = IOAPIC_REG_REDTBL_BASE + (PinNum * 2);
    kIoApicWriteReg32(VirtualBase, Index, (UINT32)IOAPIC_REDTBL_MASKED);
}
