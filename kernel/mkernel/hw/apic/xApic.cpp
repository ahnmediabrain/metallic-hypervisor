
#include "stdafx.h"
#include <MSR.h>
#include "AsmUtils.h"
#include "MemMgr.h"
#include "hw/apic/Apic.h"
#include "hw/apic/xApic.h"


ApicInterface s_XApicImpl;
static struct {
    UINTN       MmioPhysAddr;
    UINTN       MmioVirtAddr;
    bool        ExtendedApicIDSupport;
} s_XApicData;

static OS_STATUS xApicInitializeSubsystem();
static void xApicEnableApic();
static UINT32 xApicGetMaxApicID();
static UINT32 xApicGetMaxIntRoutableApicID();
static UINT32 xApicGetApicID();
static bool xApicIsBSP();
static void xApicSendEoi();
static void xApicTimerStart(UINT32 InitialCounter, bool Periodic, UINT32 IntVec);
static UINT32 xApicTimerReadCounter();
static void xApicTimerStop();
static void xApicSendIPI(UINT32 Flags, UINT32 TargetApicID);

static UINT32 xApicReadReg32(UINT32 Offset);
static void xApicWriteReg32(UINT32 Offset, UINT32 Value);

ApicInterface *kXApicTryGetInterface() {
    /* ensure that (x)APIC is supported */
    CPUID_RESULT cpuid;
    kCPUID(1, 0, &cpuid);
    if (!(cpuid.EDX & (1 << 9)))
        return nullptr;

    s_XApicImpl.InitializeSubsystem = xApicInitializeSubsystem;
    s_XApicImpl.EnableApic = xApicEnableApic;
    s_XApicImpl.GetMaxApicID = xApicGetMaxApicID;
    s_XApicImpl.GetMaxIntRoutableApicID = xApicGetMaxIntRoutableApicID;
    s_XApicImpl.GetApicID = xApicGetApicID;
    s_XApicImpl.IsBSP = xApicIsBSP;
    s_XApicImpl.SendEoi = xApicSendEoi;
    s_XApicImpl.TimerStart = xApicTimerStart;
    s_XApicImpl.TimerReadCounter = xApicTimerReadCounter;
    s_XApicImpl.TimerStop = xApicTimerStop;
    s_XApicImpl.SendIPI = xApicSendIPI;

    return &s_XApicImpl;
}


/* xAPIC implementation */

static OS_STATUS xApicInitializeSubsystem() {
    /* Enable Local APIC MMIO */
    UINT64 ApicBaseReg = kReadMSR(IA32_APIC_BASE);
    if (!(ApicBaseReg & IA32_APIC_BASE_ENABLE)) {
        kWriteMSR(IA32_APIC_BASE, ApicBaseReg | IA32_APIC_BASE_ENABLE);
    }

    /* Map MMIO region onto virtual memory */
    s_XApicData.MmioPhysAddr = ApicBaseReg & ~0xFFFULL;
    s_XApicData.MmioVirtAddr = (UINTN)kMapPhysicalMemory((void *)s_XApicData.MmioPhysAddr, 4096);
    if (s_XApicData.MmioVirtAddr == (UINTN)nullptr)
        return E_OUTOFRESOURCE;

    /* Enable extended APIC ID if supported */
    UINT32 VerReg = xApicReadReg32(LOCAL_APIC_VER_REG);
    if (VerReg & (1UL << 31)) {
        UINT32 ExtFtReg = xApicReadReg32(LOCAL_APIC_AMD_EXTENDED_FEATURE);
        if (ExtFtReg & (1UL << 2)) {
            UINT32 ExtCtReg = xApicReadReg32(LOCAL_APIC_AMD_EXTENDED_CONTROL);
            ExtCtReg |= (1ULL << 2); /* Extended APIC ID Enable bit */
            xApicWriteReg32(LOCAL_APIC_AMD_EXTENDED_CONTROL, ExtCtReg);
            s_XApicData.ExtendedApicIDSupport = true;
        }
        else {
            s_XApicData.ExtendedApicIDSupport = false;
        }
    }
    else {
        s_XApicData.ExtendedApicIDSupport = false;
    }

    return E_SUCCESS;
}

static void xApicEnableApic() {
    /* Enable Local APIC MMIO */
    UINT64 ApicBaseReg = kReadMSR(IA32_APIC_BASE);
    ApicBaseReg &= ~0x000FFFFFFFFFF000ULL;
    ApicBaseReg |= s_XApicData.MmioPhysAddr;
    ApicBaseReg |= IA32_APIC_BASE_ENABLE;
    kWriteMSR(IA32_APIC_BASE, ApicBaseReg);

    /* Set local enable bit */
    UINT32 SpuriousIntVectorReg = xApicReadReg32(LOCAL_APIC_SPURIOUS_INT_VEC_REG);
    SpuriousIntVectorReg |= LOCAL_APIC_DELIV_LOWPR;
    SpuriousIntVectorReg |= 0xFF; // spurious interrupt vector
    xApicWriteReg32(LOCAL_APIC_SPURIOUS_INT_VEC_REG, SpuriousIntVectorReg);
    xApicWriteReg32(LOCAL_APIC_TPR, 0); // receive all interrupts

    /* Mask LVT entries */
    xApicWriteReg32(LOCAL_APIC_TIMER_LVT, LOCAL_APIC_LVT_MASKED);
    xApicWriteReg32(LOCAL_APIC_CMCI_LVT, LOCAL_APIC_LVT_MASKED);
    xApicWriteReg32(LOCAL_APIC_LINT0_LVT, LOCAL_APIC_LVT_MASKED);
    xApicWriteReg32(LOCAL_APIC_LINT1_LVT, LOCAL_APIC_LVT_MASKED);
    xApicWriteReg32(LOCAL_APIC_ERROR_LVT, LOCAL_APIC_LVT_MASKED);
    xApicWriteReg32(LOCAL_APIC_PERFCNT_LVT, LOCAL_APIC_LVT_MASKED);
    xApicWriteReg32(LOCAL_APIC_THERMSENS_LVT, LOCAL_APIC_LVT_MASKED);
}
static UINT32 xApicGetMaxApicID() {
    return (s_XApicData.ExtendedApicIDSupport) ? 0xFE : 0xE;
}
static UINT32 xApicGetMaxIntRoutableApicID() {
    return (s_XApicData.ExtendedApicIDSupport) ? 0xFE : 0xE;
}
static UINT32 xApicGetApicID() {
    return (BYTE)(xApicReadReg32(LOCAL_APIC_ID_REG) >> 24);
}
static bool xApicIsBSP() {
    return !!(kReadMSR(IA32_APIC_BASE) & IA32_APIC_BASE_BSP);
}
static void xApicSendEoi() {
    /* For xAPIC, any value can be written to
     * the EOI register to signal EOI */
    xApicWriteReg32(LOCAL_APIC_EOI, 0);
}
static void xApicTimerStart(UINT32 InitialCounter, bool Periodic, UINT32 IntVec) {
    UINT32 TimerLVT = 0;
    TimerLVT |= (Periodic) ? (0b01 << 17) : (0b00 /* one-shot */ << 17);
    if (IntVec == 0) TimerLVT |= LOCAL_APIC_LVT_MASKED;
    else TimerLVT |= IntVec;
    xApicTimerStop();

    xApicWriteReg32(LOCAL_APIC_TIMER_DIVCONFIG, 0b0001 /* divide by 4 */);
    xApicWriteReg32(LOCAL_APIC_TIMER_LVT, TimerLVT); /* bits[18:17]=01 -> periodic */
    xApicWriteReg32(LOCAL_APIC_TIMER_INITCOUNT, InitialCounter); /* start timer */
}
static UINT32 xApicTimerReadCounter() {
    return xApicReadReg32(LOCAL_APIC_TIMER_CURCOUNT);
}
static void xApicTimerStop() {
    /* Writing zero to the timer initial count register effectively
     * disables the timer, both in one-shot and periodic mode. */
    xApicWriteReg32(LOCAL_APIC_TIMER_INITCOUNT, 0);
}

static void xApicSendIPI(UINT32 Flags, UINT32 TargetApicID) {
    xApicWriteReg32(LOCAL_APIC_INT_CMD_REG_HIGH, TargetApicID << 24);
    xApicWriteReg32(LOCAL_APIC_INT_CMD_REG_LOW, Flags);

    /* Wait until IPI transmit completes */
    while (xApicReadReg32(LOCAL_APIC_INT_CMD_REG_LOW) & LOCAL_APIC_SEND_PENDING);
}

/* Internal utility functions */

static UINT32 xApicReadReg32(UINT32 Offset) {
    volatile UINT32 *Ptr = (volatile UINT32 *)(s_XApicData.MmioVirtAddr + Offset);
    return *Ptr;
}
static void xApicWriteReg32(UINT32 Offset, UINT32 Value) {
    volatile UINT32 *Ptr = (volatile UINT32 *)(s_XApicData.MmioVirtAddr + Offset);
    *Ptr = Value;
}
