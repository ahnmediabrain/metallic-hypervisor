
/* ApicInternal.h - Internal API prototypes */

#pragma once


struct ApicInterface {
    OS_STATUS (*InitializeSubsystem)(); /* called before any call to the below APIs */
    void (*EnableApic)();
    UINT32 (*GetMaxApicID)();
    UINT32 (*GetMaxIntRoutableApicID)();
    UINT32 (*GetApicID)();
    bool (*IsBSP)();
    void (*SendEoi)();
    void (*TimerStart)(UINT32 InitialCounter, bool Periodic, UINT32 IntVec);
    UINT32 (*TimerReadCounter)();
    void (*TimerStop)();
    void (*SendIPI)(UINT32 Flags, UINT32 TargetApicID);
};
