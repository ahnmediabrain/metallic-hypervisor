
#pragma once


struct IOAPIC_DESC {
    UINT64          PhysicalAddress;// MMIO physical base address
    UINT64          VirtualAddress; // MMIO virtual base address
    UINT32          GsiBase;        // GSI base number
    UINT32          InputCount;     // Number of input pins (typically 16 or 24)
};
struct IOAPIC_INPUT_DESC {
    UINT64          VirtualAddress; // Virtual base address of I/O APIC MMIO
    UINT32          GsiNum;         // Global system interrupt (GSI) number
    UINT32          PinNum;         // Pin number on the I/O APIC
    UINT32          Flags;          // Interrupt flags
};

#define CORE_MAX        4096
#define IOAPIC_MAX      64

typedef struct {
    DWORD           ProcessorCount;
    DWORD           ApicIDMax;
    QWORD           ProcessorACPIID[CORE_MAX];  // APIC ID -> ACPI ID ((QWORD)-1 if the processor does not exist)
    DWORD           IoApicCount;
    IOAPIC_DESC     IoApicDesc[IOAPIC_MAX];
    DWORD           BSPAPICID;
    bool            IsPICPresent;
    IOAPIC_INPUT_DESC   IsaInputs[16];
} ApicConfig;


#define IOAPIC_REG_ID               0x00
#define IOAPIC_REG_VER              0x01
#define IOAPIC_REG_ARB              0x02        // Arbitration register
#define IOAPIC_REG_REDTBL_BASE      0x10

#define IOAPIC_REDTBL_MASKED        0x00010000ULL   // bit[16] = 1
#define IOAPIC_REDTBL_LVTRIGGER     0x00008000ULL   // bit[15] = 1
#define IOAPIC_REDTBL_EGTRIGGER     0x00000000ULL   // bit[15] = 0
#define IOAPIC_REDTBL_ACTIVELOW     0x00002000ULL   // bit[13] = 1
#define IOAPIC_REDTBL_ACTIVEHIGH    0x00000000ULL   // bit[13] = 0
#define IOAPIC_REDTBL_DSTLOGICAL    0x00000800ULL   // bit[11] = 1
#define IOAPIC_REDTBL_DSTPHYSICAL   0x00000000ULL   // bit[11] = 0

/*
 * Be aware these APIs are not thread-safe.
 * Also note that the I/O APIC MMIO region
 * must be mapped as strongly uncacheable.
 */

void kIoApicMaskAll(UINT64 VirtualBase);

UINT32 kIoApicReadReg32(UINT64 VirtualBase, UINT32 Index);
void kIoApicWriteReg32(UINT64 VirtualBase, UINT32 Index, UINT32 Value);

/* Gets the number of input pins of the I/O APIC. */
UINT32 kIoApicGetInputCount(UINT64 VirtualBase);

/* Sets the I/O APIC to redirect an interrupt to a
 * specific core and interrupt vector. */
void kIoApicSetupRedirection(UINT64 VirtualBase, UINT32 PinNum, BYTE TargetApicID, UINT32 IntVec,
    bool IsLevelTrigger, bool IsActiveLow);

/* Instructs the I/O APIC to redirect an interrupt to a
 * specific CPU specified by its Local APIC ID.
 * Note that contrary to kIoApicSetupRedirection, this routine
 * only modifies the target CPU. To change other settings
 * and/or toggle enable/disable, use kIoApicSetupRedirection.
 */
void kIoApicChangeRedirection(UINT64 VirtualBase, UINT32 PinNum, BYTE NewTargetApicID);

/* Sets the I/O APIC to no longer redirect an interrupt
 * to any CPU and/or any interrupt vector. */
void kIoApicDisableRedirection(UINT64 VirtualBase, UINT32 PinNum);
