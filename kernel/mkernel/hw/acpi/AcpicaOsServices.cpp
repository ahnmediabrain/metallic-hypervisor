
#include "stdafx.h"
#include "AsmUtils.h"
#include "Acpi.h"
#include "Console.h"
#include "GlobalVariable.h"
#include "Debug.h"
#include "Interrupt.h"
#include "MemMgr.h"
#include "Pci.h"
#include "Scheduler.h"
#include "SchedulerCore.h"
#include "SyncObjs.h"
#include "Time.h"
#include "hw/acpi/AcpicaOsServices.h"

/* Indicates number of pending or running DPCs. */
static UINTN s_PendingDPCs;

ACPI_STATUS AcpiOsInitialize() {
    s_PendingDPCs = 0;
    return AE_OK;
}

ACPI_STATUS AcpiOsTerminate() {
    return AE_OK;
}

ACPI_PHYSICAL_ADDRESS AcpiOsGetRootPointer() {
    if (!g_KernelEntryData->AcpiDesc.Available)
        return (ACPI_PHYSICAL_ADDRESS) nullptr;

    return g_KernelEntryData->AcpiDesc.RsdpAddr;
}

ACPI_STATUS AcpiOsPredefinedOverride(
    const ACPI_PREDEFINED_NAMES *PredefinedObject,
    ACPI_STRING *NewValue) {
    *NewValue = nullptr;
    return AE_OK;
}

ACPI_STATUS AcpiOsTableOverride(
    ACPI_TABLE_HEADER *ExistingTable,
    ACPI_TABLE_HEADER **NewTable) {
    *NewTable = nullptr;
    return AE_OK;
}

ACPI_STATUS AcpiOsPhysicalTableOverride(
    ACPI_TABLE_HEADER *ExistingTable,
    ACPI_PHYSICAL_ADDRESS *NewAddress,
    UINT32 *NewTableLength) {
    *NewAddress = (ACPI_PHYSICAL_ADDRESS) nullptr;
    return AE_OK;
}

void *AcpiOsMapMemory(
    ACPI_PHYSICAL_ADDRESS PhysicalAddress,
    ACPI_SIZE Length) {
    return kMapPhysicalMemory((void *)PhysicalAddress, Length);
}

void AcpiOsUnmapMemory(
    void *LogicalAddress,
    ACPI_SIZE Length) {
    kUnmapPhysicalMemory(LogicalAddress);
}

ACPI_STATUS AcpiOsGetPhysicalAddress(
    void *LogicalAddress,
    ACPI_PHYSICAL_ADDRESS *PhysicalAddress) {
    if (LogicalAddress == nullptr || PhysicalAddress == nullptr)
        return AE_BAD_PARAMETER;
    UINTN Result = kVirt2Phys((UINTN)LogicalAddress);
    if (Result == INVALID_MEMORY_ADDRESS)
        return AE_ERROR;

    *PhysicalAddress = Result;
    return AE_OK;
}

void *AcpiOsAllocate(
    ACPI_SIZE Size) {
    return kMalloc(Size);
}

void AcpiOsFree(
    void *Memory) {
    kFree(Memory);
}

ACPI_THREAD_ID AcpiOsGetThreadId() {
    return kGetCurrentThreadID();
}

struct AcpiOsExecuteData {
    ACPI_OSD_EXEC_CALLBACK  EntryPoint;
    void                    *Context;
};
static UINTN AcpiOsExecuteThreadEntry(void *UserData) {
    AcpiOsExecuteData *ThunkingInfo = (AcpiOsExecuteData *)UserData;
    ThunkingInfo->EntryPoint(ThunkingInfo->Context);
    kFree(ThunkingInfo);

    kInterlockedDecrement64(&s_PendingDPCs);
    return 0;
}
ACPI_STATUS AcpiOsExecute(
    ACPI_EXECUTE_TYPE Type,
    ACPI_OSD_EXEC_CALLBACK Function,
    void *Context) {
    /* Actually this needs to be implemented using DPC.
     * However we don't have such fancy separation of jobs.
     * Hence we just create a new thread for execution.
     */

    AcpiOsExecuteData *ThunkingInfo = (AcpiOsExecuteData *)kMalloc(sizeof(AcpiOsExecuteData));
    if (ThunkingInfo == nullptr)
        return AE_NO_MEMORY;
    ThunkingInfo->EntryPoint = Function;
    ThunkingInfo->Context = Context;

    CreateThreadConfig threadConfig;
    threadConfig.EntryPoint = AcpiOsExecuteThreadEntry;
    threadConfig.Parent = EMPTY_HANDLE;
    threadConfig.StackSize = KERNEL_THREAD_STACKSZ_DEFAULT;
    threadConfig.UserData = ThunkingInfo;

    kInterlockedIncrement64(&s_PendingDPCs);
    OS_STATUS e = kCreateThread(&threadConfig);
    if (E_FAILED(e)) {
        kFree(ThunkingInfo);
        return AE_NO_MEMORY;
    }

    return AE_OK;
}

void AcpiOsSleep(
    UINT64 Milliseconds) {
    kSleep(Milliseconds);
}

void AcpiOsStall(
    UINT32 Microseconds) {
    /* caller will ensure the given time value is not too large */
    kSpinWait(Microseconds);
}

void AcpiOsWaitEventsComplete() {
    /* spin-wait until all pending DPCs complete */
    while (kInterlockedRead64(&s_PendingDPCs) > 0) {
        kPause();
    }
}

ACPI_STATUS AcpiOsCreateMutex(
    ACPI_MUTEX *OutHandle) {
    K_MUTEX *pMutex = (K_MUTEX *)kMalloc(sizeof(K_MUTEX));
    if (pMutex == nullptr)
        return AE_NO_MEMORY;

    kMutexInit(pMutex);
    *OutHandle = pMutex;
    return AE_OK;
}
void AcpiOsDeleteMutex(
    ACPI_MUTEX Handle) {
    kAssert(Handle->RefCount == 0);
    kFree(Handle);
}
ACPI_STATUS AcpiOsAcquireMutex(
    ACPI_MUTEX Handle,
    UINT16 TimeoutMilliseconds) {
    if (Handle == nullptr)
        return AE_BAD_PARAMETER;

    UINT64 TimeoutMicroseconds = (TimeoutMilliseconds == ACPI_WAIT_FOREVER) ? K_WAIT_FOREVER : (((UINT64)TimeoutMilliseconds) * 1000);
    OS_STATUS e = kMutexAcquire((K_MUTEX *)Handle, TimeoutMicroseconds);
    if (E_FAILED(e))
        return AE_TIME;

    return AE_OK;
}
void AcpiOsReleaseMutex(
    ACPI_MUTEX Handle) {
    OS_STATUS e = kMutexRelease((K_MUTEX *)Handle);
    kAssert(E_SUCCEEDED(e));
}
ACPI_STATUS
AcpiOsCreateSemaphore(
    UINT32 MaxUnits,
    UINT32 InitialUnits,
    ACPI_SEMAPHORE *OutHandle) {
    K_SEMAPHORE *Semaphore = (K_SEMAPHORE *)kMalloc(sizeof(K_SEMAPHORE));
    if (Semaphore == nullptr)
        return AE_NO_MEMORY;

    if (MaxUnits < InitialUnits)
        return AE_BAD_PARAMETER;

    if ((UINTN)MaxUnits == ((UINTN)-1)) {
        MaxUnits--;
        if (MaxUnits < InitialUnits)
            InitialUnits--;
    }

    kSemaphoreInit(Semaphore, MaxUnits, MaxUnits - InitialUnits);
    *OutHandle = Semaphore;
    return AE_OK;
}
ACPI_STATUS AcpiOsDeleteSemaphore(
    ACPI_SEMAPHORE Handle) {
    K_SEMAPHORE *Semaphore = (K_SEMAPHORE *)Handle;
    kAssert(Semaphore->RefCount == 0);
    kFree(Semaphore);
    return AE_OK;
}
ACPI_STATUS AcpiOsWaitSemaphore(
    ACPI_SEMAPHORE Handle,
    UINT32 Units,
    UINT16 TimeoutMilliseconds) {
    if (Handle == nullptr)
        return AE_BAD_PARAMETER;
    if (Units == 0)
        return AE_OK;
    if (Units > 1)
        return AE_NOT_IMPLEMENTED;

    UINT64 TimeoutMicroseconds = (TimeoutMilliseconds == ACPI_WAIT_FOREVER) ? K_WAIT_FOREVER : (((UINT64)TimeoutMilliseconds) * 1000);
    kSemaphoreAcquire((K_SEMAPHORE *)Handle, TimeoutMicroseconds, nullptr);
    return AE_OK;
}
ACPI_STATUS AcpiOsSignalSemaphore(
    ACPI_SEMAPHORE Handle,
    UINT32 Units) {
    if (Handle == nullptr)
        return AE_BAD_PARAMETER;
    if (Units == 0)
        return AE_OK;
    if (Units > 1)
        return AE_NOT_IMPLEMENTED;

    OS_STATUS e = kSemaphoreRelease((K_SEMAPHORE *)Handle, nullptr);
    if (e == E_SYNCOBJ_NOTOWNED)
        return AE_LIMIT;

    return AE_OK;
}
ACPI_STATUS AcpiOsCreateLock(
    ACPI_SPINLOCK *OutHandle) {
    K_SPINLOCK *Lock = (K_SPINLOCK *)kMalloc(sizeof(K_SPINLOCK));
    if (Lock == nullptr)
        return AE_NO_MEMORY;

    kSpinlockInit(Lock);
    *OutHandle = Lock;
    return AE_OK;
}
void AcpiOsDeleteLock(
    ACPI_SPINLOCK Handle) {
    kAssert(Handle != nullptr);
    kFree(Handle);
}
ACPI_CPU_FLAGS AcpiOsAcquireLock(
    ACPI_SPINLOCK Handle) {
    ACPI_CPU_FLAGS Flags = kReadRFlags();
    kSpinlockAcquire((K_SPINLOCK *)Handle);
    return Flags;
}
void AcpiOsReleaseLock(
    ACPI_SPINLOCK Handle,
    ACPI_CPU_FLAGS Flags) {
    kSpinlockRelease((K_SPINLOCK *)Handle);
    kWriteRFlags(Flags);
}

struct AcpiOsInterruptHandlerData {
    ACPI_OSD_HANDLER        Handler;
    void                    *Context;
};
static void AcpiOsInterruptHandlerEntry(void *UserData) {
    AcpiOsInterruptHandlerData *ThunkingInfo = (AcpiOsInterruptHandlerData *)UserData;
    ThunkingInfo->Handler(ThunkingInfo->Context);
}

ACPI_STATUS
AcpiOsInstallInterruptHandler(
    UINT32 GlobalSysInt,
    ACPI_OSD_HANDLER Handler,
    void *Context) {
    /* SCI (System Control Interrupt):
     *   A system interrupt used by hardware to
     *   notify the OS of ACPI events. The SCI is
     *   an active, low, shareable, level interrupt.
     */

    AcpiOsInterruptHandlerData *ThunkingInfo = (AcpiOsInterruptHandlerData *)kMalloc(sizeof(AcpiOsInterruptHandlerData));
    if (ThunkingInfo == nullptr)
        return AE_NO_MEMORY;

    ThunkingInfo->Handler = Handler;
    ThunkingInfo->Context = Context;

    OS_STATUS e = kAcpiRegisterGsiHandler(GlobalSysInt,
        AcpiOsInterruptHandlerEntry, false,
        true, true, ThunkingInfo
    );
    switch (e) {
    case E_INVALID_PARAMETER:
        return AE_BAD_PARAMETER;
    case E_NOT_AVAILABLE:
        return AE_ALREADY_EXISTS;
    case E_OUTOFRESOURCE:
        return AE_LIMIT;
    }

    if (E_FAILED(e))
        return AE_ERROR;

    return AE_OK;
}
ACPI_STATUS
AcpiOsRemoveInterruptHandler(
    UINT32 GlobalSysInt,
    ACPI_OSD_HANDLER Handler) {
    void *UserData;
    OS_STATUS e;

    e = kAcpiUnregisterGsiHandler(GlobalSysInt, AcpiOsInterruptHandlerEntry, &UserData);
    if (E_FAILED(e)) {
        switch (e) {
        case E_INVALID_PARAMETER:
            return AE_BAD_PARAMETER;
        case E_NOTFOUND:
            return AE_NOT_EXIST;
        default:
            return AE_ERROR;
        }
    }

    kFree(UserData);
    return AE_OK;
}

ACPI_STATUS AcpiOsReadMemory(
    ACPI_PHYSICAL_ADDRESS Address,
    UINT64 *Value,
    UINT32 Width) {
    switch (Width) {
    case 8: case 16: case 32: case 64: break;
    default: return AE_BAD_PARAMETER;
    }

    void *VirtualAddress = kMapPhysicalMemory((void *)Address, Width / 8);
    if (VirtualAddress == nullptr)
        return AE_NO_MEMORY;

    switch (Width) {
    case 8:
        *Value = *(UINT8 *)VirtualAddress;
        break;
    case 16:
        *Value = *(UINT16 *)VirtualAddress;
        break;
    case 32:
        *Value = *(UINT32 *)VirtualAddress;
        break;
    case 64:
        *Value = *(UINT64 *)VirtualAddress;
        break;
    }

    kUnmapPhysicalMemory(VirtualAddress);
    return AE_OK;
}
ACPI_STATUS AcpiOsWriteMemory(
    ACPI_PHYSICAL_ADDRESS Address,
    UINT64 Value,
    UINT32 Width) {
    switch (Width) {
    case 8: case 16: case 32: case 64: break;
    default: return AE_BAD_PARAMETER;
    }

    void *VirtualAddress = kMapPhysicalMemory((void *)Address, Width / 8);
    if (VirtualAddress == nullptr)
        return AE_NO_MEMORY;

    switch (Width) {
    case 8:
        *(UINT8 *)VirtualAddress = (UINT8)Value;
        break;
    case 16:
        *(UINT16 *)VirtualAddress = (UINT16)Value;
        break;
    case 32:
        *(UINT32 *)VirtualAddress = (UINT32)Value;
        break;
    case 64:
        *(UINT64 *)VirtualAddress = (UINT64)Value;
        break;
    }

    kUnmapPhysicalMemory(VirtualAddress);
    return AE_OK;
}
ACPI_STATUS AcpiOsReadPort(
    ACPI_IO_ADDRESS Address,
    UINT32 *Value,
    UINT32 Width) {
    if (Address > 0xFFFF)
        return AE_SUPPORT;

    switch (Width) {
    case 8:
        *Value = kPortIn8((WORD)Address);
        break;
    case 16:
        *Value = kPortIn16((WORD)Address);
        break;
    case 32:
        *Value = kPortIn32((WORD)Address);
        break;
    default:
        return AE_BAD_PARAMETER;
    }
    return AE_OK;
}
ACPI_STATUS AcpiOsWritePort(
    ACPI_IO_ADDRESS Address,
    UINT32 Value,
    UINT32 Width) {
    if (Address > 0xFFFF)
        return AE_SUPPORT;

    switch (Width) {
    case 8:
        kPortOut8((WORD)Address, (BYTE)Value);
        break;
    case 16:
        kPortOut16((WORD)Address, (UINT16)Value);
        break;
    case 32:
        kPortOut32((WORD)Address, Value);
        break;
    default:
        return AE_BAD_PARAMETER;
    }
    return AE_OK;
}
ACPI_STATUS AcpiOsReadPciConfiguration(
    ACPI_PCI_ID *PciId,
    UINT32 Register,
    UINT64 *Value,
    UINT32 Width) {
    if (PciId->Bus >= 256 || PciId->Device >= 32 || PciId->Function >= 8)
        return AE_BAD_PARAMETER;

    AcpiMcfgEntry *Entry = kAcpiFindMcfgEntry(PciId->Segment, (UINT8)PciId->Bus);
    if (Entry != nullptr) {
        UINT8 *VirtAddr = (UINT8 *)kMapPhysicalMemory((void *)Entry->BaseAddress, 256 * 1024 * 1024);
        if (VirtAddr == nullptr)
            return AE_NO_MEMORY;

        ACPI_STATUS ret = AE_OK;
        switch (Width) {
        case 8:
            *Value = *(UINT8 volatile *)(VirtAddr + Register);
            break;
        case 16:
            *Value = *(UINT16 volatile *)(VirtAddr + Register);
            break;
        case 32:
            *Value = *(UINT32 volatile *)(VirtAddr + Register);
            break;
        case 64:
#if ACPI_MACHINE_WIDTH >= 64
            *Value = *(UINT64 volatile *)(VirtAddr + Register);
#else
            ret = AE_SUPPORT;
#endif
            break;
        default:
            ret = AE_BAD_PARAMETER;
            break;
        }
        kUnmapPhysicalMemory(VirtAddr);
        return ret;
    }

    /* Fallback to port I/O */
    if (PciId->Segment != 0)
        return AE_SUPPORT;

    switch (Width) {
    case 8:
        *Value = kPciConfigRead8(MAKEBDFVALUE(PciId->Bus, PciId->Device, PciId->Function), Register);
        break;
    case 16:
        *Value = kPciConfigRead16(MAKEBDFVALUE(PciId->Bus, PciId->Device, PciId->Function), Register);
        break;
    case 32:
        *Value = kPciConfigRead32(MAKEBDFVALUE(PciId->Bus, PciId->Device, PciId->Function), Register);
        break;
    default:
        return AE_SUPPORT;
    }
    return AE_OK;
}
ACPI_STATUS AcpiOsWritePciConfiguration(
    ACPI_PCI_ID *PciId,
    UINT32 Register,
    UINT64 Value,
    UINT32 Width) {
    if (PciId->Bus >= 256 || PciId->Device >= 32 || PciId->Function >= 8)
        return AE_BAD_PARAMETER;

    AcpiMcfgEntry *Entry = kAcpiFindMcfgEntry(PciId->Segment, (UINT8)PciId->Bus);
    if (Entry != nullptr) {
        UINT8 *VirtAddr = (UINT8 *)kMapPhysicalMemory((void *)Entry->BaseAddress, 256 * 1024 * 1024);
        if (VirtAddr == nullptr)
            return AE_NO_MEMORY;

        ACPI_STATUS ret = AE_OK;
        switch (Width) {
        case 8:
            *(UINT8 volatile *)(VirtAddr + Register) = (UINT8)Value;
            break;
        case 16:
            *(UINT16 volatile *)(VirtAddr + Register) = (UINT16)Value;
            break;
        case 32:
            *(UINT32 volatile *)(VirtAddr + Register) = (UINT32)Value;
            break;
        case 64:
#if ACPI_MACHINE_WIDTH >= 64
            *(UINT64 volatile *)(VirtAddr + Register) = (UINT64)Value;
#else
            ret = AE_SUPPORT;
#endif
            break;
        default:
            ret = AE_BAD_PARAMETER;
            break;
        }
        kUnmapPhysicalMemory(VirtAddr);
        return ret;
    }

    /* Fallback to port I/O */
    if (PciId->Segment != 0)
        return AE_SUPPORT;

    switch (Width) {
    case 8:
        kPciConfigWrite8(MAKEBDFVALUE(PciId->Bus, PciId->Device, PciId->Function), Register, (UINT8)Value);
        break;
    case 16:
        kPciConfigWrite16(MAKEBDFVALUE(PciId->Bus, PciId->Device, PciId->Function), Register, (UINT16)Value);
        break;
    case 32:
        kPciConfigWrite32(MAKEBDFVALUE(PciId->Bus, PciId->Device, PciId->Function), Register, (UINT32)Value);
        break;
    default:
        return AE_SUPPORT;
    }
    return AE_OK;
}
void ACPI_INTERNAL_VAR_XFACE AcpiOsPrintf(
    const char *Format,
    ...) {
    va_list Args;
    va_start(Args, Format);
    AcpiOsVprintf(Format, Args);
    va_end(Args);
}
void AcpiOsVprintf(
    const char *Format,
    va_list Args) {
    kConsoleVPrintf8(Format, Args);
}
ACPI_STATUS AcpiOsSignal(
    UINT32 Function,
    const void *Info) {
    kConsolePrintf(TEXT("ACPICA: Fatal: Signal %u, Info = %p\r\n"), Function, Info);
    kHang();

    return AE_ERROR; /* never reached */
}
UINT64 AcpiOsGetTimer() {
    UINT64 Value = kGetTimeCounterValue();
    UINT64 Frequency = kGetTimeCounterFrequency();
    return (Value * 10000000 / Frequency);
}
ACPI_STATUS AcpiOsEnterSleep(
    UINT8 SleepState,
    UINT32 RegaValue,
    UINT32 RegbValue) {
    kConsolePrintf(TEXT("ACPICA: Entering ACPI sleep\r\n"));
    kConsolePrintf(TEXT("SleepState = %u, RegaValue = %u, RegbValue = %u\r\n"),
        SleepState, RegaValue, RegbValue);

    /* TODO : do OS-specific cleanup work if needed */

    return AE_OK;
}
