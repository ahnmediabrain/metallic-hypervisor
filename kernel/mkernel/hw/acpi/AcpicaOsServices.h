
#pragma once

#include "hw/acpi/acpica/include/acpica.h"

ACPI_STATUS AcpiOsInitialize();
ACPI_STATUS AcpiOsTerminate();
ACPI_PHYSICAL_ADDRESS AcpiOsGetRootPointer();
ACPI_STATUS AcpiOsPredefinedOverride(
    const ACPI_PREDEFINED_NAMES *PredefinedObject, 
    ACPI_STRING *NewValue);
ACPI_STATUS AcpiOsTableOverride(
    ACPI_TABLE_HEADER *ExistingTable,
    ACPI_TABLE_HEADER **NewTable);
ACPI_STATUS AcpiOsPhysicalTableOverride(
    ACPI_TABLE_HEADER *ExistingTable,
    ACPI_PHYSICAL_ADDRESS *NewAddress,
    UINT32 *NewTableLength);
void *AcpiOsMapMemory(
    ACPI_PHYSICAL_ADDRESS PhysicalAddress,
    ACPI_SIZE Length);
void AcpiOsUnmapMemory(
    void *LogicalAddress,
    ACPI_SIZE Length);
ACPI_STATUS AcpiOsGetPhysicalAddress(
    void *LogicalAddress,
    ACPI_PHYSICAL_ADDRESS *PhysicalAddress);
void *AcpiOsAllocate(
    ACPI_SIZE Size);
void AcpiOsFree(
    void *Memory);
ACPI_THREAD_ID AcpiOsGetThreadId();
ACPI_STATUS AcpiOsExecute(
    ACPI_EXECUTE_TYPE Type,
    ACPI_OSD_EXEC_CALLBACK Function,
    void *Context);
void AcpiOsSleep(
    UINT64 Milliseconds);
void AcpiOsStall(
    UINT32 Microseconds);
void AcpiOsWaitEventsComplete();
ACPI_STATUS AcpiOsCreateMutex(
    ACPI_MUTEX *OutHandle);
void AcpiOsDeleteMutex(
    ACPI_MUTEX Handle);
ACPI_STATUS AcpiOsAcquireMutex(
    ACPI_MUTEX Handle,
    UINT16 TimeoutMilliseconds);
void AcpiOsReleaseMutex(
    ACPI_MUTEX Handle);
ACPI_STATUS
AcpiOsCreateSemaphore(
    UINT32 MaxUnits,
    UINT32 InitialUnits,
    ACPI_SEMAPHORE *OutHandle);
ACPI_STATUS AcpiOsDeleteSemaphore(
    ACPI_SEMAPHORE Handle);
ACPI_STATUS AcpiOsWaitSemaphore(
    ACPI_SEMAPHORE Handle,
    UINT32 Units,
    UINT16 TimeoutMilliseconds);
ACPI_STATUS AcpiOsSignalSemaphore(
    ACPI_SEMAPHORE Handle,
    UINT32 Units);
ACPI_STATUS AcpiOsCreateLock(
    ACPI_SPINLOCK *OutHandle);
void AcpiOsDeleteLock(
    ACPI_SPINLOCK Handle);
ACPI_CPU_FLAGS AcpiOsAcquireLock(
    ACPI_SPINLOCK Handle);
void AcpiOsReleaseLock(
    ACPI_SPINLOCK Handle,
    ACPI_CPU_FLAGS Flags);
ACPI_STATUS
AcpiOsInstallInterruptHandler(
    UINT32 GlobalSysInt,
    ACPI_OSD_HANDLER Handler,
    void *Context);
ACPI_STATUS
AcpiOsRemoveInterruptHandler(
    UINT32 GlobalSysInt,
    ACPI_OSD_HANDLER Handler);
ACPI_STATUS AcpiOsReadMemory(
    ACPI_PHYSICAL_ADDRESS Address,
    UINT64 *Value,
    UINT32 Width);
ACPI_STATUS AcpiOsWriteMemory(
    ACPI_PHYSICAL_ADDRESS Address,
    UINT64 Value,
    UINT32 Width);
ACPI_STATUS AcpiOsReadPort(
    ACPI_IO_ADDRESS Address,
    UINT32 *Value,
    UINT32 Width);
ACPI_STATUS AcpiOsWritePort(
    ACPI_IO_ADDRESS Address,
    UINT32 Value,
    UINT32 Width);
ACPI_STATUS AcpiOsReadPciConfiguration(
    ACPI_PCI_ID *PciId,
    UINT32 Register,
    UINT64 *Value,
    UINT32 Width);
ACPI_STATUS AcpiOsWritePciConfiguration(
    ACPI_PCI_ID *PciId,
    UINT32 Register,
    UINT64 Value,
    UINT32 Width);
void ACPI_INTERNAL_VAR_XFACE AcpiOsPrintf(
    const char *Format,
    ...);
void AcpiOsVprintf(
    const char *Format,
    va_list Args);
ACPI_STATUS AcpiOsSignal(
    UINT32 Function,
    const void *Info);
UINT64 AcpiOsGetTimer();
ACPI_STATUS AcpiOsEnterSleep(
    UINT8 SleepState,
    UINT32 RegaValue,
    UINT32 RegbValue);
