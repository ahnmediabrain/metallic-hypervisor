/******************************************************************************
 *
 * Name: acmetallic.h - OS specific defines, etc. for Metallic
 *
 *****************************************************************************/

#pragma once

#include "AsmUtils.h"
#include "SyncObjs.h"

/* Host-dependent types and defines for user- and kernel-space ACPICA */

#define ACPI_MUTEX_TYPE             ACPI_OSL_MUTEX
#define ACPI_MUTEX                  struct K_MUTEX *

#define ACPI_USE_NATIVE_DIVIDE
#define ACPI_USE_NATIVE_MATH64

/* #define ACPI_THREAD_ID               thread_id */

#define ACPI_SEMAPHORE              struct K_SEMAPHORE *
#define ACPI_SPINLOCK               struct K_SPINLOCK *

#define ACPI_MACHINE_WIDTH          64

#include <stdarg.h>

#if defined(DEBUG) || defined(_DEBUG)
#define ACPI_DEBUG_OUTPUT
#endif

/* Host-dependent types and defines for in-kernel ACPICA */

/* ACPICA cache implementation is adequate. */
#define ACPI_USE_LOCAL_CACHE

#define ACPI_FLUSH_CPU_CACHE() kWriteBackInvalidateCache();

