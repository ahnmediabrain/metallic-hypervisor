
#include "stdafx.h"
#include "AsmUtils.h"
#include "Console.h"
#include "Debug.h"
#include "MemMgrBuddy.h"
#include "StdLib.h"
#include "SyncObjs.h"


UINT64 const BuddyAllocator::INVALID = (UINT64)-1;

void BuddyAllocator::Install(UINT64 PhysicalStart_param, UINT64 Pages_param) {
    kAssert(Pages_param >= 1);
    kAssert(PhysicalStart_param % 4096 == 0);
    kAssert((PhysicalStart_param + Pages_param * 4096) > PhysicalStart_param);

    // Set basic configuration parameters
    this->PhysicalStart = PhysicalStart_param;
    this->NumberOfPagesLogCeil = kHighestBitSet(Pages_param - 1) + 1;
    this->TotalPages = Pages_param;
    for (int i = 0; i <= this->NumberOfPagesLogCeil; i++) {
        this->FreeListHead[i] = BuddyAllocator::INVALID;
    }

    // Initialize internal data structures
    this->Bitmap = ((BYTE *)this) + sizeof(BuddyAllocator);
    UINT64 BitmapBits = (1ULL << (this->NumberOfPagesLogCeil + 1)) - 1;
    UINT64 BitmapBytes = (BitmapBits + 7) / 8;
    kMemSet(this->Bitmap, 0, BitmapBytes);
    this->FreeList = (UINTN *)(this->Bitmap + BitmapBytes);
    SetUse(this->NumberOfPagesLogCeil, 0, true);
    AddToFreeList(this->NumberOfPagesLogCeil, 0);

    // Mark trailing non-owned area as unusable (allocated).
    // Note that we ensure this area cannot be freed (range check).
    UINT64 PageNum = 0;
    UINT64 lv = this->NumberOfPagesLogCeil;
    while (this->TotalPages < (PageNum + (1ULL << lv))) {
        Split(lv, PageNum, lv - 1);
        lv--;
        // Mark the right half if the boundary resides in the left half
        UINT64 mid = PageNum + (1ULL << lv);
        if (this->TotalPages <= mid) {
            RemoveFromFreeList(lv, mid);
        }
        else {
            PageNum = mid;
        }
    }

    // Initialize spinlock
    kSpinlockInit(&this->Lock);
}

UINT64 BuddyAllocator::TryAlloc(UINT64 Bytes, UINT64 *PagesAllocated) {
    if (Bytes == 0) return BuddyAllocator::INVALID;
    kSpinlockAcquire(&this->Lock);
    UINT64 lv_min = 0;
    UINT64 Result = BuddyAllocator::INVALID;
    UINT64 Pages = (Bytes + 4095) / 4096;
    while ((1ULL << lv_min) < Pages) lv_min++;
    for (UINT64 lv = lv_min; lv <= this->NumberOfPagesLogCeil; lv++) {
        if (FreeListHead[lv] != BuddyAllocator::INVALID) {
            UINT64 PageNum = FreeListHead[lv];
            if (lv > lv_min) Split(lv, PageNum, lv_min);
            Result = AllocInternal(lv_min, PageNum);
            *PagesAllocated = 1ULL << lv_min;
            break;
        }
    }
    kSpinlockRelease(&this->Lock);
    return Result;
}

UINT64 BuddyAllocator::Free(UINT64 PhysicalAddress) {
    kAssert(this->Owns(PhysicalAddress));
    kAssert(PhysicalAddress % 4096 == 0);
    kSpinlockAcquire(&this->Lock);

    UINT64 Result = 0;
    UINT64 PageNum = (PhysicalAddress - this->PhysicalStart) / 4096;
    for (UINT64 lv = 0; lv <= this->NumberOfPagesLogCeil; lv++) {
        if (IsInUse(lv, PageNum)) {
            kAssert((PageNum % (1ULL << lv)) == 0);
            FreeInternal(lv, PageNum);
            Merge(lv, PageNum);
            Result = 1ULL << lv;
            break;
        }
    }
    kSpinlockRelease(&this->Lock);
    return Result;
}

bool BuddyAllocator::Owns(UINT64 PhysicalAddress)
{
    return (PhysicalAddress >= this->PhysicalStart) &&
        (PhysicalAddress < (this->PhysicalStart + this->TotalPages * 4096));
}

UINT64 BuddyAllocator::GetStorageRequired(UINT64 NumberOfPagesLogCeli)
{
    UINT64 Entries = (1ULL << (NumberOfPagesLogCeli + 1)) - 1;
    UINT64 Bytes = sizeof(BuddyAllocator); // Header
    Bytes += 2 * Entries * sizeof(UINT64); // Free list
    Bytes += (Entries + 7) / 8;            // Bitmap
    return Bytes;
}

//******** Internal Utility Functions (mid-level) ********//

UINT64 BuddyAllocator::AllocInternal(UINT64 lv, UINT64 PageNum) {
    RemoveFromFreeList(lv, PageNum);
    return (PhysicalStart + (PageNum * 4096));
}
void BuddyAllocator::FreeInternal(UINT64 lv, UINT64 PageNum) {
    AddToFreeList(lv, PageNum);
}
void BuddyAllocator::Split(UINT64 lv, UINT64 PageNum, UINT64 lv_target) {
    while (lv > lv_target) {
        RemoveFromFreeList(lv, PageNum);
        SetUse(lv - 1, PageNum + (1ULL << (lv - 1)), true);
        SetUse(lv - 1, PageNum, true);
        AddToFreeList(lv - 1, PageNum + (1ULL << (lv - 1)));
        AddToFreeList(lv - 1, PageNum);
        lv--;
    }
}
void BuddyAllocator::Merge(UINT64 lv, UINT64 PageNum) {
    while (lv < this->NumberOfPagesLogCeil) {
        UINT64 PairPageNum = PageNum ^ (1ULL << lv);
        if (IsInUse(lv, PairPageNum)) break;
        RemoveFromFreeList(lv, PageNum);
        RemoveFromFreeList(lv, PairPageNum);
        PageNum &= ~(1ULL << lv); /* use the left block as the representative of the two */
        AddToFreeList(lv + 1, PageNum);
        lv++;
    }
}

//******** Internal Utility Functions (low-level) ********//

void BuddyAllocator::AddToFreeList(UINT64 lv, UINT64 PageNum) {
    kAssert(IsInUse(lv, PageNum) == true);
    kAssert((PageNum % (1ULL << lv)) == 0);
    this->ListPrev(lv, PageNum) = BuddyAllocator::INVALID;
    this->ListNext(lv, PageNum) = FreeListHead[lv];
    if (FreeListHead[lv] != BuddyAllocator::INVALID) {
        this->ListPrev(lv, FreeListHead[lv]) = PageNum;
    }
    FreeListHead[lv] = PageNum;
    SetUse(lv, PageNum, false);
}
void BuddyAllocator::RemoveFromFreeList(UINT64 lv, UINT64 PageNum) {
    kAssert(IsInUse(lv, PageNum) == false);
    kAssert((PageNum % (1ULL << lv)) == 0);
    UINT64 PrevAvail = this->ListPrev(lv, PageNum);
    UINT64 NextAvail = this->ListNext(lv, PageNum);
    if (PrevAvail != BuddyAllocator::INVALID) {
        this->ListNext(lv, PrevAvail) = NextAvail;
    }
    if (NextAvail != BuddyAllocator::INVALID) {
        this->ListPrev(lv, NextAvail) = PrevAvail;
    }
    if (FreeListHead[lv] == PageNum) {
        FreeListHead[lv] = NextAvail;
    }
    SetUse(lv, PageNum, true);
}
bool BuddyAllocator::IsInUse(UINT64 lv, UINT64 PageNum) {
    UINT64 i = ItemIndex(lv, PageNum);
    UINT64 byte_idx = i / 8;
    UINT64 bit_idx = i % 8;
    return (this->Bitmap[byte_idx] & (1 << bit_idx)) != 0;
}
void BuddyAllocator::SetUse(UINT64 lv, UINT64 PageNum, bool value) {
    UINT64 i = ItemIndex(lv, PageNum);
    UINT64 byte_idx = i / 8;
    UINT64 bit_idx = i % 8;
    if (value) {
        this->Bitmap[byte_idx] |= (1UL << bit_idx);
    }
    else {
        this->Bitmap[byte_idx] &= ~(1UL << bit_idx);
    }
}
UINT64 &BuddyAllocator::ListNext(UINT64 lv, UINT64 PageNum) {
    return this->FreeList[2 * ItemIndex(lv, PageNum)];
}
UINT64 &BuddyAllocator::ListPrev(UINT64 lv, UINT64 PageNum) {
    return this->FreeList[2 * ItemIndex(lv, PageNum) + 1];
}
UINT64 BuddyAllocator::ItemIndex(UINT64 lv, UINT64 PageNum) {
    UINT64 Index = (1ULL << (this->NumberOfPagesLogCeil - lv)) - 1;
    return Index + (PageNum >> lv);
}
