﻿
#pragma once

/* String/Memory copy/set/move/len/compare */

void kStrCpy(CHAR16 *Destination, const CHAR16 *Source);
UINTN kStrLen(const CHAR16 *String);
INTN kStrCmp(const CHAR16 *String1, const CHAR16 *String2);
INTN kStrNCmp(const CHAR16 *String1, const CHAR16 *String2, INTN Count);
INTN kStrNCmp8(const char *String1, const char *String2, INTN Count);
void kStrInv(CHAR16 *String);

void kMemSet(void *Mem, BYTE Value, K_SIZE Size);
void kMemCpy(void *Destination, const void *Source, K_SIZE Num);
INTN kMemCmp(const void *Mem1, const void *Mem2, K_SIZE Size);
void *kMemMove(void *Destination, const void *Source, K_SIZE Num);

/* Integer <--> String conversion */

INT64 kAToI64(const CHAR16 *String, UINT64 Scale);
UINT64 kAToU64(const CHAR16 *String, UINT64 Scale);
void kIToA64(CHAR16 *Buffer, INT64 iValue, UINT64 Scale, BOOL bUpper, UINT64 iWidth = 0);
void kUToA64(CHAR16 *Buffer, UINT64 iValue, UINT64 Scale, BOOL bUpper, UINT64 iWidth = 0);

/* Sorting */

void kSort(void *Base, K_SIZE Num, K_SIZE Size, bool(*CompFunc)(const void *, const void *)); // In-memory sort

/* Miscellaneous */

BOOL kIsPowerOfTwo64(UINT64 Num);
UINT64 kGetMaxBit(UINT64 Num);
UINT64 kGetMinBit(UINT64 Num);
