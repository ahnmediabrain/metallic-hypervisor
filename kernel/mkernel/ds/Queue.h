
#pragma once


// The (unsynchronized) queue.
struct Queue {
    UINTN       Front;          // The front element to be popped.
    UINTN       Back;           // The next position to insert new element.
    UINTN       ItemCount;
    UINTN       BufferSize;
    UINTN      *Data;
};

void kInitQueue(Queue *Q);
void kEnqueue(Queue *Q, UINTN Item);
bool kDequeue(Queue *Q, UINTN *pItem);
void kDestroyQueue(Queue *Q);
