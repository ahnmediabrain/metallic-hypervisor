
#include "stdafx.h"
#include "Debug.h"
#include "MemMgr.h"
#include "StdLib.h"
#include "ds/Queue.h"


#define MIN_QUEUE_SZ    16

static void DoubleBuffer(Queue *Q);
static void HalveBuffer(Queue *Q);
static void MigrateBuffer(Queue *Q, UINTN NewBufSize);

void kInitQueue(Queue *Q) {
    Q->Front = 0;
    Q->Back = 0;
    Q->ItemCount = 0;
    Q->BufferSize = MIN_QUEUE_SZ;
    Q->Data = (UINTN *)kMalloc(sizeof(UINTN) * Q->BufferSize);
}

void kEnqueue(Queue *Q, UINTN Item) {
    if (Q->ItemCount == Q->BufferSize)
        DoubleBuffer(Q);

    Q->Data[Q->Back] = Item;
    Q->Back++;
    if (Q->Back == Q->BufferSize)
        Q->Back = 0;

    Q->ItemCount++;
}

bool kDequeue(Queue *Q, UINTN *pItem) {
    if (Q->ItemCount == 0)
        return false;

    *pItem = Q->Data[Q->Front];
    Q->Front++;
    if (Q->Front == Q->BufferSize)
        Q->Front = 0;

    Q->ItemCount--;
    if (Q->BufferSize > MIN_QUEUE_SZ && Q->ItemCount <= (Q->BufferSize / 4))
        HalveBuffer(Q);

    return true;
}

void kDestroyQueue(Queue *Q) {
    kFree(Q->Data);
}

static void DoubleBuffer(Queue *Q) {
    MigrateBuffer(Q, Q->BufferSize * 2);
}
static void HalveBuffer(Queue *Q) {
    MigrateBuffer(Q, Q->BufferSize / 2);
}
static void MigrateBuffer(Queue *Q, UINTN NewBufSize) {
    kAssert(NewBufSize >= Q->ItemCount);
    UINTN *NewBuf = (UINTN *)kMalloc(sizeof(UINTN) * NewBufSize);
    if (Q->Front < Q->Back) {
        // [Front..Back)
        kMemCpy(NewBuf, Q->Data + Q->Front, sizeof(UINTN) * Q->ItemCount);
    }
    else {
        // [0..Back) U [Front..BufferSize)
        kMemCpy(NewBuf, Q->Data + Q->Front, sizeof(UINTN) * (Q->BufferSize - Q->Front));
        kMemCpy(NewBuf + (Q->BufferSize - Q->Front), Q->Data, sizeof(UINTN) * Q->Back);
    }
    Q->Front = 0;
    Q->Back = Q->ItemCount;
    Q->BufferSize = NewBufSize;
    kFree(Q->Data);
    Q->Data = NewBuf;
}
