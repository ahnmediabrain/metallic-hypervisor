
#pragma once


// The doubly-linked list data structure
struct DList {
    DList      *Prev;
    DList      *Next;
    void       *Data;
};



