﻿
#pragma once


/* Macros for the 16-bit bus:device.function format */
#define MAKEBDFVALUE(b,d,f) ((UINT16)( ((UINT16)b) << 8 | ((UINT16)d) << 5 | ((UINT16)f)))
#define BDF_GetB(bdf) (((UINT16)bdf) >> 8)
#define BDF_GetD(bdf) ((((UINT16)bdf) >> 3) & 0x1F)
#define BDF_GetF(bdf) (((UINT16)bdf) & 0x7) 


void kInitializePciSubsystem();

/* PCI config read/write helpers */

UINT8 kPciConfigRead8(UINT16 BDFValue, UINT32 Offset);
void kPciConfigWrite8(UINT16 BDFValue, UINT32 Offset, UINT8 Value);
UINT16 kPciConfigRead16(UINT16 BDFValue, UINT32 Offset);
void kPciConfigWrite16(UINT16 BDFValue, UINT32 Offset, UINT16 Value);
UINT32 kPciConfigRead32(UINT16 BDFValue, UINT32 Offset);
void kPciConfigWrite32(UINT16 BDFValue, UINT32 Offset, UINT32 Value);

/* Actual implementations */

void kScanForPciDevices();
