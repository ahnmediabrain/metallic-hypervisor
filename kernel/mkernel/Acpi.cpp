﻿
#include "stdafx.h"
#include "Acpi.h"
#include "hw/acpi/acpica/include/acpica.h"
#include "AcpiTables.h"
#include "APEntryPoint.h"
#include "APStartupBinary.h"
#include "AsmUtils.h"
#include "Console.h"
#include "CoreLocalStorage.h"
#include "GlobalVariable.h"
#include "Debug.h"
#include "Interrupt.h"
#include "MemMgr.h"
#include "Pit.h"
#include "StdLib.h"
#include "SyncObjs.h"
#include "hw/apic/Apic.h"
#include "hw/apic/IoApic.h"

static struct {
    UINT32  GsiNum;
    UINT32  IntVec;      // Zero if the entry is invalid.
    UINT32  RefCount;    // Reference count for the GSI GsiNum.
    bool    AllowShare;
} s_GsiHandlers[1024];
static K_SPINLOCK s_GsiHandlerLock;
static AcpiMcfg *s_McfgVirtPtr;

// Maps physical address of ACPI table onto virtual memory space
static void *MapAcpiTable(UINTN TablePhysicalAddr) {
    AcpiHeader *Header = (AcpiHeader *)kMapPhysicalMemory((void *)TablePhysicalAddr, sizeof(AcpiHeader));
    UINTN Bytes = Header->Length;
    kUnmapPhysicalMemory(Header);
    return kMapPhysicalMemory((void *)TablePhysicalAddr, Bytes);
}

// Finds the corresponding I/O APIC descriptor for the GSI.
static const IOAPIC_DESC *FindIoApic(UINT32 GsiNum) {
    for (UINTN i = 0; i < g_ApicConfig.IoApicCount; i++) {
        const IOAPIC_DESC *IoApic = &g_ApicConfig.IoApicDesc[i];
        if (IoApic->GsiBase <= GsiNum && GsiNum < (IoApic->GsiBase + IoApic->InputCount))
            return IoApic;
    }
    return nullptr;
}

void kInitializeAcpiSubsystem() {
    for (UINTN i = 0; i < sizeof(s_GsiHandlers) / sizeof(s_GsiHandlers[0]); i++) {
        s_GsiHandlers[i].IntVec = 0;
    }
    kSpinlockInit(&s_GsiHandlerLock);
}
void kAcpiParse(UINTN RsdpAddr)
{
    kConsolePrintf(TEXT("ACPI: RSDP address is %p\r\n"), RsdpAddr);
    AcpiRsdp *Rsdp = (AcpiRsdp *)kMapPhysicalMemory((void *)RsdpAddr, sizeof(AcpiRsdp));
    if (!kAcpiCheckSignature(Rsdp, ACPI_SIG_RSDP)) {
        kConsolePrintf(TEXT("ACPI: Invalid RSDP signature\r\n"));
        kHang();
    }
    if (!kAcpiValidateChecksum(Rsdp, Rsdp->Length)) {
        kConsolePrintf(TEXT("ACPI: Invalid RSDP checksum"));
        kHang();
    }
    if (Rsdp->Revision < 2) {
        kConsolePrintf(TEXT("ACPI: Revision %d is unsupported\r\n"), Rsdp->Revision);
        kHang();
    }
    AcpiXsdt *Xsdt = (AcpiXsdt *)MapAcpiTable(Rsdp->XsdtAddress);
    kConsolePrintf(TEXT("ACPI: XSDT address is %p(phys), %p(virt)\r\n"), Rsdp->XsdtAddress, Xsdt);
    UINT32 XsdtEntries = (Xsdt->Header.Length - sizeof(Xsdt->Header)) / 8;
    kConsolePrintf(TEXT("ACPI: XSDT has %d entries\r\n"), XsdtEntries);
    kUnmapPhysicalMemory(Rsdp);

    UINTN MadtPhys = 0;
    UINTN McfgPhys = 0;
    for (UINT32 i = 0; i < XsdtEntries; i++) {
        UINTN PhysAddr = Xsdt->PointerToOtherSDTs[i];
        UINTN VirtAddr = (UINTN)MapAcpiTable(PhysAddr);
        kConsolePrintf(TEXT("ACPI: %dth SDT: Signature = "), i);
        for (int j = 0; j < 4; j++) {
            kConsolePutChar(((BYTE *)VirtAddr)[j]);
        }
        kConsolePrintf(TEXT(", Length = %d\r\n"), *(UINT32 *)(VirtAddr + 4));
        if (kAcpiCheckSignature((void *)VirtAddr, ACPI_SIG_MADT)) {
            MadtPhys = PhysAddr;
        }
        if (kAcpiCheckSignature((void *)VirtAddr, ACPI_SIG_MCFG)) {
            McfgPhys = PhysAddr;
        }
        if (!kAcpiValidateChecksum((void *)VirtAddr, *(UINT32 *)(VirtAddr + 4))) {
            kConsolePrintf(TEXT("ACPI: %dth SDT: Invalid checksum\r\n"), i);
            kHang();
        }
        kUnmapPhysicalMemory((void *)VirtAddr);
    }
    kUnmapPhysicalMemory(Xsdt);
    if (MadtPhys == 0) {
        kConsolePrintf(TEXT("ACPI: MADT Table not found; cannot continue booting\r\n"));
        kHang();
    }
    kConsolePrintf(TEXT("ACPI: MADT Table located at %p(phys)\r\n"), MadtPhys);
    AcpiMadt *Madt = (AcpiMadt *)MapAcpiTable(MadtPhys);
    kConsolePrintf(TEXT("ACPI: MADT Table has length %d\r\n"), Madt->Header.Length);

    /* MCFG table */
    if (McfgPhys == 0) {
        s_McfgVirtPtr = nullptr;
    }
    else {
        s_McfgVirtPtr = (AcpiMcfg *)MapAcpiTable(McfgPhys);
    }

    kMemSet(g_ApicConfig.ProcessorACPIID, 0xff, sizeof(g_ApicConfig.ProcessorACPIID));
    g_ApicConfig.IoApicCount = 0;
    g_ApicConfig.ProcessorCount = 0;
    g_ApicConfig.IsPICPresent = !!(Madt->Flags & ACPI_MADT_LEGACY_PIC);
    for (DWORD i = 0; i < 16; i++) {
        auto &d = g_ApicConfig.IsaInputs[i];
        d.GsiNum = i;
        d.Flags = 0;
    }

    UINT32 i = sizeof(AcpiMadt);
    while (i < Madt->Header.Length) {
        BYTE *MadtPtr = ((BYTE *)Madt) + i;
        BYTE Type = MadtPtr[0];
        BYTE Length = MadtPtr[1];
        //kConsolePrintf(TEXT("ACPI: MADT Entry: Type=%d, Length=%d\r\n"), Type, Length);
        switch (Type) {
        case MADT_TYPE_LOCAL_APIC:
        {
            // only count if the Local APIC has enabled flag set
            auto *p = (AcpiMadtLocalApic *)MadtPtr;
            if (p->Flags & 1) {
                if (g_ApicConfig.ProcessorACPIID[p->APICID] != (QWORD)-1) {
                    kConsolePrintf(TEXT("ACPI Error: Invalid Local APIC description\r\n"));
                    kHang();
                }
                g_ApicConfig.ProcessorACPIID[p->APICID] = p->ACPIProcessorID;
                g_ApicConfig.ProcessorCount++;
                kConsolePrintf(TEXT("ACPI: Local APIC Found, APIC ID=%d, ACPI ID=%d\r\n"), p->APICID, p->ACPIProcessorID);
            }
            break;
        }
        case MADT_TYPE_IO_APIC:
        {
            if (g_ApicConfig.IoApicCount == IOAPIC_MAX) {
                kConsolePrintf(TEXT("ACPI Error: Number of I/O APIC exceeds supported maximum of %d\r\n"), IOAPIC_MAX);
                kHang();
            }

            auto *p = (AcpiMadtIoApic *)MadtPtr;
            kConsolePrintf(TEXT("ACPI: I/O APIC Found at address %p\r\n"), p->IOAPICAddress);
            auto &d = g_ApicConfig.IoApicDesc[g_ApicConfig.IoApicCount++];
            d.PhysicalAddress = p->IOAPICAddress;
            d.VirtualAddress = (UINTN)kMapPhysicalMemory((void *)d.PhysicalAddress, 256);
            d.GsiBase = p->GlobalSysIntBase;
            d.InputCount = kIoApicGetInputCount(d.VirtualAddress);

            kIoApicMaskAll(d.VirtualAddress);
            break;
        }
        case MADT_TYPE_INT_SRC_OVRR:
        {
            auto *p = (AcpiMadtIntSrcOvrr *)MadtPtr;
            if (p->Source <= 15) {
                g_ApicConfig.IsaInputs[p->Source].GsiNum = p->GlobalSysInt;
                g_ApicConfig.IsaInputs[p->Source].Flags = p->Flags;
            }
            else {
                kConsolePrintf(TEXT("ACPI: ISA GSI Override entry specifies nonexistent PIC IRQ %u; ignoring\r\n"), p->Source);
            }
            break;
        }
        case MADT_TYPE_NMI_SRC:
        {
            break;
        }
        case MADT_TYPE_LOCAL_APIC_NMI:
        {
            break;
        }
        case MADT_TYPE_LOCAL_APIC_ADDR_OVRR:
        {
            break;
        }
        case MADT_TYPE_IO_SAPIC:
        case MADT_TYPE_LOCAL_SAPIC:
        case MADT_TYPE_PLATFORM_INT_SRC:
            kConsolePuts(TEXT("ACPI: Error: SAPIC is unsupported (Is the system IA64?)"));
            kHang();
        case MADT_TYPE_LOCAL_X2APIC:
        {
            // only count if the Local APIC has enabled flag set
            auto *p = (AcpiMadtLocalX2Apic *)MadtPtr;
            if (p->Flags & 1) {
                if (g_ApicConfig.ProcessorACPIID[p->X2ApicID] != (QWORD)-1) {
                    kConsolePrintf(TEXT("ACPI Error: Invalid Local APIC description\r\n"));
                    kHang();
                }
                g_ApicConfig.ProcessorACPIID[p->X2ApicID] = p->AcpiProcessorUid;
                g_ApicConfig.ProcessorCount++;
                kConsolePrintf(TEXT("ACPI: Local x2APIC Found, APIC ID=%d, ACPI ID=%d\r\n"), p->X2ApicID, p->AcpiProcessorUid);
            }
            break;
        }
        case MADT_TYPE_LOCAL_X2APIC_NMI:
        {
            break;
        }
        default:
            kConsolePrintf(TEXT("ACPI: Error: Unknown MADT entry %d of Length %d\r\n"), Type, Length);
            break;
        }
        i += Length;
    }
    kUnmapPhysicalMemory(Madt);

    if (g_ApicConfig.IoApicCount == 0) {
        kConsolePuts(TEXT("ACPI Error: No I/O APIC found\r\n"));
        kHang();
    }
    kSort(g_ApicConfig.IoApicDesc, g_ApicConfig.IoApicCount, sizeof(IOAPIC_DESC),
        [](const void *a, const void *b) -> bool {
        const IOAPIC_DESC *L = (const IOAPIC_DESC *)a;
        const IOAPIC_DESC *R = (const IOAPIC_DESC *)b;
        return (L->GsiBase < R->GsiBase);
    });
    for (UINTN j = 1; j < g_ApicConfig.IoApicCount; j++) {
        IOAPIC_DESC &Prev = g_ApicConfig.IoApicDesc[j - 1];
        IOAPIC_DESC &Cur = g_ApicConfig.IoApicDesc[j];
        UINT32 PrevLast = Prev.GsiBase + Prev.InputCount - 1;
        UINT32 CurLast = Cur.GsiBase + Cur.InputCount - 1;
        if (PrevLast < Prev.GsiBase ||
            CurLast < Cur.GsiBase ||
            Cur.GsiBase <= PrevLast) {
            kConsolePuts(TEXT("ACPI Error: Invalid I/O APIC description\r\n"));
            kHang();
        }
    }
    for (DWORD j = 0; j < 16; j++) {
        if (j == 2) continue; // do not care about slave PIC

        auto &d = g_ApicConfig.IsaInputs[j];
        const IOAPIC_DESC *IoApic = FindIoApic(d.GsiNum);
        if (IoApic == nullptr) {
            kConsolePrintf(TEXT("ACPI Error: Global system interrupt %u doesn't correspond to any I/O APIC\r\n"), d.GsiNum);
            kHang();
        }

        d.VirtualAddress = IoApic->VirtualAddress;
        d.PinNum = d.GsiNum - IoApic->GsiBase;
    }

    // ** Prepare Per-Core Local Storage **
    // Step 1 - Determine Necessary Size
    // Step 2 - Find Continuous Memory Region of Necessary Size
    // Step 3 - Fill in the stack size into the local storages

    DWORD APICIDMax = 0;
    for (DWORD j = 0; j < CORE_MAX; j++) {
        if (g_ApicConfig.ProcessorACPIID[j] != (QWORD)-1) {
            APICIDMax = j;
        }
    }
    g_ApicConfig.ApicIDMax = APICIDMax;
    UINTN RequiredStorageSize = sizeof(CoreLocalStorage) * (APICIDMax + 1);
    CoreLocalStorage *StorageBase = (CoreLocalStorage *)kAllocContiguousPages((RequiredStorageSize + 4095) / 4096);
    if (StorageBase == nullptr) {
        kConsolePrintf(TEXT("Error: Failed to allocate core local storage area of size %llu\r\n"), RequiredStorageSize);
        kHang();
    }
    else {
        kConsolePrintf(TEXT("Core local storage allocated at %p, Size = %llu\r\n"),
            StorageBase, RequiredStorageSize);
    }
    kSetCoreLocalStorageBase((UINT64)StorageBase, RequiredStorageSize);
    for (DWORD j = 0; j <= APICIDMax; j++) {
        CoreLocalStorage &cls = StorageBase[j];
        cls.StackSize = sizeof(cls.StackSize) + sizeof(cls.Stack);
    }

    // Disable PIC if enabled
    if (g_ApicConfig.IsPICPresent) {
        kDisablePIC();
    }
    g_ApicConfig.BSPAPICID = kApicGetApicID();
}

static UINT64 s_NextAPCount;
void kStartApplicationProcessors() {
    /* Load AP startup code */

    UINT64 StartAddress = AP_STARTUP_LOAD_ADDR;
    kAssert(StartAddress % 0x1000 == 0);

    kConsolePrintf(TEXT("AP Startup Code Addr = %p, Code Size = %d, "),
        APStartupBinaryCode, APStartupBinaryCodeSize);
    kConsolePrintf(TEXT("First 8 Bytes of code = "));
    for (int i = 0; i < 8; i++) {
        BYTE b = APStartupBinaryCode[i];
        kConsolePrintf(TEXT("%X%X "), b >> 4, b & 0xf);
    }
    kConsolePrintf(TEXT("\r\n"));
    kMemCpy((void *)StartAddress, APStartupBinaryCode, APStartupBinaryCodeSize);

    APDataBlock *pAPDataBlock = (APDataBlock *)AP_DATA_BLOCK_ADDR;
    pAPDataBlock->EntryPoint = (UINT64)&kAPEntryPointThunk;
    pAPDataBlock->PageTableBase = kReadCR3();
    pAPDataBlock->CoreLocalStorageBase = kGetCoreLocalStorageBase();
    pAPDataBlock->CoreLocalStorageSize = kGetCoreLocalStorageSize();
    kConsolePrintf(TEXT("AP Kernel EntryPoint Thunk Address is %p\r\n"), (void *)pAPDataBlock->EntryPoint);

    /* Wake up processors that have APIC ID
     * less than or equal to the supported maximum. */
    UINT32 ApicIDMax = min(g_ApicConfig.ApicIDMax, kApicGetMaxApicID());
    g_APCount = 0;
    for (UINT32 ApicID = 0; ApicID <= ApicIDMax; ApicID++) {
        if (ApicID == g_ApicConfig.BSPAPICID)
            continue; /* Do NOT reset self */

        if (g_ApicConfig.ProcessorACPIID[ApicID] != (QWORD)-1) {
            s_NextAPCount = g_APCount + 1;
            kConsolePrintf(TEXT("Waking up processor with ApicID=%u..."), ApicID);
            OS_STATUS e = kApicWakeUpProcessor(ApicID, (UINT32)StartAddress, []()->bool {
                return kInterlockedRead64(&g_APCount) == s_NextAPCount;
            });
            if (E_FAILED(e))
                kConsolePrintf(TEXT("Failed to wake up ApicID=%u\r\n"), ApicID);
            else
                kConsolePrintf(TEXT("Successfully woke up ApicID=%u\r\n"), ApicID);
        }
    }
    kConsolePrintf(TEXT("Done waking up %lld Application Processors\r\n"), g_APCount);
}

OS_STATUS kAcpiIsaToIoApicInputDesc(UINT32 IsaPinNum, IOAPIC_INPUT_DESC *Desc) {
    if (IsaPinNum > 15 /* Past the maximum PIC pin number */ ||
        IsaPinNum == 2 /* Slave PIC pin */)
        return E_INVALID_PARAMETER;

    kMemCpy(Desc, &g_ApicConfig.IsaInputs[IsaPinNum], sizeof(IOAPIC_INPUT_DESC));
    return E_SUCCESS;
}
OS_STATUS kAcpiGsiToIoApicInputDesc(UINT32 GsiNum, IOAPIC_INPUT_DESC *Desc) {
    const IOAPIC_DESC *IoApic = FindIoApic(GsiNum);
    if (IoApic != nullptr) {
        Desc->VirtualAddress = IoApic->VirtualAddress;
        Desc->GsiNum = GsiNum;
        Desc->PinNum = GsiNum - IoApic->GsiBase;
        Desc->Flags = 0; /* conforms to the specification of the bus */
        return E_SUCCESS;
    }
    return E_INVALID_PARAMETER;
}


OS_STATUS kAcpiRegisterGsiHandler(UINT32 GsiNum, IrqCallback Handler, bool AllowShare,
    bool IsLevelTrigger, bool IsActiveLow, void *UserData) {
    UINTN i;
    UINT32 IntVec;
    IOAPIC_INPUT_DESC ioapic_desc;
    bool SaveSucceeded;
    OS_STATUS e;

    kSpinlockAcquire(&s_GsiHandlerLock);

    /* Check if the GSI has already been mapped */
    for (i = 0; i < sizeof(s_GsiHandlers) / sizeof(s_GsiHandlers[0]); i++) {
        if (s_GsiHandlers[i].GsiNum == GsiNum) {
            IntVec = s_GsiHandlers[i].IntVec;
            if (AllowShare && s_GsiHandlers[i].AllowShare) {
                s_GsiHandlers[i].RefCount++;
                kSpinlockRelease(&s_GsiHandlerLock);

                /* Register the handler and we're done */
                e = kRegisterIrqCallback(Handler, 1, IRQ_FLAG_SHAREABLE | IRQ_FLAG_USE_VECTOR | IRQ_FLAG_PARAM_CONTEXT,
                    UserData, &IntVec, nullptr, nullptr);
                return e;
            }
            else {
                kSpinlockRelease(&s_GsiHandlerLock);
                return E_NOT_AVAILABLE;
            }
        }
    }

    /* Register the handler */

    e = kAcpiGsiToIoApicInputDesc(GsiNum, &ioapic_desc);
    if (E_FAILED(e)) {
        kSpinlockRelease(&s_GsiHandlerLock);
        return e;
    }
    e = kRegisterIrqCallback(Handler, 1, IRQ_FLAG_SHAREABLE | IRQ_FLAG_PARAM_CONTEXT,
        UserData, &IntVec, nullptr, nullptr);
    if (E_FAILED(e)) {
        kSpinlockRelease(&s_GsiHandlerLock);
        return e;
    }

    /* Save the GSI-Vector mapping information */

    SaveSucceeded = false;
    kSpinlockAcquire(&s_GsiHandlerLock);
    for (UINTN i = 0; i < sizeof(s_GsiHandlers) / sizeof(s_GsiHandlers[0]); i++) {
        if (s_GsiHandlers[i].IntVec == 0) {
            s_GsiHandlers[i].GsiNum = GsiNum;
            s_GsiHandlers[i].IntVec = IntVec;
            s_GsiHandlers[i].RefCount = 1;
            s_GsiHandlers[i].AllowShare = AllowShare;
            SaveSucceeded = true;
            break;
        }
    }
    if (!SaveSucceeded) {
        kUnregisterIrqCallback(Handler, IntVec);
        kSpinlockRelease(&s_GsiHandlerLock);
        return E_OUTOFRESOURCE;
    }

    /* Enable I/O APIC routing */
    UINT32 TargetApicID = kApicGetApicID();
    kIoApicSetupRedirection(ioapic_desc.VirtualAddress, ioapic_desc.PinNum, TargetApicID, IntVec,
        IsLevelTrigger, IsActiveLow);

    kSpinlockRelease(&s_GsiHandlerLock);
    return E_SUCCESS;
}
OS_STATUS kAcpiUnregisterGsiHandler(UINT32 GsiNum, IrqCallback Handler, void **pUserData) {
    UINT32 IntVec;
    UINTN i;
    IOAPIC_INPUT_DESC ioapic_desc;

    /* Find corresponding interrupt vector */

    IntVec = 0;
    kSpinlockAcquire(&s_GsiHandlerLock);
    for (i = 0; i < sizeof(s_GsiHandlers) / sizeof(s_GsiHandlers[0]); i++) {
        if (s_GsiHandlers[i].IntVec != 0 && s_GsiHandlers[i].GsiNum == GsiNum) {
            IntVec = s_GsiHandlers[i].IntVec;
            s_GsiHandlers[i].RefCount--;
            break;
        }
    }
    if (IntVec == 0) {
        kSpinlockRelease(&s_GsiHandlerLock);
        return E_NOTFOUND;
    }

    if (s_GsiHandlers[i].RefCount == 0) {
        /* Disable I/O APIC routing */
        kAssert(E_SUCCEEDED(kAcpiGsiToIoApicInputDesc(GsiNum, &ioapic_desc)));
        kIoApicDisableRedirection(ioapic_desc.VirtualAddress, ioapic_desc.PinNum);

        /* Mark the entry as invalid */
        s_GsiHandlers[i].IntVec = 0;
    }

    kSpinlockRelease(&s_GsiHandlerLock);

    return kUnregisterIrqCallback(Handler, IntVec, pUserData);
}

AcpiMcfgEntry *kAcpiFindMcfgEntry(UINT16 PciSegGroupNum, UINT8 BusNum) {
    if (s_McfgVirtPtr != nullptr) {
        UINTN Count = (s_McfgVirtPtr->Header.Length - sizeof(AcpiMcfg)) / sizeof(AcpiMcfgEntry);
        for (UINTN i = 0; i < Count; i++) {
            AcpiMcfgEntry *Entry = (AcpiMcfgEntry *)(((UINTN)s_McfgVirtPtr) +
                sizeof(AcpiMcfg) + i * sizeof(AcpiMcfgEntry));
            if (Entry->PciSegGroupNum == PciSegGroupNum &&
                Entry->StartBusNum <= BusNum && BusNum <= Entry->EndBusNum)
                return Entry;
        }
    }
    return nullptr;
}

OS_STATUS kAcpiInitializeAcpica() {
    ACPI_STATUS Status;
    /* Initialize the ACPICA subsystem */
    Status = AcpiInitializeSubsystem();
    if (ACPI_FAILURE(Status)) {
        kConsolePrintf(TEXT("ACPICA: Fatal: Failed to initialize subsystem, code = %u\r\n"), Status);
        return E_ERROR;
    }
    kConsolePrintf(TEXT("ACPICA: Successfully initialized subsystem\r\n"));
    /* Initialize the ACPICA Table Manager and get all ACPI tables */
    Status = AcpiInitializeTables(NULL, 256, FALSE);
    if (ACPI_FAILURE(Status)) {
        kConsolePrintf(TEXT("ACPICA: Fatal: Failed to initialize tables, code = %u\r\n"), Status);
        return E_ERROR;
    }
    kConsolePrintf(TEXT("ACPICA: Successfully initialized tables\r\n"));
    /* Create the ACPI namespace from ACPI tables */
    Status = AcpiLoadTables();
    if (ACPI_FAILURE(Status)) {
        kConsolePrintf(TEXT("ACPICA: Fatal: Failed to load tables, code = %u\r\n"), Status);
        return E_ERROR;
    }
    kConsolePrintf(TEXT("ACPICA: Successfully loaded tables\r\n"));
    /* Note: Local handlers should be installed here */
    /* Initialize the ACPI hardware */
    Status = AcpiEnableSubsystem(ACPI_FULL_INITIALIZATION);
    if (ACPI_FAILURE(Status)) {
        kConsolePrintf(TEXT("ACPICA: Fatal: Failed to enable subsystem, code = %u\r\n"), Status);
        return E_ERROR;
    }
    kConsolePrintf(TEXT("ACPICA: Successfully enabled subsystem\r\n"));
    /* Complete the ACPI namespace object initialization */
    Status = AcpiInitializeObjects(ACPI_FULL_INITIALIZATION);
    if (ACPI_FAILURE(Status)) {
        kConsolePrintf(TEXT("ACPICA: Fatal: Failed to initialize objects, code = %u\r\n"), Status);
        return E_ERROR;
    }
    kConsolePrintf(TEXT("ACPICA: Successfully initialized objects\r\n"));
    /* Run _SB_._INI method */
    Status = AcpiEvaluateObject(nullptr, "\\_SB_._INI", nullptr, nullptr);
    if (ACPI_FAILURE(Status) && Status != AE_NOT_EXIST) {
        kConsolePrintf(TEXT("ACPICA: Fatal: Failted to evaluate \\_SB_.INI method, code = %u\r\n"), Status);
        return E_ERROR;
    }
    kConsolePrintf(TEXT("ACPICA: Subsystem initialization complete\r\n"));
    return E_SUCCESS;
}

void kAcpiPowerOff() {
    UINT8 S5 = 5; /* S5 = Soft Off */

    kConsolePrintf(TEXT("Powering off..."));
    AcpiEnterSleepStatePrep(S5);
    kEnableInterrupt(FALSE);
    AcpiEnterSleepState(S5);
}
