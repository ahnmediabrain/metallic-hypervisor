﻿
#pragma once

#define PIT_FREQUENCY           1193182
#define PIT_PORT_CTL            0x43
#define PIT_PORT_COUNTER0       0x40
#define PIT_PORT_COUNTER1       0x41
#define PIT_PORT_COUNTER2       0x42

#define PIT_CONTROL_COUNTER0    (0b00 << 6)
#define PIT_CONTROL_COUNTER1    (0b01 << 6)
#define PIT_CONTROL_COUNTER2    (0b10 << 6)
#define PIT_RW_LATCH            (0b00 << 4)
#define PIT_RW_LSB              (0b01 << 4)
#define PIT_RW_MSB              (0b10 << 4)
#define PIT_RW_LSBMSB           (0b11 << 4)
#define PIT_MODE0               (0b000 << 2)
#define PIT_MODE2               (0b001 << 2)

void kInitializePITForPeriodicMode();
void kInitializePITForOneShotMode();

UINT16 kPITReadCounter(); // Reads the counter from PIT channel 0
void kPITWaitMicroseconds(UINT64 microseconds);
void kTestPIT();
