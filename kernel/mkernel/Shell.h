﻿
#pragma once

// Starts shell on the global console.
//  Note that the console must be initialized
//  prior to running the thread.
//  Also note that at most one shell thread
//  should be run at a time.
UINTN kShellThread(void *Unused);
