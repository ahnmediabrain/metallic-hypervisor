﻿
#include "stdafx.h"
#include "Acpi.h"
#include "hw/acpi/acpica/include/acpica.h"
#include "AsmUtils.h"
#include "Console.h"
#include "Debug.h"
#include "MemMgr.h"
#include "SyncObjs.h"

#include "Pci.h"


/* Lock to serialize access to PCI MMIO information and/or configuration space */
static K_SPINLOCK s_PciDataLock;

/* Host bridge description */
struct HostBridgeDesc {
    UINT16      SegmentGroup;
    UINT8       BusMin;
    UINT8       BusMax;
    UINT8       HostBridgeBusNum;
    UINT64      ConfigBaseAddr; // (UINT64)-1 if MMIO is not available
} s_HostBridges[256];
static UINT64 s_HostBridgeCount;

void kInitializePciSubsystem() {
    kSpinlockInit(&s_PciDataLock);

    s_HostBridgeCount = 0;
    auto DeviceHandler = [](ACPI_HANDLE Object,
        UINT32 NestingLevel,
        void *Context,
        void **ReturnValue) -> ACPI_STATUS {
        ACPI_DEVICE_INFO *DeviceInfo;
        ACPI_BUFFER Buffer;
        ACPI_OBJECT Obj;
        ACPI_HANDLE DummyHandle;
        ACPI_STATUS Status;

        Status = AcpiGetObjectInfo(Object, &DeviceInfo);
        if (ACPI_FAILURE(Status)) {
            return Status;
        }

        /* Only deal with root bridges */
        if ((DeviceInfo->Flags & ACPI_PCI_ROOT_BRIDGE) == 0)
            return AE_OK;

        Buffer.Length = 0;
        AcpiGetName(Object, ACPI_FULL_PATHNAME, &Buffer);
        Buffer.Pointer = kMalloc(Buffer.Length);
        AcpiGetName(Object, ACPI_FULL_PATHNAME, &Buffer);

        kConsolePrintf(TEXT("[PCI Root Bridge] (%S, %S, %S)\r\n"),
            Buffer.Pointer,
            (DeviceInfo->Valid & ACPI_VALID_UID) ? DeviceInfo->UniqueId.String : "N/A",
            (DeviceInfo->Valid & ACPI_VALID_HID) ? DeviceInfo->HardwareId.String : "N/A");
        kFree(Buffer.Pointer);
        ACPI_FREE(DeviceInfo);

        /* Check device status */
        Buffer.Length = sizeof(Obj);
        Buffer.Pointer = &Obj;
        if (AcpiGetHandle(Object, "_STA", &DummyHandle) == AE_NOT_FOUND) {
            Obj.Type = ACPI_TYPE_INTEGER;
            Obj.Integer.Value = ACPI_STA_DEVICE_FUNCTIONING;
        }
        else {
            Status = AcpiEvaluateObjectTyped(Object, "_STA", nullptr, &Buffer, ACPI_TYPE_INTEGER);
            if (ACPI_FAILURE(Status)) {
                kConsolePrintf(TEXT("Warning: Failed to execute _STA method, code = %u\r\n"), Status);
                return AE_OK;
            }
        }

        /* Initialize device */
        if (Obj.Integer.Value & ACPI_STA_DEVICE_PRESENT) {
            if (AcpiGetHandle(Object, "_INI", &DummyHandle) != AE_NOT_FOUND) {
                Status = AcpiEvaluateObject(Object, "_INI", nullptr, nullptr);
                if (ACPI_FAILURE(Status) && Status != AE_NOT_EXIST) {
                    kConsolePrintf(TEXT("Warning: Failed to execute _INI method, code = %u\r\n"), Status);
                    return AE_OK;
                }
            }
        }
        else if ((Obj.Integer.Value & ACPI_STA_DEVICE_FUNCTIONING) == 0) {
            /* Device not present, not functional */
            return AE_OK;
        }

        HostBridgeDesc HostBridge;

        /* Find segment group number */
        Buffer.Length = sizeof(Obj);
        Buffer.Pointer = &Obj;
        Status = AcpiEvaluateObjectTyped(Object, "_SEG", nullptr, &Buffer, ACPI_TYPE_INTEGER);
        if (ACPI_SUCCESS(Status)) {
            if (Obj.Integer.Value >= 65536) {
                kConsolePrintf(TEXT("Warning: PCI segment group number is invalid (%llu)\r\n"), Obj.Integer.Value);
                return AE_OK;
            }
            HostBridge.SegmentGroup = (UINT16)Obj.Integer.Value;
        }
        else {
            HostBridge.SegmentGroup = 0;
        }

        /* Find host bridge bus number */
        Buffer.Length = sizeof(Obj);
        Buffer.Pointer = &Obj;
        Status = AcpiEvaluateObjectTyped(Object, "_BBN", nullptr, &Buffer, ACPI_TYPE_INTEGER);
        if (ACPI_SUCCESS(Status)) {
            HostBridge.HostBridgeBusNum = (UINT8)Obj.Integer.Value;
        }
        else {
            kConsolePrintf(TEXT("Warning: Failed to execute _BBN method, code = %u\r\n"), Status);
            return AE_OK;
        }

        /* Find bus number decode range */
        Buffer.Length = 0;
        AcpiEvaluateObject(Object, "_CRS", nullptr, &Buffer);
        Buffer.Pointer = kMalloc(Buffer.Length);
        Status = AcpiEvaluateObjectTyped(Object, "_CRS", nullptr, &Buffer, ACPI_TYPE_BUFFER);
        if (ACPI_SUCCESS(Status)) {
            ACPI_OBJECT *ObjPtr = (ACPI_OBJECT *)Buffer.Pointer;
            UINT32 i = 0;
            UINT8 *Ptr = (UINT8 *)ObjPtr->Buffer.Pointer;
#if 0
            kConsolePrintf(TEXT("_CRS method-returned data: "));
            for (UINT32 i = 0; i < ObjPtr->Buffer.Length; i++)
                kConsolePrintf(TEXT("%.2X "), Ptr[i]);
            kConsolePrintf(TEXT("\r\n"));
#endif
            bool BusNumRangeDescFound = false;
            while (i < ObjPtr->Buffer.Length) {
                bool Large;
                UINT32 Type, Len;
                if (Ptr[i] & 0x80) {
                    /* large resource data type (see ACPI spec 6.4.3) */
                    if (i + 3 > Buffer.Length) {
                        kConsolePrintf(TEXT("Warning: _CRS method returned invalid data (1)\r\n"));
                        return AE_OK;
                    }
                    Large = true;
                    Type = Ptr[i] & 0x7F;
                    Len = Ptr[i + 1] | (((UINT16)Ptr[i + 2]) << 8);
                    i += 3;
                }
                else {
                    /* small resource data type (see ACPI spec 6.4.2) */
                    Large = false;
                    Type = (Ptr[i] >> 3) & 0xF;
                    Len = Ptr[i] & 0x7;
                    i += 1;
                }
                /* range check */
                if (i + Len > Buffer.Length) {
                    kConsolePrintf(TEXT("Warning: _CRS method returned invalid data (2)\r\n"));
                    return AE_OK;
                }
#if 0
                for (UINT32 j = 0; j < Len; j++)
                    kConsolePrintf(TEXT("%.2X "), Ptr[i + j]);
                kConsolePrintf(TEXT("\r\n"));
#endif
                if (Large && Type == 0x8 /* Word Address Space Descriptor */) {
                    if (Ptr[i] == 0x2 /* Resource Type - Bus number range */) {
                        if (BusNumRangeDescFound) {
                            kConsolePrintf(TEXT("Warning: Multiple bus number range descriptor found\r\n"));
                            return AE_OK;
                        }
                        if (Len < 13) {
                            kConsolePrintf(TEXT("Warning: _CRS method returned invalid data (3)\r\n"));
                            return AE_OK;
                        }
                        UINT16 BusMin = *(UINT16 *)&Ptr[i + 5];
                        UINT16 BusMax = *(UINT16 *)&Ptr[i + 7];
                        if (BusMin > BusMax || BusMin > 255) {
                            kConsolePrintf(TEXT("Warning: _CRS method returned invalid data (4)\r\n"));
                            return AE_OK;
                        }
                        HostBridge.BusMin = (UINT8)BusMin;
                        HostBridge.BusMax = (UINT8)min(BusMax, 255);
                        kConsolePrintf(TEXT("BusMin = %u, BusMax = %u\r\n"), BusMin, BusMax);
                        BusNumRangeDescFound = true;
                    }
                }
                i += Len;
            }
            kFree(ObjPtr);

            if (!BusNumRangeDescFound) {
                kConsolePrintf(TEXT("Warning: Bus number range descriptor not found\r\n"));
                return AE_OK;
            }
        }
        else {
            kConsolePrintf(TEXT("Warning: Failed to execute _CRS method, code = %u\r\n"), Status);
            return AE_OK;
        }

        /* Locate MMIO base address */

        /* Try _CBA method first. Note that it may or may not be available
         * since the ACPI specification defines it to be optional */
        Buffer.Length = sizeof(Obj);
        Buffer.Pointer = &Obj;
        Status = AcpiEvaluateObjectTyped(Object, "_CBA", nullptr, &Buffer, ACPI_TYPE_INTEGER);
        if (ACPI_SUCCESS(Status)) {
            kConsolePrintf(TEXT("Using _CBA information\r\n"));
            HostBridge.ConfigBaseAddr = Obj.Integer.Value;
        }
        else {
            /* Try MCFG table */
            AcpiMcfgEntry *McfgEntry = kAcpiFindMcfgEntry(HostBridge.SegmentGroup, HostBridge.HostBridgeBusNum);
            if (McfgEntry != nullptr) {
                kConsolePrintf(TEXT("Using MCFG information\r\n"));
                HostBridge.ConfigBaseAddr = McfgEntry->BaseAddress;
            }
            else {
                if (HostBridge.SegmentGroup == 0) {
                    /* Segment Group 0 is accessible through Port I/O */
                    HostBridge.ConfigBaseAddr = (UINT64)-1;
                }
                else {
                    kConsolePrintf(TEXT("Warning: Failed to locate PCI MMIO base address\r\n"));
                    return AE_OK;
                }
            }
        }
        if (HostBridge.ConfigBaseAddr != (UINT64)-1)
            kConsolePrintf(TEXT("MMIO Base Address is %p\r\n"), HostBridge.ConfigBaseAddr);
        else
            kConsolePrintf(TEXT("MMIO not available; using Port I/O\r\n"));

        /* Add to PCI host bridge list */
        s_HostBridges[s_HostBridgeCount++] = HostBridge;

        return AE_OK;
    };
    AcpiGetDevices(nullptr, DeviceHandler, nullptr, nullptr);
    kConsolePrintf(TEXT("Done discovering %llu host bridge%s\r\n"), s_HostBridgeCount, 
        s_HostBridgeCount > 1 ? TEXT("s") : TEXT(""));
    kConsolePrintf(TEXT("Further discovery is not implemented; will stop here for now\r\n"));
    kHang();
}

/* PCI config read/write helpers */

UINT8 kPciConfigRead8(UINT16 BDFValue, UINT32 Offset) {
    UINT32 Address, Value;
    kAssert(Offset < 256);
    Address = 0x80000000 | ((UINT32)BDFValue) << 8 | Offset;

    kSpinlockAcquire(&s_PciDataLock);

    kPortOut32(0xCF8, Address);
    Value = kPortIn8(0xCFC);

    kSpinlockRelease(&s_PciDataLock);
    return (UINT8)Value;
}
void kPciConfigWrite8(UINT16 BDFValue, UINT32 Offset, UINT8 Value) {
    UINT32 Address;
    kAssert(Offset < 256);
    Address = 0x80000000 | ((UINT32)BDFValue) << 8 | Offset;

    kSpinlockAcquire(&s_PciDataLock);

    kPortOut32(0xCF8, Address);
    kPortOut8(0xCFC, Value);

    kSpinlockRelease(&s_PciDataLock);
}
UINT16 kPciConfigRead16(UINT16 BDFValue, UINT32 Offset) {
    UINT32 Address, Value;
    kAssert(Offset < 256);
    Address = 0x80000000 | ((UINT32)BDFValue) << 8 | Offset;

    kSpinlockAcquire(&s_PciDataLock);

    kPortOut32(0xCF8, Address);
    Value = kPortIn16(0xCFC);

    kSpinlockRelease(&s_PciDataLock);
    return (UINT16)Value;
}
void kPciConfigWrite16(UINT16 BDFValue, UINT32 Offset, UINT16 Value) {
    UINT32 Address;
    kAssert(Offset < 256);
    Address = 0x80000000 | ((UINT32)BDFValue) << 8 | Offset;

    kSpinlockAcquire(&s_PciDataLock);

    kPortOut32(0xCF8, Address);
    kPortOut16(0xCFC, Value);

    kSpinlockRelease(&s_PciDataLock);
}
UINT32 kPciConfigRead32(UINT16 BDFValue, UINT32 Offset) {
    UINT32 Address, Value;
    kAssert(Offset < 256);
    Address = 0x80000000 | ((UINT32)BDFValue) << 8 | Offset;

    kSpinlockAcquire(&s_PciDataLock);

    kPortOut32(0xCF8, Address);
    Value = kPortIn32(0xCFC);

    kSpinlockRelease(&s_PciDataLock);
    return Value;
}
void kPciConfigWrite32(UINT16 BDFValue, UINT32 Offset, UINT32 Value) {
    UINT32 Address;
    kAssert(Offset < 256);
    Address = 0x80000000 | ((UINT32)BDFValue) << 8 | Offset;

    kSpinlockAcquire(&s_PciDataLock);

    kPortOut32(0xCF8, Address);
    kPortOut32(0xCFC, Value);

    kSpinlockRelease(&s_PciDataLock);
}

/* Actual implementations */

void kScanForPciDevices() {
    kConsolePrintf(TEXT("PCI configuration begin\r\n"));
    for (UINT16 b = 0; b < 256; b++) {
        for (UINT16 d = 0; d < 32; d++) {
            for (UINT16 f = 0; f < 8; f++) {
                UINT16 BDFValue = MAKEBDFVALUE(b, d, f);
                UINT16 VendorID = kPciConfigRead16(BDFValue, 0);
                if (VendorID != 0xFFFF /* invalid */) {
                    UINT16 DeviceID = kPciConfigRead16(BDFValue, 2);
                    kConsolePrintf(TEXT("    PCI %u:%u.%u [%X:%X]\r\n"), b, d, f, VendorID, DeviceID);
                }
            }
        }
    }
    kConsolePrintf(TEXT("PCI configuration end\r\n"));
}
