
#pragma once

#include "Context.h"
#include "SyncObjs.h"

struct Thread;

struct Process {
    K_SPINLOCK      Lock;
    Thread         *Threads;
    UINTN           NumberOfThreads;
    void           *MemorySpace;
    void           *Handles;
};


#define THREAD_TOUCHED_XMM_REGS     (1ULL << 0)     // When true, the XMM registers need to be restored.
#define THREAD_TOUCHED_AVX_REGS     (1ULL << 1)     // When true, the AVX registers need to be restored.
#define THREAD_KILL_PENDING         (1ULL << 2)
#define THREAD_WAITING_OBJ_FREE     (1ULL << 3)     // The scheduler has processed the kill; only the object needs destruction.

struct Thread {
    K_SPINLOCK      Lock;
    volatile UINTN  RefCount;           // Reference count (including the runqueue)
    UINTN           ThreadID;
    Process        *Parent;
    void* volatile  UserData;
    void           *Stack;
    UINTN           StackSize;
    volatile UINT64 Flags;
    UINT64          CurrentCPU;
    Context         ThreadContext;
    SIMDContext     ThreadSIMDContext;
};

struct SchedLocalData {
    Thread             *CurrentThread;      // nullptr if none
    Context             IdleThreadContext;
    BYTE                IdleThreadStack[3072];
};

// Initializes the global scheduler data structure.
// Note that this function should be called only once globally.
// (e.g. BSP)
void kInitializeScheduler();

// Starts the scheduler on current core.
// Note that this function should be called only once per core.
// To yield CPU time afterwards, use kYield().
// Also note that this function never returns.
void kStartScheduler();

// Inserts the specified thread into the run queue.
void kAddToRunQueue(Thread *pThread);

// Yields current thread and runs pending threads on current core.
// If no thread is available, the idle thread is executed.
void kYield();

// Sleeps current thread for specified duration (in milliseconds).
// If the supplied value is zero, the effect is the same as calling kYield().
void kSleep(UINT64 Milliseconds);

// Increments the reference count and returns new reference count.
UINTN kIncrementThreadRefCount(Thread *pThread);
// Decrements the reference count and returns new reference count.
// If the thread is kill-pending and has been completely dereferenced by this call,
// the accompanied data structure is freed.
UINTN kDecrementThreadRefCount(Thread *pThread);
