_DATA SEGMENT
_DATA ENDS

_TEXT SEGMENT

kGetCoreLocalStorage PROTO C
kAPEntryPoint PROTO C

; Note that we shouldn't use stack here before setting it up
kAPEntryPointThunk PROC
	mov eax, 01h
	cpuid
	test ecx, 200000h				; Is x2APIC supported?
	jz kAPEntryPointThunk_x2APIC_NotAvail
	mov eax, 0Bh					; x2APIC is supported - get APIC ID from CPUID.0Bh:EDX
	cpuid
	mov rcx, rdx					; Upper 32bits of the register is automatically zeroed
	jmp kAPEntryPointThunk_GotLocalAPICID
kAPEntryPointThunk_x2APIC_NotAvail:
	shr rbx, 24						; Fallback to 8-bit APIC ID value
	and rbx, 0FFh
	mov rcx, rbx
kAPEntryPointThunk_GotLocalAPICID:
	mov rbx, QWORD PTR [5020h]		; Base address (virtual) of core local storage
	mov rax, QWORD PTR [5028h]		; Size of core local storage in bytes
	mul rcx							; rcx contains APIC ID
	add rax, rbx
	mov rbx, QWORD PTR [rax]		; Obtain stack size
	add rax, rbx					; Compute stack top address
	sub rax, 32						; Parameter homing space
	mov rsp, rax					; Load true stack
	mov rbp, rax
	jmp kAPEntryPoint
kAPEntryPointThunk ENDP

_TEXT ENDS

END
