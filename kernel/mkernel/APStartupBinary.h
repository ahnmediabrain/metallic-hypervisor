﻿
// APStartupBinary.h : Declaration for Application Processor (AP) start-up entrypoint code

#pragma once

#define AP_DATA_BLOCK_ADDR          0x05000
#define AP_STARTUP_LOAD_ADDR        0x06000

struct APDataBlock {
    UINT64      EntryPoint;                 // Offset 00h
    UINT64      LocalApicPhysicalBase;      // [Deprecated] Offset 08h
    UINT64      LocalApicVirtualBase;       // [Deprecated] Offset 10h
    UINT64      PageTableBase;              // Offset 18h
    UINT64      CoreLocalStorageBase;       // Offset 20h  
    UINT64      CoreLocalStorageSize;       // Offset 28h
};

// AP binary code size in bytes
extern "C" unsigned int APStartupBinaryCodeSize;
// AP binary code (entrypoint resides on the first byte)
// WARNING: 'unsigned char *Code' is incorrect
//          unless that label points to a pointer to the code
extern "C" const unsigned char APStartupBinaryCode[];
