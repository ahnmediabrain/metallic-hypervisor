
#include "stdafx.h"
#include "Acpi.h"
#include "hw/apic/Apic.h"
#include "AsmUtils.h"
#include "Console.h"
#include "CoreLocalStorage.h"
#include "Debug.h"
#include "MemMgr.h"
#include "SchedulerCore.h"
#include "StdLib.h"

#include "Interrupt.h"


struct IrqHandlerEntry {
    IrqHandlerEntry *Next;
    IrqCallback     Handler;
    UINT32          IDTBlockCount;
    UINT32          Flags;
    void            *UserData;
    IrqRerouter     Rerouter;
    void            *RerouterContext;
};

static struct {
    K_RWSPINLOCK        Lock;
    IrqHandlerEntry     *Entries[256];
} s_IrqData;


void kInitializeIrqSubsystem() {
    kRwSpinlockInit(&s_IrqData.Lock);
    for (UINTN i = 0; i < 256; i++) {
        s_IrqData.Entries[i] = nullptr;
    }
}

OS_STATUS kRegisterIrqCallback(IrqCallback Handler,
    UINT32 IDTBlockCount, UINT32 Flags, void *UserData, UINT32 *pIntVec,
    IrqRerouter Rerouter, void *RerouterContext) {
    UINT32 AllocIntVec;
    UINT32 SearchLo, SearchHi;
    bool Exclusive;
    bool Feasible;
    BOOL bInterruptEnable;

    if (!kIsPowerOfTwo64(IDTBlockCount) || IDTBlockCount > 16) {
        return E_INVALID_PARAMETER;
    }

    Flags &= ~IRQ_FLAG_VECTOR_HEAD;
    bInterruptEnable = kEnableInterrupt(FALSE);
    if (Flags & IRQ_FLAG_KERNEL) { /* Kernel handlers are always shared,
                                      so no need to check for shareability */
        if (*pIntVec > 63 || (*pIntVec + IDTBlockCount - 1) > 63) {
            kEnableInterrupt(bInterruptEnable);
            return E_INVALID_PARAMETER;
        }
        Flags &= ~IRQ_FLAG_EXCLUSIVE;
        AllocIntVec = *pIntVec;
        kRwSpinlockAcquireWriter(&s_IrqData.Lock);
    }
    else { /* Find available vector block */
        if (IDTBlockCount > 1)
            Flags |= IRQ_FLAG_EXCLUSIVE; /* having single vector exclusively is better than sharing multiple vectors */

        Exclusive = !!(Flags & IRQ_FLAG_EXCLUSIVE);
        Feasible = false;

        if (Flags & IRQ_FLAG_USE_VECTOR)
            SearchLo = SearchHi = *pIntVec;
        else
            SearchLo = 64, SearchHi = 251 - IDTBlockCount + 1;

        kRwSpinlockAcquireWriter(&s_IrqData.Lock);
        for (AllocIntVec = SearchLo; AllocIntVec <= SearchHi; AllocIntVec += IDTBlockCount) {
            Feasible = true;
            for (UINT32 i = 0; Feasible && (i < IDTBlockCount); i++) {
                /* Need only check head since shareable and exclusive are never mixed */
                auto *head = s_IrqData.Entries[AllocIntVec + i];
                if (head) {
                    if (Exclusive) {
                        Feasible = false;
                    }
                    else {
                        if (head->Flags & IRQ_FLAG_EXCLUSIVE) Feasible = false;
                    }
                    /* Stop early */
                    if (!Feasible) break;
                }
            }
            /* Stop if a feasible vector has been found */
            if (Feasible) break;
        }
        if (!Feasible) {
            kRwSpinlockReleaseWriter(&s_IrqData.Lock);
            kEnableInterrupt(bInterruptEnable);
            return E_INVALID_PARAMETER;
        }
    }

    /* Attach handler for each vector */

    for (UINT32 i = AllocIntVec; i < (AllocIntVec + IDTBlockCount); i++) {
        IrqHandlerEntry *Entry = (IrqHandlerEntry *)kMalloc(sizeof(IrqHandlerEntry));
        Entry->Next = s_IrqData.Entries[AllocIntVec];
        Entry->Handler = Handler;
        Entry->IDTBlockCount = IDTBlockCount;
        Entry->Flags = Flags;
        if (i == AllocIntVec) Entry->Flags |= IRQ_FLAG_VECTOR_HEAD;
        Entry->UserData = UserData;
        Entry->Rerouter = Rerouter;
        Entry->RerouterContext = RerouterContext;
        s_IrqData.Entries[i] = Entry;
    }

    kRwSpinlockReleaseWriter(&s_IrqData.Lock);
    kEnableInterrupt(bInterruptEnable);
    if (pIntVec != nullptr)
        *pIntVec = AllocIntVec;

    return E_SUCCESS;
}

OS_STATUS kUnregisterIrqCallback(IrqCallback Handler, UINT32 IntVec, void **pUserData) {
    BOOL bInterruptEnable;
    UINT32 SearchLo, SearchHi;
    UINT32 IDTBlockCount;
    IrqHandlerEntry *p, *prev;

    if (IntVec == 0xFFFFFFFF)
        SearchLo = 0, SearchHi = 251;
    else if (IntVec <= 251)
        SearchLo = SearchHi = IntVec;
    else
        return E_INVALID_PARAMETER;

    bInterruptEnable = kEnableInterrupt(FALSE);
    kRwSpinlockAcquireWriter(&s_IrqData.Lock);

    /* Validate the given vector is the first vector of the block */

    IDTBlockCount = 0;
    for (UINT32 i = SearchLo; i <= SearchHi; i++) {
        for (p = s_IrqData.Entries[i]; p != nullptr; p = p->Next) {
            if (p->Handler == Handler && (p->Flags & IRQ_FLAG_VECTOR_HEAD)) {
                if (IDTBlockCount > 0) {
                    kRwSpinlockReleaseWriter(&s_IrqData.Lock);
                    kEnableInterrupt(bInterruptEnable);
                    return E_UNSUPPORTED;
                }
                IntVec = i;
                IDTBlockCount = p->IDTBlockCount;

                /* We stop here; otherwise handlers registered for
                 * the same handler multiple times won't have any chance
                 * to get removed. */
                break;
            }
        }
        /* Although we've found one, we may have more entries
         * with the same handler for another vector(s);
         * hence we keep searching to ensure the entry is unique. */
    }
    if (IDTBlockCount == 0) {
        kRwSpinlockReleaseWriter(&s_IrqData.Lock);
        kEnableInterrupt(bInterruptEnable);
        return E_NOTFOUND;
    }

    /* Actual removal of handler(s) */

    for (UINT32 i = IntVec; i < (IntVec + IDTBlockCount); i++) {
        prev = nullptr;
        for (p = s_IrqData.Entries[i]; p != nullptr; p = p->Next) {
            if (p->Handler == Handler) {
                if (pUserData)
                    *pUserData = p->UserData;

                if (prev)
                    prev->Next = p->Next;
                else
                    s_IrqData.Entries[i] = p->Next;

                kFree(p);
                break;
            }
            prev = p;
        }
        if (p == nullptr) {
            kRwSpinlockReleaseWriter(&s_IrqData.Lock);
            kConsolePrintf(TEXT("Fatal error: IRQ table corrupt; handler for IRQ vector %u not found\r\n"), i);
            kHang();
        }
    }

    kRwSpinlockReleaseWriter(&s_IrqData.Lock);
    kEnableInterrupt(bInterruptEnable);
    return E_SUCCESS;
}

// The interrupt handler routine. This routine is called from
// the assembly stub. Note that the parameter ErrorCode is
// only valid for interrupts that have an error code.
extern "C" void kInterruptHandler(UINT32 IntVec, Context *pContext, UINT64 ErrorCode) {
#if 0
    kConsolePrintf(TEXT("Interrupt %llu occurred in Core %u! SS=0x%X, Return RIP=%p, RSP=%p, RFLAGS=%llX, SS=0x%X\r\n"),
        IntVec, kApicGetApicID(), kReadSS(), *(UINT64 *)(((BYTE *)pContext) + 144 + 8),
        *(UINT64 *)(((BYTE *)pContext) + 144 + 32),
        *(UINT64 *)(((BYTE *)pContext) + 144 + 24),
        *(UINT16 *)(((BYTE *)pContext) + 144 + 40));
    kConsolePrintf(TEXT("Context = %p\r\n"), pContext);
#endif
    IrqContext irq_ctx;

    /* Invoke through the current invocation list */

    kRwSpinlockAcquireReader(&s_IrqData.Lock);
    for (IrqHandlerEntry *p = s_IrqData.Entries[IntVec]; p != nullptr; p = p->Next) {
        void *Param;
        switch (p->Flags & IRQ_FLAG_PARAM_MASK) {
        case IRQ_FLAG_PARAM_USERDATA:
            Param = p->UserData;
            break;
        case IRQ_FLAG_PARAM_CONTEXT:
            Param = pContext;
            break;
        case IRQ_FLAG_PARAM_EXTENDED:
            irq_ctx.UserData = p->UserData;
            irq_ctx.pContext = pContext;
            irq_ctx.ErrorCode = ErrorCode;
            irq_ctx.IntVec = IntVec;
            Param = &irq_ctx;
            break;
        case IRQ_FLAG_PARAM_NONE:
        default:
            Param = nullptr;
            break;
        }
        p->Handler(Param);
    }
    kRwSpinlockReleaseReader(&s_IrqData.Lock);

    if (IntVec >= 32 && IntVec != 0x80 && IntVec != IRQ_SCHED_SYNC && IntVec != IRQ_VM_SYNC) {
        kApicSendEoi();
    }
#if 0
    kConsolePrintf(TEXT("Context = %p\r\n"), pContext);
    kConsolePrintf(TEXT("Interrupt %llu final in Core %u! SS=0x%X, Return RIP=%p, RSP=%p, RFLAGS=%llX, SS=0x%X\r\n"),
        IntVec, kApicGetApicID(), kReadSS(), *(UINT64 *)(((BYTE *)pContext) + 144 + 8),
        *(UINT64 *)(((BYTE *)pContext) + 144 + 32),
        *(UINT64 *)(((BYTE *)pContext) + 144 + 24),
        *(UINT16 *)(((BYTE *)pContext) + 144 + 40));
#endif
}
