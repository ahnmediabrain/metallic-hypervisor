
#include "stdafx.h"
#include "hw/apic/Apic.h"
#include "AsmUtils.h"
#include "Console.h"
#include "CoreLocalStorage.h"
#include "Debug.h"
#include "MemMgr.h"
#include "StdLib.h"
#include "Scheduler.h"
#include "SchedulerCore.h"


volatile UINTN g_ThreadIDCounter = 0;
static void ThreadEntry(THREADENTRYPOINT EntryPoint, void *UserData);

OS_STATUS kCreateProcess(CreateProcessConfig *pConfig, ProcessPointle *pProcessPointle) {
    return OS_STATUS();
}

OS_STATUS kCreateThread(CreateThreadConfig *pConfig, ThreadPointle *pThreadPointle) {
    kAssert(pConfig->Parent == EMPTY_HANDLE); // FIXME (not implemented yet)
    if (pConfig->StackSize == 0 || pConfig->StackSize > 16777216)
        return E_INVALID_PARAMETER;

    UINTN StackSize = ((pConfig->StackSize + 4095) / 4096) * 4096;
    Thread *pThread = (Thread *)kMalloc(sizeof(Thread));
    pThread->RefCount = 1; // reference count for the scheduler (decremented upon removal from the runqueue)
    pThread->ThreadID = kInterlockedIncrement64(&g_ThreadIDCounter);
    pThread->Parent = nullptr;
    pThread->Flags = 0;
    pThread->UserData = pConfig->UserData;
    pThread->Stack = kAllocPages(StackSize / 4096);
    pThread->StackSize = StackSize;

    kMemSet(&pThread->ThreadContext, 0xCD, sizeof(Context));
    kMemSet(&pThread->ThreadSIMDContext, 0xCD, sizeof(SIMDContext));
    pThread->ThreadContext.RFLAGS = 0x200; // Enable Interrupts
    pThread->ThreadContext.RCX = (UINT64)pConfig->EntryPoint;
    pThread->ThreadContext.RDX = (UINT64)pConfig->UserData;
    pThread->ThreadContext.RIP = (UINT64)ThreadEntry;
    pThread->ThreadContext.RSP = (UINT64)((UINTN)pThread->Stack + StackSize - 32);

    kSpinlockInit(&pThread->Lock);
    kConsolePrintf(TEXT("Thread %llu created, ptr=%p, stack=%p\r\n"), pThread->ThreadID, pThread, pThread->Stack);

    if (pThreadPointle != nullptr) {
        pThread->RefCount++;
        *pThreadPointle = (ThreadPointle)pThread;
    }
    kAddToRunQueue(pThread);

    return E_SUCCESS;
}

void kExitThread(QWORD ExitCode)
{
    // We need not worry about the thread object validity,
    // because the scheduler holds reference count
    // until it removes current thread from the runqueue.
    SchedLocalData *pLocalData = (SchedLocalData *)kGetCoreLocalStorage()->SchedulerData;
    Thread *pThread = pLocalData->CurrentThread;
    kAssert(pThread != nullptr);

    // Set the kill-pending flag and invoke scheduler.
    // The scheduler will kill this thread.
    pThread->Flags |= THREAD_KILL_PENDING;
    while (1) {
        kYield();
    }
}

OS_STATUS kKillThread(ThreadPointle Pointle)
{
    Thread *pThread = (Thread *)Pointle;
    kSpinlockAcquire(&pThread->Lock);
    pThread->Flags |= THREAD_KILL_PENDING;
    kSpinlockRelease(&pThread->Lock);
    return E_SUCCESS;
}

OS_STATUS kKillProcess(ProcessPointle Pointle)
{
    return OS_STATUS();
}

UINTN kIncrementThreadRefCount(ThreadPointle Pointle) {
    return kIncrementThreadRefCount((Thread *)Pointle);
}

UINTN kDecrementThreadRefCount(ThreadPointle Pointle) {
    return kDecrementThreadRefCount((Thread *)Pointle);
}

ThreadPointle kGetCurrentThread() {
    SchedLocalData *pLocalData = (SchedLocalData *)kGetCoreLocalStorage()->SchedulerData;
    return (ThreadPointle)pLocalData->CurrentThread;
}

UINTN kGetCurrentThreadID() {
    Thread *CurrentThread = (Thread *)kGetCurrentThread();
    if (CurrentThread != nullptr) {
        return CurrentThread->ThreadID;
    }
    return IDLE_THREAD_ID_BASE - (UINTN)kApicGetApicID();
}

UINTN kGetThreadID(ThreadPointle Pointle) {
    Thread *pThread = (Thread *)Pointle;
    return pThread->ThreadID;
}

void *kSetThreadUserData(void *NewValue) {
    Thread *CurrentThread = (Thread *)kGetCurrentThread();
    kSpinlockAcquire(&CurrentThread->Lock);
    void *OldValue = CurrentThread->UserData;
    CurrentThread->UserData = NewValue;
    kSpinlockRelease(&CurrentThread->Lock);
    return OldValue;
}

void *kGetThreadUserData() {
    Thread *CurrentThread = (Thread *)kGetCurrentThread();
    return CurrentThread->UserData;
}

// Marshalling routine for thread entry
static void ThreadEntry(THREADENTRYPOINT EntryPoint, void *UserData) {
    UINTN ExitCode = EntryPoint(UserData);
    kExitThread(ExitCode);
}

// Threading functionality test
static UINTN kTestThread(void *UserData) {
    UINTN ThreadID = kGetCurrentThreadID();
    for (UINTN i = 1; i <= 30; i++) {
        kConsolePrintf(TEXT("Thread %lld is running on core %u, count = %lld; RFLAGS=%llX\r\n"),
            ThreadID, kApicGetApicID(), i, kReadRFlags());
        if (UserData != nullptr && i == 10) {
            ThreadPointle tp = (ThreadPointle)UserData;
            kDecrementThreadRefCount(tp);
            kKillThread(tp);
        }
        kHalt();
        //for (int j = 0; j < 100000000; j++);
        //kConsolePrintf(TEXT("%u-%X-%llX "), kLocalApicReadReg32(LOCAL_APIC_TIMER_CURCOUNT), kLocalApicReadReg32(LOCAL_APIC_TIMER_LVT), kReadRFlags());
    }
    return 0;
}
void kTestThreading() {
    ThreadPointle T[7];
    for (UINTN i = 0; i < 7; i++) {
        CreateThreadConfig threadConfig;
        threadConfig.Parent = EMPTY_HANDLE;
        threadConfig.StackSize = KERNEL_THREAD_STACKSZ_DEFAULT;
        threadConfig.EntryPoint = kTestThread;
        threadConfig.UserData = nullptr;
        if (i == 3) threadConfig.UserData = (void *)T[0];
        if (i == 4) threadConfig.UserData = (void *)T[1];
        ThreadPointle p;
        kCreateThread(&threadConfig, &p);
        if (i == 0) T[0] = p;
        if (i == 1) T[1] = p;
    }
}
