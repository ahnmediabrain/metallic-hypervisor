﻿
#pragma once

void kConsoleInitAsDevNull();
void kConsoleInit(K_GRAPHICS_MODE_DESC *pGMDesc, void *pFontFileOnMemory);
UINT32 kConsoleWidth();
UINT32 kConsoleHeight();
void kConsoleGotoXY(UINT32 X, UINT32 Y);
UINT32 kConsolePosX();
UINT32 kConsolePosY();
void kConsolePutCharAtCurrentPos(CHAR16 c);
void kConsolePutChar(CHAR16 c); // put char and advance caret by one
void kConsolePuts(const CHAR16 *str);
void kConsolePrintf(const CHAR16 *FormatString, ...);
void kConsoleVPrintf(const CHAR16 *FormatString, va_list vl);
void kConsolePrintf8(const char *FormatString, ...);
void kConsoleVPrintf8(const char *FormatString, va_list vl);
void kConsoleAdvanceCaret();
void kConsoleClearCurrentPos();
void kConsoleClearScreen();

void kConsoleRunTest();
