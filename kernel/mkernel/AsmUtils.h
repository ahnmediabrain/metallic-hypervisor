﻿
#pragma once

struct CPUID_RESULT {
    DWORD       EAX;
    DWORD       EBX;
    DWORD       ECX;
    DWORD       EDX;
};

extern "C" {
    void kDisablePIC();
    UINT64 kReadMSR(UINT32 RegisterNum);
    void kWriteMSR(UINT32 RegisterNum, UINT64 Value);
    UINT64 kReadTSC();
    BYTE kPortIn8(WORD PortNum);
    UINT16 kPortIn16(WORD PortNum);
    UINT32 kPortIn32(WORD PortNum);
    void kPortOut8(WORD PortNum, BYTE Value);
    void kPortOut16(WORD PortNum, UINT16 Value);
    void kPortOut32(WORD PortNum, UINT32 Value);
    void kHalt();
    void kPause();
    // Toggles interrupt enable/disable and returns previous enable state
    BOOL kEnableInterrupt(BOOL bEnable);
    // Readss the value in the target address atomically.
    UINT64 kInterlockedRead64(UINT64 volatile *Target);
    // Returns the value in the target address before execution.
    UINT64 kInterlockedExchange64(UINT64 volatile *Target, UINT64 NewValue);
    // Returns the value in the target address before execution.
    UINT64 kInterlockedCompareAndExchange64(UINT64 volatile *Target, UINT64 Comparand, UINT64 NewValue);
    // Returns the incremented value.
    UINT64 kInterlockedIncrement64(UINT64 volatile *Target);
    // Returns the decremented value.
    UINT64 kInterlockedDecrement64(UINT64 volatile *Target);
    // Querys CPUID.
    void kCPUID(DWORD EAX, DWORD ECX, CPUID_RESULT *Result);
    // Loads specified adderss to GDTR.
    void kLoadGDTR(void *Address, WORD NewCS, WORD NewDS);
    // Loads specified address to IDTR.
    void kLoadIDTR(void *Address);
    // Loads specified TSS selector to TR.
    void kLoadTR(WORD NewSelector);
    // Reads the value of Control Register 0 (CR0).
    UINT64 kReadCR0();
    // Writes the value to Control Register 0 (CR0).
    void kWriteCR0(UINT64 NewValue);
    // Reads the value of Control Register 3 (CR3).
    UINT64 kReadCR3();
    // Writes the value to Control Register 3 (CR3).
    void kWriteCR3(UINT64 NewValue);
    // Reads the value of Control Register 4 (CR4).
    UINT64 kReadCR4();
    // Writes the value to Control Register 4 (CR4).
    void kWriteCR4(UINT64 NewValue);
    // Reads the RFLAGS register.
    UINT64 kReadRFlags();
    // Writes to the RFLAGS register.
    void kWriteRFlags(UINT64 NewValue);
    // Writes back all modified cache lines in the processor's
    // internal cache to main memory and invalidates (flushes) the
    // internal caches. The instruction then issues a special-function
    // bus cycle that directs external caches to also write back
    // modified data and another bus cycle to indicate that the
    // external caches should be invalidated.
    void kWriteBackInvalidateCache();
    // Generates the specified interrupt vector.
    void kGenerateInterrupt(BYTE IRQNumber);
    // Computes the position of the highest bit set.
    // Returns (UINT64)-1 when the supplied value is zero.
    UINT64 kHighestBitSet(UINT64 Value);
    // Computes the position of the lowest bit set.
    // Returns (UINT64)-1 when the supplied value is zero.
    UINT64 kLowestBitSet(UINT64 Value);
    UINT16 kReadSS();
}
