_DATA SEGMENT
_DATA ENDS

_TEXT SEGMENT

kDisablePIC PROC
	; Code borrowed directly from https://wiki.osdev.org/PIC
    mov al, 0ffh
    out 0a1h, al
    out 21h, al
	ret
kDisablePIC ENDP

kReadMSR PROC
	rdmsr           ; First parameter is stored on RCX by calling convention
                    ; So, no need to move it before calling rdmsr
	shl rdx, 32     ; Upper 32 bits of RDX and RAX are cleared automatically
	or rax, rdx
	ret
kReadMSR ENDP

; ECX = register number
; RDX = value to write
kWriteMSR PROC
    mov eax, edx
	shr rdx, 32
    wrmsr
    ret
kWriteMSR ENDP

kReadTSC PROC
    rdtsc           ; Stores 64-bit timestamp counter to EDX:EAX
    shl rdx, 32
	or rax, rdx
    ret
kReadTSC ENDP

kPortIn8 PROC
    mov dx, cx
	in al, dx
    ret
kPortIn8 ENDP

kPortIn16 PROC
    mov dx, cx
	in ax, dx
    ret
kPortIn16 ENDP

kPortIn32 PROC
    mov dx, cx
	in eax, dx
    ret
kPortIn32 ENDP

kPortOut8 PROC
    mov al, dl
    mov dx, cx
    out dx, al
    ret
kPortOut8 ENDP

kPortOut16 PROC
    mov ax, dx
    mov dx, cx
    out dx, ax
    ret
kPortOut16 ENDP

kPortOut32 PROC
    mov eax, edx
    mov dx, cx
    out dx, eax
    ret
kPortOut32 ENDP

kHalt PROC
	hlt
	ret
kHalt ENDP

kPause PROC
	pause
	ret
kPause ENDP

; rcx = Enable if not FALSE
kEnableInterrupt PROC
	; Save previous state
	pushfq
	pop rax
	shr rax, 9			; bit 9 of RFLAGS = IF (interrupt flag)
	and rax, 1
	; Apply new state
	test rcx, rcx
	jz kEnableInterrupt_Disable
kEnableInterrupt_Enable:
	sti
	ret
kEnableInterrupt_Disable:
	cli
	ret
kEnableInterrupt ENDP

; rcx = Target Address
kInterlockedRead64 PROC
	xor rax, rax
	lock xadd [rcx], rax		; Add zero
	ret
kInterlockedRead64 ENDP

; rcx = Target Address
; rdx = New Value
kInterlockedExchange64 PROC
	mov rax, rdx
	lock xchg [rcx], rax
	ret
kInterlockedExchange64 ENDP

; rcx = Target Address
; rdx = Comparand
; r8  = New Value
kInterlockedCompareAndExchange64 PROC
	mov rax, rdx
	lock cmpxchg [rcx], r8
	ret
kInterlockedCompareAndExchange64 ENDP

; rcx = Target Address
kInterlockedIncrement64 PROC
	mov rax, 1
	lock xadd [rcx], rax
	inc rax
	ret
kInterlockedIncrement64 ENDP

; rcx = Target Address
kInterlockedDecrement64 PROC
	mov rax, -1
	lock xadd [rcx], rax
	dec rax
	ret
kInterlockedDecrement64 ENDP

; rcx = EAX input
; rdx = ECX input
; r8  = Target Address
kCPUID PROC
	mov eax, ecx
	mov ecx, edx
	cpuid
	mov [r8+ 0], eax
	mov [r8+ 4], ebx
	mov [r8+ 8], ecx
	mov [r8+12], edx
	ret
kCPUID ENDP

; rcx = Target Address
; dx  = New CS
; r8w = New DS
kLoadGDTR PROC
	and rdx, 0ffffh
	push rdx
	mov rdx, kLoadGDTR_UpdateSegments
	push rdx
	db 0Fh			; lgdt [rcx]
	db 01h
	db 11h
	retfq
kLoadGDTR_UpdateSegments:
	mov ax, r8w
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	ret
kLoadGDTR ENDP

; rcx = Target Address
kLoadIDTR PROC
	db 0Fh			; lidt [rcx]
	db 01h
	db 19h
	ret
kLoadIDTR ENDP

; cx = New Selector Value
kLoadTR PROC
	ltr cx
	ret
kLoadTR ENDP

kReadCR0 PROC
	mov rax, cr0
	ret
kReadCR0 ENDP

; rcx = New CR0 Value
kWriteCR0 PROC
	mov cr0, rcx
	ret
kWriteCR0 ENDP

kReadCR3 PROC
	mov rax, cr3
	ret
kReadCR3 ENDP

; rcx = New CR3 Value
kWriteCR3 PROC
	mov cr3, rcx
	ret
kWriteCR3 ENDP

kReadCR4 PROC
	mov rax, cr4
	ret
kReadCR4 ENDP

; rcx = New CR4 Value
kWriteCR4 PROC
	mov cr4, rcx
	ret
kWriteCR4 ENDP

kReadRFlags PROC
	pushfq
	pop rax
	ret
kReadRFlags ENDP

; rcx = New RFLAGS value
kWriteRFlags PROC
	push rcx
	popfq
	ret
kWriteRFlags ENDP

kWriteBackInvalidateCache PROC
	wbinvd
	ret
kWriteBackInvalidateCache ENDP

; cl = IRQ number to generate
kGenerateInterrupt PROC
	xor rax, rax
	mov al, cl
	shl rax, 8
	add rax, 0C300CDh		; cd YZ : int 0xYZ
							; c3    : ret
	push rax
	call rsp
	add rsp, 8
	ret
kGenerateInterrupt ENDP

; rcx = Value to test
kHighestBitSet PROC
	test rcx, rcx
	jz kHighestBitSet_zero
	bsr rax, rcx
	ret
kHighestBitSet_zero:
	xor rax, rax
	dec rax
	ret
kHighestBitSet ENDP

; rcx = Value to test
kLowestBitSet PROC
	test rcx, rcx
	jz kLowestBitSet_zero
	bsf rax, rcx
	ret
kLowestBitSet_zero:
	xor rax, rax
	dec rax
	ret
kLowestBitSet ENDP

kReadSS PROC
	mov ax, ss
	ret
kReadSS ENDP

_TEXT ENDS

END
