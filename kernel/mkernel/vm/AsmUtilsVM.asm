_DATA SEGMENT
_DATA ENDS

_TEXT SEGMENT

; Note that many of these instructions require virtual address
; where the physical address (64 bits) is stored.
; Hence we store the physical address temporarily on stack.

; rcx = Physical Address (Not Virtual) to VM Control Structure (VMCS)
kVmxVMClear PROC
	push rcx
	vmclear QWORD PTR [rsp]
	pushfq
	pop rax
	add rsp, 8
	ret
kVmxVMClear ENDP

kVmxVMLaunch PROC
	vmlaunch
	pushfq
	pop rax
	ret
kVmxVMLaunch ENDP

kVmxVMResume PROC
	vmresume
	pushfq
	pop rax
	ret
kVmxVMResume ENDP

; rcx = Physical Address (Not Virtual) to VM Control Structure (VMCS)
kVmxVMPtrLd PROC
	push rcx
	vmptrld QWORD PTR [rsp]
	pushfq
	pop rax
	add rsp, 8
	ret
kVmxVMPtrLd ENDP

; rcx = Address to Store the Pointer to VM Control Structure (VMCS)
kVmxVMPtrSt PROC
	vmptrst QWORD PTR [rcx]
	pushfq
	pop rax
	ret
kVmxVMPtrSt ENDP

; rcx = Field Index to Read
; rdx = Memory Address to Store Read Value
kVmxVMRead PROC
	vmread QWORD PTR [rdx], rcx
	pushfq
	pop rax
	ret
kVmxVMRead ENDP

; rcx = Field Index to Write
; rdx = Value to Write
kVmxVMWrite PROC
	push rdx
	vmwrite rcx, QWORD PTR [rsp]
	pushfq
	pop rax
	add rsp, 8
	ret
kVmxVMWrite ENDP

; rcx = Physical Address (Not Virtual) Pointing to the VMXON Region
kVmxOn PROC
	push rcx
	vmxon QWORD PTR [rsp]
	pushfq
	pop rax
	add rsp, 8
	ret
kVmxOn ENDP

kVmxOff PROC
	vmxoff
	pushfq
	pop rax
	ret
kVmxOff ENDP

_TEXT ENDS

END
