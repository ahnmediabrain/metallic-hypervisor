
#include "stdafx.h"
#include "AsmUtils.h"
#include "Console.h"
#include "Debug.h"
#include "CoreLocalStorage.h"
#include "GlobalVariable.h"
#include "MemMgr.h"
#include "PeLoader.h"
#include "Ramdisk.h"
#include "SyncObjs.h"
#include <MSR.h>

#include "vm/AsmUtilsVM.h"
#include "vm/EPT.h"
#include "vm/VMCore.h"
#include "vm/VM.h"


OS_STATUS kVMCreate(VMConfig *pVMConfig, VMHandle *pVMH) {
    VirtualMachine *pVM = (VirtualMachine *)kMalloc(sizeof(VirtualMachine));

    // Save setup
    // (actual VM creation will be done upon power-on)
    pVM->VCpuCount = pVMConfig->VCpuCount;
    pVM->MemoryInBytes = ((pVMConfig->MemoryInBytes + 4095) / 4096) * 4096;

    // Miscellaneous setup
    kSpinlockInit(&pVM->Lock);
    pVM->State = VM_STATE_OFF;

    *pVMH = (VMHandle)pVM;
    return E_SUCCESS;
}
OS_STATUS kVMDestroy(VMHandle VMH) {
    VirtualMachine *pVM = (VirtualMachine *)VMH;
    switch (pVM->State) {
    case VM_STATE_OFF:
        break;
    case VM_STATE_RUNNING:
        kVMPowerOff(VMH);
        break;
    default:
        kAssert(false);
        return E_NOTIMPL;
    }

    kFree(pVM);
    return E_SUCCESS;
}

OS_STATUS kVMPowerOn(VMHandle VMH) {
    VirtualMachine *pVM = (VirtualMachine *)VMH;
    switch (pVM->State) {
    case VM_STATE_OFF:
        kVMCoreStartVM(pVM);
        return E_SUCCESS;
    }
    return E_NOTIMPL;
}
OS_STATUS kVMPowerOff(VMHandle VMH) {
    VirtualMachine *pVM = (VirtualMachine *)VMH;
    switch (pVM->State) {
    case VM_STATE_RUNNING:
        kVMCoreTerminateVM(pVM, VM_TERMINATION_EXTRQ, false);
        return E_SUCCESS;
    }
    return E_NOTIMPL;
}

OS_STATUS kVMHibernate(VMHandle VMH) {
    return E_NOTIMPL;
}

OS_STATUS kVMResumeFromHibernation(VMHandle VMH) {
    return E_NOTIMPL;
}

OS_STATUS kVMPause(VMHandle VMH) {
    return E_NOTIMPL;
}

OS_STATUS kVMResume(VMHandle VMH) {
    return E_NOTIMPL;
}
