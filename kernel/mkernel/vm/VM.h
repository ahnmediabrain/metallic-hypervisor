
#pragma once

#include "VMUtils.h"


// The opaque pointer type for virtual machine data structure.
typedef UINTN VMHandle;

struct VMConfig {
    UINT32      VCpuCount;
    UINT32      MemoryInBytes;
};

OS_STATUS kVMCreate(VMConfig *pVMConfig, VMHandle *pVMH);
OS_STATUS kVMDestroy(VMHandle VMH);

OS_STATUS kVMPowerOn(VMHandle VMH);
OS_STATUS kVMPowerOff(VMHandle VMH);
OS_STATUS kVMHibernate(VMHandle VMH);
OS_STATUS kVMResumeFromHibernation(VMHandle VMH);
OS_STATUS kVMPause(VMHandle VMH);
OS_STATUS kVMResume(VMHandle VMH);


// Possible VM states
#define VM_STATE_OFF                    0
#define VM_STATE_RUNNING                1
#define VM_STATE_PAUSED                 2
#define VM_STATE_ACPI_SLEEP             3
#define VM_STATE_HIBERNATE              4
#define VM_STATE_TERMINATION_PENDING    5

// Possible VM termination reasons
#define VM_TERMINATION_EXTRQ            1   // Termination due to external request
#define VM_TERMINATION_ACPI             2   // Power-off through ACPI request from guest
#define VM_TERMINATION_TRIPLE_FAULT     3   // Guest OS incurred a triple fault
#define VM_TERMINATION_UNHANDLED_VMEXIT 4   // VM-exit was unhandled
#define VM_TERMINATION_LAUNCH_FAILURE   5   // VM launch (or resume) has failed
