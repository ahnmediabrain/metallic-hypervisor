
#pragma once

#include "Context.h"

struct VirtualMachine;

struct alignas(4096) VirtualCpu {
    alignas(4096) BYTE  Vmcs[4096];
    alignas(4096) BYTE  VirtualApicState[4096];
    BYTE                Stack[16384];
    Context             GeneralPurposeRegs;
    SIMDContext         FloatingPointRegs;
    struct {
        QWORD           CR2;
    } ControlRegs;
    VirtualMachine     *Machine;
    UINTN               VCpuThreadID;
    DWORD               LastCpuApicID;
    DWORD               RunState;
    DWORD               Flags;
};

#include "VMCore.h"

// Creates a VCPU and puts it in an idle state.
OS_STATUS kVCpuCreate(VirtualMachine *pVM, VirtualCpu **ppVCpu);
// Launches the specified VCPU on current PCPU.
OS_STATUS kVCpuLaunch(VirtualCpu *pVCpu);
OS_STATUS kVCpuResume(VirtualCpu *pVCpu);
// Destroys the specified VCPU.
OS_STATUS kVCpuDestroy(VirtualCpu *pVCpu);


#define VCPU_STATE_RUNNABLE         0
#define VCPU_STATE_RUNNING          1
#define VCPU_STATE_HLT              2

#define VCPU_FLAG_INT_ENABLE        (1UL << 0)
