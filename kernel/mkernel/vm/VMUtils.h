
#pragma once


// Checks whether or not a VMX instruction failed
// using the error information stored in the RFLAGS register.
void kVMCheckError(UINT64 RFlags);

// Initializes a VMCS region to be used with VMX instructions.
// The Intel manual states that any VMCS region must be initialized
// with VMCS revision identifier at the first 4 bytes.
void kInitializeVMCS(void *VmcsPtr);

// Checks whether current hardware supports all of the
// necessary virtualization features, and hangs if not.
void kEnsureHWVirtualizationSupport();

// Enters VMX operation on current core.
// Note that the function depends on the Core Local Storage
// as it uses it for VMXON region.
void kEnterVMXOperation();
