
#include "stdafx.h"
#include "AsmUtils.h"
#include "Console.h"
#include "Debug.h"
#include "CoreLocalStorage.h"
#include "MemMgr.h"
#include <MSR.h>

#include "hw/apic/Apic.h"

#include "vm/AsmUtilsVM.h"
#include "vm/VMUtils.h"


void kVMCheckError(UINT64 RFlags) {
    if (RFlags & FLAGS_CF) {
        kConsolePrintf(TEXT("VM Error occurred with invalid VMCS pointer\r\n"));
        kAssert(false);
    }
    if (RFlags & FLAGS_ZF) {
        kConsolePrintf(TEXT("VM Error occurred with valid VMCS pointer\r\n"));
        kAssert(false);
    }
}

void kInitializeVMCS(void *VmcsPtr) {
    // Bit 30:0 of the MSR IA32_VMX_BASIC must be copied to
    // the first four bytes of the VMXON region.
    UINT64 VMXBasicMSR = kReadMSR(IA32_VMX_BASIC);
    *(DWORD *)VmcsPtr = (DWORD)VMXBasicMSR & 0x7fffffff;
}

void kEnsureHWVirtualizationSupport() {
    // Check if VT-x is supported
    CPUID_RESULT cpuid;
    kCPUID(1, 0, &cpuid);
    if ((cpuid.ECX & (1 << 5)) == 0) {
        if (kIsBSP()) kConsolePrintf(TEXT("VM Error: This CPU doesn't support VT-x\r\n"));
        kHang();
    }
    // Check if EPT is supported
    UINT64 MSR_IA32_VMX_PROCBASED_CTLS = kReadMSR(IA32_VMX_PROCBASED_CTLS);
    if ((MSR_IA32_VMX_PROCBASED_CTLS & (1ULL << 63)) == 0) {
        if (kIsBSP()) kConsolePrintf(TEXT("VM Error: This CPU doesn't support MSR IA32_VMX_PROCBASED_CTLS2"));
        kHang();
    }
    UINT64 EPTSupportMSR = kReadMSR(IA32_VMX_PROCBASED_CTLS2);
    if ((EPTSupportMSR & (1ULL << 33)) == 0) {
        if (kIsBSP()) kConsolePrintf(TEXT("VM Error: This CPU doesn't support EPT (extended page tables)\r\n"));
        kHang();
    }
    // Check if INVEPT instruction is supported
    UINT64 INVEPTSupportMSR = kReadMSR(IA32_VMX_EPT_VPID_CAP);
    if ((INVEPTSupportMSR & (1ULL << 20)) == 0) {
        if (kIsBSP()) kConsolePrintf(TEXT("VM Error: This CPU doesn't support INVEPT instruction\r\n"));
        kHang();
    }
    // Check if unrestricted guest is supported
    UINT64 UnrestrictedGuestSupportMSR = kReadMSR(IA32_VMX_MISC);
    if ((UnrestrictedGuestSupportMSR & (1ULL << 5)) == 0) {
        if (kIsBSP()) kConsolePrintf(TEXT("VM Error: This CPU doesn't support unrestricted guest\r\n"));
        kHang();
    }
    if (kIsBSP()) kConsolePrintf(TEXT("VM: Processor supports VT-x/EPT, INVEPT instruction, and unrestricted guest\r\n"));

    // Enable VT-x if not
    UINT64 FeatureControlMSR = kReadMSR(IA32_FEATURE_CONTROL);
    if (kIsBSP()) kConsolePrintf(TEXT("VM: Default IA32_FEATURE_CONTROL value = 0x%llX\r\n"), FeatureControlMSR);
    if ((FeatureControlMSR & IA32_FEATURE_CONTROL_LOCK) == 1) {
        if ((FeatureControlMSR & IA32_FEATURE_CONTROL_VMXOUTSMX) == 0) {
            if (kIsBSP()) kConsolePrintf(TEXT("VM Error: VT-x is locked off. Please contact the firmware vendor for solution.\r\n"));
            kHang();
        }
        else {
            if (kIsBSP()) kConsolePrintf(TEXT("VM: VT-x is locked on\r\n"));
        }
    }
    else {
        // Intel specification mandates locking the lock bit of the MSR
        // before executing VMX instructions.
        FeatureControlMSR |= IA32_FEATURE_CONTROL_LOCK;
        FeatureControlMSR |= IA32_FEATURE_CONTROL_VMXOUTSMX;
        kWriteMSR(IA32_FEATURE_CONTROL, FeatureControlMSR);
        if (kIsBSP()) kConsolePrintf(TEXT("VM: VT-x has been enabled\r\n"));
    }
}

void kEnterVMXOperation() {
    UINT64 CR0, CR4;
    kEnsureHWVirtualizationSupport();

    // CR4.VMXE[bit 13] must be set before entering VMX operation.
    CR4 = kReadCR4();
    CR4 |= 1ULL << 13;
    kWriteCR4(CR4);

    // Certain bits of CR0 and CR4 must be stay fixed.
    UINT64 CR0Fixed0 = kReadMSR(IA32_VMX_CR0_FIXED0);
    UINT64 CR0Fixed1 = kReadMSR(IA32_VMX_CR0_FIXED1);
    CR0 = kReadCR0();
    CR0 &= ~CR0Fixed0;
    CR0 |= CR0Fixed1;
    kWriteCR0(CR0);
    UINT64 CR4Fixed0 = kReadMSR(IA32_VMX_CR4_FIXED0);
    UINT64 CR4Fixed1 = kReadMSR(IA32_VMX_CR4_FIXED1);
    CR4 = kReadCR4();
    CR4 &= ~CR4Fixed0;
    CR4 |= CR4Fixed1;
    kWriteCR4(CR4);

    // The VMXON region is a special kind of VMCS;
    // hence it needs to be initialized in the same way as normal VMCS for VCPUs.
    void *VMXONRegionPtr = kGetCoreLocalStorage()->VMXONRegion;
    kInitializeVMCS(VMXONRegionPtr);

    // Enter VMX operation!
    UINTN VMXONPhysPtr = kVirt2Phys((UINTN)VMXONRegionPtr);
    kConsolePrintf(TEXT("Core %u: VMXON region physical address is %p\r\n"), kApicGetApicID(), VMXONPhysPtr);
    kVMCheckError(kVmxOn((void *)VMXONPhysPtr));
}
