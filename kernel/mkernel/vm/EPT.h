
#pragma once


#define MD_GUEST_ALLOW_READ     (1ULL << 0)
#define MD_GUEST_ALLOW_WRITE    (1ULL << 1)
#define MD_GUEST_ALLOW_EXECUTE  (1ULL << 2)

struct VMMemoryDescriptor {
    // Physical address of the memory chunk start.
    UINTN           PhysicalStart;
    // Virtual address of the memory chunk start. Used in kernel.
    UINTN           VirtualStart;
    // Address for the virtual machine.
    // Used for building EPT (extended page table).
    UINTN           GuestPhysicalStart;
    // Memory size in bytes. This size must be 4K-aligned.
    UINTN           Size;
    // Flags setting. (access control, etc.)
    UINTN           Flags;
};


// Creates EPT based on the given memory descriptors.
// Returns virtual address to EPT-PML4 table.
void *kCreateEPT(VMMemoryDescriptor *pMD, UINTN NumberOfDescriptors);
// Frees the memory of EPT allocated by kCreateEPT.
// void *EPTVirtAddr: virtual (not physical) address of the EPT-PML4 table.
void kFreeEPT(void *EPTVirtAddr);
