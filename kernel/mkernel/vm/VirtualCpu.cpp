
#include "stdafx.h"
#include "AsmUtils.h"
#include "MemMgr.h"
#include "Scheduler.h"

#include "hw/apic/Apic.h"

#include "vm/AsmUtilsVM.h"
#include "vm/VMUtils.h"
#include "vm/VMXConstants.h"
#include "vm/VirtualCpu.h"
#include "vm/VM.h"
#include "vm/VMCore.h"


static UINTN kVCpuThread(void *UserData);
static void kVMExitHandler();

OS_STATUS kVCpuCreate(VirtualMachine *pVM, VirtualCpu **ppVCpu) {
    VirtualCpu *pVCpu = (VirtualCpu *)kAllocPages((sizeof(VirtualCpu) + 4095) / 4096);

    kInitializeVMCS(pVCpu->Vmcs);
    pVCpu->LastCpuApicID = kApicGetApicID();
    pVCpu->Machine = pVM;
    pVCpu->RunState = VCPU_STATE_RUNNABLE;
    pVCpu->Flags = 0;
    *ppVCpu = pVCpu;

    return E_SUCCESS;
}
OS_STATUS kVCpuLaunch(VirtualCpu *pVCpu) {
    CreateThreadConfig threadConfig;
    threadConfig.Parent = EMPTY_HANDLE;
    threadConfig.EntryPoint = kVCpuThread;
    threadConfig.StackSize = KERNEL_THREAD_STACKSZ_DEFAULT;
    threadConfig.UserData = pVCpu;

    ThreadPointle Handle;
    OS_STATUS e = kCreateThread(&threadConfig, &Handle);
    if (E_FAILED(e)) return e;

    pVCpu->VCpuThreadID = kGetThreadID(Handle);
    return E_SUCCESS;
}
OS_STATUS kVCpuResume(VirtualCpu *pVCpu) {
    return E_NOTIMPL;
}
OS_STATUS kVCpuDestroy(VirtualCpu *pVCpu) {
    return E_NOTIMPL;
}


static UINTN kVCpuThread(void *UserData) {
    VirtualCpu *pVCpu = (VirtualCpu *)UserData;

    if (pVCpu->Machine->State == VM_STATE_TERMINATION_PENDING) {
        kInterlockedIncrement64(&pVCpu->Machine->JoinedVCpuCount);
        kExitThread(0);
    }

    // Load VMCS on curent PCPU

    // Setup guest-state fields

    // Setup host-state fields
    kVmxVMWrite(VMCSF_HOST_RIP, (unsigned long long)&kVMExitHandler);
    kVmxVMWrite(VMCSF_HOST_RSP, (unsigned long long)(pVCpu->Stack + sizeof(pVCpu->Stack)));

    // Setup control fields
    kVmxVMWrite(VMCSF_EPT_PTR_FULL, (unsigned long long)pVCpu->Machine->ExtendedPageTable);

    // Launch!
    UINT64 RFlags = kVmxVMLaunch();

    // If the below code is executed, it means launch failure.
    // Hence we terminate the entire VM.
    kVMCoreTerminateVM(pVCpu->Machine, VM_TERMINATION_LAUNCH_FAILURE, true);
    return 0; // Never executes.
}

static void kVMExitHandler() {
    // Note:
    //   1> This handler belongs to the thread of the interrupted VCPU.
    //   2> Upon VM exit, the processor automatically disables interrupt.
    //      Hence it needs to be re-enabled.

    ThreadPointle VCpuThread = kGetCurrentThread();
    VirtualCpu *pVCpu = (VirtualCpu *)kGetThreadUserData();

    UINT64 ExitReason;
    bool Handled, Terminated;

    Handled = false;
    Terminated = false;
    kVmxVMRead(VMCSF_EXIT_REASON, &ExitReason);

    switch (ExitReason) {
    case EXIT_REASON_NMI:
        break;
    case EXIT_REASON_EXT_INT:
        // Enabling interrupt will invoke the host handler
        // through the IDT.
        kEnableInterrupt(TRUE);
        // After processing interrupt, the handler will return here.
        Handled = true;
        break;
    case EXIT_REASON_TRIPLE_FAULT:
        kVMCoreTerminateVM(pVCpu->Machine, VM_TERMINATION_TRIPLE_FAULT, true);
        Terminated = true;
        break;
    case EXIT_REASON_INIT_SIGNAL:
        break;
    case EXIT_REASON_SIPI:
        break;
    case EXIT_REASON_IO_SMI:
        break;
    case EXIT_REASON_OTHER_SMI:
        break;
    case EXIT_REASON_INT_WINDOW:
        break;
    case EXIT_REASON_NMI_WINDOW:
        break;
    case EXIT_REASON_TASK_SWITCH:
        break;
    case EXIT_REASON_CPUID:
        break;
    case EXIT_REASON_GETSEC:
        break;
    case EXIT_REASON_HLT:
        break;
    case EXIT_REASON_INVD:
        break;
    case EXIT_REASON_INVLPG:
        break;
    case EXIT_REASON_RDPMC:
        break;
    case EXIT_REASON_RDTSC:
        break;
    case EXIT_REASON_RSM:
        break;
    case EXIT_REASON_VMCALL:
        break;
    case EXIT_REASON_VMCLEAR:
        break;
    case EXIT_REASON_VMLAUNCH:
        break;
    case EXIT_REASON_VMPTRLD:
        break;
    case EXIT_REASON_VMPTRST:
        break;
    case EXIT_REASON_VMREAD:
        break;
    case EXIT_REASON_VMRESUME:
        break;
    case EXIT_REASON_VMWRITE:
        break;
    case EXIT_REASON_VMXOFF:
        break;
    case EXIT_REASON_VMXON:
        break;
    case EXIT_REASON_CTL_REG_ACCESS:
        break;
    case EXIT_REASON_MOV_DR:
        break;
    case EXIT_REASON_IO_INSTRUCTION:
        break;
    case EXIT_REASON_RDMSR:
        break;
    case EXIT_REASON_WRMSR:
        break;
    case EXIT_REASON_INVALID_GUEST_STATE:
        break;
    case EXIT_REASON_MSR_LOADING:
        break;
    case EXIT_REASON_MWAIT:
        break;
    case EXIT_REASON_MONITOR_TRAP_FLAG:
        break;
    case EXIT_REASON_MONITOR:
        break;
    case EXIT_REASON_PAUSE:
        break;
    case EXIT_REASON_MACHINE_CHECK:
        break;
    case EXIT_REASON_TPR_BELOW_THRESH:
        break;
    case EXIT_REASON_APIC_ACCESS:
        break;
    case EXIT_REASON_VIRTUALIZED_EOI:
        break;
    case EXIT_REASON_GDTR_OR_IDTR:
        break;
    case EXIT_REASON_LDTR_OR_TR:
        break;
    case EXIT_REASON_EPT_VIOLATION:
        break;
    case EXIT_REASON_EPT_MISCONFIG:
        break;
    case EXIT_REASON_INVEPT:
        break;
    case EXIT_REASON_RDTSCP:
        break;
    case EXIT_REASON_PREEMPT_TIMER:
        break;
    case EXIT_REASON_INVVPID:
        break;
    case EXIT_REASON_WBINVD:
        break;
    case EXIT_REASON_XSETBV:
        break;
    case EXIT_REASON_APIC_WRITE:
        break;
    case EXIT_REASON_RDRAND:
        break;
    case EXIT_REASON_INVPCID:
        break;
    case EXIT_REASON_VMFUNC:
        break;
    case EXIT_REASON_ENCLS:
        break;
    case EXIT_REASON_RDSEED:
        break;
    case EXIT_REASON_PAGEMOD_LOG_FULL:
        break;
    case EXIT_REASON_XSAVES:
        break;
    case EXIT_REASON_XRSTORS:
        break;
    }
    if (!Handled) {
        kVMCoreTerminateVM(pVCpu->Machine, VM_TERMINATION_UNHANDLED_VMEXIT, true);
    }
    else {
        kVCpuThread(pVCpu);
    }
}
