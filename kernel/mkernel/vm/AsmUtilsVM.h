
#pragma once

#define FLAGS_CF        (1ULL << 0)
#define FLAGS_ZF        (1ULL << 6)

extern "C" {
    // Copies VMCS data to VMCS region in memory.
    UINT64 kVmxVMClear(void *VmcsPtr);
    // Launches virtual machine managed by current VMCS.
    // Will only return if launch fails.
    UINT64 kVmxVMLaunch();
    // Resumes virtual machine managed by current VMCS.
    // Will only return if resume fails.
    UINT64 kVmxVMResume();
    // Loads the current VMCS pointer from memory.
    UINT64 kVmxVMPtrLd(void *VmcsPtr);
    // Stores the current VMCS pointer into memory.
    UINT64 kVmxVMPtrSt(void *VmcsPtr);
    // Reads a specified VMCS field.
    UINT64 kVmxVMRead(unsigned long long FieldIndex, unsigned long long *OutPtr);
    // Writes a specified VMCS field.
    UINT64 kVmxVMWrite(unsigned long long FieldIndex, unsigned long long ValueToWrite);
    // Enters VMX root operation.
    UINT64 kVmxOn(void *VmxOnRegionPtr);
    // Leaves VMX root operation.
    UINT64 kVmxOff();
}
