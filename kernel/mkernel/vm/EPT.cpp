
#include "stdafx.h"
#include "MemMgr.h"
#include "StdLib.h"

#include "vm/EPT.h"


static void ConvertVirtToPhysAddr(UINT64 *root, UINTN level) {
    if (root == nullptr || level >= 4) return;
    for (UINTN i = 0; i < 512; i++) {
        ConvertVirtToPhysAddr((UINT64 *)*root, level + 1);
        UINT64 VirtAddr = (*root) & ~0x3FFULL;
        UINT64 Flags = (*root) & 0x3FF;
        *root = kVirt2Phys(VirtAddr) | Flags;
        root++;
    }
};

void *kCreateEPT(VMMemoryDescriptor *pMD, UINTN NumberOfDescriptors) {
    // Compute the number of pages needed for extended page table (EPT).
    // Note that this may be over-estimated a bit.
    UINTN TotalPages = 1;
    for (UINTN i = 0; i < NumberOfDescriptors; i++) {
        VMMemoryDescriptor &md = pMD[i];
        UINTN Pages = md.Size / 4096;
        for (UINTN j = 0; j < 3; j++) {
            // 2~513-> 2, 514~1023-> 3, ...
            Pages = (Pages + 1022) / 512;
            TotalPages += Pages;
        }
    }

    // Actually create EPT.
    UINT64 *EPT = (UINT64 *)kAllocPages(TotalPages);
    UINT64 *FreeEntry = EPT;

    auto alloc_entry = [&]() -> UINT64 * {
        UINT64 *Entry = FreeEntry;
        FreeEntry += 512; // advance by 512*sizeof(UINT64) = 4096 bytes
        kMemSet(Entry, 0, 4096);
        return Entry;
    };
    alloc_entry(); // for PML4 table

    for (UINTN i = 0; i < NumberOfDescriptors; i++) {
        VMMemoryDescriptor &md = pMD[i];

        UINT64 AccessFlagsAllowAll = 0x403; // Allow read, write, execute
        UINT64 AccessFlags = 0;
        if (md.Flags & MD_GUEST_ALLOW_READ) AccessFlags |= 0x1;
        if (md.Flags & MD_GUEST_ALLOW_WRITE) AccessFlags |= 0x2;
        if (md.Flags & MD_GUEST_ALLOW_EXECUTE) AccessFlags |= 0x400;

        for (UINTN Ptr = 0; Ptr < md.Size; Ptr += 4096) {
            UINTN GuestPhysicalStart = md.GuestPhysicalStart + Ptr;
            UINTN PhysicalStart = md.PhysicalStart + Ptr;

            // Create EPT entry for current page
            // Note that we temporarily store virtual addresses
            //   in intermediate entries for convenience.
            UINTN L2idx = (GuestPhysicalStart >> 39) & 0x1ff;
            UINTN L3idx = (GuestPhysicalStart >> 30) & 0x1ff;
            UINTN L4idx = (GuestPhysicalStart >> 21) & 0x1ff;
            UINTN L5idx = (GuestPhysicalStart >> 12) & 0x1ff;

            UINT64 *Level1 = EPT;
            UINT64 *Level2 = (UINT64 *)Level1[L2idx];
            if (Level2 == nullptr) {
                Level2 = alloc_entry();
                Level1[L2idx] = (UINT64)Level2 | AccessFlagsAllowAll;
            }
            UINT64 *Level3 = (UINT64 *)Level2[L3idx];
            if (Level3 == nullptr) {
                Level3 = alloc_entry();
                Level2[L3idx] = (UINT64)Level3 | AccessFlagsAllowAll;
            }
            UINT64 *Level4 = (UINT64 *)Level3[L4idx];
            if (Level4 == nullptr) {
                Level4 = alloc_entry();
                Level3[L4idx] = (UINT64)Level4 | AccessFlagsAllowAll;
            }
            Level4[L5idx] = PhysicalStart | AccessFlags;
        }
    }

    // Convert virtual-address entries into physical address.
    ConvertVirtToPhysAddr(EPT, 1);

    return EPT;
}

void kFreeEPT(void *EPTVirtAddr) {
    kFree(EPTVirtAddr);
}
