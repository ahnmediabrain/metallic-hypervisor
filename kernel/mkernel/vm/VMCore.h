
#pragma once

#include "vm/EPT.h"
#include "vm/VirtualCpu.h"
#include "vm/VirtualDevice.h"
#include "SyncObjs.h"


typedef OS_STATUS(*VMEXIT_HANDLER) (VirtualCpu *pVCpu);

struct VMEventHandlers {
    VMEXIT_HANDLER      ExitHandler;
};

struct VirtualMachine {
    K_SPINLOCK          Lock;
    UINT64              State;
    UINT64              VCpuCount;
    VirtualCpu        **VCpus;
    UINT64              MemoryInBytes;
    VMMemoryDescriptor *MD;
    UINT64              MDCount;
    void               *ExtendedPageTable;
    UINT64              VDevCount;
    VirtualDevice     **VDevs;
    UINT64              JoinedVCpuCount;        // Used for VM termination
    QWORD               LastTerminationReason;  // Termination reason for last VM termination
};

void kInitializeVM(VirtualMachine *pVM);
void kRegisterVirtualDevice(VirtualMachine *pVM, VirtualDevice *pVDev);

void kVMCoreStartVM(VirtualMachine *pVM);
void kVMCoreTerminateVM(VirtualMachine *pVM, QWORD TerminationReason, bool IsVCpuThread);