
#include "stdafx.h"
#include "AsmUtils.h"
#include "hw/Apic/Apic.h"
#include "Console.h"
#include "Debug.h"
#include "Interrupt.h"
#include "MemMgr.h"
#include "PeLoader.h"
#include "Ramdisk.h"
#include "Scheduler.h"
#include <MSR.h>

#include "vm/AsmUtilsVM.h"
#include "vm/VMCore.h"
#include "vm/VM.h"

/*
void kVMEnter(VirtualMachine *pVM) {
    // Setup VMCS basics
    void *VmcsPhysPtr = (void *)kVirt2Phys((UINTN)pVM->Vmcs);
    kConsolePrintf(TEXT("1"));
    kVMCheckError(kVmxVMClear(VmcsPhysPtr));
    kConsolePrintf(TEXT("2"));
    kVMCheckError(kVmxVMPtrLd(VmcsPhysPtr));
    kConsolePrintf(TEXT("3"));

    // Fill in the VMCS data structure

    // Launch VM!
    kVMCheckError(kVmxVMLaunch());
    kConsolePrintf(TEXT("4"));
}
*/

void kVMCoreStartVM(VirtualMachine *pVM) {
    // Allocate memory for VM
    // FIXME: try to allocate multiple chunks of memory if single chunk cannot serve
    pVM->MDCount = 3;
    pVM->MD = (VMMemoryDescriptor *)kMalloc(sizeof(VMMemoryDescriptor *) * pVM->MDCount);

    UINTN LowBytes = min(pVM->MemoryInBytes, 3ULL << 30); // Low memory: max. 3GB
    UINTN HighBytes = pVM->MemoryInBytes - LowBytes;

    VMMemoryDescriptor &mdLow = pVM->MD[0];
    mdLow.VirtualStart = (UINTN)kAllocContiguousPages(LowBytes / 4096);
    mdLow.PhysicalStart = kVirt2Phys(mdLow.VirtualStart);
    mdLow.GuestPhysicalStart = 0;
    mdLow.Size = LowBytes;
    mdLow.Flags = MD_GUEST_ALLOW_READ | MD_GUEST_ALLOW_WRITE | MD_GUEST_ALLOW_EXECUTE;

    VMMemoryDescriptor &mdFirmware = pVM->MD[1];
    UINTN FirmwareRegionBytes = 4ULL << 20; // Firmware region: 4MB (code + data)
    mdFirmware.VirtualStart = (UINTN)kAllocContiguousPages(FirmwareRegionBytes / 4096);
    mdFirmware.PhysicalStart = kVirt2Phys(mdFirmware.VirtualStart);
    mdFirmware.GuestPhysicalStart = (1ULL << 32) - FirmwareRegionBytes;
    mdFirmware.Flags = MD_GUEST_ALLOW_READ | MD_GUEST_ALLOW_WRITE | MD_GUEST_ALLOW_EXECUTE;

    VMMemoryDescriptor &mdHigh = pVM->MD[2];
    mdLow.VirtualStart = (UINTN)kAllocContiguousPages(HighBytes / 4096);
    mdLow.PhysicalStart = kVirt2Phys(mdHigh.VirtualStart);
    mdLow.GuestPhysicalStart = 0;
    mdLow.Size = HighBytes;
    mdLow.Flags = MD_GUEST_ALLOW_READ | MD_GUEST_ALLOW_WRITE | MD_GUEST_ALLOW_EXECUTE;

    // Load firmware
    RamdiskFileDesc fw;
    kAssert(E_SUCCEEDED(kRamdiskFindFile(TEXT("VMFWEFI.PE"), &fw)));
    UINTN szFwLoaded;
    kPeCalcLoadedSize(fw.Data, fw.Size, &szFwLoaded);
    kAssert(szFwLoaded <= (1ULL << 20));

    UINTN FwEntryPoint;
    kLoadPe(fw.Data, fw.Size, (void *)(mdFirmware.VirtualStart),
        szFwLoaded, mdFirmware.GuestPhysicalStart, &FwEntryPoint);

    // Create EPT
    pVM->ExtendedPageTable = kCreateEPT(pVM->MD, pVM->MDCount);

    // Create VCPUs
    pVM->VCpus = (VirtualCpu **)kMalloc(sizeof(VirtualCpu **) * pVM->VCpuCount);
    for (UINTN i = 0; i < pVM->VCpuCount; i++) {
        VirtualCpu *pVCpu;
        kVCpuCreate(pVM, &pVCpu);
        pVM->VCpus[i] = pVCpu;
    }

    pVM->State = VM_STATE_RUNNING;
}

void kVMCoreTerminateVM(VirtualMachine *pVM, QWORD TerminationReason, bool IsVCpuThread) {
    bool IsTerminatingAgent = false;

    // Set as crashed
    kSpinlockAcquire(&pVM->Lock);
    if (pVM->State != VM_STATE_TERMINATION_PENDING) {
        pVM->State = VM_STATE_TERMINATION_PENDING;
        pVM->JoinedVCpuCount = 0;
        IsTerminatingAgent = true;
    }
    else {
        // Somebody else has already initiated termination of the VM
    }
    kSpinlockRelease(&pVM->Lock);

    // Actual termination process
    if (IsVCpuThread) {
        kInterlockedIncrement64(&pVM->JoinedVCpuCount);
    }
    if (IsTerminatingAgent) {
        // Send IPI
        // (VM exits will occur on other cores)
        UINT32 IcrLow_VMSync = LOCAL_APIC_DELIV_FIXED | LOCAL_APIC_ASSERT |
            LOCAL_APIC_DST_ALL_NOSELF | IRQ_VM_SYNC;
        kApicSendIPI(IcrLow_VMSync, 0);

        // Wait until all cores join
        while (pVM->JoinedVCpuCount < pVM->VCpuCount);

        // Destroy virtual devices
        // FIXME: implement!

        // Destroy resouces
        kFreeEPT(pVM->ExtendedPageTable);
        for (UINTN i = 0; i < pVM->MDCount; i++) {
            kFreePages((void *)pVM->MD[i].VirtualStart);
        }
        kFree(pVM->MD);

        for (UINTN i = 0; i < pVM->VCpuCount; i++) {
            kVCpuDestroy(pVM->VCpus[i]);
        }
        kFree(pVM->VCpus);

        // Update state
        pVM->LastTerminationReason = TerminationReason;
        pVM->State = VM_STATE_OFF;
    }
    if (IsVCpuThread) {
        kExitThread(0);
    }
}
