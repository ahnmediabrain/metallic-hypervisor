
#pragma once


#define VDEVICE_TYPE_PCI        0
#define VDEVICE_TYPE_LEGACY     1


struct VirtualDevice {
    void           *PortIOHandler;
    void           *MMIOHandler;
    void           *Suspend;
    void           *Resume;
};
