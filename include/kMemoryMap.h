﻿
/* kMemoryMap.h */

#pragma once

#include <kTypes.h>

#pragma pack(push, 1)

// Memory map

typedef UINTN K_MEMORY_TYPE;

// Note: Type 숫자가 작을수록 우선순위가 높음.

#define K_MEMORY_UNUSABLE           0       // 이용 불가능 (Bad memory 등)
#define K_MEMORY_RESERVED           1       // 예약됨
#define K_MEMORY_MMIO               2       // Memory Mapped I/O 영역
#define K_MEMORY_FW_RUNTIME         3       // 펌웨어가 런타임에 사용하려고 예약한 영역.
#define K_MEMORY_ACPI_NVS           4       // ACPI NVS : ACPI 정보 저장용으로 예약됨. Sleep/Hibernation 시 저장 필요
#define K_MEMORY_ACPI_REC           5       // ACPI Reclaim : 해당 영역을 읽은 후 사용 가능
#define K_MEMORY_KERNEL_AREA        6       // 로더가 커널 영역으로 할당한 영역
#define K_MEMORY_AVAILABLE          7       // 사용 가능
#define K_MEMORY_TYPE_COUNT         (K_MEMORY_AVAILABLE + 1)

typedef UINTN K_MEMORY_ATTR;

#define K_MEMORY_ATTR_CACHE_UC		0x0000000000000001ULL
#define K_MEMORY_ATTR_CACHE_WC		0x0000000000000002ULL
#define K_MEMORY_ATTR_CACHE_WT		0x0000000000000004ULL
#define K_MEMORY_ATTR_CACHE_WB		0x0000000000000008ULL
#define K_MEMORY_ATTR_CACHE_UCE		0x0000000000000010ULL
#define K_MEMORY_ATTR_SECURITY_WP	0x0000000000010000ULL
#define K_MEMORY_ATTR_SECURITY_RP	0x0000000000020000ULL
#define K_MEMORY_ATTR_SECURITY_XP	0x0000000000040000ULL

typedef struct K_MEMORY_MAP
{
    UINTN			StartingAddr;
    UINTN			Size;
    K_MEMORY_TYPE	Type;

} K_MEMORY_MAP;

#pragma pack(pop)
