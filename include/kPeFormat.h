﻿
/* kPeFormat.h */

#pragma once

#include <kTypes.h>

#pragma pack(push, 1)

typedef struct _IMAGE_DOS_HEADER { // DOS .EXE header
    WORD e_magic;   // Magic number 일반적인 Signature
    WORD e_cblp;    // Bytes on last page of file
    WORD e_cp;  // Pages in file
    WORD e_crlc;    // Relocations
    WORD e_cparhdr; // Size of header in paragraphs
    WORD e_minalloc;// Minimum extra paragraphs needed
    WORD e_maxalloc;// Maximum extra paragraphs needed
    WORD e_ss;  // Initial (relative) SS value
    WORD e_sp;  // Initial SP value
    WORD e_csum;    // Checksum
    WORD e_ip;  // Initial IP value
    WORD e_cs;  // Initial (relative) CS value
    WORD e_lfarlc;  // File address of relocation table
    WORD e_ovno;    // Overlay number
    WORD e_res[4];  // Reserved words
    WORD e_oemid;   // OEM identifier (for e_oeminfo)
    WORD e_oeminfo; // OEM information; e_oemid specific
    WORD e_res2[10];// Reserved words
    LONG e_lfanew;  // File address of new exe header (NT헤더의 시작 위치)
} IMAGE_DOS_HEADER, *PIMAGE_DOS_HEADER;

#define IMAGE_DOS_SIGNATURE 0x5A4D      // 'MZ' in little endian

typedef struct _IMAGE_FILE_HEADER {
    WORD  Machine;
    WORD  NumberOfSections;
    DWORD TimeDateStamp;
    DWORD PointerToSymbolTable;
    DWORD NumberOfSymbols;
    WORD  SizeOfOptionalHeader;
    WORD  Characteristics;
} IMAGE_FILE_HEADER, *PIMAGE_FILE_HEADER;

#define IMAGE_FILE_MACHINE_I386                 0x014C  // Intel 386
#define IMAGE_FILE_MACHINE_AMD64                0x8664  // AMD64 (K8)

typedef struct _IMAGE_DATA_DIRECTORY {
    DWORD VirtualAddress;
    DWORD Size;
} IMAGE_DATA_DIRECTORY, *PIMAGE_DATA_DIRECTORY;

#define IMAGE_NUMBEROF_DIRECTORY_ENTRIES        16
#define IMAGE_DIRECTORY_ENTRY_EXPORT            0   // Export Directory
#define IMAGE_DIRECTORY_ENTRY_IMPORT            1   // Import Directory
#define IMAGE_DIRECTORY_ENTRY_RESOURCE          2   // Resource Directory
#define IMAGE_DIRECTORY_ENTRY_EXCEPTION         3   // Exception Directory
#define IMAGE_DIRECTORY_ENTRY_SECURITY          4   // Security Directory
#define IMAGE_DIRECTORY_ENTRY_BASERELOC         5   // Base Relocation Table
#define IMAGE_DIRECTORY_ENTRY_DEBUG             6   // Debug Directory
#define IMAGE_DIRECTORY_ENTRY_ARCHITECTURE      7   // Architecture Specific Data
#define IMAGE_DIRECTORY_ENTRY_GLOBALPTR         8   // RVA of GP
#define IMAGE_DIRECTORY_ENTRY_TLS               9   // TLS Directory
#define IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG       10  // Load Configuration Directory
#define IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT      11  // Bound Import Directory in headers
#define IMAGE_DIRECTORY_ENTRY_IAT               12  // Import Address Table
#define IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT      13  // Delay Load Import Descriptors
#define IMAGE_DIRECTORY_ENTRY_COM_DESCRIPTOR    14  //COM Runtime descriptor

typedef struct _IMAGE_OPTIONAL_HEADER64 {
    WORD        Magic;
    BYTE        MajorLinkerVersion;
    BYTE        MinorLinkerVersion;
    DWORD       SizeOfCode;
    DWORD       SizeOfInitializedData;
    DWORD       SizeOfUninitializedData;
    DWORD       AddressOfEntryPoint;
    DWORD       BaseOfCode;
    ULONGLONG   ImageBase;
    DWORD       SectionAlignment;
    DWORD       FileAlignment;
    WORD        MajorOperatingSystemVersion;
    WORD        MinorOperatingSystemVersion;
    WORD        MajorImageVersion;
    WORD        MinorImageVersion;
    WORD        MajorSubsystemVersion;
    WORD        MinorSubsystemVersion;
    DWORD       Win32VersionValue;
    DWORD       SizeOfImage;
    DWORD       SizeOfHeaders;
    DWORD       CheckSum;
    WORD        Subsystem;
    WORD        DllCharacteristics;
    ULONGLONG   SizeOfStackReserve;
    ULONGLONG   SizeOfStackCommit;
    ULONGLONG   SizeOfHeapReserve;
    ULONGLONG   SizeOfHeapCommit;
    DWORD       LoaderFlags;
    DWORD       NumberOfRvaAndSizes;
    IMAGE_DATA_DIRECTORY DataDirectory[IMAGE_NUMBEROF_DIRECTORY_ENTRIES];
} IMAGE_OPTIONAL_HEADER64, *PIMAGE_OPTIONAL_HEADER64;

typedef struct _IMAGE_NT_HEADERS64 {
    DWORD                   Signature;
    IMAGE_FILE_HEADER       FileHeader;
    IMAGE_OPTIONAL_HEADER64 OptionalHeader;
} IMAGE_NT_HEADERS64, *PIMAGE_NT_HEADERS64;

#define IMAGE_NT_SIGNATURE      0x00004550      // "PE\0\0" in little endian

#define IMAGE_SIZEOF_SHORT_NAME 8

typedef struct _IMAGE_SECTION_HEADER {
    BYTE  Name[IMAGE_SIZEOF_SHORT_NAME];        // section name in UTF-8
    union {
        DWORD PhysicalAddress;
        DWORD VirtualSize;
    } Misc;
    DWORD VirtualAddress;       // RVA of section start
    DWORD SizeOfRawData;        // section size in the file
    DWORD PointerToRawData;     // section start offset in the file
    DWORD PointerToRelocations;
    DWORD PointerToLinenumbers;
    WORD  NumberOfRelocations;
    WORD  NumberOfLinenumbers;
    DWORD Characteristics;
} IMAGE_SECTION_HEADER, *PIMAGE_SECTION_HEADER;

typedef struct _IMAGE_BASE_RELOCATION {
    DWORD    VirtualAddress;
    DWORD    SizeOfBlock;
    WORD     TypeOffset[1];     // relocation type[15:12] and relocation offset[11:0]
} IMAGE_BASE_RELOCATION, *PIMAGE_BASE_RELOCATION;

#define IMAGE_REL_BASED_ABSOLUTE        0       // The base relocation is skipped. This type can be used to pad a block.
#define IMAGE_REL_BASED_HIGH            1       // The base relocation adds the high 16 bits of the difference to the 16-bit field at offset. The 16-bit field represents the high value of a 32-bit word.
#define IMAGE_REL_BASED_LOW             2       // The base relocation adds the low 16 bits of the difference to the 16-bit field at offset. The 16-bit field represents the low half of a 32-bit word.
#define IMAGE_REL_BASED_HIGHLOW         3       // The base relocation applies all 32 bits of the difference to the 32-bit field at offset.
#define IMAGE_REL_BASED_HIGHADJ         4       // The base relocation adds the high 16 bits of the difference to the 16-bit field at offset. The 16-bit field represents the high value of a 32-bit word. The low 16 bits of the 32-bit value are stored in the 16-bit word that follows this base relocation. This means that this base relocation occupies two slots.
#define IMAGE_REL_BASED_DIR64           10      // The base relocation applies the difference to the 64-bit field at offset.

#pragma pack(pop)
