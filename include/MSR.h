﻿
// MSR.h : defines model-specific register (MSR) numbers

#pragma once

#define IA32_APIC_BASE                  27
#define IA32_APIC_BASE_BSP              (1UL << 8)
#define IA32_APIC_BASE_ENABLE_X2APIC    (1UL << 10)
#define IA32_APIC_BASE_ENABLE           (1UL << 11)

#define IA32_FEATURE_CONTROL            0x0000003A
#define IA32_FEATURE_CONTROL_LOCK       (1UL << 0)
#define IA32_FEATURE_CONTROL_VMXINSMX   (1UL << 1)
#define IA32_FEATURE_CONTROL_VMXOUTSMX  (1UL << 2)

#define IA32_VMX_BASIC                  0x00000480
#define IA32_VMX_PROCBASED_CTLS         0x00000482
#define IA32_VMX_MISC                   0x00000485
#define IA32_VMX_CR0_FIXED0             0x00000486
#define IA32_VMX_CR0_FIXED1             0x00000487
#define IA32_VMX_CR4_FIXED0             0x00000488
#define IA32_VMX_CR4_FIXED1             0x00000489
#define IA32_VMX_PROCBASED_CTLS2        0x0000048B
#define IA32_VMX_EPT_VPID_CAP           0x0000048C
