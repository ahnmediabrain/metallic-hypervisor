﻿
/* kBase.h */

#pragma once

/* Base types */

#ifndef UEFI

typedef unsigned char		UINT8;
typedef unsigned short		UINT16;
typedef unsigned int		UINT32;
typedef unsigned long long	UINT64;

typedef signed char			INT8;
typedef signed short		INT16;
typedef signed int			INT32;
typedef signed long long	INT64;

/* UINTN: 플랫폼에 따른 Native 정수형 */
typedef UINT64				UINTN;
typedef INT64				INTN;

typedef char				CHAR8;
typedef unsigned short		CHAR16;

typedef struct GUID
{
	UINT32  Data1;
	UINT16  Data2;
	UINT16  Data3;
	UINT8   Data4[8];

} GUID;

#endif

typedef INT32				LONG;
typedef UINT32				ULONG;
typedef INT64				LONGLONG;
typedef UINT64				ULONGLONG;

typedef UINT16				WORD;
typedef UINT32				DWORD;
typedef UINT64				QWORD;

typedef UINTN				K_ADDRESS;
typedef UINTN				K_SIZE;

typedef UINT8				BYTE;
typedef UINTN				BOOL;

typedef UINTN				OS_STATUS;

typedef INTN				LONG_PTR;
typedef UINTN				ULONG_PTR;

typedef GUID				UUID;

#define OSAPI               __cdecl

#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

typedef UINT32              COLOR;

/* Base macro functions and definitions */

// Text, char, makefourcc
#define TEXT(t) ((CHAR16 *)(L##t))
#define CHAR(t) ((CHAR16)(L##t))
#define CUT8_32(t) ((UINT32)((t)&0xFF)) // t의 하위 8비트를 취하고 UINT32로 캐스팅
#define MAKEFOURCC(a,b,c,d) ((CUT8_32(d)<<24) | (CUT8_32(c)<<16) | (CUT8_32(b)<<8) | CUT8_32(a))

// Sign function
#define SIGN(t) (!(t) ? 0 : ((t)>0 ? 1 : (-1)))

// IsNumber function
#define ISNUMBER(t) (((t) >= CHAR('0') && (t) <= CHAR('9')) ? TRUE : FALSE)

// Maximum function
#ifndef max
#define max(a,b) ((a)>(b)?(a):(b))
#endif

// Minimum function
#ifndef min
#define min(a,b) ((a)<(b)?(a):(b))
#endif

// Absolute value function
#ifndef abs
#define abs(a) ((a)>=0?(a):(-(a)))
#endif

// Difference function
#ifndef diff
#define diff(a,b) (max(a,b)-min(a,b))
#endif

// NULL
#ifdef NULL
#undef NULL
#endif
#define NULL 0

// TRUE, FALSE
#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

// MAX, MIN, ABS
#ifndef MAX
#define MAX(a,b) (((a)>(b))?(a):(b))
#endif
#ifndef MIN
#define MIN(a,b) (((a)<(b))?(a):(b))
#endif
#ifndef ABS
#define ABS(x) (((x)>0)?(x):(-(x)))
#endif

// Color macros
#define RGB(r,g,b) (COLOR)(((BYTE)(b)) | (((COLOR)((BYTE)(g)))<<8) | (((COLOR)((BYTE)(r)))<<16))
#define COLOR_R(c) ((BYTE)(((COLOR)(c))>>16))
#define COLOR_G(c) ((BYTE)(((COLOR)(c))>>8))
#define COLOR_B(c) ((BYTE)(((COLOR)(c))>>0))

// Pointer offset add function
#define ptroffadd(p, t, n) ((void *)(((BYTE *)(p)) + ((t)*(n))))

// Invalid handle value
#define EMPTY_HANDLE            ((UINTN)-1)

// ERROR CODES (+: failed, 0: success, -: warning/info)
#define E_FAILED(e)             (((INTN)(e)) > 0)
#define E_SUCCEEDED(e)          (!E_FAILED(e))
#define E_SUCCESS               0
#define E_ERROR                 1       // Generic unknown (unspecified) error
#define E_INVALID_PARAMETER     2
#define E_UNSUPPORTED           3
#define E_INVALID_INPUT         4
#define E_BUFFER_TOO_SMALL      5
#define E_NOTIMPL               6
#define E_NOTFOUND              7
#define E_SYNCOBJ_NOTOWNED      8       // Trying to release a synchronization object that is not owned (not locked)
#define E_TIMEOUT               9
#define E_OUTOFRESOURCE         10
#define E_NOT_AVAILABLE         11
#define E_HWERROR               12      // Hardware malfunction
