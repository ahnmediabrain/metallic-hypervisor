﻿
/* kEntryPoint.h */

#pragma once

#include <stddef.h>

/* Kernel Entry Point */

#include <kTypes.h>
#include <kMemoryMap.h>
//#include <kRamdisk.h>
//#include <kExecutable.h>
//#include <kFirmware.h>

#pragma pack(push, 1)

typedef struct {
    UINTN           Size;
    CHAR16          FileName[8];    // Space-padded
    UINT64          DataOffset;     // Offset from the start of K_RAMDISK structure
} K_RAMDISK_FILE;

typedef struct {
    UINTN           FileCount;
    K_RAMDISK_FILE  Files[1];
} K_RAMDISK;

#define K_RAMDISK_HEADER_SIZE   (offsetof(K_RAMDISK, Files))

#define FW_TYPE_EFI             0
#define FW_TYPE_BIOS            1

// K_FWSPECIFIC_BIOS 구조체
typedef struct K_FWSPECIFIC_BIOS
{
    UINTN            Size;

} K_FWSPECIFIC_BIOS;

// K_FWSPECIFIC_EFI 구조체
typedef struct K_FWSPECIFIC_EFI
{
    UINTN           Size;
    void            *pEfiSystemTable;

} K_FWSPECIFIC_EFI;

#define K_DEFAULT_HORZRES       1024
#define K_DEFAULT_VERTRES       768

#define K_VIDEO_TYPE_EFI        0
#define K_VIDEO_TYPE_VBE        1

// 'X' means 'reserved' (not used)
#define K_VIDEO_PIXEL_RGBX      0    // 32-bit including X
#define K_VIDEO_PIXEL_BGRX      1    // 32-bit including X

typedef struct {
    UINTN framebuffer_base_phys;     // 프레임버퍼의 물리 주소
    UINTN framebuffer_base_virt;
    UINTN framebuffer_size;
    UINT32 resHorz;
    UINT32 resVert;
    UINT32 ccPixelPerScanLine;
    UINT32 videoAttr;
    UINT32 videoType;
    UINT32 videoDepth;
    UINT32 pixelMode;
} K_GRAPHICS_MODE_DESC;

typedef struct {
    UINTN                   Available;      // Is ACPI 2.0 or higher available on this computer? (TRUE=1, FALSE=0)
    UINTN                   RsdpAddr;        // Address to Root System Description Pointer structure
} K_ACPI_DESC;

// K_KERNEL_ENTRY_DATA 구조체
typedef struct {
    UINTN                   Size; // 구조체의 크기
    K_ADDRESS               Kernel_Addr; // 커널이 로드된 가상 주소
    K_ADDRESS               Kernel_EntryPoint; // 커널 엔트리 포인트 가상 주소
    K_ADDRESS               Kernel_Stack_Bottom; // 커널 스택 가상 주소
    K_ADDRESS               Kernel_Stack_Size; // 커널 스택 크기
    UINTN                   KernelImageSize; // 메모리에 로드된 커널의 크기    
    K_MEMORY_MAP           *MemoryMap; // 메모리 맵 가상 주소
                                       // (각 영역은 4KB 경계로 정렬되고, 겹치는 영역은 정제되고, Low Address 순으로 정렬되어야 함)
    UINTN                   MemoryMapCount; // 메모리 맵의 수
    UINTN                   RamdiskOffset; // 현 구조체의 시작 부분을 기준으로 할 때 부트 램디스크 오프셋
                                           // (부트 램디스크는 커널 엔트리 데이터 구조체 뒤쪽에 붙어 있는 형태임)
    UUID                    SystemVolumeUuid; // 시스템 볼륨 GUID
    K_GRAPHICS_MODE_DESC    GraphicsModeDesc; // 그래픽 모드 디스크립터
    K_ACPI_DESC             AcpiDesc; // ACPI 디스크립터
    UINTN                   FwType; // 펌웨어 종류
    UINTN                   FwSpecificOffset; // 펌웨어에 따른 데이터 (BIOS: None, EFI: 시스템 테이블 등)

} K_KERNEL_ENTRY_DATA;

// 커널 엔트리 포인트
typedef
void
(OSAPI *KERNEL_ENTRYPOINT)(
  IN  K_KERNEL_ENTRY_DATA        *Data
  );

#define K_KERNEL_UPPER_64G      0xFFFFFFF000000000ULL   // 2^64 - 2^36

#pragma pack(pop)
